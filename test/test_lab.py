#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2021-4-3
#  Description   : Module that contains all test lab functions

from _pytest.monkeypatch import monkeypatch
import pytest
import pyvisa
import time

from txasic_cal.lab import lab
from txasic_cal.host import host

# Mock pyvisa open resource
class MockPyVisa:
    @staticmethod
    def query(*args, **kwargs):
        return "0"

    @staticmethod
    def read(*args, **kwargs):
        return "0"

    @staticmethod
    def write(*args, **kwargs):
        return "Mock pyvisa write {}".format(args[0])


# monkeypatched pyvisa.open_resource
@pytest.fixture
def mock_pyvisa_open_resource(monkeypatch):
    def mock_pyvisa(*args, **kwargs):
        return MockPyVisa()

    monkeypatch.setattr(pyvisa.ResourceManager, "open_resource", mock_pyvisa)


class MockTemperature:
    def __init__(self, start_temp):
        """Mock temperature changes

        Args:
            start_temp (float): Starting temperature
        """
        self.max_temp = 85.0
        self.min_temp = -40
        self.cur_temp = start_temp
        self.cur_order = 'asc'
        self.cur_setpoint = 30.0

        self.count = 0

    def mock_get_temperature(self, *args, **kwargs):
        """Mock temperature rise or fall as +/- 0.1 deg C from current temperature

        Returns:
            [float]: Temperature as float
        """
        if self.cur_order == 'asc':
            self.cur_temp += 0.1
        else:
            self.cur_temp -= 0.1

        if self.cur_temp > self.max_temp:
            self.cur_temp = self.max_temp
        if self.cur_temp < self.min_temp:
            self.cur_temp = self.min_temp

        return self.cur_temp

    def mock_flat_temp(self, *args, **kwargs):
        """Mock flat temperature, situation where the temperature chamber
        is not responding correctly and the temperature doesn't change for 100
        iterations

        Returns:
            [type]: Temperature as float
        """
        if self.cur_order == 'asc':
            self.cur_temp = self.cur_setpoint + 2
        else:
            self.cur_temp = self.cur_setpoint - 2

        self.count += 1

        if self.count >= 100:
            self.cur_temp = self.cur_setpoint

        return self.cur_temp
        

# Monkeypatch time.sleep calls
@pytest.fixture
def mock_time(monkeypatch):
    def mock_sleep(*args, **kwargs):
        return 1
    
    monkeypatch.setattr(time, "sleep", mock_sleep)


class TestLabMockPyVisa:
    """Class functions to test lab given that pyvisa is mocked
    """
    lab_ob = lab.lab()
    
    def test_connect_instruments(self, mock_pyvisa_open_resource):
        """Connect instruments should return None if instruments are connected successfully

        Args:
            mock_pyvisa_open_resource (function): Monkeypatch mock pyvisa fixture
        """
        TestLabMockPyVisa.lab_ob.connected_instruments = ['Power Meter', 'Oven', 'Scope']
        assert TestLabMockPyVisa.lab_ob.connect_instruments() == None

    def test_init(self, mock_pyvisa_open_resource):
        """Init should return None if instruments are connected successfully

        Args:
            mock_pyvisa_open_resource (function): Monkeypatch mock pyvisa fixture
        """
        TestLabMockPyVisa.lab_ob.connected_instruments = ['Power Meter', 'Oven', 'Scope']
        assert TestLabMockPyVisa.lab_ob.init() == None

    def test_lab_read(self, mock_pyvisa_open_resource):
        """Test individual lab read functions used for calibration

        Args:
            mock_pyvisa_open_resource (function): Monkeypatch mock pyvisa fixture
        """
        # Init instruments before starting
        TestLabMockPyVisa.lab_ob.connected_instruments = ['Power Meter', 'Oven', 'Scope']
        assert TestLabMockPyVisa.lab_ob.init() == None

        # All mocked query and read will return 0
        assert TestLabMockPyVisa.lab_ob.optical_power() == 0.0
        assert TestLabMockPyVisa.lab_ob.pulse_width() == 0.0
        assert TestLabMockPyVisa.lab_ob.pulse_amp() == 0.0
        assert TestLabMockPyVisa.lab_ob.sphere_peak_power(100) == 0.0
        assert TestLabMockPyVisa.lab_ob.get_oven_temperature() == 0.0

        # Since oven is not required for calibration, we can remove it from calibration
        # If removed, the response for oven should be None
        TestLabMockPyVisa.lab_ob.connected_instruments = ['Power Meter', 'Scope']
        assert TestLabMockPyVisa.lab_ob.get_oven_temperature() == None

    def test_lab_write(self, mock_pyvisa_open_resource):
        """Test individual lab write functions used for calibration

        Args:
            mock_pyvisa_open_resource (function): Monkeypatch mock pyvisa fixture
        """
        TestLabMockPyVisa.lab_ob.connected_instruments = ['Power Meter', 'Oven', 'Scope']
        assert TestLabMockPyVisa.lab_ob.init() == None

        # Mocked write will return None
        assert TestLabMockPyVisa.lab_ob.set_oven_temperature_sp(30) == None


class TestLab:
    """Class functions to test lab
    """
    lab_ob = lab.lab()
    mock_temp = MockTemperature(start_temp=29.9)

    def test_connect_instruments_without_instruments(self):
        """Test connect instruments
        """
        # If no instruments are given, return should be None
        TestLab.lab_ob.connected_instruments = []
        assert TestLab.lab_ob.connect_instruments() == None

        # If calibration instruments are given and they are not connected, system will exit
        TestLab.lab_ob.connected_instruments = ['Power Meter', 'Oven']
        with pytest.raises(SystemExit):
            TestLab.lab_ob.connect_instruments()

        # If non-calibration required instruments are given, return should be None
        # Non-calibration meaning instruments that are not essential
        TestLab.lab_ob.connected_instruments = ['Scope', 'Power Supply', 'AFG', 'Template', 'Gonio']
        assert TestLab.lab_ob.connect_instruments() == None
    
    def test_init_without_instruments(self):
        """Test init
        """
        # If no instruments are given, power meter will fail to init and thus there will be an attribute error
        TestLab.lab_ob.connected_instruments = []
        assert TestLab.lab_ob.init() == None

    def test_set_temperature(self, mock_time):
        """Test set temperature function
        Set temperature function returns soak/wait time for system and number of iterations

        Args:
            mock_time (function): Monkeypatch mock time fixture
        """
        # Placeholder for current get/set functions
        hold_get_oven_temperature = TestLab.lab_ob.get_oven_temperature
        hold_set_oven_temperature_sp = TestLab.lab_ob.set_oven_temperature_sp
        
        # Mocked get/set functions
        TestLab.lab_ob.get_oven_temperature = TestLab.mock_temp.mock_get_temperature
        TestLab.lab_ob.set_oven_temperature_sp = lambda x: 1

        # Start temp = 30
        # Setpoint = 30
        TestLab.mock_temp.cur_setpoint = 30
        # Test 1: Current temperature matches setpoint, 0 wait time required
        assert TestLab.lab_ob.set_temperature(30.0)[0] == 0
        
        # Setpoint = 40
        TestLab.mock_temp.cur_temp = 29.9
        TestLab.mock_temp.cur_setpoint = 40
        # Test 2: 10C different to setpoint, 1 min wait time required
        assert TestLab.lab_ob.set_temperature(40.0)[0] == 1

        # Start temp = -40
        TestLab.mock_temp.cur_temp = -40.1
        # Setpoint = 85
        TestLab.mock_temp.cur_setpoint = 85.0
        # Test 3: 125 degree change, 12.5 min wait time required
        assert TestLab.lab_ob.set_temperature(85.0)[0] == 12.5

        # Start temp = 85
        TestLab.mock_temp.cur_order = 'des'
        TestLab.mock_temp.cur_temp = 85.1
        # Setpoint = -40
        TestLab.mock_temp.cur_setpoint = -40
        # Test 4: Similar to test 3 but temperature is decreasing
        assert TestLab.lab_ob.set_temperature(-40.0)[0] == 12.5

        # Flat temp test
        TestLab.lab_ob.get_oven_temperature = TestLab.mock_temp.mock_flat_temp
        # Start temp = 30
        TestLab.mock_temp.cur_temp = 29.9
        TestLab.mock_temp.cur_order = 'asc'
        # Setpoint = 30
        TestLab.mock_temp.cur_setpoint = 30.0

        # For flat temp test, the mocked flat test function will return the setpoint after 100 iteration
        # For 0-99 iterations, the mocked flat test function will return setpoint +/- 2
        # Set temperature function will check the 98th iteration and adjust setpoint so that the temperature chamber is not stuck
        # First return will 0.2 min of wait time since mock_flat_temp does setpoint + 2 for the fist call
        # Second return will be number of iterations after initial two checks, since mock_flat_temp will return setpoint at 100 iter
        # set_temperature will return 98 as the iteration count
        assert TestLab.lab_ob.set_temperature(30.0) == (0.2, 98)

        # Revert get/set placeholder
        TestLab.lab_ob.get_oven_temperature = hold_get_oven_temperature
        TestLab.lab_ob.set_oven_temperature_sp = hold_set_oven_temperature_sp

