import unittest
from txasic_cal.lab import lab
from txasic_cal.host import host
from txasic_cal.lab.instruments import PowerSupply
from txasic_cal.lab import instruments
from txasic_cal.lab import lab

ps = instruments

def test_USB_PS_hex():
    hex_test = 'USB0::0x05E6::0x2230::9201039::0::INSTR'
    y = ps.PowerSupply.Keithley2230(hex_test)
    z = y.main_USB

    # check to see if hex converted to dec
    if 'USB0::1510::8752' in z:
        result = 1
    else:
        result = 2
    assert result == 1

def test_USB_PS_dec():
    dec_test = 'USB0::1510::8752::9201039::0::INSTR'
    y = ps.PowerSupply.Keithley2230(dec_test)
    z = y.main_USB

    if 'USB0::1510::8752' in z:
        result = 1
    else:
        result = 2
    assert result == 1

def test_USB_PS_none():
    empty_test = ''
    y = ps.PowerSupply.Keithley2230(empty_test)
    z = y.main_USB

    if 'USB0::1510::8752' in z:
        result = 1
    else:
        result = 2
    assert result == 1

def test_USB_PM_hex():
    hex_test = 'USB0::0x1313::0x8078::P0028278::INSTR'
    y = ps.Powermeter.Thorlabs_PM200(hex_test)
    z = y.main_USB

    # check to see if hex converted to dec
    if 'USB0::4883::32888' in z:
        result = 1
    else:
        result = 2
    assert result == 1

def test_USB_PM_dec():
    dec_test = 'USB0::4883::32888::P0028278::INSTR'
    y = ps.Powermeter.Thorlabs_PM200(dec_test)
    z = y.main_USB

    if 'USB0::4883::32888' in z:
        result = 1
    else:
        result = 2
    assert result == 1

def test_USB_PM_none():
    empty_test = ''
    y = ps.Powermeter.Thorlabs_PM200(empty_test)
    z = y.main_USB

    if 'USB0::4883::32888' in z:
        result = 1
    else:
        result = 2

    assert result == 1


if __name__ == '__main__':
    unittest.main()