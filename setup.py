import distutils.cmd
import subprocess
import os
from setuptools import setup, find_packages
from src import txasic_cal

class RulesUpdateCommand(distutils.cmd.Command):
   description = 'Create velodyne_equipment rules to read/write to usbtmc without sudo. This command must be executed with sudo.'
   user_options = []

   def initialize_options(self):
      """Set default values for options."""
      # Each user option must be listed here with their default value.
      pass

   def finalize_options(self):
      """Post-process options."""
      pass

   def run(self):
      with open('/etc/udev/rules.d/velodyne_equipment.rules', 'a') as fb:
         fb.write('SUBSYSTEM=="usb", MODE="0666", GROUP="usbusers"\n')

      subprocess.run(['groupadd', 'usbusers'])
      subprocess.run(['usermod', '-a', '-G', 'usbusers', os.path.expanduser('~').split('/')[2]])
      print('System reboot required.')


class BashAliasUpdate(distutils.cmd.Command):
   description = 'Update bash aliases with txcal commands for different setups.\n\
      txcal_v8 = TxCal for V8\n\
      temp_txcal_v8 = Temperature-TxCal for V8\n\
      txcal_v16 = TxCal for V16\n\
      txcal_vb2 = TxCal for VB2\n\
      txcal_vlx16 = TxCal for VLX16\n\
      txcal_apamla1 = TxCal for APAMLA1\n\
      txcal_apamla2 = TxCal for APAMLA2\n\
      post_txcal_v8 = Post TxCal for V8\n\
      post_txcal_v16 = Post TxCal for V16\n\
      post_txcal_vb2 = Post TxCal for VB2\n\
      post_txcal_vlx16 = Post TxCal for VLX16\n\
      post_txcal_apamla1 = Post TxCal for APAMLA1\n\
      post_txcal_apamla2 = Post TxCal for APAMLA2\n\
      txcal_lab = Interative lab python terminal\n'
   user_options = []

   def initialize_options(self):
      """Set default values for options."""
      # Each user option must be listed here with their default value.
      pass

   def finalize_options(self):
      """Post-process options."""
      pass

   def run(self):
      with open(os.path.expanduser('~') + '/.bash_aliases', 'a') as fb:
         fb.write('alias txcal_v8="txcal -s velarray -m V8"\n')
         fb.write('alias temp_txcal_v8="txcal -s velarray -m V8 -v TS -it -vn 4.5"\n')
         fb.write('alias txcal_v16="txcal -s velarray -m V16"\n')
         fb.write('alias txcal_vb2="txcal -s velabit -m VB2"\n')
         fb.write('alias txcal_vlx16="txcal -s vlx -m VLX16"\n')
         fb.write('alias txcal_apamla1="txcal -s apa -m APAMLA1"\n')
         fb.write('alias txcal_apamla2="txcal -s apa -m APAMLA2"\n')
         fb.write('alias post_txcal_v8="txcal -s velarray -m V8 -pc"\n')
         fb.write('alias post_txcal_v16="txcal -s velarray -m V16 -pc"\n')
         fb.write('alias post_txcal_vb2="txcal -s velabit -m VB2 -pc"\n')
         fb.write('alias post_txcal_vlx16="txcal -s vlx -m VLX16 -pc"\n')
         fb.write('alias post_txcal_apamla1="txcal -s apa -m APAMLA1 -pc"\n')
         fb.write('alias post_txcal_apamla2="txcal -s apa -m APAMLA2 -pc"\n')
         fb.write('alias txcal_lab="cd ~/txasic_cal/; python3 -i -m src.txasic_cal.lab.lab"\n')

      print("Source the bash_alias file.")
      print("\n\tsource ~/.bash_aliases")

setup(
   name='txasic_cal',
   author='Vijay Muthukumaran',
   author_email='vmuthukumaran@velodyne.com',
   version=txasic_cal.__version__,
   url='https://bitbucket.org/velodynelidar/txasic_cal/src/develop/',
   description='Tx Calibration Software Setup',
   long_description=open('README.md').read(),
   packages=find_packages('src'),
   package_dir={'': 'src'},
   package_data={'txasic_cal':['pfmea_code/pfmea_operation_codes.csv', 'GUI/velodyne_logo.png', 'GUI/windows_icon.png']},
   install_requires=[
      "python-dateutil",
      "numpy",
      "pandas",
      "scipy",
      "scapy",
      "matplotlib",
      "gitpython",
      "colorlog",
      "pyusb",
      "pyserial",
      "pyvisa-py==0.4.1",
      "dataclasses"
   ],
   cmdclass={
      'create_rules': RulesUpdateCommand,
      'update_aliases' : BashAliasUpdate
   },
   entry_points={
      'console_scripts': [
         'txcal = txasic_cal.__main__:main',
         'txcal_gui = txasic_cal.GUI.__main__:main',
      ]
   }
)
