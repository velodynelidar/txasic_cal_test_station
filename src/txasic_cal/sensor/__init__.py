from typing import DefaultDict
from txasic_cal.tools.diagnostic_packet import run_diagnostic_packet

SENSOR_MODEL_NUMS = DefaultDict(lambda: None)
SENSOR_MODEL_NUMS['0x3']= 'velarray.V8'
SENSOR_MODEL_NUMS['0x4'] = 'velarray.V16'

# Get sensor model number
SENSOR_MODEL_ID, SYSTEM_MODEL_HW_VERSION, SENSOR_MODEL_FW_VERSION = run_diagnostic_packet()
SENSOR_MODEL = SENSOR_MODEL_NUMS[SYSTEM_MODEL_HW_VERSION]