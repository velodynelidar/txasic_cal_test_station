#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-11-10
#  Description   : Software for velabit representation and communication

import importlib
import ast
from txasic_cal.communication import mcm_sdk_client
import time

from txasic_cal.tools.logger import logger
from txasic_cal.tools.packet_reader import packet_reader
from txasic_cal.pfmea_code import pfmea_table

class velabit(mcm_sdk_client.mcm_sdk_client):

    def __init__(self, config, sdk_scripts):
        """Top level velabit class

        Arguments:
            config {module} -- [sensor config module to be used]
            sdk_scripts {module} -- [sensor sdk scripts to be used]
        """
        super(velabit, self).__init__(velabit.__name__)
        self.config = config
        self.sdk_scripts = sdk_scripts

        self.cur_ch = 0
        self.bias = 0
        self.boot = 0
        self.boot_in_dec = 0
        self.bias_in_dec = 0
        self.lp = self.config.LP_BITS[0]
        self.cur_prf = self.config.DEFAULT_PRF

        self.vcsel_recharge_ctr = 0
        self.vcsel_recharge_ctr_in_dec = 0
        self.cur_vcsel_prf = self.config.DEFAULT_VCSEL_PRF

        self._unit_version = None
        self._unit_build_date = None
        self._unit_build_githash = None
        self._unit_model_number = None
        self._unit_hw_revision = None
        self._unit_serial_number = None

        self.start_boot = config.START_BOOT
        self.start_bias = config.START_BIAS

        self._v12 = None
        self._v5 = None
        self._v3_3 = None
        self._vlas = None
        self._hv = None

    def init(self, sensor):
        sdk_version = self.connect(sensor)

        # Unit information
        # self._unit_version = ".".join(str(n) for n in self.client.subcomponents['platform_regs'].get_image_version())
        # self._unit_build_date = self.client.subcomponents['platform_regs'].get_build_date_time()
        # self._unit_build_githash = self.client.subcomponents['platform_regs'].get_build_git_hash()
        # self._unit_model_number = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_model_number())
        # self._unit_hw_revision = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_revision())
        # hi, lo = self.client.subcomponents['platform_regs'].get_serial_number()
        # self._unit_serial_number = "{:08X}{:08X}".format(hi, lo)

        # Voltage rails
        self.get_12v()
        self.get_5v()
        self.get_3_3v()
        self.get_vlas()
        self.get_hv()

        return sdk_version

    def close(self):
        """Close connection to unit and ensure all channels are disabled
        """
        if self._is_connected:
            self.disable_all_channel()
            self.disconnect()

    def ch_init(self, ch_num):
        """Initialize channel

        Arguments:
            ch_num {int} -- [Channel number]
        """
        self.cur_ch = ch_num
        self.bias = self.start_bias
        self.boot = self.start_boot
        self.boot_in_dec = self.convert_from_volt('boot', self.boot)
        self.bias_in_dec = self.convert_from_volt('bias', self.bias)
        self.lp = self.config.LP_BITS[1]
        
        self.enable_channel(ch_num)
        self.set_bias(self.bias)
        self.set_boot(self.boot)
        self.set_lp(self.lp)

    def ch_deinit(self):
        self.disable_channel(self.cur_ch)

    # override super execute
    def execute(self, cmd):
        return super().execute(cmd)

    def convert_from_dec(self, cur_input, cur_value):
        """Convert given input from decimal value to voltage

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Decimal value

        Returns:
            [float] -- Converted decimal value to float
        """
        new_value = round(cur_value * self.config.MAX_DAC_VOLT / self.config.MAX_DAC_DECIMAL, 3)

        return new_value

    def convert_from_volt(self, cur_input, cur_value):
        """Convert given input from voltage to decimal value

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Voltage value

        Returns:
            [float] -- Converted float to decimal value
        """
        new_value = int(cur_value * self.config.MAX_DAC_DECIMAL / self.config.MAX_DAC_VOLT)

        return new_value

    def enable_channel(self, ch_num=None, power_level=None):
        """Enable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
        """
        self.client.subcomponents['timing'].fire_start()

    def disable_channel(self, ch_num=None, power_level=None):
        """Disable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
        """
        self.client.subcomponents['timing'].fire_stop()

    def set_boot(self, boot, ch_num=None, pl=None):
        """Set boot for given channel

        Arguments:
            boot {float} -- Boot value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if boot < self.config.BOOT_MIN or boot > self.config.BOOT_MAX:
            logger.error("Exiting set_boot because boot = {}".format(boot))
            return
        
        self.boot = boot
        self.boot_in_dec = self.convert_from_volt('boot', self.boot)

        logger.debug("Set channel {} boot as {} in dec {}".format(ch_num, self.boot, self.boot_in_dec))

        self.client.set_boot(self.boot_in_dec)
        time.sleep(0.1)

    def set_bias(self, bias, ch_num=None, pl=None):
        """Set bias for given channel

        Arguments:
            bias {float} -- Bias value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if bias < self.config.BIAS_MIN or bias > self.config.BIAS_MAX:
            logger.error("Exiting set_bias because bias = {}".format(bias))
            return
        
        self.bias = bias
        self.bias_in_dec = self.convert_from_volt('bias', self.bias)

        logger.debug("Set channel {} bias as {} in dec {}".format(ch_num, self.bias, self.bias_in_dec))

        self.client.set_bias(self.bias_in_dec)
        time.sleep(0.1)

    def set_lp(self, lp, ch_num=None, pl=None):
        """Set lp for given channel

        Arguments:
            lp {int} -- LP bits used by ASIC OPT11

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if lp not in self.config.LP_BITS:
            logger.error("LP bit {} not found. Exiting.".format(lp))

        self.lp = lp

        logger.debug("Set channel {} lp as {}".format(ch_num, lp))

        self.client.set_lp(self.lp)
        time.sleep(0.1)

    def enable_all_channel(self):
        """Enable all channels
        """
        pass

    def disable_all_channel(self):
        """Disable all channels
        """
        self.client.subcomponents['timing'].fire_stop()

    def set_power_all(self, pl=0):
        """Set power to all channels

        Keyword Arguments:
            pl {int} -- Power level to write boot, bias, lp values to (default: {0})
        """
        pass

    def get_temperature(self, ch_num=None):
        """Get temperature from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            (Not being used for velabit)

        Returns:
            [float] -- [Temperature of unit in C]
        """
        temperature = self.client.subcomponents['adc_mpg_ctrl'].get_monitor_temp_sense_rtemp()[1]

        return temperature
        
    def get_adc_cap(self, ch_num=None, azimuth=6000):
        """Get ADC capture from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})

        Returns:
            [int array] -- [Integer array of adc capture]
        """
        pass

    def reg_read(self, addr):
        """Read register from unit

        Arguments:
            addr {hex} -- Register address

        Returns:
            [int] -- [Register value]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_read.__name__, True).format(addr)

        logger.debug("Reg read addr {}".format(addr))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg readout to {} failed. Setting reg_read value to None".
                         format(addr))
            reg_read = None
        else:
            reg_read = ast.literal_eval(r.split(" ")[2][:-2])
            logger.debug("Reg read data is {}".format(reg_read))

        return reg_read

    def reg_write(self, addr, data):
        """Write to register

        Arguments:
            addr {hex} -- Register address to write to
            data {hex} -- data

        Returns:
            [str] -- [Error message if failed]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_write.__name__, True).format(addr, data)
        # cmd = "/usr/local/bin/velodyne/host/reg_access -w -a {} -d {} -A 192.168.1.100 -P 4420".format(addr,data)

        logger.debug("Reg write addr {} with data {}".format(addr, data))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg write to {} with data {} failed. Setting reg_write value to None".\
                format(addr, data))
            reg_write = None
        else:
            reg_write = ast.literal_eval(r.split(" ")[2][:-2])

        return reg_write

    def reg_access_wrap(self, addr, offset, maxval, val):
        """Read register and then write at field position

        Arguments:
            addr {hex} -- Register address to write to
            offset {int} -- Field offset
            maxval {int} -- Max field value
            val {int} -- Value to write to field

        Returns:
            [str] -- [Error message if failed]
        """
        result = self.reg_read(addr)
        data = result
        new_data = (data & ~(maxval << offset)) | (val << offset)
        logger.debug('\taddr={} was {}, now {:x}'.format(addr, data, new_data))

        return self.reg_write(addr, new_data)

    def check_fault(self):
        """Check if faults are cleared.
        """
        return self.client.subcomponents['fault'].check_fault()

    def clear_fault(self):
        """Clear faults
        """
        self.client.subcomponents['fault'].clear_fault()
        return self.client.subcomponents['fault'].check_fault()

    def get_prf(self):
        """Get current PRF

        Returns:
            [float] -- [Current PRF]
        """
        r = self.client.subcomponents['timing'].calc_firing_rate()
        prf = None
        if r is None or r == "":
            logger.warn("Get prf failed.")
            prf = None
        else:
            prf = r
            self.cur_prf = prf

            logger.debug("PRF is {}".format(prf))

        return prf

    def get_vcsel_prf(self, maintainence='n'):
        """Get current PRF

        Keyword Arguments:
            maintainence {str} -- 'n' for no maintainence,
            'm' for with maintainence (default: {'n'})

        Returns:
            [float] -- [Current PRF]
        """
        pass

    def enable_vcsel(self, enable):
        pass

    def set_vcsel_recharge(self, recharge_ctr):
        """Set recharge cycles for vcsel

        Arguments:
            recharge_ctr {int} -- recharge controls in cycles
        """
        pass

    def get_12v(self):
        """Get 12V

        Return:
            [float] : Float value of 12V
        """
        v12 = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_12v0_reg()[1])

        v12 = round(v12, 3)
        if self._v12 is not None and round(v12) != round(self._v12):
            logger.error('12V rail changed from {} to {}'.format(self._v12, v12))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG12V')
            logger.input("Check 12V. Hit enter to continue.")

        self._v12 = v12
        logger.debug("12V is at {}".format(v12))

        return self._v12

    def get_3_3v(self):
        """Get 3.3V

        Return:
            [float] : Float value of 3.3V
        """
        v3_3 = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_3v3_reg()[1])
        v3_3 = round(v3_3, 3)

        logger.debug("3.3V is at {}".format(v3_3))

        if self._v3_3 is not None and round(v3_3) != round(self._v3_3):
            logger.error('3.3V rail changed from {} to {}'.format(self._v3_3, v3_3))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG3-3V')
            logger.input("Check 3.3V. Hit enter to continue.")

        self._v3_3 = v3_3

        return self._v3_3

    def get_vlas(self):
        """Get 20V (vlaser)

        Return:
            [float] : Float value of 20V
        """
        r = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_vlas_reg()[1])
        if r is None or r == "":
            logger.error("20V read failed.")
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
            vlas = None
        else:
            vlas = r
            logger.debug("20V is at {}".format(vlas))

        vlas = round(vlas, 3)

        self._vlas = vlas

        return self._vlas

    def get_vlas_before_fire(self):
        """Get 20V (vlaser) before laser fire

        Return
            [float] : Float value of 20V
        """
        # TODO: get ASIC2C vlas values?
        r = self.api_field_read("get_field_pltf_cfg_vlas_adc_readout_vlas_before_fire")

        if r is None or r == "":
            logger.error('20V after laser reg read error.')
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
        else:
            # Conversion from mcm-sdk, velabit, platform_cfg.py
            vlas_after_fire = r * 0.0075978

        vlas_after_fire = round(vlas_after_fire, 3)

        return vlas_after_fire

    def get_vlas_after_fire(self):
        """Get 20V (vlaser) after laser fire

        Return
            [float] : Float value of 20V
        """
        r = self.api_field_read("get_field_pltf_cfg_vlas_adc_readout_vlas_after_fire")

        if r is None or r == "":
            logger.error('20V after laser reg read error.')
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
        else:
            # Conversion from mcm-sdk, velabit, platform_cfg.py
            vlas_after_fire = r * 0.0075978

        vlas_after_fire = round(vlas_after_fire, 3)

        return vlas_after_fire

    def get_5v(self):
        """Get 5V

        Return:
            [float] : Float value of 5V
        """
        v5 = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_5v0_reg()[1])
        v5 = round(v5, 3)

        logger.debug("5V is at {}".format(v5))

        if self._v5 is not None and round(v5) != round(self._v5):
            logger.error('5V rail changed from {} to {}'.format(self._v5, v5))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG5V')
            logger.input("Check 5V. Hit enter to continue.")

        self._v5 = v5

        return self._v5

    def get_hv(self):
        """Get HV

        Return:
            [float] : Float value of HV
        """
        hv = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_hv_vlas_hv()[1])

        hv = round(hv, 3)

        self._hv = hv

        return self._hv

    def get_system_temperature(self):
        """Get system temperature in C
        """
        system_temp = float(self.client.subcomponents['adc_mpg_ctrl'].get_monitor_temp_sense_vtherm()[1])

        return round(system_temp, 3)

    def gen_pcap(self, pkt_cnt, csv_name):
        """Generate pcap using packet_reader

        Arguments:
            pkt_cnt {int} -- Number of packets to read
            csv_name {str} -- Csv file string name
        """
        pkt_reader = packet_reader(self.config.PACKET_FORMAT, csv_name)
        pkt_reader.read_from_device(self.config.DEV_IPADDR, self.config.DEV_PORT, pkt_cnt)

    def get_nfbias_snapshot(self, csv_file):
        """Get current nfbias snapshot and store in csv file

        Arguments:
            csv_file [str] -- String name of csv file
        """
        pass

    def get_nfbias_width(self, csv_file):
        """Get current nfwidth and store in csv file

        Arguments:
            csv_file [str] -- String name of csv file
        """
        pass

    def set_vertical_mirror_position(self, position):
        """Set vertical scan mirror position

        Arguments:
            position [float] -- Float position of vertical scan mirror
        """
        pass

    def set_nfbias_enable(self, enable):
        """Set nfbias enable

        Arguments:
            enable [int] -- 1/0 to enable or disable
        """
        pass

    def init_tx_cal(self):
        """Init tx cal mode
        """
        logger.debug("Init TxCal mode.")
        self.client.init_tx_cal()

    def scan_fault_allow(self):
        """Scan fault allow (Eye-unsafe)
        """
        logger.debug("Allow scan fault allow.")
        self.client.subcomponents['fault'].set_scan_fault_allow(1)

    def api_field_write(self, field_name, data):
        """Set fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
            data (int): Field value
        """
        logger.debug("Writing to field {} with data {}".format(field_name, data))
        status = getattr(self.client.regs, field_name)(data)

        if status != 0:
            logger.warning("API field write status {}".format(status))

    def api_field_read(self, field_name):
        """Read fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
        """
        logger.debug("Reading from field {}".format(field_name))
        data, status = getattr(self.client.regs, field_name)()

        if status != 0:
            logger.warning("API field read status {}".format(status))

        return data

    def populate_cur_row(self, version, results_row=None, display=True):
        """Populate current results row

        Keyword Arguments:
            results_row {dict} -- Dict of results row (default: {None})
            display {bool} -- True/False to dispaly results (default: {True})
        """
        if results_row is None:
            logger.error("Please provide results_row")
            return
        
        results_row['Channel'] = self.cur_ch
        results_row['LP'] = hex(self.lp)
        results_row['Boot'] = self.boot
        results_row['Boot in Hex'] = hex(self.boot_in_dec)
        results_row['Bias'] = self.bias
        results_row['Bias in Hex'] = hex(self.bias_in_dec)

        cur_prf = self.get_prf()
        if cur_prf != self.cur_prf:
            logger.error("PRF has changed.")
            logger.input("Please hit enter to continue.")
        
        self.cur_prf = cur_prf

        results_row['Temperature'] = self.get_temperature()

        results_row['Unit_Version'] = self._unit_version
        results_row['Unit_Build_Date'] = self._unit_build_date
        results_row['Unit_Build_Githash'] = self._unit_build_githash
        results_row['Unit_Model_Number'] = self._unit_model_number
        results_row['Unit_HW_Revision'] = self._unit_hw_revision
        results_row['Unit_Serial_Number'] = self._unit_serial_number

        # Voltages
        results_row['12V'] = self.get_12v()
        results_row['3.3V'] = self.get_3_3v()
        results_row['5V'] = self.get_5v()
        results_row['21V'] = self.get_vlas()
        results_row['System Temperature(C)'] = self.get_system_temperature()
        results_row['HV'] = self.get_hv()

        results_row['VLAS_Before_Fire(V)'] = self.get_vlas_before_fire()
        results_row['VLAS_After_Fire(V)'] = self.get_vlas_after_fire()

class VB2(velabit):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.velabit.vb2.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.velabit.vb2.sdk_scripts")
        super(VB2, self).__init__(self.config, self.sdk_scripts)

        self.channels= [
                        0, # Top, Top, ch 0
                        ]

    def __str__(self):
        return 'VB2'

    def init(self):
        # Write the initialization registers here
        sdk_version = super(VB2, self).init(sensor=self)

        return sdk_version

    def populate_cur_row(self, version, results_row=None, display=True):
        if results_row is None:
            logger.error("Please provide results_row")
            return
            
        super().populate_cur_row(version, results_row, display)


if __name__ == "__main__":
    vb2 = VB2()

