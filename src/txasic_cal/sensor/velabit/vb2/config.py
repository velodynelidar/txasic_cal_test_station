## Sensor Configurations
HW_VERSION = 2
NUM_LCP = 1
NUM_CHANNELS_PER_LCP = 1
NUM_CHANNELS = NUM_LCP * NUM_CHANNELS_PER_LCP

## Results File Variables (TODO: Automate from diagnostic packet)
UNIT_NAME = "P2"
# Table headers that is used to load to the sensor (should match file under mcm-sdk used to load pl table)
TX_POWER_LEVEL_TABLE_HEADER = ['Channel', 'Power Level', 'LP', 'Boot', 'Boot in Hex', 'Bias',
                            'Bias in Hex', 'PRF', 'Temperature', '12V', '3.3V', '5V', '21V', 'System Temperature(C)', 
                            # 'VLAS_Before_Fire(V)', 'VLAS_After_Fire(V)',
                            'Unit_Version', 'Unit_Build_Date', 'Unit_Build_Githash', 'Unit_Model_Number', 'Unit_HW_Revision', 'Unit_Serial_Number']


DEFAULT_PRF = 75
DEFAULT_VCSEL_PRF = 60

CH_NUM = [[(ch * NUM_LCP + lcp % NUM_LCP) for ch in range(NUM_CHANNELS_PER_LCP)] for lcp in range(NUM_LCP)]

CH_NUM_PER_LCP = {ch:ch//NUM_LCP for ch in CH_NUM[0]}

LCP_NUM_PER_CH = {ch:ch%NUM_LCP for ch in CH_NUM[0]}

LCP_DICT = {'Top':{'Top':0,'Bottom':1}}

REVERSE_LCP_DICT = {ch:('Top', 'Top') for ch in CH_NUM[0]}

MAX_POWER_LEVEL = 16
POWER_LEVEL_NUM = [i for i in range(0, MAX_POWER_LEVEL)]

BOOT_MAX = 8.0
BOOT_MIN = 4.0
BIAS_MAX = 5.0
BIAS_MIN = 0

# Max DAC value - 8 V or 3FFF
MAX_DAC_DECIMAL = 1023
MAX_DAC_VOLT = 10
MAX_BOOT_DAC_VOLT = BOOT_MAX - BOOT_MIN
MAX_BIAS_DAC_VOLT = BIAS_MAX - BIAS_MIN

LP_BITS = [15, 14, 13, 11, 12, 9, 8, 4, 0]
# LP_BITS = [15, 14, 13, 11, 12, 10, 9, 6, 5, 3, 4, 2, 1, 0]

# Overwritten by init_txcal_version
START_BOOT = 5.2
START_BIAS = 2.7

SCRATCHPAD_PL = 10

PACKET_FORMAT = "rpf"
DEV_IPADDR = "0.0.0.0"
DEV_PORT = 2368

SDK_SENSOR_NAME = 'velabit_a0'
SENSOR_IP_ADDR = '192.168.1.100'
SENSOR_PORT = '0x1080'
