from txasic_cal.tools.logger import logger

# __cmd_line_dict__ should match sdk CLI control of sensor
__cmd_line_dict__ = {
    }

def return_dict_cl(cmd=None, is_reg_access=False):
    if cmd is None:
        logger.error("No cmd given. Please enter cmd.")
        return

    # Or use python3 -m velaplatform.velabit -H 2
    # sdk_loc = "vbita2c "
    # sdk_loc = "cd ~/vijay/build_mcm/installed/"
    sdk_loc = "cd /usr/local/"
    python_platform = "share/velodyne/;python3 -m velaplatform.velabit -H 2 "
    reg_platform = "bin/velodyne/host/;./"

    if is_reg_access:
        return sdk_loc + reg_platform + __cmd_line_dict__[cmd]
    else:
        return sdk_loc + python_platform + __cmd_line_dict__[cmd]
