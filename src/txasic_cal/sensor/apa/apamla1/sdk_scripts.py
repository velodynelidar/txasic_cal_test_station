from txasic_cal.tools.logger import logger

# __cmd_line_dict__ should match sdk CLI control of sensor
__cmd_line_dict__ = {
    'enable_channel':'pwr_ctrl --set_power {},{},{},on',
    'disable_channel':'pwr_ctrl --set_power {},{},{},off',
    'set_boot':'pwr_ctrl --set_boot {},{},{},{}',
    'set_bias':'pwr_ctrl --set_bias {},{},{},{}',
    'set_lp':'pwr_ctrl --set_lp {},{},{},{}',
    'set_boot_all':'pwr_ctrl --set_boot_all {},{}',
    'set_bias_all':'pwr_ctrl --set_bias_all {},{}',
    'set_lp_all':'pwr_ctrl --set_lp_all {},{}',
    'enable_all_channel':'pwr_ctrl --enable_all',
    'disable_all_channel':'pwr_ctrl --disable_all',
    'set_power_all':'pwr_ctrl --set_power_all {}',
    'get_temperature':'monitor -t {} --stdout',
    'get_adc_cap':'adc_capture -c {} -a {} -i 1 --std',
    'reg_read':'reg_access -r -a {}',
    'reg_write':'reg_access -w -a {} -d {}',
    'check_fault':'fault --check',
    'clear_fault':'fault --clear',
    'get_prf':'ptg_ctrl --get_prf {}',
    'enable_vcsel':'pwr_ctrl --vcsel_enable_fire {}',
    'set_vcsel_recharge': 'pwr_ctrl --vcsel_recharge {}',
    'get_vcsel_prf':'ptg_ctrl --get_vcsel_prf {}',
    'set_opamp_high_gain':'opm --high_gain',
    'set_opamp_low_gain':'opm --low_gain',
    'get_opm_adc':'opm --adc_read {} --stdout',
    'set_opm_mode':'opm --set_mode {}',
    'get_12v':'monitor --v12 --stdout',
    'get_3_3v':'monitor --v3_3 --stdout',
    'get_vlas':'monitor --v21 --stdout',
    'get_5v':'monitor --v5 --stdout',
    'get_system_temperature':'monitor --fpga_temp --stdout',
    'get_nfbias_snapshot':'pwr_ctrl --get_nfbias_snapshot',
    'get_nfbias_width':'pwr_ctrl --get_nfbias_width',
    'set_vertical_mirror_position':'he_scan_ctrl --cal_mode {}',
    'set_nfbias_enable':'pwr_ctrl --set_nfbias_en {}'
    }

def return_dict_cl(cmd=None, is_reg_access=False):
    if cmd is None:
        logger.error("No cmd given. Please enter cmd.")
        return

    # sdk_loc = "apamla1 "
    # sdk_loc = "cd ~/vijay/build_mcm/installed/"
    sdk_loc = "cd /usr/local/"
    python_platform = "share/velodyne/;python3 -m velaplatform.apa -H mla1 "
    reg_platform = "bin/velodyne/host/;./"

    if is_reg_access:
        return sdk_loc + reg_platform + __cmd_line_dict__[cmd]
    else:
        return sdk_loc + python_platform + __cmd_line_dict__[cmd]
