#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2021-4-12
#  Description   : Software for apa representation and communication

import importlib
import numpy as np
import ast
from txasic_cal.communication import mcm_sdk_client
import time

from txasic_cal.tools.logger import logger
from txasic_cal.tools.packet_reader import packet_reader
from txasic_cal.pfmea_code import pfmea_table

class apa(mcm_sdk_client.mcm_sdk_client):

    def __init__(self, config, sdk_scripts):
        """Top level apa class

        Arguments:
            config {module} -- [sensor config module to be used]
            sdk_scripts {module} -- [sensor sdk scripts to be used]
        """
        super(apa, self).__init__(apa.__name__)
        self.config = config
        self.sdk_scripts = sdk_scripts

        self.cur_ch = 0
        self.bias = 0
        self.boot = 0
        self.boot_in_dec = 0
        self.bias_in_dec = 0
        self.lp = self.config.LP_BITS[0]
        self.cur_prf = None

        self.vcsel_recharge_ctr = 0
        self.vcsel_recharge_ctr_in_dec = 0
        self.cur_vcsel_prf = None

        self._unit_version = None
        self._unit_build_date = None
        self._unit_build_githash = None
        self._unit_model_number = None
        self._unit_hw_revision = None
        self._unit_serial_number = None

        self.start_boot = config.START_BOOT
        self.start_bias = config.START_BIAS

        self._v12 = None
        self._v5 = None
        self._v3_3 = None
        self._vlas = None
        self._hv = None

    def init(self, sensor):
        sdk_version = self.connect(sensor)

        # Unit information
        # self._unit_version = ".".join(str(n) for n in self.client.subcomponents['platform_regs'].get_image_version())
        # self._unit_build_date = self.client.subcomponents['platform_regs'].get_build_date_time()
        # self._unit_build_githash = self.client.subcomponents['platform_regs'].get_build_git_hash()
        # self._unit_model_number = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_model_number())
        # self._unit_hw_revision = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_revision())
        # hi, lo = self.client.subcomponents['platform_regs'].get_serial_number()
        # self._unit_serial_number = "{:08X}{:08X}".format(hi, lo)

        # Voltage rails
        # self.get_12v()
        # self.get_5v()
        # self.get_3_3v()
        # self.get_vlas()
        # self.get_hv()

        return sdk_version

    def close(self):
        """Close connection to unit and ensure all channels are disabled
        """
        if self._is_connected:
            # self.disable_all_channel()
            self.enable_vcsel(0)
            self.disconnect()

    def ch_init(self, ch_num):
        """Initialize channel

        Arguments:
            ch_num {int} -- [Channel number]
        """
        self.cur_ch = ch_num
        self.bias = self.start_bias
        self.boot = self.start_boot
        self.boot_in_dec = self.convert_from_volt('boot', self.boot)
        self.bias_in_dec = self.convert_from_volt('bias', self.bias)
        self.lp = self.config.LP_BITS[1]

        self.set_bias(self.bias)
        self.set_boot(self.boot)
        self.set_lp(self.lp)
        self.enable_channel(ch_num)

    def ch_deinit(self):
        self.disable_channel(self.cur_ch)

    # override super execute
    def execute(self, cmd):
        return super().execute(cmd)

    def convert_from_dec(self, cur_input, cur_value):
        """Convert given input from decimal value to voltage

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Decimal value

        Returns:
            [float] -- Converted decimal value to float
        """
        if cur_input == 'boot':
            new_value = round(cur_value * self.config.MAX_BOOT_DAC_VOLT / self.config.MAX_DAC_DECIMAL, 3)
        elif cur_input == 'bias':
            new_value = round(cur_value * self.config.MAX_BIAS_DAC_VOLT / self.config.MAX_DAC_DECIMAL, 3)

        return new_value

    def convert_from_volt(self, cur_input, cur_value):
        """Convert given input from voltage to decimal value

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Voltage value

        Returns:
            [float] -- Converted float to decimal value
        """
        if cur_input == 'boot':
            new_value = int((cur_value * self.config.MAX_DAC_DECIMAL) / self.config.MAX_BOOT_DAC_VOLT)
        elif cur_input == 'bias':
            new_value = int((cur_value * self.config.MAX_DAC_DECIMAL) / self.config.MAX_BIAS_DAC_VOLT)

        return new_value

    def enable_channel(self, ch_num=None, power_level=None):
        """Enable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            power_level {int}-- Optional argument, if not provided, 
            scratchpad power level is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(power_level == None):
            power_level = self.config.SCRATCHPAD_PL

        logger.debug("Enabling channel {} power level {}".format(ch_num, power_level))

        self.client.subcomponents['pwr_ctrl'].set_power(ch_num, power_level, 'on')
        self.client.subcomponents['timing'].set_firing_state(ch_num, 1)

    def disable_channel(self, ch_num=None, power_level=None):
        """Disable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            power_level {int}-- Optional argument, if not provided, 
            scratchpad power level is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(power_level == None):
            power_level = self.config.SCRATCHPAD_PL

        logger.debug("Disabling channel {}".format(ch_num))

        self.client.subcomponents['pwr_ctrl'].set_power(ch_num, power_level, 'off')
        self.client.subcomponents['timing'].set_firing_state(ch_num, 0)

    def set_boot(self, boot, ch_num=None, pl=None, set_all=False):
        """Set boot for given channel

        Arguments:
            boot {float} -- Boot value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if boot < self.config.BOOT_MIN:
            logger.error("Boot = {} lower than {}. Setting to {}".format(boot, self.config.BOOT_MIN, self.config.BOOT_MIN))
            boot = self.config.BOOT_MIN
        elif boot > self.config.BOOT_MAX:
            logger.error("Boot = {} greater than {}. Setting to {}".format(boot, self.config.BOOT_MAX, self.config.BOOT_MAX))
            boot = self.config.BOOT_MAX
        
        self.boot = boot
        self.boot_in_dec = self.convert_from_volt("boot", boot)

        logger.debug("Set channel {} boot as {} in dec {}".format(ch_num, boot, self.boot_in_dec))

        # if not set_all:
            # self.client.subcomponents['pwr_ctrl'].set_boot(self.boot_in_dec, ch_num, pl)
        # else:
        #     self.client.subcomponents['pwr_ctrl'].set_boot_all(self.boot_in_dec, pl)

        # APA uses global params instead of setting individual BRAM values
        self.client.regs.set_field_pwrlvl_global_params_boot(self.boot_in_dec)

        time.sleep(0.1)

    def set_bias(self, bias, ch_num=None, pl=None, set_all=False):
        """Set bias for given channel

        Arguments:
            bias {float} -- Bias value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if bias < self.config.BIAS_MIN:
            logger.error("Bias = {} lower than {}. Setting to {}".format(bias, self.config.BIAS_MIN, self.config.BIAS_MIN))
            bias = self.config.BIAS_MIN
        elif bias > self.config.BIAS_MAX:
            logger.error("Bias = {} greater than {}. Setting to {}".format(bias, self.config.BIAS_MAX, self.config.BIAS_MAX))
            bias = self.config.BIAS_MAX
        
        self.bias = bias
        self.bias_in_dec = self.convert_from_volt("bias", bias)

        logger.debug("Set channel {} bias as {} in dec {}".format(ch_num, bias, self.bias_in_dec))

        # if not set_all:
        #     self.client.subcomponents['pwr_ctrl'].set_bias(self.bias_in_dec, ch_num, pl)
        # else:
        #     self.client.subcomponents['pwr_ctrl'].set_bias_all(self.bias_in_dec, pl)

        # APA uses global params instead of setting individual BRAM values
        self.client.regs.set_field_pwrlvl_global_params_bias(self.bias_in_dec)

        time.sleep(0.1)

    def set_lp(self, lp, ch_num=None, pl=None, set_all=False):
        """Set lp for given channel

        Arguments:
            lp {int} -- LP bits used by ASIC OPT11

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if lp not in self.config.LP_BITS:
            logger.error("LP bit {} not found. Exiting.".format(lp))

        self.lp = lp

        logger.debug("Set channel {} lp as {}".format(ch_num, lp))
        
        # if not set_all:
        #     self.client.subcomponents['pwr_ctrl'].set_lp(lp, ch_num, pl)
        # else:
        #     self.client.subcomponents['pwr_ctrl'].set_lp_all(lp, pl)

        # APA uses global params instead of setting individual BRAM values
        self.client.regs.set_field_pwrlvl_global_params_lp(self.lp)

        time.sleep(0.1)

    def enable_all_channel(self):
        """Enable all channels
        """
        logger.debug("Enable all channel.")

        self.client.subcomponents['pwr_ctrl'].set_all_channel_trigger_state(True)

    def disable_all_channel(self):
        """Disable all channels
        """
        logger.debug("Disable all channel.")

        self.client.subcomponents['pwr_ctrl'].set_all_channel_trigger_state(False)

    def set_power_all(self, pl=10):
        """Set power to all channels

        Keyword Arguments:
            pl {int} -- Power level to write boot, bias, lp values to (default: {10})
        """
        if pl not in self.config.POWER_LEVEL_NUM:
            logger.error("Please enter correct power level.")
            return

        self.client.subcomponents['pwr_ctrl'].set_all_channel_pwrlvl(pl)

    def get_temperature(self, ch_num=None):
        """Get temperature from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})

        Returns:
            [float] -- [Temperature of unit in C]
        """
        pass

    def get_adc_cap(self, ch_num=None, azm=6000):
        """Get ADC capture from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            azm {int} -- azimuth to capture adc cap, default - 6000

        Returns:
            [int array] -- [Integer array of adc capture]
        """
        pass

    def reg_read(self, addr):
        """Read register from unit

        Arguments:
            addr {hex} -- Register address

        Returns:
            [int] -- [Register value]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_read.__name__, True).format(addr)

        logger.debug("Reg read addr {}".format(addr))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg readout to {} failed. Setting reg_read value to None".
                         format(addr))
            reg_read = None
        else:
            reg_read = ast.literal_eval(r.split(" ")[2][:-2])
            logger.debug("Reg read data is {}".format(reg_read))

        return reg_read

    def reg_write(self, addr, data):
        """Write to register

        Arguments:
            addr {hex} -- Register address to write to
            data {hex} -- data

        Returns:
            [str] -- [Error message if failed]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_write.__name__, True).format(addr, data)
        # cmd = "/usr/local/bin/velodyne/host/reg_access -w -a {} -d {} -A 192.168.1.100 -P 4420".format(addr,data)

        logger.debug("Reg write addr {} with data {}".format(addr, data))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg write to {} with data {} failed. Setting reg_write value to None".\
                format(addr, data))
            reg_write = None
        else:
            reg_write = ast.literal_eval(r.split(" ")[2][:-2])

        return reg_write

    def reg_access_wrap(self, addr, offset, maxval, val):
        """Read register and then write at field position

        Arguments:
            addr {hex} -- Register address to write to
            offset {int} -- Field offset
            maxval {int} -- Max field value
            val {int} -- Value to write to field

        Returns:
            [str] -- [Error message if failed]
        """
        result = self.reg_read(addr)
        data = result
        new_data = (data & ~(maxval << offset)) | (val << offset)
        logger.debug('\taddr={} was {}, now {:x}'.format(addr, data, new_data))

        return self.reg_write(addr, new_data)

    def check_fault(self):
        """Check if faults are cleared.
        """
        fault = self.client.subcomponents['fault'].check_faults()

        return fault

    def clear_fault(self):
        """Clear faults and check if faults are cleared.
        """
        self.client.subcomponents['fault'].clear_faults()

        fault = self.check_fault()

        return fault

    def get_prf(self):
        """Get current PRF

        Returns:
            [float] -- [Current PRF in KHz]
        """
        prf = round(self.client.subcomponents['timing'].get_prf() * 1e-3, 3)

        if prf != self.cur_prf and self.cur_prf != None:
            logger.error("PRF has changed from {} to {}".format(self.cur_prf, prf))
            input("Please hit enter to continue.")

        self.cur_prf = prf

        return prf

    def get_vcsel_prf(self, maintenance='n'):
        """Get current PRF

        Keyword Arguments:
            maintenance {str} -- 'n' for no maintenance,
            'm' for with maintenance (default: {'n'})

        Returns:
            [float] -- [Current PRF]
        """
        pass

    def enable_vcsel(self, enable):
        """Enable/disable vcsel

        Arguments:
            enable [int] : 0 - disable, 1 - enable
        """
        pass

    def set_vcsel_recharge(self, recharge_ctr):
        """Set recharge cycles for vcsel

        Arguments:
            recharge_ctr {int} -- recharge controls in cycles
        """
        pass

    def set_opamp_gain(self, ch_num, pl, gain):
        """Set OPM gain

        Arguments:
            gain [int] : 1 - low, 0 - high
        """
        pass

    def get_opm_adc(self, num_iter):
        """Get OPM ADC readings for a given number of iterations

        Arguments:
            num_iter [int] : Number of iterations
        """
        pass

    def set_opm_mode(self, mode):
        """Set OPM mode

        Arguments:
            mode [int] : 0 - Idle, 1 - Temperature Mode, 2 - OPM Mode
        """
        pass
    
    def get_12v(self):
        """Get 12V

        Return:
            [float] : Float value of 12V
        """
        pass

    def get_3_3v(self):
        """Get 3.3V

        Return:
            [float] : Float value of 3.3V
        """
        pass

    def get_vlas(self):
        """Get 21V (vlaser)

        Return:
            [float] : Float value of 21V
        """
        pass

    def get_5v(self):
        """Get 5V

        Return:
            [float] : Float value of 5V
        """
        pass

    def get_hv(self):
        """Get HV

        Return:
            [float] : Float value of HV
        """
        pass

    def get_system_temperature(self):
        """Get system temperature in C
        """
        pass

    def gen_pcap(self, pkt_cnt, csv_name):
        """Generate pcap using packet_reader

        Arguments:
            pkt_cnt {int} -- Number of packets to read
            csv_name {str} -- Csv file string name
        """
        pkt_reader = packet_reader(self.config.PACKET_FORMAT, csv_name)
        pkt_reader.read_from_device(self.config.DEV_IPADDR, self.config.DEV_PORT, pkt_cnt)

    def get_nfbias_snapshot(self):
        """Get current nfbias snapshot and store in csv file
        """
        pass

    def get_nfbias_width(self):
        """Get current nfwidth and store in csv file
        """
        pass

    def set_nfbias_enable(self, enable):
        """Set nfbias enable

        Arguments:
            enable [int] -- 1/0 to enable or disable
        """
        pass

    def init_tx_cal(self):
        """Init tx cal mode
        """
        # set global params reg enable
        self.client.regs.set_field_pwrlvl_global_params_en_reg(1)

        self.client.init_tx_cal()

    def scan_fault_allow(self):
        """Scan fault allow (Eye-unsafe)
        """
        pass
        # logger.debug("Allow scan fault allow.")
        # self.client.subcomponents['he_scan_ctrl'].set_scan_fault_allow_status(True)

    def api_field_write(self, field_name, data):
        """Set fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
            data (int): Field value
        """
        logger.debug("Writing to field {} with data {}".format(field_name, data))
        status = getattr(self.client.regs, field_name)(data)

        if status != 0:
            logger.error("API field write status {}".format(status))
            return -1

        return 0

    def api_field_read(self, field_name):
        """Read fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
        """
        logger.debug("Reading from field {}".format(field_name))
        data, status = getattr(self.client.regs, field_name)()

        if status != 0:
            logger.error("API field read status {}".format(status))
            return -1

        return data

    def populate_cur_row(self, version, results_row=None, display=True):
        """Populate current results row

        Keyword Arguments:
            results_row {dict} -- Dict of results row (default: {None})
            display {bool} -- True/False to dispaly results (default: {True})
        """
        if results_row is None:
            logger.error("Please provide results_row")
            return

        if version[0:2] == "VC":
            results_row['Channel'] = self.cur_ch
            results_row['Board'] = self.client.config.get_mla_from_ch(self.cur_ch)
            results_row['TROSA'] = self.client.config.get_quad_from_ch(self.cur_ch)
            results_row['Recharge clock cycles in hex'] = hex(self.vcsel_recharge_ctr_in_dec)
            results_row['Recharge Time (in microseconds)'] = self.vcsel_recharge_ctr
        else:
            results_row['Channel'] = self.cur_ch
            results_row['Board'] = self.client.config.get_mla_from_ch(self.cur_ch)
            results_row['TROSA'] = self.client.config.get_quad_from_ch(self.cur_ch)
            results_row['LP'] = hex(self.lp)
            results_row['Boot'] = self.boot
            results_row['Boot in Hex'] = hex(self.boot_in_dec)
            results_row['Bias'] = self.bias
            results_row['Bias in Hex'] = hex(self.bias_in_dec)

        # results_row['Temperature'] = self.get_temperature()

        # results_row['Unit_Version'] = self._unit_version
        # results_row['Unit_Build_Date'] = self._unit_build_date
        # results_row['Unit_Build_Githash'] = self._unit_build_githash
        # results_row['Unit_Model_Number'] = self._unit_model_number
        # results_row['Unit_HW_Revision'] = self._unit_hw_revision
        # results_row['Unit_Serial_Number'] = self._unit_serial_number

        # Voltages
        # results_row['12V'] = self.get_12v()
        # results_row['3.3V'] = self.get_3_3v()
        # results_row['5V'] = self.get_5v()
        # results_row['21V'] = self.get_vlas()
        # results_row['System Temperature(C)'] = self.get_system_temperature()
        # results_row['HV'] = self.get_hv()


class APAMLA1(apa):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.apa.apamla1.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.apa.apamla1.sdk_scripts")

        self.channels= [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31,
                        32,
                        33,
                        34,
                        35,
                        36,
                        37,
                        38,
                        39,
                        40,
                        41,
                        42,
                        43,
                        44,
                        45,
                        46,
                        47,
                        48,
                        49,
                        50,
                        51,
                        52,
                        53,
                        54,
                        55,
                        56,
                        57,
                        58,
                        59,
                        60,
                        61,
                        62,
                        63,
                        ]

        super(APAMLA1, self).__init__(self.config, self.sdk_scripts)

    def init(self):
        # Write the initialization registers here
        sdk_version = super(APAMLA1, self).init(sensor=self)

        # Set power per quad enable register to 0 (needed to use BRAM)
        for i in range(self.client.config.NUM_QUAD):
            eval(f"self.client.regs.set_field_quad_ctrl_{i}_pwr_per_quad_en(0)")

        # Set tx allow to 1
        self.client.regs.set_field_timing_state_tx_allow(1)

        return sdk_version

    def __str__(self):
        return 'APAMLA1'

    def populate_cur_row(self, version, results_row=None, display=True):
        if results_row is None:
            logger.error("Please provide results_row")
            return

        super().populate_cur_row(version, results_row, display)


class APAMLA2(apa):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.apa.apamla2.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.apa.apamla2.sdk_scripts")

        self.channels= [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31,
                        32,
                        33,
                        34,
                        35,
                        36,
                        37,
                        38,
                        39,
                        40,
                        41,
                        42,
                        43,
                        44,
                        45,
                        46,
                        47,
                        48,
                        49,
                        50,
                        51,
                        52,
                        53,
                        54,
                        55,
                        56,
                        57,
                        58,
                        59,
                        60,
                        61,
                        62,
                        63,
                        ]

        super(APAMLA2, self).__init__(self.config, self.sdk_scripts)

    def init(self):
        # Write the initialization registers here
        sdk_version = super(APAMLA2, self).init(sensor=self)

        # Set power per quad enable register to 0 (needed to use BRAM)
        for i in range(self.client.config.NUM_QUAD):
            eval(f"self.client.regs.set_field_quad_ctrl_{i}_pwr_per_quad_en(0)")

        # Set tx allow to 1
        self.client.regs.set_field_timing_state_tx_allow(1)

        return sdk_version

    def __str__(self):
        return 'APAMLA2'

    def populate_cur_row(self, version, results_row=None, display=True):
        if results_row is None:
            logger.error("Please provide results_row")
            return

        super().populate_cur_row(version, results_row, display)


if __name__ == "__main__":
    apa = APAMLA1()

