## Sensor Configurations
HW_VERSION = 101
NUM_LCP = 1
NUM_CHANNELS_PER_LCP = 16
NUM_CHANNELS = NUM_LCP * NUM_CHANNELS_PER_LCP
NUM_TROSA = 4
NUM_CH_TROSA = 4

## Results File Variables (TODO: Automate from diagnostic packet)
UNIT_NAME = "P2"
# Table headers that is used to load to the sensor (should match file under mcm-sdk used to load pl table)
TX_POWER_LEVEL_TABLE_HEADER = ['Board', 'TROSA', 'Channel', 'Power Level', 'LP', 'Boot', 'Boot in Hex', 'Bias',
                            'Bias in Hex', 'PRF', 'Temperature', 
                            '12V', '3.3V', '5V', '21V', 'System Temperature(C)', 'HV', 'HV_Setpoint_0', 'HV_Setpoint_1',
                            'Unit_Version', 'Unit_Build_Date', 'Unit_Build_Githash', 'Unit_Model_Number', 'Unit_HW_Revision', 'Unit_Serial_Number']

VCSEL_POWER_LEVEL_TABLE_HEADER = ['Board', 'TROSA', 'Channel', 'Power Level', 'Recharge clock cycles in hex' ,'Recharge Time (in microseconds)',
                            'PRF', 'Temperature', '12V', '3.3V', '5V', '21V', 'System Temperature(C)',
                            'Unit_Version', 'Unit_Build_Date', 'Unit_Build_Githash', 'Unit_Model_Number', 'Unit_HW_Revision', 'Unit_Serial_Number']

DEFAULT_PRF = 38.367
DEFAULT_VCSEL_PRF = 60

LCP_DICT = {'Top':{'Top':0,'Bottom':1},'Bottom':{'Top':2,'Bottom':3}}

CH_NUM_TROSA = [[ch for ch in range(NUM_CH_TROSA)] for _ in range(NUM_TROSA)]
TROSA_NUM_PER_CH = {ch:ch//NUM_CH_TROSA for ch in range(NUM_TROSA*NUM_CH_TROSA)}

MAX_POWER_LEVEL = 16
POWER_LEVEL_NUM = [i for i in range(0, MAX_POWER_LEVEL)]

BOOT_MAX = 8.0
BOOT_MIN = 4.0
BIAS_MAX = 5.0
BIAS_MIN = 1.5

# Max DAC value - 8 V or 3FFF
MAX_DAC_DECIMAL = 16383
MAX_BOOT_DAC_VOLT = 8
MAX_BIAS_DAC_VOLT = 5

LP_BITS = [15, 14, 13, 11, 12, 9, 8, 4, 0]
# LP_BITS = [15, 14, 13, 11, 12, 10, 9, 6, 5, 3, 4, 2, 1, 0]

START_BOOT = 5.2
START_BIAS = 2.7

SCRATCHPAD_PL = 10

## Vcsel configurations
VCSEL_RECHARGE_MIN = 0
VCSEL_RECHARGE_MAX = 5.863

VCSEL_RECHARGE_CONVERISON = 5.33

PACKET_FORMAT = "rpf"
DEV_IPADDR = "0.0.0.0"
# Port might need to change for VLX
DEV_PORT = 2368

SDK_SENSOR_NAME = 'vlx_a0_mz'
SENSOR_IP_ADDR = '192.168.1.100'
SENSOR_PORT = '0x1081'