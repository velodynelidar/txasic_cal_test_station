## Sensor Configurations
HW_VERSION = 2
NUM_LCP = 4
NUM_CHANNELS_PER_LCP = 8
NUM_CHANNELS = NUM_LCP * NUM_CHANNELS_PER_LCP

DEFAULT_PRF = 75

CH_NUM = [[(ch * NUM_LCP + lcp % NUM_LCP) for ch in range(NUM_CHANNELS_PER_LCP)] for lcp in range(NUM_LCP)]

CH_NUM_PER_LCP = {}
for lcp in range(NUM_LCP):
    CH_NUM_PER_LCP.update(dict(map(lambda ch:(ch, ch//NUM_LCP), CH_NUM[lcp])))

LCP_NUM_PER_CH = {}
for lcp in range(NUM_LCP):
    LCP_NUM_PER_CH.update(dict(map(lambda ch:(ch, ch%NUM_LCP), CH_NUM[lcp])))

LCP_DICT = {'Top':{'Top':0,'Bottom':1},'Bottom':{'Top':2,'Bottom':3}}

REVERSE_LCP_DICT = {ch:('Top', 'Top') for ch in CH_NUM[0]}
REVERSE_LCP_DICT.update({ch:('Top', 'Bottom') for ch in CH_NUM[1]})
REVERSE_LCP_DICT.update({ch:('Bottom', 'Top') for ch in CH_NUM[2]})
REVERSE_LCP_DICT.update({ch:('Bottom', 'Bottom') for ch in CH_NUM[3]})

MAX_POWER_LEVEL = 16
POWER_LEVEL_NUM = [i for i in range(0, MAX_POWER_LEVEL)]

BOOT_MAX = 8.0
BOOT_MIN = 4.0
BIAS_MAX = 4.5
BIAS_MIN = 1.5

# Max DAC value - 8 V or 3FFF
MAX_DAC_DECIMAL = 16383
MAX_DAC_VOLT = 8

LP_BITS = [15, 14, 13, 11, 12, 9, 8, 4, 0]

START_BOOT = 6.0
START_BIAS = 3.0

## Results File Variables (TODO: Automate from diagnostic packet)
UNIT_NAME = "P2_"
# Table headers that is used to load to the sensor (should match file under mcm-sdk used to load pl table)
POWER_LEVEL_TABLE_HEADER = ['Board', 'TROSA', 'Channel', 'Power Level', 'LP', 'Boot', 'Boot in Hex', 'Bias', 'Bias in Hex']

SCRATCHPAD_PL = 0