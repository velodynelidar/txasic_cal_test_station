from txasic_cal.tools.logger import logger

# __cmd_line_dict__ should match sdk CLI control of sensor
__cmd_line_dict__ = {
    'enable_channel':'pwr_ctrl --set_power {},{},{},on',
    'disable_channel':'pwr_ctrl --set_power {},{},{},off',
    'set_boot':'pwr_ctrl --set_boot {},{},{},{}',
    'set_bias':'pwr_ctrl --set_bias {},{},{},{}',
    'set_lp':'pwr_ctrl --set_lp {},{},{},{}',
    'enable_all_channel':'pwr_ctrl --enable_all',
    'disable_all_channel':'pwr_ctrl --disable_all',
    'set_power_all':'pwr_ctrl --set_power_all {}',
    'get_mcm_temperature':'mcm_sense_adc -t {}',
    'adc_capture':'adc_capture -c {} -i 1 --std',
    'reg_read':'reg_access -r -a {}',
    'reg_write':'reg_access -w -a {} -d {}',
    'check_fault':'fault --check',
    'clear_fault':'fault --clear',
    'get_prf':'ptg_ctrl --get_prf -{}'
    }

def return_dict_ssh_cl(cmd=None):
    if cmd is None:
        logger.error("No cmd given. Please enter cmd.")
        return
    
    return __cmd_line_dict__[cmd]