## Sensor Configurations
HW_VERSION = 3
NUM_LCP = 1
NUM_CHANNELS_PER_LCP = 16
NUM_CHANNELS = NUM_LCP * NUM_CHANNELS_PER_LCP
NUM_TROSA = 2
NUM_CH_TROSA = 8

## Results File Variables (TODO: Automate from diagnostic packet)
UNIT_NAME = "P2"
# Table headers that is used to load to the sensor (should match file under mcm-sdk used to load pl table)
TX_POWER_LEVEL_TABLE_HEADER = ['Board', 'TROSA', 'Channel', 'Power Level', 'LP', 'Boot', 'Boot in Hex', 'Bias',
                            'Bias in Hex', 'PRF', 'Temperature', 
                            # 'OPM_Low_Gain_ADC_Mean', 'OPM_Low_Gain_ADC_Std',
                            # 'OPM_Low_Gain_ADC_Max', 'OPM_Low_Gain_ADC_Min', 'OPM_Low_Gain_ADC', 'OPM_High_Gain_ADC_Mean',
                            # 'OPM_High_Gain_ADC_Std', 'OPM_High_Gain_ADC_Max', 'OPM_High_Gain_ADC_Min', 'OPM_High_Gain_ADC',
                            '12V', '3.3V', '5V', '21V', 'System Temperature(C)', 'HV', 'HV_Setpoint_0', 'HV_Setpoint_1',
                            'Unit_Version', 'Unit_Build_Date', 'Unit_Build_Githash', 'Unit_Model_Number', 'Unit_HW_Revision', 'Unit_Serial_Number']

VCSEL_POWER_LEVEL_TABLE_HEADER = ['Board', 'TROSA', 'Channel', 'Power Level', 'Recharge clock cycles in hex' ,'Recharge Time (in microseconds)',
                            'PRF', 'Temperature', '12V', '3.3V', '5V', '21V', 'System Temperature(C)',
                            'Unit_Version', 'Unit_Build_Date', 'Unit_Build_Githash', 'Unit_Model_Number', 'Unit_HW_Revision', 'Unit_Serial_Number']

DEFAULT_PRF = 75
DEFAULT_VCSEL_PRF = 60

CH_NUM = [[(ch * NUM_LCP + lcp % NUM_LCP) for ch in range(NUM_CHANNELS_PER_LCP)] for lcp in range(NUM_LCP)]

CH_NUM_PER_LCP = {ch:ch//NUM_LCP for ch in CH_NUM[0]}

LCP_NUM_PER_CH = {ch:ch%NUM_LCP for ch in CH_NUM[0]}

LCP_DICT = {'Top':{'Top':0,'Bottom':1},'Bottom':{'Top':2,'Bottom':3}}

REVERSE_LCP_DICT = {ch:('Top', 'Top') for ch in CH_NUM[0]}

CH_NUM_TROSA = [[ch for ch in range(NUM_CH_TROSA)] for _ in range(NUM_TROSA)]
TROSA_NUM_PER_CH = {ch:ch//NUM_CH_TROSA for ch in CH_NUM[0]}

MAX_POWER_LEVEL = 16
POWER_LEVEL_NUM = [i for i in range(0, MAX_POWER_LEVEL)]

BOOT_MAX = 8.0
BOOT_MIN = 4.0
BIAS_MAX = 5.0
BIAS_MIN = 1.5

# Max DAC value - 8 V or 3FFF
MAX_DAC_DECIMAL = 16383
MAX_DAC_VOLT = 8

LP_BITS = [15, 14, 13, 11, 12, 9, 8, 4, 0]
# LP_BITS = [15, 14, 13, 11, 12, 10, 9, 6, 5, 3, 4, 2, 1, 0]

START_BOOT = 5.2
START_BIAS = 2.7

SCRATCHPAD_PL = 10

## Vcsel configurations
VCSEL_RECHARGE_MIN = 0
VCSEL_RECHARGE_MAX = 3.198

VCSEL_RECHARGE_CONVERISON = 5.33

PACKET_FORMAT = "rpf"
DEV_IPADDR = "0.0.0.0"
DEV_PORT = 2368

HV_SETPOINT = ['150', '150']

SDK_SENSOR_NAME = 'velarray_v16sr'
SENSOR_IP_ADDR = '192.168.1.100'
SENSOR_PORT = '0x1080'