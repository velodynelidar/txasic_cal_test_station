#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Software for velarray representation and communication

import importlib
import numpy as np
import ast
from txasic_cal.communication import mcm_sdk_client
import time

from txasic_cal.tools.logger import logger
from txasic_cal.tools.packet_reader import packet_reader
from txasic_cal.pfmea_code import pfmea_table

class velarray(mcm_sdk_client.mcm_sdk_client):

    def __init__(self, config, sdk_scripts):
        """Top level Velarray class

        Arguments:
            config {module} -- [sensor config module to be used]
            sdk_scripts {module} -- [sensor sdk scripts to be used]
        """
        super(velarray, self).__init__(velarray.__name__)
        self.config = config
        self.sdk_scripts = sdk_scripts

        self.cur_ch = 0
        self.bias = 0
        self.boot = 0
        self.boot_in_dec = 0
        self.bias_in_dec = 0
        self.lp = self.config.LP_BITS[0]
        self.cur_prf = None

        self.vcsel_recharge_ctr = 0
        self.vcsel_recharge_ctr_in_dec = 0
        self.cur_vcsel_prf = None

        self._unit_version = None
        self._unit_build_date = None
        self._unit_build_githash = None
        self._unit_model_number = None
        self._unit_hw_revision = None
        self._unit_serial_number = None

        self.start_boot = config.START_BOOT
        self.start_bias = config.START_BIAS

        self._v12 = None
        self._v5 = None
        self._v3_3 = None
        self._vlas = None
        self._hv = None

        self.nfk_bias = [0 for _ in range(self.config.NUM_CHANNELS)]
        self.nfk_width = [0 for _ in range(self.config.NUM_CHANNELS)]

    def init(self, sensor):
        sdk_version = self.connect(sensor)

        # Unit information
        self._unit_version = ".".join(str(n) for n in self.client.subcomponents['platform_regs'].get_image_version())
        self._unit_build_date = self.client.subcomponents['platform_regs'].get_build_date_time()
        self._unit_build_githash = self.client.subcomponents['platform_regs'].get_build_git_hash()
        self._unit_model_number = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_model_number())
        self._unit_hw_revision = "{:02X}".format(self.client.subcomponents['platform_regs'].get_hw_revision())
        hi, lo = self.client.subcomponents['platform_regs'].get_serial_number()
        self._unit_serial_number = "{:08X}{:08X}".format(hi, lo)

        # Voltage rails
        self.get_12v()
        self.get_5v()
        self.get_3_3v()
        self.get_vlas()
        self.get_hv()

        return sdk_version

    def close(self):
        """Close connection to unit and ensure all channels are disabled
        """
        if self._is_connected:
            self.disable_all_channel()
            self.enable_vcsel(0)
            self.disconnect()

    def ch_init(self, ch_num):
        """Initialize channel

        Arguments:
            ch_num {int} -- [Channel number]
        """
        self.cur_ch = ch_num
        self.bias = self.start_bias
        self.boot = self.start_boot
        self.boot_in_dec = self.convert_from_volt('boot', self.boot)
        self.bias_in_dec = self.convert_from_volt('bias', self.bias)
        self.lp = self.config.LP_BITS[1]

        self.set_bias(self.bias)
        self.set_boot(self.boot)
        self.set_lp(self.lp)
        self.enable_channel(ch_num)

    def ch_deinit(self):
        self.disable_channel(self.cur_ch)

    # override super execute
    def execute(self, cmd):
        return super().execute(cmd)

    def convert_from_dec(self, cur_input, cur_value):
        """Convert given input from decimal value to voltage

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Decimal value

        Returns:
            [float] -- Converted decimal value to float
        """
        new_value = round(cur_value * self.config.MAX_DAC_VOLT / self.config.MAX_DAC_DECIMAL, 3)

        return new_value

    def convert_from_volt(self, cur_input, cur_value):
        """Convert given input from voltage to decimal value

        Arguments:
            cur_input [str] -- Bias, Boot, i.e., different inputs
            cur_value [int] -- Voltage value

        Returns:
            [float] -- Converted float to decimal value
        """
        new_value = int((cur_value * self.config.MAX_DAC_DECIMAL) / self.config.MAX_DAC_VOLT)

        return new_value

    def enable_channel(self, ch_num=None, power_level=None):
        """Enable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            power_level {int}-- Optional argument, if not provided, 
            scratchpad power level is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(power_level == None):
            power_level = self.config.SCRATCHPAD_PL

        logger.debug("Enabling channel {} power level {}".format(ch_num, power_level))

        self.client.subcomponents['pwr_ctrl'].set_power(self.config.LCP_NUM_PER_CH[ch_num], self.config.CH_NUM_PER_LCP[ch_num],
                                                        power_level, 'on')

    def disable_channel(self, ch_num=None, power_level=None):
        """Disable channel

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            power_level {int}-- Optional argument, if not provided, 
            scratchpad power level is used (default: {None})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(power_level == None):
            power_level = self.config.SCRATCHPAD_PL

        logger.debug("Disabling channel {}".format(ch_num))

        self.client.subcomponents['pwr_ctrl'].set_power(self.config.LCP_NUM_PER_CH[ch_num], self.config.CH_NUM_PER_LCP[ch_num],
                                                        power_level, 'off')

    def set_boot(self, boot, ch_num=None, pl=None, set_all=True):
        """Set boot for given channel

        Arguments:
            boot {float} -- Boot value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if boot < self.config.BOOT_MIN:
            logger.error("Boot = {} lower than {}. Setting to {}".format(boot, self.config.BOOT_MIN, self.config.BOOT_MIN))
            boot = self.config.BOOT_MIN
        elif boot > self.config.BOOT_MAX:
            logger.error("Boot = {} greater than {}. Setting to {}".format(boot, self.config.BOOT_MAX, self.config.BOOT_MAX))
            boot = self.config.BOOT_MAX
        
        self.boot = boot
        self.boot_in_dec = self.convert_from_volt('boot', boot)

        logger.debug("Set channel {} boot as {} in dec {}".format(ch_num, boot, self.boot_in_dec))

        if not set_all:
            self.client.subcomponents['pwr_ctrl'].set_boot(self.boot_in_dec, self.config.LCP_NUM_PER_CH[ch_num],\
                 self.config.CH_NUM_PER_LCP[ch_num], pl)
        else:
            self.client.subcomponents['pwr_ctrl'].set_boot_all(self.boot_in_dec, pl)

        time.sleep(0.1)

    def set_bias(self, bias, ch_num=None, pl=None, set_all=True):
        """Set bias for given channel

        Arguments:
            bias {float} -- Bias value in voltage

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if bias < self.config.BIAS_MIN:
            logger.error("Bias = {} lower than {}. Setting to {}".format(bias, self.config.BIAS_MIN, self.config.BIAS_MIN))
            bias = self.config.BIAS_MIN
        elif bias > self.config.BIAS_MAX:
            logger.error("Bias = {} greater than {}. Setting to {}".format(bias, self.config.BIAS_MAX, self.config.BIAS_MAX))
            bias = self.config.BIAS_MAX
        
        self.bias = bias
        self.bias_in_dec = self.convert_from_volt('bias', bias)

        logger.debug("Set channel {} bias as {} in dec {}".format(ch_num, bias, self.bias_in_dec))

        if not set_all:
            self.client.subcomponents['pwr_ctrl'].set_bias(self.bias_in_dec, self.config.LCP_NUM_PER_CH[ch_num],\
                 self.config.CH_NUM_PER_LCP[ch_num], pl)
        else:
            self.client.subcomponents['pwr_ctrl'].set_bias_all(self.bias_in_dec, pl)

        time.sleep(0.1)

    def set_lp(self, lp, ch_num=None, pl=None, set_all=True):
        """Set lp for given channel

        Arguments:
            lp {int} -- LP bits used by ASIC OPT11

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            pl {int} -- Optional argument, if not provided, 
            scratchpad_pl is used (default: {None})
            set_all {bool} -- Optional argument, if not provided, 
            value gets written to all channels (default: {True})
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL
        if lp not in self.config.LP_BITS:
            logger.error("LP bit {} not found. Exiting.".format(lp))

        self.lp = lp

        logger.debug("Set channel {} lp as {}".format(ch_num, lp))
        
        if not set_all:
            self.client.subcomponents['pwr_ctrl'].set_lp(lp, self.config.LCP_NUM_PER_CH[ch_num],\
                 self.config.CH_NUM_PER_LCP[ch_num], pl)
        else:
            self.client.subcomponents['pwr_ctrl'].set_lp_all(lp, pl)

        time.sleep(0.1)

    def enable_all_channel(self):
        """Enable all channels
        """
        logger.debug("Enable all channel.")

        self.client.subcomponents['pwr_ctrl'].set_all_channel_trigger_state(True)

    def disable_all_channel(self):
        """Disable all channels
        """
        logger.debug("Disable all channel.")

        self.client.subcomponents['pwr_ctrl'].set_all_channel_trigger_state(False)

    def set_power_all(self, pl=10):
        """Set power to all channels

        Keyword Arguments:
            pl {int} -- Power level to write boot, bias, lp values to (default: {10})
        """
        if pl not in self.config.POWER_LEVEL_NUM:
            logger.error("Please enter correct power level.")
            return

        self.client.subcomponents['pwr_ctrl'].set_all_channel_pwrlvl(pl)

    def get_temperature(self, ch_num=None):
        """Get temperature from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})

        Returns:
            [float] -- [Temperature of unit in C]
        """
        if(ch_num == None):
            ch_num = self.cur_ch

        logger.debug("Find temperature for LCP {}".format(self.config.TROSA_NUM_PER_CH[ch_num]))

        temperature = self.client.subcomponents['monitor'].get_trosa_temperature(self.config.TROSA_NUM_PER_CH[ch_num])

        return temperature

    def get_adc_cap(self, ch_num=None, azm=6000):
        """Get ADC capture from unit

        Keyword Arguments:
            ch_num {int} -- Optional argument, if not provided, 
            stored cur_ch is used (default: {None})
            azm {int} -- azimuth to capture adc cap, default - 6000

        Returns:
            [int array] -- [Integer array of adc capture]
        """
        if(ch_num == None):
            ch_num = self.cur_ch

        logger.debug("Get ADC Cap for ch {} at azm {}".format(ch_num, azm))

        # Set azimuth and azimuth direction to capture trigger
        self.client.subcomponents['adc_capture']._azm_trigger(azm, 0)

        adc_cap = self.client.subcomponents['adc_capture'].get_data(ch_num)

        return adc_cap

    def reg_read(self, addr):
        """Read register from unit

        Arguments:
            addr {hex} -- Register address

        Returns:
            [int] -- [Register value]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_read.__name__, True).format(addr)

        logger.debug("Reg read addr {}".format(addr))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg readout to {} failed. Setting reg_read value to None".
                         format(addr))
            reg_read = None
        else:
            reg_read = ast.literal_eval(r.split(" ")[2][:-2])
            logger.debug("Reg read data is {}".format(reg_read))

        return reg_read

    def reg_write(self, addr, data):
        """Write to register

        Arguments:
            addr {hex} -- Register address to write to
            data {hex} -- data

        Returns:
            [str] -- [Error message if failed]
        """
        cmd = self.sdk_scripts.return_dict_cl(self.reg_write.__name__, True).format(addr, data)
        # cmd = "/usr/local/bin/velodyne/host/reg_access -w -a {} -d {} -A 192.168.1.100 -P 4420".format(addr,data)

        logger.debug("Reg write addr {} with data {}".format(addr, data))

        r = self.execute(cmd)
        if r is None or r == "":
            logger.warn("Reg write to {} with data {} failed. Setting reg_write value to None".\
                format(addr, data))
            reg_write = None
        else:
            reg_write = ast.literal_eval(r.split(" ")[2][:-2])

        return reg_write

    def reg_access_wrap(self, addr, offset, maxval, val):
        """Read register and then write at field position

        Arguments:
            addr {hex} -- Register address to write to
            offset {int} -- Field offset
            maxval {int} -- Max field value
            val {int} -- Value to write to field

        Returns:
            [str] -- [Error message if failed]
        """
        result = self.reg_read(addr)
        data = result
        new_data = (data & ~(maxval << offset)) | (val << offset)
        logger.debug('\taddr={} was {}, now {:x}'.format(addr, data, new_data))

        return self.reg_write(addr, new_data)

    def check_fault(self):
        """Check if faults are cleared.
        """
        fault = self.client.subcomponents['fault'].check_faults()

        return fault

    def clear_fault(self):
        """Clear faults and check if faults are cleared.
        """
        self.client.subcomponents['fault'].clear_faults()

        fault = self.check_fault()

        return fault

    def get_prf(self, maintenance='n'):
        """Get current PRF

        Keyword Arguments:
            maintenance {str} -- 'n' for no maintenance,
            'm' for with maintenance (default: {'n'})

        Returns:
            [float] -- [Current PRF]
        """
        prf = self.client.subcomponents['ptg_ctrl'].get_prf(maintenance)

        if prf != self.cur_prf and self.cur_prf != None:
            logger.error("PRF has changed from {} to {}".format(self.cur_prf, prf))
            logger.input("Please hit enter to continue.")

        self.cur_prf = prf

        return prf

    def get_vcsel_prf(self, maintenance='n'):
        """Get current PRF

        Keyword Arguments:
            maintenance {str} -- 'n' for no maintenance,
            'm' for with maintenance (default: {'n'})

        Returns:
            [float] -- [Current PRF]
        """
        prf = self.client.subcomponents['ptg_ctrl'].get_vcsel_prf(maintenance)

        if prf != self.cur_vcsel_prf and self.cur_vcsel_prf != None:
            logger.error("VCSEL PRF has changed from {} to {}".format(self.cur_vcsel_prf, prf))
            logger.input("Please hit enter to continue.")

        self.cur_vcsel_prf = prf

        return prf

    def enable_vcsel(self, enable):
        """Enable/disable vcsel

        Arguments:
            enable [int] : 0 - disable, 1 - enable
        """
        self.client.subcomponents['pwr_ctrl'].vcsel_enable(enable)

    def set_vcsel_recharge(self, recharge_ctr):
        """Set recharge cycles for vcsel

        Arguments:
            recharge_ctr {int} -- recharge controls in cycles
        """
        if recharge_ctr < self.config.VCSEL_RECHARGE_MIN or recharge_ctr > self.config.VCSEL_RECHARGE_MAX:
            logger.error("Exiting set_vcsel_recharge because recharge_ctr = {}".format(recharge_ctr))
            return

        self.vcsel_recharge_ctr = recharge_ctr
        self.vcsel_recharge_ctr_in_dec = int((self.vcsel_recharge_ctr * 1000) // self.config.VCSEL_RECHARGE_CONVERISON)

        logger.debug("Set vcsel recharge_ctr as {} us in dec {}".format(self.vcsel_recharge_ctr, self.vcsel_recharge_ctr_in_dec))

        self.client.subcomponents['pwr_ctrl'].set_vcsel_override_recharge_clk_cycles(self.vcsel_recharge_ctr_in_dec)
        time.sleep(0.1)

    def set_opamp_gain(self, ch_num, pl, gain):
        """Set OPM gain

        Arguments:
            gain [int] : 1 - low, 0 - high
        """
        if(ch_num == None):
            ch_num = self.cur_ch
        if(pl == None):
            pl = self.config.SCRATCHPAD_PL

        self.client.subcomponents['opm'].set_opm_gain(self.config.LCP_NUM_PER_CH[ch_num], self.config.CH_NUM_PER_LCP[ch_num], pl, gain)

    def get_opm_adc(self, num_iter):
        """Get OPM ADC readings for a given number of iterations

        Arguments:
            num_iter [int] : Number of iterations
        """
        opm_adc = self.client.subcomponents['opm'].get_opm_adc(int(num_iter))

        return opm_adc

    def set_opm_mode(self, mode):
        """Set OPM mode

        Arguments:
            mode [int] : 0 - Idle, 1 - Temperature Mode, 2 - OPM Mode
        """
        self.client.subcomponents['opm'].set_opm_mode(int(mode))
    
    def get_12v(self):
        """Get 12V

        Return:
            [float] : Float value of 12V
        """
        # Check 12V fault
        if self.api_field_read("get_field_powersupply_fault_12v0_fault"):
            logger.error('12V fault.')
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG12V')
            logger.input("Check 12V. Hit enter to continue.")

        v12 = self.client.subcomponents['monitor'].get_powerboard_12v()

        v12 = round(v12, 3)
        if self._v12 is not None and round(v12) != round(self._v12):
            logger.error('12V rail changed from {} to {}'.format(self._v12, v12))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG12V')
            logger.input("Check 12V. Hit enter to continue.")

        self._v12 = v12
        logger.debug("12V is at {}".format(v12))

        return self._v12

    def get_3_3v(self):
        """Get 3.3V

        Return:
            [float] : Float value of 3.3V
        """
        # Check 3.3V fault
        if self.api_field_read("get_field_powersupply_fault_3v3_fault"):
            logger.error('3.3V fault.')
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG3-3V')
            logger.input("Check 3.3V. Hit enter to continue.")

        v3_3 = self.client.subcomponents['monitor'].get_powerboard_3_3v()
        v3_3 = round(v3_3, 3)

        logger.debug("3.3V is at {}".format(v3_3))

        if self._v3_3 is not None and round(v3_3) != round(self._v3_3):
            logger.error('3.3V rail changed from {} to {}'.format(self._v3_3, v3_3))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG3-3V')
            logger.input("Check 3.3V. Hit enter to continue.")

        self._v3_3 = v3_3

        return self._v3_3

    def get_vlas(self):
        """Get 21V (vlaser)

        Return:
            [float] : Float value of 21V
        """
        # Check 21V fault
        if self.api_field_read("get_field_powersupply_fault_21v0_fault"):
            logger.error('21V fault.')
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
            logger.input("Check 21V. Hit enter to continue.")

        vlas = self.client.subcomponents['monitor'].get_powerboard_21v()
        vlas = round(vlas, 3)

        logger.debug("21V is at {}".format(vlas))

        if self._vlas is not None and round(vlas) != round(self._vlas):
            logger.error('21V rail changed from {} to {}'.format(self._vlas, vlas))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
            logger.input("Check 21V. Hit enter to continue.")

        self._vlas = vlas

        return self._vlas

    def get_5v(self):
        """Get 5V

        Return:
            [float] : Float value of 5V
        """
        # 5V fault not available under sdk, TODO: Check with FW team
        v5 = self.client.subcomponents['monitor'].get_mainboard_5v()
        v5 = round(v5, 3)

        logger.debug("5V is at {}".format(v5))

        if self._v5 is not None and round(v5) != round(self._v5):
            logger.error('5V rail changed from {} to {}'.format(self._v5, v5))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG5V')
            logger.input("Check 5V. Hit enter to continue.")

        self._v5 = v5

        return self._v5

    def get_hv(self):
        """Get HV

        Return:
            [float] : Float value of HV
        """
        # Check HV fault
        if self.api_field_read("get_field_powersupply_fault_hv_fault"):
            logger.warn('HV fault.')
            pfmea_table.pfmea_filter.warn('UNIT_PRE_REGAPDHV')

        hv = self.client.subcomponents['monitor'].get_mainboard_hv()

        hv = round(hv, 3)

        self._hv = hv

        return self._hv

    def get_system_temperature(self):
        """Get system temperature in C
        """
        system_temp = self.client.subcomponents['monitor'].get_system_temperature()

        return round(system_temp, 3)

    def gen_pcap(self, pkt_cnt, csv_name):
        """Generate pcap using packet_reader

        Arguments:
            pkt_cnt {int} -- Number of packets to read
            csv_name {str} -- Csv file string name
        """
        pkt_reader = packet_reader(self.config.PACKET_FORMAT, csv_name)
        pkt_reader.read_from_device(self.config.DEV_IPADDR, self.config.DEV_PORT, pkt_cnt)

    def get_nfbias_snapshot(self):
        """Get current nfbias snapshot and store in csv file
        """
        for data in self.client.subcomponents['pwr_ctrl'].get_nfbias_snapshot():
            # Position 1 is channel, Position 3 is bias data
            ch = int(data[1])
            bias = int(data[3])

            self.nfk_bias[ch] = bias

    def get_nfbias_width(self):
        """Get current nfwidth and store in csv file
        """
        for data in self.client.subcomponents['pwr_ctrl'].get_nfbias_widths():
            # Position 1 is channel, Position 2 is width data
            ch = int(data[1])
            width = float(data[2])

            self.nfk_width[ch] = width

    def set_vertical_mirror_position(self, position):
        """Set vertical scan mirror position

        Arguments:
            position [float] -- Float position of vertical scan mirror
        """
        self.client.subcomponents['he_scan_ctrl'].cal_mode(position)

    def set_nfbias_enable(self, enable):
        """Set nfbias enable

        Arguments:
            enable [int] -- 1/0 to enable or disable
        """
        self.client.subcomponents['pwr_ctrl'].set_nfbias_en(enable)

    def init_tx_cal(self):
        """Init tx cal mode
        """
        logger.debug("Init TxCal mode.")
        self.client.subcomponents['calmode_ctrl'].initialize_tx_cal()

    def scan_fault_allow(self):
        """Scan fault allow (Eye-unsafe)
        """
        logger.debug("Allow scan fault allow.")
        self.client.subcomponents['he_scan_ctrl'].set_scan_fault_allow_status(True)

    def api_field_write(self, field_name, data):
        """Set fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
            data (int): Field value
        """
        logger.debug("Writing to field {} with data {}".format(field_name, data))
        status = getattr(self.client.regs, field_name)(data)

        if status != 0:
            logger.error("API field write status {}".format(status))
            return -1

        return 0

    def api_field_read(self, field_name):
        """Read fpga field

        Args:
            field_name (str): Field name as described in py_libreg.py under mcm-sdk
        """
        logger.debug("Reading from field {}".format(field_name))
        data, status = getattr(self.client.regs, field_name)()

        if status != 0:
            logger.error("API field read status {}".format(status))
            return -1

        return data

    def populate_cur_row(self, version, results_row=None, display=True):
        """Populate current results row

        Keyword Arguments:
            results_row {dict} -- Dict of results row (default: {None})
            display {bool} -- True/False to dispaly results (default: {True})
        """
        if results_row is None:
            logger.error("Please provide results_row")
            return

        if version[0:2] == "VC":
            results_row['Channel'] = self.cur_ch
            results_row['Board'] = self.config.REVERSE_LCP_DICT[self.cur_ch][0]
            results_row['TROSA'] = self.config.REVERSE_LCP_DICT[self.cur_ch][1]
            results_row['Recharge clock cycles in hex'] = hex(self.vcsel_recharge_ctr_in_dec)
            results_row['Recharge Time (in microseconds)'] = self.vcsel_recharge_ctr
        else:
            results_row['Channel'] = self.cur_ch
            results_row['Board'] = self.config.REVERSE_LCP_DICT[self.cur_ch][0]
            results_row['TROSA'] = self.config.REVERSE_LCP_DICT[self.cur_ch][1]
            results_row['LP'] = hex(self.lp)
            results_row['Boot'] = self.boot
            results_row['Boot in Hex'] = hex(self.boot_in_dec)
            results_row['Bias'] = self.bias
            results_row['Bias in Hex'] = hex(self.bias_in_dec)

        results_row['Temperature'] = self.get_temperature()

        # if 'ADC_Capture' in results_row.keys():
        #     try:
        #         results_row['ADC_Capture'] = self.get_adc_cap()
        #     except ValueError:
        #         results_row['ADC_Capture'] = None

            # from txasic_cal.tools.post_processing import pulse_processing
            # adc_pulse_info = pulse_processing.get_pulse_info(results_row['ADCCAP'])

            # results_row['Dazzle Base Width(ns)'] = np.squeeze(adc_pulse_info._pulse_fwidth)
            # results_row['Dazzle Half Width(ns)'] = np.squeeze(adc_pulse_info._pulse_hwidth)
            # results_row['Dazzle Sat Width(ns)'] = np.squeeze(adc_pulse_info._pulse_satwidth)
            # results_row['Dazzle Amplitude'] = np.squeeze(adc_pulse_info._pulse_amp)
            # results_row['Dazzle Rise Time(ns)'] = np.squeeze(adc_pulse_info._pulse_rise)

        results_row['Unit_Version'] = self._unit_version
        results_row['Unit_Build_Date'] = self._unit_build_date
        results_row['Unit_Build_Githash'] = self._unit_build_githash
        results_row['Unit_Model_Number'] = self._unit_model_number
        results_row['Unit_HW_Revision'] = self._unit_hw_revision
        results_row['Unit_Serial_Number'] = self._unit_serial_number

        # OPM
        # self.set_opm_mode(2)
        # num_iter = 100
        # # Low gain
        # self.set_opamp_low_gain()
        # opm_adc = self.get_opm_adc(num_iter)

        # results_row['OPM_Low_Gain_ADC_Mean'] = statistics.mean(opm_adc)
        # results_row['OPM_Low_Gain_ADC_Std'] = statistics.stdev(opm_adc)
        # results_row['OPM_Low_Gain_ADC_Max'] = max(opm_adc)
        # results_row['OPM_Low_Gain_ADC_Min'] = min(opm_adc)
        # results_row['OPM_Low_Gain_ADC'] = opm_adc

        # logger.debug('OPM_Low_Gain_ADC_Mean: {}'.format(results_row['OPM_Low_Gain_ADC_Mean']))

        # # High gain
        # self.set_opamp_high_gain()

        # opm_adc = self.get_opm_adc(num_iter)

        # results_row['OPM_High_Gain_ADC_Mean'] = statistics.mean(opm_adc)
        # results_row['OPM_High_Gain_ADC_Std'] = statistics.stdev(opm_adc)
        # results_row['OPM_High_Gain_ADC_Max'] = max(opm_adc)
        # results_row['OPM_High_Gain_ADC_Min'] = min(opm_adc)
        # results_row['OPM_High_Gain_ADC'] = opm_adc

        # logger.debug('OPM_High_Gain_ADC_Mean: {}'.format(results_row['OPM_High_Gain_ADC_Mean']))

        # # reset mode
        # self.set_opm_mode(1)

        # Voltages
        results_row['12V'] = self.get_12v()
        results_row['3.3V'] = self.get_3_3v()
        results_row['5V'] = self.get_5v()
        results_row['21V'] = self.get_vlas()
        results_row['System Temperature(C)'] = self.get_system_temperature()
        results_row['HV'] = self.get_hv()

    def set_intensity(self):
        """ Selects between different options available for intensity field in sensor packet output.

        Arguments:
                option : int
                    0 : Reflectivity for regular returns, Raw Intensity for VCSEL returns.
                    1 : Mu
                    2 : Sigma
                    3 : DC Mu
                    4 : Transmitted TX power(seen on packet output as actual tx power multiplied by 16.)
                    5 : Base
                    6 : AC raw sample
                    7 : Dat is Sat count (Refer to RTL for understanding these fields further)
                    8 : Raw intensity.
        """
        self.client.subcomponents['det_ctrl'].set_diag_mux_sel_max1_intensity_sel(8)

class V32(velarray):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.velarray.v32.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.velarray.v32.sdk_scripts")

        self.channels= [
                        0,  # Top, Top, ch 0
                        4,  # Top, Top, ch 1
                        8,  # Top, Top, ch 2
                        12, # Top, Top, ch 3
                        16, # Top, Top, ch 4
                        20, # Top, Top, ch 5
                        24, # Top, Top, ch 6
                        28, # Top, Top, ch 7
                        1,  # Top, Bottom, ch 0
                        5,  # Top, Bottom, ch 1
                        9,  # Top, Bottom, ch 2
                        13, # Top, Bottom, ch 3
                        17, # Top, Bottom, ch 4
                        21, # Top, Bottom, ch 5
                        25, # Top, Bottom, ch 6
                        29, # Top, Bottom, ch 7
                        2,  # Bottom, Top, ch 0
                        6,  # Bottom, Top, ch 1
                        10, # Bottom, Top, ch 2
                        14, # Bottom, Top, ch 3
                        18, # Bottom, Top, ch 4
                        22, # Bottom, Top, ch 5
                        26, # Bottom, Top, ch 6
                        30, # Bottom, Top, ch 7
                        3,  # Bottom, Bottom, ch 0
                        7,  # Bottom, Bottom, ch 1
                        11, # Bottom, Bottom, ch 2
                        15, # Bottom, Bottom, ch 3
                        19, # Bottom, Bottom, ch 4
                        23, # Bottom, Bottom, ch 5
                        27, # Bottom, Bottom, ch 6
                        31  # Bottom, Bottom, ch 7
                        ]

        super(V32, self).__init__(self.config, self.sdk_scripts)

    def init(self):
        # Write the initialization registers here
        super(V32, self).init()

    def __str__(self):
        return 'V32'

        
class V8(velarray):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.velarray.v8.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.velarray.v8.sdk_scripts")

        self.channels= [
                        0, # Top, Top, ch 0
                        1, # Top, Top, ch 1
                        2, # Top, Top, ch 2
                        3, # Top, Top, ch 3
                        4, # Top, Top, ch 4
                        5, # Top, Top, ch 5
                        6, # Top, Top, ch 6
                        7, # Top, Top, ch 7
                        ]

        super(V8, self).__init__(self.config, self.sdk_scripts)

    def init(self):
        # Write the initialization registers here
        sdk_version = super(V8, self).init(sensor=self)

        self.hv_setpoint = [self.api_field_read('get_field_apd_bias_0_setpoint_hv')]

        return sdk_version
    
    def __str__(self):
        return 'V8'

    def populate_cur_row(self, version, results_row=None, display=True):
        if results_row is None:
            logger.error("Please provide results_row")
            return

        super().populate_cur_row(version, results_row, display)

        # HV Setpoints
        results_row['HV_Setpoint_0'] = self.hv_setpoint[0]


class V16(velarray):
    def __init__(self):
        self.config = importlib.import_module("txasic_cal.sensor.velarray.v16.config")
        self.sdk_scripts = importlib.import_module("txasic_cal.sensor.velarray.v16.sdk_scripts")

        self.channels= [
                        0, # Top, Top, ch 0
                        1, # Top, Top, ch 1
                        2, # Top, Top, ch 2
                        3, # Top, Top, ch 3
                        4, # Top, Top, ch 4
                        5, # Top, Top, ch 5
                        6, # Top, Top, ch 6
                        7, # Top, Top, ch 7
                        8, # Top, Top, ch 8
                        9, # Top, Top, ch 9
                        10, # Top, Top, ch 10
                        11, # Top, Top, ch 11
                        12, # Top, Top, ch 12
                        13, # Top, Top, ch 13
                        14, # Top, Top, ch 14
                        15, # Top, Top, ch 15
                        ]

        super(V16, self).__init__(self.config, self.sdk_scripts)

    def init(self):
        # Write the initialization registers here
        sdk_version = super(V16, self).init(sensor=self)

        self.hv_setpoint = [self.api_field_read('get_field_mla_apd_bias_0_setpoint_hv'),
                            self.api_field_read('get_field_mla_apd_bias_1_setpoint_hv')]

        return sdk_version

    def __str__(self):
        return 'V16'

    def populate_cur_row(self, version, results_row=None, display=True):
        if results_row is None:
            logger.error("Please provide results_row")
            return

        super().populate_cur_row(version, results_row, display)

        # HV Setpoints
        results_row['HV_Setpoint_0'] = self.hv_setpoint[0]
        results_row['HV_Setpoint_1'] = self.hv_setpoint[1]


if __name__ == "__main__":
    velarray = V16()

