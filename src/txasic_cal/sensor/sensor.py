#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Software for creating sensor representation for cal

import importlib

from txasic_cal.tools.logger import logger

class sensor:
    def __init__(self, sensor_module, version):
        """Top level sensor class used to import sensor modules

        Arguments:
            sensor_module {str} -- [Sensor module within this folder]
        """
        self.sensor_name, self.sensor_type = sensor_module.split('.')
        self.sensor_import_folder = importlib.import_module("txasic_cal.sensor." + self.sensor_name)
        self.sensor_module = getattr(self.sensor_import_folder, self.sensor_type)()
        if version[0:2] == "VC":
            self.results_header = self.sensor_module.config.VCSEL_POWER_LEVEL_TABLE_HEADER
        else:
            self.results_header = self.sensor_module.config.TX_POWER_LEVEL_TABLE_HEADER
        self.current_measurements = {i: None for i in self.results_header}

        self.config = self.sensor_module.config

    def __enter__(self):
        return self

    def __exit__(self, *exargs):
        # Call close on exit
        self.sensor_module.close()

    def init(self):
        return self.sensor_module.init()

    def set_input(self, *args, **kwargs):
        func_routine = getattr(self.sensor_module, kwargs['result'])
        return func_routine(*args)

    def measure(self, *args, **kwargs):
        return self.set_input(*args, **kwargs)

    def populate_cur_row(self, version, results_row, display=True):
        """Populate current row with sensor inputs and measurements

        Arguments:
            results_row {dict} -- [Dict of results]

        Keyword Arguments:
            display {bool} -- [True/False to display results to stdout] (default: {True})
        """
        logger.debug("********Sensor Results Start********")
        self.sensor_module.populate_cur_row(version, self.current_measurements, display)
        # Power Level, PRF will be updated by calibration
        results_row.update({i: self.current_measurements[i] for i in self.results_header
                            if i not in ["Power Level", "PRF", "ADC_Capture"]})

        if display:
            for key, value in enumerate(self.current_measurements):
                logger.info("{}={}".format(value, self.current_measurements[value]))

        logger.debug("********Sensor Results End********")
