#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Client software for communicating with MCM-SDK

import subprocess
import time
import re

from txasic_cal.communication import config
from txasic_cal.tools.logger import logger

class mcm_sdk_client:
    def __init__(self, platform):
        """Client class to interface with mcm-sdk

        Args:
            platform (str): Platform to import (velarray/velabit etc.)
        """
        self._type = config.COMM_TYPE
        self._is_connected = False
        if self._type == 'ssh':
            from txasic_cal.communication.ssh import ssh_linux
            self.client = ssh_linux.ssh_client()
        elif self._type == 'api':
            self.client = None
            if platform == 'velarray':
                try:
                    from velaplatform.velarray import velarray
                    self.client_platform = velarray
                except ModuleNotFoundError:
                    logger.warning(f"Module not found for {platform}")
                    pass
            elif platform == 'velabit':
                try:
                    from velaplatform.velabit import velabit
                    self.client_platform = velabit
                except ModuleNotFoundError:
                    logger.warning(f"Module not found for {platform}")
                    pass
            elif platform == 'vlx':
                try:
                    from velaplatform.vlx import vlx
                    self.client_platform = vlx
                except ModuleNotFoundError:
                    pass
            elif platform == 'apa':
                try:
                    from velaplatform.apa import apa
                    self.client_platform = apa
                except ModuleNotFoundError:
                    pass

    def connect(self, sensor_module):
        """Connect to sensor

        Args:
            sensor_module (sensor module): Sensor module object

        Returns:
            str: mcm-sdk version
        """
        self._is_connected = True
        if self._type == 'ssh':
            self.client.connect(config.IP_ADDRESS, config.USERNAME, config.PASSWORD, config.PORT)
        elif self._type == 'api':
            # TODO: RPC connect here
            # max(50-((3))*10,10)
            self.client = getattr(self.client_platform, sensor_module.config.SDK_SENSOR_NAME)\
                (logger=logger, log_level=0, ip_addr=sensor_module.config.SENSOR_IP_ADDR, port=sensor_module.config.SENSOR_PORT)
            from velaplatform import build_info
            return build_info.get_build_info('VERSION')

    def disconnect(self):
        """Disconnect from sensor and set client to None
        """
        self._is_connected = False
        self.client = None

    def execute(self, cmd):
        result = None
        if self._type == 'ssh':
            result = self.client.execute(cmd)
        elif self._type == 'cli' or self._type == 'api':
            resp = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            reasc = re.compile(r'\x1b[^m]*m')
            result = resp.stderr
            try:
                if resp.stdout.decode('utf-8').split('\n')[0]:
                    result = reasc.sub('', resp.stdout.decode('utf-8').split('\n')[0])
                else:
                    result = reasc.sub('', resp.stderr.decode('utf-8').split('\n')[-2])
            except IndexError:
                logger.error("IndexError at mcm_sdk_client. Setting result to \"\"")
                result = ""

        # ignore sdk first print to stderr
        if 'velarray' in result:
            result = None
        time.sleep(config.SLEEP_TIME)

        return result
