import sys

from txasic_cal.host import host

## Communication type
# Updated on cal start based on host
COMM_TYPE = host.LAB_HOST_DETAILS[host.HOST_NAME]["communication"]["COMM_TYPE"]

## SSH
# Updated on cal start based on host
IP_ADDRESS = host.LAB_HOST_DETAILS[host.HOST_NAME]["communication"].get("IP_ADDRESS")
USERNAME = host.LAB_HOST_DETAILS[host.HOST_NAME]["communication"].get("USERNAME")
PASSWORD = host.LAB_HOST_DETAILS[host.HOST_NAME]["communication"].get("PASSWORD")
PORT = 22

SLEEP_TIME = 0.1

CUSTOM_SDK = False
CUSTOM_SDK_LOC = r'/home/velodyne/vijay/build_mcm/installed/share/velodyne'

if CUSTOM_SDK:
    try:
        pythonpath_index = sys.path.index(r'/usr/local/share/velodyne')
        sys.path[pythonpath_index] = CUSTOM_SDK_LOC
    except ValueError:
        sys.path.append(CUSTOM_SDK_LOC)
