from txasic_cal.host import host

# Updated on cal start based on host
SCOPE = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("SCOPE")
POWERMETER = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("POWERMETER")
OVEN = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("OVEN")
TEMPLATE = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("TEMPLATE")
POWER_SUPPLY = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("POWER_SUPPLY")

# KDC Name
Y_sn = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("Y_sn")
X_sn = host.LAB_HOST_DETAILS[host.HOST_NAME]["instruments"].get("X_sn")
