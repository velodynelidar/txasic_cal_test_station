# $ID: Multimeter_2831E.py 2018-15-02 Nishitha $
# This drive is for BK Precision Digital Multimeter
# Added Keithley 2002 Multimeter 2018-21-03 Nishitha $
# Model Number:  BK PRECISION 2831E Digital Multimeter

import pyvisa
import time

MULTIMETER_PRECISION = -4

class Keithley_2002(object):

    def __init__(self, USB):

        self.rm = pyvisa.ResourceManager()
        self.instance = self.rm.open_resource(str(USB))
        #self.timeout = 250000

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def GetFunc(self):
        '''
        # It Returns the currently selected function
        '''
        return self.instance.query(":FUNC?")

    def Reset(self):
        '''
        # It resets the instrument.
        '''
        return self.instance.write("*RST")

    def Fetch_reading(self):
        '''
        # This query command is used to request the latest post-processed reading.
        '''
        return self.instance.query(":FETCH?")

    '******************AC Voltage Measurements****************'
    def Meas_ACVolt(self):
        '''
        # This command is used to configure 'AC Voltage' measurement function and read the values.
        '''
        cmd = "MEAS:VOLT:AC?"
        self.instance.write(cmd)
        time.sleep(5)
        result = self.instance.read()
        # To get rid of unwanted string from the result
        result = result.split(",")[0]
        result = result[0:MULTIMETER_PRECISION]
        return result
        #return self.instance.query(cmd)

    def ACVolt_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:NPLC %f" %rate
        self.instance.write(cmd)

    def GetACVolt_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC 'VOLT:AC';:VOLT:AC:NPLC?")

    def ACVolt_SetAutoNPLC(self,en):
        '''
        # It sets the AUTO NPLC cycles selection
        :param en: ON | OFF
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:NPLC:AUTO %s" %en
        self.instance.write(cmd)

    def ACVolt_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for AC Voltage. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def ACVolt_SetRange(self,range):
        '''
        # Set the measurement range upper limit and lower limit for AC Voltage
        :param range: 0 to 775 Expected reading is AC volts (ACV)
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:RANGE %f"%range
        self.instance.write(cmd)

    def GetACVoltRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'VOLT:AC';:VOLT:AC:RANGE?")

    def ACVolt_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for ACV and enables it. (Reading = Input signal - Reference)
        :param ref: -1100 to 1100 Reference for ACV
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:REF %f;:VOLT:AC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetACVoltRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'VOLT:AC';:VOLT:AC:REF?")

    def SetACVolt_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:DIG %d" %n
        self.instance.write(cmd)

    def Set_ACVolt_Detector(self,type):
        '''
        # It sets the path to select AC type measurements listed below:
        :param type: RMS | AVERage | PEAK | LFRM | NPEak | PPEak
        '''
        cmd = "FUNC 'VOLT:AC';:VOLT:AC:DET %s"%type
        self.instance.write(cmd)

    '***************DC Voltage Measurements***********'
    def Meas_DCVolt(self):
        '''
        # This command is used to configure 'DC Voltage' measurement function and read the values.
        '''
        cmd = "MEAS:VOLT:DC?"
        result = self.instance.query(cmd)
        # To get rid of unwanted string from the result
        result = result.split(",")[0]
        result = result[0:MULTIMETER_PRECISION]
        return result

    def DCVolt_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:NPLC %f" %rate
        self.instance.write(cmd)

    def DCVolt_SetAutoNPLC(self,en):
        '''
        # It sets the AUTO NPLC cycles selection
        :param en: ON | OFF
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:NPLC:AUTO %s" %en
        self.instance.write(cmd)

    def GetDCVolt_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC 'VOLT:DC';:VOLT:DC:NPLC?")

    def DCVolt_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for DC Voltage. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def DCVolt_SetRange(self,range):
        '''
        # Set the measurement range for DC Voltage
        :param range: 0 to 1100 Expected reading is DC volts (DCV)
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:RANGE %f"%range
        self.instance.write(cmd)

    def GetDCVoltRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'VOLT:DC';:VOLT:DC:RANGE?")

    def DCVolt_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCV and enables it. (Reading = Input signal - Reference)
        :param ref: -1100 to +1100 Reference for DCV
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:REF %f;:VOLT:DC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetDCVoltRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'VOLT:DC';:VOLT:DC:REF?")

    def SetDCVolt_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:DIG %d" %n
        self.instance.write(cmd)

    '**********AC Current Measurements***********'
    def Meas_ACCurrent(self):
        '''
         # This command is used to configure 'AC Current' measurement function and read the values.
        '''
        cmd = "MEAS:CURR:AC?"
        self.instance.write(cmd)
        time.sleep(5)
        result = self.instance.read()
        # To get rid of unwanted string from the result
        result = result.split(",")[0]
        result = result[0:MULTIMETER_PRECISION]
        return result
        #return self.instance.query(cmd)

    def ACCurr_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:NPLC %f" %rate
        self.instance.write(cmd)

    def GetACCurr_NPLCycles(self):
        '''
         # It Queries the programmed power line cycle integration rate
        '''
        return self.instance.query("FUNC 'CURR:AC';:CURR:AC:NPLC?")

    def ACCurr_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for AC Current. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def ACCurr_SetRange(self,range):
        '''
        # Set the measurement range for AC Current.
        :param range: 0 to +2.1 Expected reading is AC Current in Amperes (ACI)
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:RANGE %f"%range
        self.instance.write(cmd)

    def GetACCurrRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'CURR:AC';:CURR:AC:RANGE?")

    def ACCurr_SetRefValue(self,ref,refstatus):
        '''
         # It Specifies the reference for ACI and enables it. (Reading = Input signal - Reference)
        :param ref: -2.1 to +2.1 Reference for ACI
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:REF %f;:CURR:AC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetACCurrRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'CURR:AC';:CURR:AC:REF?")

    def SetACCurr_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:DIG %d" %n
        self.instance.write(cmd)

    def Set_ACCurr_Detector(self,type):
        '''
        # It sets the path to select AC type measurements listed below:
        :param type: RMS | AVERage
        '''
        cmd = "FUNC 'CURR:AC';:CURR:AC:DET %s"%type
        self.instance.write(cmd)

    '************DC Current Measuremets**********'
    def Meas_DCCurr(self):
        '''
        # This command is used to configure 'DC Current' measurement function and read the values.
        '''
        cmd = "MEAS:CURR:DC?"
        result = self.instance.query(cmd)
        # To get rid of unwanted string from the result
        result = result.split(",")[0]
        result = result[0:MULTIMETER_PRECISION]
        return result

    def DCCurr_SetNPLCycles(self,rate):
        '''
         # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:NPLC %f" %rate
        self.instance.write(cmd)

    def GetDCCurr_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC 'CURR:DC';:CURR:DC:NPLC?")

    def DCCurr_SetAutoRange(self,AutoEn):
        '''
         # It sets the Auto range for DC Current. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def DCCurr_SetRange(self,range):
        '''
        # Set the measurement range for DC Current
        :param range: 0 to +2.1 Expected reading is DC Current (DCI)
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:RANGE %f"%range
        self.instance.write(cmd)

    def GetDCCurrRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'CURR:DC';:CURR:DC:RANGE?")

    def DCCurr_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCI and enables it. (Reading = Input signal - Reference)
        :param ref: -2.1 to +2.1 Reference for DCI
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:REF %f;:CURR:DC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetDCCurrRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'CURR:DC';:CURR:DC:REF?")

    def SetDCCurr_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:DIG %d" %n
        self.instance.write(cmd)

    '*************Resistance Measurements******************'
    def Meas_Resistance(self):
        '''
        # This command is used to configure 'Resistance (Omega2)' measurement function and read the values.
        '''
        cmd = "MEAS:RES?"
        return self.instance.query(cmd)

    def Res_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'RES';:RES:NPLC %f" %rate
        self.instance.write(cmd)

    def GetRes_NPLCycles(self):
        '''
        # It Queries the programmed power line cycle integration rate
        '''
        return self.instance.query("FUNC 'RES';:RES:NPLC?")

    def Res_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for Resistance (ohm). With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'RES';:RES:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def Res_SetRange(self,range):
        '''
        # Set the measurement range for Resistance
        :param range: 0 to 1.05e9 Expected reading is Ohms
        '''
        cmd = "FUNC 'RES';:RES:RANGE %f"%range
        self.instance.write(cmd)

    def GetResRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'RES';:RES:RANGE?")

    def Res_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for resistance (ohms) and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 1.05e9 Reference for ohm
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'RES';:RES:REF %f;:RES:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetResRefValue(self):
        '''
         # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'RES';:RES:REF?")

    def SetRes_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'RES';:RES:DIG %d" %n
        self.instance.write(cmd)

    '*************FResistance (four-wire) Measurements******************'

    def Meas_FResistance(self):
        '''
        # This command is used to configure 'Resistance (Omega4)' Four wire Resistance measurement function and read the values.
        '''
        cmd = "MEAS:FRES?"
        return self.instance.query(cmd)

    def FRes_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.01 to 50 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 0.01
                    MAXimum 50
        '''
        cmd = "FUNC 'FRES';:FRES:NPLC %f" %rate
        self.instance.write(cmd)

    def Get_FRes_NPLCycles(self):
        '''
        # It Queries the programmed power line cycle integration rate
        '''
        return self.instance.query("FUNC 'FRES';:FRES:NPLC?")

    def FRes_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for Resistance (ohm). With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'FRES';:FRES:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def FRes_SetRange(self,range):
        '''
        # Set the measurement range for Resistance
        :param range: 0 to 2.1e6 Expected reading is Ohms
        '''
        cmd = "FUNC 'FRES';:FRES:RANGE %f"%range
        self.instance.write(cmd)

    def Get_FResRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'FRES';:FRES:RANGE?")

    def FRes_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for resistance (ohms) and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 2.1e6 Reference for ohm
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'FRES';:FRES:REF %f;:FRES:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def Get_FRes_RefValue(self):
        '''
         # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'FRES';:FRES:REF?")

    def Set_FRes_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 to 9 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'FRES';:FRES:DIG %d" %n
        self.instance.write(cmd)


    '*********Frequency Measurements*************'
    def Meas_Freq(self):
        '''
        # This command is used to configure 'Frequency' measurement function and read the values.
        '''
        cmd = "MEAS:FREQ?"
        return self.instance.query(cmd)

    def Freq_Coupling(self,type_of_coupling):
        '''
        # It sets the specific input coupling based on user input
        :param type_of_coupling: DC | AC
        '''
        cmd = "FUNC 'FREQ';:FREQ:COUP %s" %type_of_coupling
        self.instance.write(cmd)

    def Set_FreqSource(self,Source):
        '''
        # It selects the specific input source for FREQuency
        :param Source: VOLT | CURR
        '''
        cmd = "FUNC 'FREQ';:FREQ:SOUR %s" %Source
        self.instance.write(cmd)

    def Freq_SetThr_Range(self,Source,range,level):
        '''
        # This command is used to specify the expected input level (Threshold range). The instrument will then automatically select the most ideal voltage or Current threshold range.
        :param Source: VOLT | CURR
        :param range: 0 to 1000 (for volts)
                      0 to 1 (for current)
        :param level: Voltage Threshold: <n> = -0.6 to +0.6 Trigger level for 1V range
                                                -6 to +6 Trigger level for 10V range
                                                -60 to +60 Trigger level for 100V range
                                                -600 to +600 Trigger level for 1000V range
                                                DEFault 0V trigger level
                      Current Threshold level: <n> = -0.0006 to +0.0006 Trigger level for 1mA range
                                                    -0.006 to +0.006 Trigger level for 10mA range
                                                    -0.06 to +0.06 Trigger level for 100mA range
                                                    -0.6 to +0.6 Trigger level for 1A range
                                                    DEFault 0A trigger level
        '''
        cmd = "FUNC 'FREQ';:FREQ:THR:%s:RANGE %f;:FREQ:THR:%s:LEVEL %f"%(Source,range,Source,level)
        self.instance.write(cmd)

    def GetFreq_ThresholdRangelevel(self,Source):
        '''
        # It queries the maximum signal level (Programmed Threshold range) and trigger level set.
        :param Source: VOLT | CURR
        '''
        return self.instance.query("FUNC 'FREQ';:FREQ:THR:%s:RANGE?;:FREQ:THR:%s:LEVEL?"%Source)

    def Freq_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for FREQ and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 15e6 Reference for FREQ
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'FREQ';:FREQ:REF %f;:FREQ:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetFreqRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'FREQ';:FREQ:REF?")

    def Set_Freq_resolution(self,n):
        '''
        # It sets the measurement resolution ( 4 or 5 digits)
        :param n: 4 | 5 | 6 | 7 | 8 | 9
        '''
        cmd = "FUNC 'FREQ';:FREQ:DIG %d" %n
        self.instance.write(cmd)

    '**********Temperature Measurements**************'

    def Meas_Temperature(self):
        '''
        # This command is used to configure 'Temperature' measurement function and read the values.
        '''
        cmd = "MEAS:TEMP?"
        return self.instance.query(cmd)

class BK_2831(object):

    def __init__(self, USB):

        self.rm = pyvisa.ResourceManager()
        self.instance = self.rm.open_resource(str(USB))
        #self.timeout = 250000

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def GetFunc(self):
        '''
        # It Returns the currently selected function
        '''
        return self.instance.query(":FUNC?")

    def Reset(self):
        '''
        # It resets the instrument.
        '''
        return self.instance.write("*RST")

    '******************AC Voltage Measurements****************'
    def Meas_ACVolt(self):
        '''
        # This command is used to select the 'AC Voltage' measurement function of the instrument.
        '''
        cmd = "FUNC VOLT:AC"
        self.instance.write(cmd)

    def ACVolt_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.1, 1, 10 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 10 - Slow
                    MAXimum 0.1 - Fast
        '''
        cmd = "FUNC VOLT:AC;:VOLT:AC:NPLC %d" %rate
        self.instance.write(cmd)

    def GetACVolt_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC VOLT:AC;:VOLT:AC:NPLC?")

    def ACVolt_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for AC Voltage. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC VOLT:AC;:VOLT:AC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def ACVolt_SetRange(self,range):
        '''
        # Set the measurement range for AC Voltage
        :param range: 0.2, 2, 20, 200, 750 Expected reading is AC volts (ACV)
        '''
        cmd = "FUNC VOLT:AC;:VOLT:AC:RANGE %s"%range
        self.instance.write(cmd)

    def GetACVoltRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC VOLT:AC;:VOLT:AC:RANGE?")

    def ACVolt_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for ACV and enables it. (Reading = Input signal - Reference)
        :param ref: -757.5 to 757.5 Reference for ACV
        :param refstatus: 1|0
        '''
        cmd = "FUNC VOLT:AC;:VOLT:AC:REF %f;:VOLT:AC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetACVoltRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC VOLT:AC;:VOLT:AC:REF?")

    '***************DC Voltage Measurements***********'
    def Meas_DCVolt(self):
        '''
        # This command is used to select the 'DC Voltage' measurement function of the instrument.
        '''
        cmd = "FUNC VOLT:DC"
        self.instance.write(cmd)

    def DCVolt_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.1, 1, 10 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 10 - Slow
                    MAXimum 0.1 - Fast
        '''
        cmd = "FUNC VOLT:DC;:VOLT:DC:NPLC %d" %rate
        self.instance.write(cmd)

    def GetDCVolt_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC VOLT:DC;:VOLT:DC:NPLC?")

    def DCVolt_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for DC Voltage. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC VOLT:DC;:VOLT:DC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def DCVolt_SetRange(self,range):
        '''
        # Set the measurement range for DC Voltage
        :param range: 0.2, 2, 20, 200, 1000 Expected reading is DC volts (DCV)
        '''
        cmd = "FUNC VOLT:DC;:VOLT:DC:RANGE %s"%range
        self.instance.write(cmd)

    def GetDCVoltRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC VOLT:DC;:VOLT:DC:RANGE?")

    def DCVolt_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCV and enables it. (Reading = Input signal - Reference)
        :param ref: -1010 to 1010 Reference for DCV
        :param refstatus: 1|0
        '''
        cmd = "FUNC VOLT:DC;:VOLT:DC:REF %f;:VOLT:DC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetDCVoltRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC VOLT:DC;:VOLT:DC:REF?")

    '**********AC Current Measurements***********'
    def Meas_ACCurrent(self):
        '''
         # This command is used to configure the 'AC Current' measurement function of the instrument.
        '''
        cmd = "FUNC CURR:AC"
        self.instance.write(cmd)

    def ACCurr_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.1, 1, 10 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 10 - Slow
                    MAXimum 0.1 - Fast
        '''
        cmd = "FUNC CURR:AC;:CURR:AC:NPLC %d" %rate
        self.instance.write(cmd)

    def GetACCurr_NPLCycles(self):
        '''
         # It Queries the programmed power line cycle integration rate
        '''
        return self.instance.query("FUNC CURR:AC;:CURR:AC:NPLC?")

    def ACCurr_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for AC Current. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC CURR:AC;:CURR:AC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def ACCurr_SetRange(self,range):
        '''
        # Set the measurement range for AC Current.
        :param range: 0 to 20 Expected reading is AC Current in Amperes (ACI)
        '''
        cmd = "FUNC CURR:AC;:CURR:AC:RANGE %s"%range
        self.instance.write(cmd)

    def GetACCurrRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC CURR:AC;:CURR:AC:RANGE?")

    def ACCurr_SetRefValue(self,ref,refstatus):
        '''
         # It Specifies the reference for ACI and enables it. (Reading = Input signal - Reference)
        :param ref: -20 to 20 Reference for ACI
        :param refstatus: 1|0
        '''
        cmd = "FUNC CURR:AC;:CURR:AC:REF %f;:CURR:AC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetACCurrRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC CURR:AC;:CURR:AC:REF?")

    '************DC Current Measuremets**********'
    def Meas_DCCurr(self):
        '''
        # This command is used to select the 'DC Current' measurement function of the instrument.
        '''
        cmd = "FUNC CURR:DC"
        self.instance.write(cmd)

    def DCCurr_SetNPLCycles(self,rate):
        '''
         # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.1, 1, 10 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 10 - Slow
                    MAXimum 0.1 - Fast
        '''
        cmd = "FUNC CURR:DC;:CURR:DC:NPLC %d" %rate
        self.instance.write(cmd)

    def GetDCCurr_NPLCycles(self):
        '''
        # It Queries the power line cycle integration rate
        '''
        return self.instance.query("FUNC CURR:DC;:CURR:DC:NPLC?")

    def DCCurr_SetAutoRange(self,AutoEn):
        '''
         # It sets the Auto range for DC Current. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC CURR:DC;:CURR:DC:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def DCCurr_SetRange(self,range):
        '''
        # Set the measurement range for DC Current
        :param range: -20 to 20 Expected reading is DC Current (DCI)
        '''
        cmd = "FUNC CURR:DC;:CURR:DC:RANGE %s"%range
        self.instance.write(cmd)

    def GetDCCurrRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC CURR:DC;:CURR:DC:RANGE?")

    def DCCurr_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCI and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 20 Reference for DCI
        :param refstatus: 1|0
        '''
        cmd = "FUNC CURR:DC;:CURR:DC:REF %f;:CURR:DC:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetDCCurrRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC CURR:DC;:CURR:DC:REF?")

    '*************Resistance Measurements******************'
    def Meas_Resistance(self):
        '''
        # This command is used to Configure the 'Resistance' measurement function of the instrument.
        '''
        cmd = "FUNC RES"
        self.instance.write(cmd)

    def Res_SetNPLCycles(self,rate):
        '''
        # Sets the integration rate for measurement speed.
        NPLC (Number of Power Line Cycles) expresses the integration period based on the power line frequency.
        :param rate: Command Parameter: <n> = 0.1, 1, 10 Set power line cycles per integration
                    DEFault 1 - Medium
                    MINimum 10 - Slow
                    MAXimum 0.1 - Fast
        '''
        cmd = "FUNC RES;:RES:NPLC %d" %rate
        self.instance.write(cmd)

    def GetRes_NPLCycles(self):
        '''
        # It Queries the programmed power line cycle integration rate
        '''
        return self.instance.query("FUNC RES;:RES:NPLC?")

    def Res_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for Resistance (ohm). With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC RES;:RES:RANGE:AUTO %d"%AutoEn
        self.instance.write(cmd)

    def Res_SetRange(self,range):
        '''
        # Set the measurement range for Resistance
        :param range: 0 to 20e6 Expected reading is Ohms
        '''
        cmd = "FUNC RES;:RES:RANGE %s"%range
        self.instance.write(cmd)

    def GetResRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC RES;:RES:RANGE?")

    def Res_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for resistance (ohms) and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 20e6 Reference for ohm
        :param refstatus: 1|0
        '''
        cmd = "FUNC RES;:RES:REF %f;:RES:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetResRefValue(self):
        '''
         # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC RES;:RES:REF?")

    '*********Frequency Measurements*************'
    def Meas_Freq(self):
        '''
        # This command is used to configure the 'Frequency' measurement function of the instrument.
        '''
        cmd = "FUNC FREQ"
        self.instance.write(cmd)

    def Freq_SetThresholdRange(self,range):
        '''
        # This command is used to specify the expected input level (Threshold range). The instrument will then automatically select the most ideal current or voltage threshold range.
        :param range: 0 to 1010 (in volts)
        '''
        cmd = "FUNC FREQ;:FREQ:THR:VOLT:RANGE %s"%range
        self.instance.write(cmd)

    def GetFreq_ThresholdRange(self):
        '''
        # It queries the maximum signal level (Programmed Threshold range).
        '''
        return self.instance.query("FUNC FREQ;:FREQ:THR:VOLT:RANGE?")

    def Freq_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for FREQ and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 1.0e6 Reference for FREQ
        :param refstatus: 1|0
        '''
        cmd = "FUNC FREQ;:FREQ:REF %f;:FREQ:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetFreqRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC FREQ;:FREQ:REF?")

    '**********Period Measurements**************'
    def Meas_Period(self):
        '''
        # This command is used to configure the 'Period' measurement function of the instrument.
        '''
        cmd = "FUNC PER"
        self.instance.write(cmd)

    def Period_SetThresholdRange(self,range):
        '''
        # This command is used to specify the expected input level (Threshold range). The instrument will then automatically select the most ideal current or voltage threshold range.
        :param range: 0 to 1010 (in volts)
        '''
        cmd = "FUNC PER;:PER:THR:VOLT:RANGE %s"%range
        self.instance.write(cmd)

    def GetPeriod_ThresholdRange(self):
        '''
        # It queries the maximum signal level (Programmed Threshold range).
        '''
        return self.instance.query("FUNC PER;:PER:THR:VOLT:RANGE?")

    def Period_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for PERiod and enables it. (Reading = Input signal - Reference)
        :param ref: 0 to 1 Reference for PER
        :param refstatus: 1|0
        '''
        cmd = "FUNC PER;:PER:REF %f;:PER:REF:STAT %d"%(ref,refstatus)
        self.instance.write(cmd)

    def GetPeriodRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC PER;:PER:REF?")


if __name__ == "__main__":
    #DMM = BK_2831("COM4")
    #print DMM.GetInfo()
    DMM = Keithley_2002("TCPIP0::10.20.50.73::gpib0,16::INSTR")
    print(DMM.GetInfo())


