# $Id: PowerSupply.py 2018-09-02 Nishitha $
# This drive is for Keithley Power Supply
# Model Number: Keithley 2230-30-1 Triple Channel DC Power Supply

import pyvisa

class Keithley2230(object):

    def __init__(self, USB):

        if '0x' in USB:
            self.main_USB = self.Hex_to_Dec(USB)
        else:
            self.main_USB = USB

        # Initiate GPIB instance
        self.rm = pyvisa.ResourceManager()
        self.list = self.rm.list_resources()
        if self.main_USB in self.list:
            self.instance = self.rm.open_resource(str(self.main_USB))
            self.instance.write('SYSTEM:REMote\n')
        else:
            self.FindInstrument()
        # self.timeout = 250000

    def FindInstrument(self):
        for instruments in self.list:
            if 'USB0::1510::8752' in instruments:
                self.main_USB = instruments
                self.instance = self.rm.open_resource(instruments)
                self.instance.write('SYSTEM:REMote\n')

    def Hex_to_Dec(self, USB):
        final_dec = ''
        for i in USB.split('::'):
            if '0x' in i:
                final_dec += str(int(i, 16))
            else:
                final_dec += i
            if i != 'INSTR':
                final_dec += '::'
        return final_dec

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def SetChannel(self, ch):
        '''
        # This command is used to select the channel of power supply
        :param ch: CH1 | CH2 | CH3
        '''
        cmd = "INST:SEL {}".format(ch)
        self.instance.write(cmd)

    def GetChannel(self):
        '''
        # This command returns the current channel of power supply
        '''
        cmd = "INST:SEL?"
        return self.instance.query(cmd)

    def SetVolt(self, ch, volt=0):
        '''
        # This command is used to select channel and set voltage level using a single command.
        :param ch: CH1 | CH2 | CH3
        :param volt: NR2 or NR3 format Eg: 2.5
        '''
        cmd = "APPLY {}, {}".format(ch, volt)
        self.instance.write(cmd)

    def SetVoltCurr(self, ch, volt=0, curr=1):
        '''
        # This command is used to select channels and set voltage and current level using a single command.
        :param ch: CH1 | CH2 | CH3
        :param volt: NR2 or NR3 format (volt)
        :param curr: NR2 or NR3 format (Amp)
        '''
        cmd = "APPLY {}, {}, {} ".format(ch, volt, curr)
        self.instance.write(cmd)

    def SetCurr(self, ch, curr=0):
        '''
        # This command is used to set the current limit for a specified channel
        :param ch: CH1 | CH2 | CH3
        :param curr: NR2 format (in Amp)
        '''
        self.SetChannel(ch)
        cmd = "CURR {}".format(curr)
        self.instance.write(cmd)

    def SetOutput(self, enable=False):
        '''
        # This is used to set the Output Enable/Disable
        :param enable: 1 | 0
        '''
        # Set the output ON/OFF
        if enable == True:
            cmd = "OUTP ON"
        else:
            cmd = "OUTP OFF"
        self.instance.write(cmd)


    def GetOutput(self):
        '''
        # This command is used to get the status of the Output
        '''
        cmd = "OUTP?"
        return (self.instance.query(cmd))

    def GetMeasVolt(self, ch):
        '''
        # Used to Get the measured voltage reading from the selected channel/All channels
        ch: CH1 |CH2 |CH3 | ALL
        '''
        cmd = "MEAS:VOLT? {}".format(ch)
        return self.instance.query(cmd)

    def GetSetVolt(self, ch):
        '''
        # It is used to Get the Set voltage reading from the selected channel
        :param ch: CH1 | CH2 | CH3
        '''
        self.SetChannel(ch)
        cmd = "VOLT?"
        return self.instance.query(cmd)

    def GetMeasCurr(self, ch):
        '''
        # It is used to Get the measured current reading from the selected channel/All channels
        ch: CH1 | CH2 | CH3 | ALL
        '''
        cmd = "MEAS:CURR? {}".format(ch)
        return self.instance.query(cmd)

    def GetSetCurr(self, ch):
        '''
        # It is used to Get the Set current reading from the selected channel
        :param ch: CH1 | CH2 | CH3
        '''
        self.SetChannel(ch)
        cmd = "CURR?"
        return self.instance.query(cmd)

    def SetVoltLimit(self,ch,volt):
        '''
        # This command limits the maximum voltage that can be programmed on the power supply.
        :param ch: CH1 | CH2 | CH3
        :param volt: MIN | MAX | (NR1/NR2/NR3 format value)
        '''
        self.SetChannel(ch)
        cmd = "VOLTAGE:LIMIT {}".format(volt)
        self.instance.write(cmd)

    def CC_Detection1(self,ch1):
        '''
        # This queries the questionable condition register (QCR) summary of a channel, where <x> is 1, 2, or 3
        if 1 : which would indicate an over voltage condition on specified channel.
        :param ch1: 1 | 2 | 3
        :return: Result 1 for CV mode
                 Result 2 for CC mode
        '''
        cmd = "STAT:QUES:INST:ISUM{}:COND?".format(ch1)
        return self.instance.query(cmd)

    def CC_Detection2(self,ch1):
        '''
        # This command is used to query the operation condition register of a channel, where <x> is 1, 2, or 3
        :param ch1: 1 | 2 |3
        :return: Result 9 for CV mode
                 Result 10/A for CC mode
        '''
        cmd = "STAT:OPER:INST:ISUM{}:COND?".format(ch1)
        return self.instance.query(cmd)


if __name__ == "__main__":
    ps = Keithley2230("USB0::0x05E6::0x2230::802901012747410003::0::INSTR")

    print(ps.GetInfo())