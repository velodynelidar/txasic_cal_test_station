#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#                  Daniel Eng <deng@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Client software for communicating with MCM-SDK

import time
import numpy as np

from txasic_cal.tools.logger import logger
from txasic_cal.lab.instruments import config

## Gonio configurations
# Steps per degree
POS_CONVERSION = 136533
# Initial step size
INITIAL_STEP = -0.1

THRESHOLD_OP = [0.1, 0.01]

SLEEP_TIME = 0.5

class Gonio:
    def __init__(self):
        self.conn_x = None
        self.conn_y = None
        self.iter = 0

        self.cur_x_pos = 0
        self.cur_y_pos = 0
        self.total_time = 0

        # TODO: Load from table
        # CH_X_POS = [243323, 246523, 247585, 239778, 242677, 244380, 236067, 230805]
        # CH_Y_POS = [338896, 303056, 269776, 226492, 202786, 166095, 122183, 80986]

        # CH_X_POS = [212653, 222894, 224598, 225872, 234399, 233539, 230120, 224997]
        # CH_Y_POS = [353364, 318375, 289359, 251812, 224505, 192083, 149417, 108458]

        # CH_X_POS = [222976, 246523, 247585, 239778, 242677, 244380, 236067, 230805]
        # CH_Y_POS = [384205, 303056, 269776, 226492, 202786, 166095, 122183, 80986]

        CH_X_POS = [249768, 246523, 247585, 239778, 242677, 244380, 236067, 230805]
        CH_Y_POS = [343159, 303056, 269776, 226492, 202786, 166095, 122183, 80986]

        CH_X_AMP_POS = [238203, 241829, 245023, 244682, 243528, 240112, 234778, 228813]
        CH_Y_AMP_POS = [338896, 303056, 264655, 226492, 193399, 155855, 114500, 78658]

        self.cur_ch = 0

        # Make it number of channels
        self.chan_op_pos = {'x':CH_X_POS, 'y':CH_Y_POS}
        self.chan_amp_pos = {'x':CH_X_AMP_POS, 'y':CH_Y_AMP_POS}

    def lab_connect(self, lab):
        self._lab = lab
    
    def init(self):
        from pylablib.aux_libs.devices import Thorlabs
        self.conn_x = Thorlabs.KDC101(config.X_sn)
        if self.conn_x is None:
            logger.error("KDC X-axis not connected.")
            return -1
        self.conn_y = Thorlabs.KDC101(config.Y_sn)
        if self.conn_y is None:
            logger.error("KDC y-axis not connected.")
            return -1

        logger.debug("Gonio KDC motors x {} and y {} connected.".format(config.X_sn, config.Y_sn))
        return 0
    
    def close(self):
        self.conn_x.close()
        self.conn_y.close()
    
    def home(self):
        logger.info("homing gonio x")
        self.conn_x.home()
        self.conn_x.wait_for_stop()
        self.conn_y.home()
        self.conn_y.wait_for_stop()

    def get_position(self):
        self.cur_x_pos = self.conn_x.get_position()
        self.cur_y_pos = self.conn_y.get_position()

    def move_to_position(self, pos_x, pos_y):
        logger.info("Moving gonio to position x-{}, y-{}."
                    .format(pos_x, pos_y))
        self.cur_x_pos = pos_x
        self.conn_x.move_to(pos_x)
        self.conn_x.wait_for_stop()

        self.cur_y_pos = pos_y
        self.conn_y.move_to(pos_y)
        self.conn_y.wait_for_stop()

    def update_ch_op_pos(self, cur_ch):
        """Update stored gonio position for a given channel

        Arguments:
            cur_ch {int} -- Channel number of sensor
        """
        self.cur_ch = cur_ch
        self.chan_op_pos['x'][cur_ch] = self.cur_x_pos
        self.chan_op_pos['y'][cur_ch] = self.cur_y_pos

    def update_ch_amp_pos(self, cur_ch):
        """Update stored gonio position for a given channel

        Arguments:
            cur_ch {int} -- Channel number of sensor
        """
        self.cur_ch =cur_ch
        self.chan_amp_pos['x'][cur_ch] = self.cur_x_pos
        self.chan_amp_pos['y'][cur_ch] = self.cur_y_pos

    def get_data(self, n=0):
        """Get lab data

        Keyword Arguments:
            n {int} -- Flag used for scope autoset (default: {0})

        Returns:
            [float] -- [optical power, pulse amplitude, and pulse width]
        """
        optical_power = self._lab.optical_power(self.cur_ch)
        logger.info("Optical-Power = {} mW".format(optical_power))

        if n == 1:
            self._lab.mso_autoset()

        pulse_amp = self._lab.pulse_amp()
        logger.info("Pulse-Amp = %f mV" % pulse_amp)

        pulse_width = self._lab.pulse_width()
        logger.info("Pulse-Width = {} ns".format(pulse_width))

        return optical_power, pulse_amp, pulse_width

    def simple_search(self, connection=None, n=0):

        opt_pow, opt_amp, _ = self.get_data()
        if(n==1 and opt_pow < THRESHOLD_OP[0] * 0.1):
            return None, None, None, None, None, None

        OP = self.get_data(n)[n]
        step = INITIAL_STEP
        failed = ["Passed", "Failed"]

        position_list = []
        power_list = []

        pos1 = connection.get_position()
        pos0 = 0
        i = 0
        time.sleep(SLEEP_TIME)

        if n == 0:
            max_i = 100
        else:
            max_i = 50

        while OP < THRESHOLD_OP[n] and i < max_i:
            #positive
            if int(pos1) != int(pos0):
                pos0 = pos1
                connection.move(step * POS_CONVERSION)
                connection.wait_for_stop()
                pos1 = connection.get_position()
                logger.info("Position = {0} steps. Iteration Step Size = {1} mm. Iteration = {2}".format(pos1, step, i))

                i = i + 1
                time.sleep(SLEEP_TIME)
                OP = self.get_data(n)[n]

                position_list.append(pos1)
                power_list.append(OP)
                #negative, change directions, /2 step size
            elif int(pos1) == int(pos0):
                step = -step
                connection.move(step * POS_CONVERSION)
                connection.wait_for_stop()
                pos0 = pos1
                pos1 = connection.get_position()

                logger.info(
                    "Position = {0} steps. Iteration Step Size = {1} mm. Iteration = {2}".format(pos1, step, i))
                i = i + 1
                time.sleep(SLEEP_TIME)
                OP = self.get_data()[n]
                position_list.append(pos1)
                power_list.append(OP)

        if i < max_i:
            j = 0
        else:
            j = 1

        max_position = connection.get_position()
        connection.wait_for_stop()
        if step > 0:
            direction = 1
        else:
            direction = -1
        logger.info("Threshold Position = {0} steps. Max Power = {1}. Iterations {2}. {3}".format(max_position, OP, i,  failed[j]))

        return max_position, OP, failed[j], direction, connection, i

    def fine_search(self, conn, n=0):
        #     #if fine search does not get better than simple search, use simple result.
        logger.info("\n\n*****Gonio tune for {}*****".format("Optical Power"\
                                                 if n == 0\
                                                 else "Pulse Amplitude"))
        start_time = time.time()

        starting_position, OP_0, failed, direction, connection, iteration_simple = self.simple_search(conn, n)

        opt_pow, opt_amp, _ = self.get_data(n)
        if (n == 1 and opt_pow < THRESHOLD_OP[0] * 0.1):
            return

        max_OP_dic = {OP_0: starting_position}
        max_OP = OP_0

        connection.move_to(starting_position)
        connection.wait_for_stop()
        OP_i = self.get_data(n)[n]

        step = direction * INITIAL_STEP * 0.5

        pos_i = connection.get_position()

        pos_0 = 0
        end_iter = 0
        switch_iter = 0
        i = 0
        switch_combo = 0
        d_OP = 0

        if n == 0:
            max_i = 150
        else:
            max_i = 50

        while i < max_i and switch_iter < 5 and switch_combo < 3 and end_iter < 2:
            # positive, continue in same direction
            if d_OP >= 0 and int(pos_i) != int(pos_0):

                connection.move(step * POS_CONVERSION)
                connection.wait_for_stop()

                OP_i = self.get_data(n)[n]
                pos_0 = pos_i
                pos_i = connection.get_position()

                d_OP = float(OP_i) - float(OP_0)

                OP_0 = OP_i

                logger.info(
                    "\n\nSwitch iteration {0}. delta power = {1} mW. Iteration Step Size = {2} mm. Iteration = {3}".format(
                        switch_iter, d_OP, step, i))
                i = i + 1
                switch_combo = 0
                if OP_i > max_OP:
                    max_OP = OP_i
                    max_OP_dic[max_OP] = pos_i

            # negative, near target change directions, /2 step size
            elif d_OP < 0 and int(pos_i) != int(pos_0):
                step = step * (-0.5)
                connection.move(step * POS_CONVERSION)
                connection.wait_for_stop()

                OP_i = self.get_data(n)[n]
                pos_0 = pos_i
                pos_i = connection.get_position()
                logger.info(
                    "\n\nSwitch iteration {0}. delta power = {1} mW. Iteration Step Size = {2} mm. Iteration = {3}".format(
                        switch_iter, d_OP, step, i))

                d_OP = float(OP_i - OP_0)
                OP_0 = OP_i

                i = i + 1
                switch_iter = switch_iter + 1
                switch_combo = switch_combo + 1

                if OP_i > max_OP:
                    max_OP = OP_i
                    max_OP_dic[max_OP] = pos_i

            elif int(pos_i) == int(pos_0):
                step = -step
                connection.move(step * POS_CONVERSION)
                connection.wait_for_stop()

                OP_i = self.get_data(n)[n]
                pos_0 = pos_i
                pos_i = connection.get_position()
                d_OP = 0
                logger.info(
                    "\n\nSwitch iteration {0}. delta power = {1} mW. Iteration Step Size = {2} mm. Iteration = {3}".format(
                        switch_iter, d_OP, step, i))

                i = i + 1
                end_iter = end_iter + 1

                # failure criteria
        connection.move_to(max_OP_dic[max_OP])
        connection.wait_for_stop()

        pulse_amp, pw = self.get_data(n)[1:]

        logger.info(
            "FINE TUNE CAL: POS = {0} steps. Max Power = {1}. Iterations {2}.".format(max_OP_dic[max_OP],
                                                                                              OP_i, i))
        logger.info(
            "Fine tune with dic, POS = {0}. Max Power = {1}".format(max_OP_dic[max_OP], max_OP))

        max_position = connection.get_position()

        end_time = time.time()

        ch_time = end_time - start_time

        logger.debug("Total channel time taken in seconds = {}".format(ch_time))

        i = i + iteration_simple

    def blind_search(self, type):
        start_time = time.time()
        # Search along y-axis first
        self.fine_search(self.conn_y, type)
        # Search along x-axis
        self.fine_search(self.conn_x, type)

        self.get_position()

        end_time = time.time()
        self.total_time = end_time - start_time

        if type == 0:
            self.update_ch_op_pos(self.cur_ch)
        elif type == 1:
            self.update_ch_amp_pos(self.cur_ch)

        logger.debug("Total gonio blind search time: {}".format(self.total_time))

    def X_Y_gonio_cal(self, type=0, cur_ch=0):
        """Gonio cal top method

        Keyword Arguments:
            type {int} -- 0 - optical power, 1 - pulse amplitude (default: {0})
            cur_ch {int} -- Current sensor channel (default: {0})
        """
        self.cur_ch = cur_ch
        self.blind_search(type)

    def populate_cur_row(self, *args, **kwargs):
        """Populate current row

        Returns:
            [dict] -- [Dict of results row]
        """
        if kwargs['measurement'] == 'Gonio_X_OP_Pos':
            # Get gonio op x pos
            try:
                self._lab.current_measurements[kwargs['measurement']] = self.chan_op_pos['x'][args[0]['Channel']]
                logger.debug("Gonio OP X Pos: {}".format(self._lab.current_measurements[kwargs['measurement']]))
            except Exception as e:
                self._lab.current_measurements[kwargs['measurement']] = None
            
            return self._lab.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Gonio_Y_OP_Pos':
            # Get gonio op Y pos
            try:
                self._lab.current_measurements[kwargs['measurement']] = self.chan_op_pos['y'][args[0]['Channel']]
                logger.debug("Gonio OP Y Pos: {}".format(self._lab.current_measurements[kwargs['measurement']]))
            except Exception as e:
                self._lab.current_measurements[kwargs['measurement']] = None
            
            return self._lab.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Gonio_X_Amp_Pos':
            # Get gonio amp x pos
            try:
                self._lab.current_measurements[kwargs['measurement']] = self.chan_amp_pos['x'][args[0]['Channel']]
                logger.debug("Gonio Amp X Pos: {}".format(self._lab.current_measurements[kwargs['measurement']]))
            except Exception as e:
                self._lab.current_measurements[kwargs['measurement']] = None
            
            return self._lab.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Gonio_Y_Amp_Pos':
            # Get gonio amp y pos
            try:
                self._lab.current_measurements[kwargs['measurement']] = self.chan_amp_pos['y'][args[0]['Channel']]
                logger.debug("Gonio Amp y Pos: {}".format(self._lab.current_measurements[kwargs['measurement']]))
            except Exception as e:
                self._lab.current_measurements[kwargs['measurement']] = None
            
            return self._lab.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Gonio_Cal_Time':
            # Get gonio cal time
            try:
                self._lab.current_measurements[kwargs['measurement']] = self.total_time
                logger.debug("Gonio Cal Time: {}".format(self._lab.current_measurements[kwargs['measurement']]))
            except Exception as e:
                self._lab.current_measurements[kwargs['measurement']] = None
            
            return self._lab.current_measurements[kwargs['measurement']]


if __name__ == '__main__':
    start_time_total = time.time()

    t = Gonio()
    t.init()
    end_time_total = time.time()

    logger.info("Total time taken in seconds = %f" % (end_time_total - start_time_total))

    logger.info("***********************************")

