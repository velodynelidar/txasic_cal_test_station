#! ../bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Power meter API

import pyvisa
import time

POWERMETER_PRECISION = -5

class Thorlabs_PM200(object):

    def __init__(self, USB):

        if '0x' in USB:
            self.main_USB = self.Hex_to_Dec(USB)
        else:
            self.main_USB = USB

        self.rm = pyvisa.ResourceManager()
        self.list = self.rm.list_resources()
        if self.main_USB in self.list:
            self.instance = self.rm.open_resource(str(self.main_USB))
        else:
            self.FindInstrument()
        # self.timeout = 250000

    def FindInstrument(self):
        for instruments in self.list:
            if 'USB0::4883::32888' in instruments:
                self.main_USB = instruments
                self.instance = self.rm.open_resource(instruments)

    def Hex_to_Dec(self, USB):
        final_dec = ''
        for i in USB.split('::'):
            if '0x' in i:
                final_dec += str(int(i, 16))
            else:
                final_dec += i
            if i != 'INSTR':
                final_dec += '::'
        return final_dec

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def GetFunc(self):
        '''
        # It Returns the currently selected function
        '''
        return self.instance.query(":FUNC?")

    def Reset(self):
        '''
        # It resets the instrument.
        '''
        return self.instance.write("*RST")

    def Fetch_reading(self):
        '''
        # This query command is used to request the latest post-processed reading.
        '''
        return self.instance.query(":FETCH?")

    '***************Power Measurements***********'
    def Meas_Pow(self):
        '''
        # This command is used to configure 'DC Power' measurement function and read the values.
        '''
        cmd = "MEAS:POW?"
        result = self.instance.query(cmd)
        return float(result)

    def DCPow_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for DC Power. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'POW:DC';:POW:DC:RANG:AUTO {}".format(AutoEn)
        self.instance.write(cmd)

    def DCPow_SetRange(self,range):
        '''
        # Set the measurement range for DC Power in Watts
        :param range: 50n - 50m W
        '''
        cmd = "FUNC 'POW:DC';:POW:DC:RANG {}".format(range)
        self.instance.write(cmd)

    def GetDCPowRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'POW:DC';:POW:DC:RANG?")

    def DCPow_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for Pow and enables it. (Reading = Input signal - Reference)
        :param ref:
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'POW:DC';:POW:DC:REF {};:POW:DC:REF:STAT {}".format(ref,refstatus)
        self.instance.write(cmd)

    def GetDCPowRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'POW:DC';:POW:DC:REF?")

    def SetDCPow_unit(self,s):
        '''
        # It sets the Pow unit
        :param s: W | DBM
        '''
        cmd = "FUNC 'POW:DC';:POW:DC:UNIT {}".format(s)
        self.instance.write(cmd)

    def GetDCPow_unit(self):
        '''
        # It sets the Pow unit
        :param s: W | DBM
        '''
        cmd = "FUNC 'POW:DC';:POW:DC:UNIT?"
        self.instance.query(cmd)

    '***************DC Voltage Measurements***********'
    def Meas_DCVolt(self):
        '''
        # This command is used to configure 'DC Voltage' measurement function and read the values.
        '''
        cmd = "MEAS:VOLT:DC?"
        result = self.instance.query(cmd)
        return float(result)

    def DCVolt_SetAutoRange(self,AutoEn):
        '''
        # It sets the Auto range for DC Voltage. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:RANG:AUTO {}".format(AutoEn)
        self.instance.write(cmd)

    def DCVolt_SetRange(self,range):
        '''
        # Set the measurement range for DC Voltage
        :param range: 0 to 1100 Expected reading is DC volts (DCV)
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:RANG {}".format(range)
        self.instance.write(cmd)

    def GetDCVoltRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'VOLT:DC';:VOLT:DC:RANG?")

    def DCVolt_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCV and enables it. (Reading = Input signal - Reference)
        :param ref: -1100 to +1100 Reference for DCV
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'VOLT:DC';:VOLT:DC:REF {};:VOLT:DC:REF:STAT {}".format(ref,refstatus)
        self.instance.write(cmd)

    def GetDCVoltRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'VOLT:DC';:VOLT:DC:REF?")

    '************DC Current Measuremets**********'
    def Meas_DCCurr(self):
        '''
        # This command is used to configure 'DC Current' measurement function and read the values.
        '''
        cmd = "MEAS:CURR:DC?"
        result = self.instance.query(cmd)
        return float(result)

    def DCCurr_SetAutoRange(self,AutoEn):
        '''
         # It sets the Auto range for DC Current. With auto ranging enabled, the instrument automatically goes to the most ideal range to perform the measurement.
        :param AutoEn: 0|1
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:RANG:AUTO {}".format(AutoEn)
        self.instance.write(cmd)

    def DCCurr_SetRange(self,range):
        '''
        # Set the measurement range for DC Current
        :param range: 0 to +2.1 Expected reading is DC Current (DCI)
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:RANG {}".format(range)
        self.instance.write(cmd)

    def GetDCCurrRange(self):
        '''
        # It Queries the measurement range of the current function.
        '''
        return self.instance.query("FUNC 'CURR:DC';:CURR:DC:RANG?")

    def DCCurr_SetRefValue(self,ref,refstatus):
        '''
        # It Specifies the reference for DCI and enables it. (Reading = Input signal - Reference)
        :param ref: -2.1 to +2.1 Reference for DCI
        :param refstatus: 1|0
        '''
        cmd = "FUNC 'CURR:DC';:CURR:DC:REF {};:CURR:DC:REF:STAT {}".format(ref,refstatus)
        self.instance.write(cmd)

    def GetDCCurrRefValue(self):
        '''
        # It Queries the reference value for relative function
        '''
        return self.instance.query("FUNC 'CURR:DC';:CURR:DC:REF?")

    '*********Frequency Measurements*************'
    def Meas_Freq(self):
        '''
        # This command is used to configure 'Frequency' measurement function and read the values.
        '''
        cmd = "MEAS:FREQ?"
        return self.instance.query(cmd)

    def GetFreq_ThresholdRangelevel(self,Source):
        '''
        # It queries the maximum signal level (Programmed Threshold range) and trigger level set.
        :param Source: VOLT | CURR
        '''
        return self.instance.query("FUNC 'FREQ';:FREQ:THR:{}:RANGE?;:FREQ:THR:{}:LEVEL?".format(Source, Source))
    
    def get_attenuation_factor(self):
        '''
        Get attenuation factor applied to power meter in dB
        '''
        return float(self.instance.query("SENS:CORR:LOSS:INP:MAGN?"))

    def get_wavelength(self):
        '''
        Get laser wavelength set

        return
            [float] - wavelength value
        '''
        return float(self.instance.query("SENS:CORR:WAV?"))

    def set_wavelength(self, wavelength):
        '''
        Set laser wavelength set

        params
            wavelength [int] - 905 (e.g.)
        '''
        cmd = "SENS:CORR:WAV {}".format(wavelength)
        self.instance.write(cmd)

    def get_power_range_auto(self):
        '''
        Get power range auto value

        return
            [int] - 1/0
        '''
        return int(self.instance.query("SENS:POW:RANG:AUTO?"))

    def set_power_range_auto(self, value):
        '''
        Set power range auto value

        params
            value [int] - 1/0
        '''
        cmd = "SENS:POW:RANG:AUTO {}".format(int(value))
        self.instance.write(cmd)

    def get_power_unit(self):
        '''
        Get power unit

        return
            [str] - W/DBM
        '''
        return self.instance.query("SENS:POW:UNIT?")

    def set_power_unit(self, value):
        '''
        Set power unit

        params
            value [str] - W/DBM
        '''
        cmd = "SENS:POW:UNIT {}".format(value.upper())
        self.instance.write(cmd)

    def get_bandwidth(self):
        '''
        Get bandwidth

        return
            [int] - Bandwidth as 1/0
        '''
        return int(self.instance.query("INP:PDI:FILT:LPAS:STAT?"))

    def set_bandwidth(self, value):
        '''
        Set bandwidth

        params
            value [str] - ON/OFF
        '''
        cmd = "INP:PDI:FILT:LPAS:STAT {}".format(value)
        self.instance.write(cmd)


if __name__ == "__main__":
    PM = Thorlabs_PM200("USB0::0x1313::0x8075::P5001603::INSTR")
    print(PM.GetInfo())
