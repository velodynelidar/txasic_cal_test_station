# $Id: TempPlate.py 2020-09-30 Danny (Code modified from Nathan Dondlinger) $
# This driver is for Teca Hot Plate

import serial
import struct
import time

class TempPlate(object):

    def __init__(self, USB):
        self.k_id = 1  # Make configurable parameter
        self.k_baud_rate = 19200
        self.k_frame_len = 8
        self.ser = serial.Serial(USB, self.k_baud_rate)

    def close(self):
        self.ser.close()

    def GetProcessTemp_PV(self):
        addr = 0x1000
        data = self.ReadBuf(addr)
        return data

    def SetTemp_SP(self, temp):
        addr = 0x0000
        temp = round(temp, 1)
        data = self.WriteBuf(addr, temp)
        return data

    def GetTemp_SP(self):
        addr = 0x0000
        data = self.ReadBuf(addr)
        return data

    def ReadBuf(self, addr, data_cnt=1):
        # build buffer, send to device, and return data as an int
        buff = bytearray(8)
        addr_packed = struct.pack('H', addr)
        data_cnt_packed = struct.pack('H', data_cnt)
        buff[0] = self.k_id
        buff[1] = 3 # Read Command
        buff[2] = addr_packed[1]
        buff[3] = addr_packed[0]
        buff[4] = data_cnt_packed[1]
        buff[5] = data_cnt_packed[0]
        CRC = self.CalculateCRC(buff[:self.k_frame_len - 2], self.k_frame_len - 2)
        buff[6] = CRC[0]
        buff[7] = CRC[1]
        data = self.Communicate(buff)
        val = data[4] << 8 | data[5]
        if (val & 0x8000) == 0x8000:
            val = -((val ^ 0xFFFF) + 1) / 10
        else:
            val = val / 10
        return val

    def WriteBuf(self, addr, data):
        data = int(data * 10)
        if data < 0:  # two's complement
            data = (abs(data) ^ 0xFFFF) + 1
        buff = bytearray(8)
        addr_packed = struct.pack('H', addr)
        data_packed = struct.pack('H', data)
        buff[0] = self.k_id
        buff[1] = 6  # Write Command
        buff[2] = addr_packed[1]
        buff[3] = addr_packed[0]
        buff[4] = data_packed[1]
        buff[5] = data_packed[0]
        CRC = self.CalculateCRC(buff[:self.k_frame_len - 2], self.k_frame_len - 2)
        buff[6] = CRC[0]
        buff[7] = CRC[1]
        data = self.Communicate(buff)
        val = data[4] << 8 | data[5]
        if (val & 0x8000) == 0x8000:
            val = -((val ^ 0xFFFF) + 1) / 10
        else:
            val = val / 10
        return val

    def Communicate(self, buff, max_tries=5):
        read_bad = True
        tries = 0
        while read_bad and (tries < max_tries):
            # Clean com port buffer
            self.ser.flushInput()
            self.ser.flushOutput()
            self.ser.write(buff)
            time.sleep(0.05)
            data = self.GetResponse()
            CRC_return = self.CalculateCRC(data[:self.k_frame_len - 2], self.k_frame_len - 2)
            if data[6] != CRC_return[0] or data[7] != CRC_return[1] or len(buff) != 8:
                read_bad = True
            else:
                read_bad = False
            time.sleep(0.03)
            tries = tries + 1
        self.CheckDevError(data)
        return data

    def GetResponse(self):
        while self.ser.in_waiting != 8:
            time.sleep(0.01)
        data = self.ser.read(self.k_frame_len)
        data = struct.unpack('8B', data)
        return data


    def CalculateCRC(self,frame, len_frame):
        CRCbuff = bytearray(2)
        CRC = 0xFFFF
        for byte_cnt in range(len_frame):
            CRC ^= frame[byte_cnt]
            for j in range(8):
                bit_val = CRC & 0x0001
                CRC = CRC >> 1
                if bit_val == 1:
                    CRC ^= 0xA001
        CRCbuff[0] = CRC & 0x00FF
        CRCbuff[1] = CRC >> 8
        return CRCbuff

    def CheckDevError(self, buff):
        # TODO:
        # More meaningful error handling
        if buff[1] >= 0x80:
            code = buff[3]
            if code == 0x01:
                print("Device function (read/write) error")
            elif code == 0x02:
                print("Address error")
            elif code == 0x03:
                print("Data error")
            else:
                print("Other device error")
        return

if __name__ == "__main__":
    tempController = TempPlate("/dev/ttyUSB0")
    print(tempController.GetProcessTemp_PV())

