# $Id: TekScope.py 2018-20-02 Nishitha $
# This drive is for Tektronix Mixed Signal Oscilloscope
# Model Number: Tektronix MSO 5204B

import time
import logging
logger = logging.getLogger(__name__)
import pyvisa
import time


class TekScope_5204B(object):

    def __init__(self, USB):

        # Initiate Ethernet instance
        self.rm = pyvisa.ResourceManager()
        self.instance = self.rm.open_resource(str(USB))
        # encoding necessary for getting waveform
        self.instance.encoding = "latin-1"
        #self.timeout = 250000

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def Reset(self):
        '''
        # Resets the Instrument...
        '''
        self.instance.write("*RST")

    def AutoSet(self,Options):
        '''
        # This command sets the vertical, horizontal, and trigger controls of the instrument to automatically acquire
        and display the selected waveform. This is equivalent to pressing the front panel AUTOSET button
        :Options: {EXECute|UNDo|VFields|VIDeo|VLines}
                EXECute autosets the displayed waveform
                UNDo returns the instrument to the setting prior to executing an autoset.
                VFields autosets the displayed waveform.
                VIDeo autosets the displayed waveform.
                VLines autosets the displayed waveform.
        '''
        cmd = "AUTOSet {}".format(Options)
        self.instance.write(cmd)

    def Clear(self):
        '''
        # Clear all acquisition, measurements
        '''
        self.instance.write("CLEAR ALL")

    def Get_Vertical_Scale(self, ch):
        '''
        Get vertical scale of MSO channel
        Args:
            ch: MSO channel

        Returns:
            vs: Vertical scale
        '''
        cmd = "CH{}?".format(ch)
        vs = float(self.instance.query(cmd).split(";")[10]) * 10 ** 3

        return vs

    def AcquisitionModesel(self,Acqmode):
        '''
        *******Acquisition Command**********
        # This sets the selected acquisition mode of the instrument.....
        Acqmode: {SAM|PEAK|HIR|AVE|WFMDB|ENV}
        '''
        cmd="ACQ:MOD {}".format(Acqmode)
        self.instance.write(cmd)


    def Display_Persistence(self,Persistence_mode):
        '''
        # This command sets or queries the persistence aspect of the display.....
        Persistence_mode: {OFF|INFPersist|VARpersist}
        '''
        cmd="DISplay:PERSistence {}".format(Persistence_mode)
        self.instance.write(cmd)

    def Cursor_Enable(self,en):
        '''
        # This sets the cursor state to be ON|OFF
        :param en: "ON"|"OFF" or it may be 0|1
        '''
        cmd="CURS:STATE {}".format(en)
        self.instance.write(cmd)

    def Cursor_Hbars(self, Ypos1,Ypos2):
        '''
        # This sets the horizantal bar cursor position relative to ground which is expressed in vertical units (usually Volts)
        :param Ypos1: 0.25 | 25E-3 (for 25mV) : Cursor1 of horizantal cursors
        :param Ypos2: -0.65 | -65E-3 (for -65mV) : Cursor2 of horizantal cursors
        '''
        cmd = "CURS:FUNC HBA;:CURS:HBA:POSITION1 {};:CURS:HBA:POSITION2 {}".format(Ypos1,Ypos2)
        self.instance.write(cmd)

    def Get_Cursor_HBarsDelta(self):
        '''
        # This returns the vertical difference between the two horizantal bar cursors
        '''
        cmd="CURS:HBA:DELT?"
        return self.instance.query(cmd)

    def Cursor_Vbars(self, Xpos1,Xpos2):
        '''
        # This sets the horizantal position for vertical bar cursors. (Units in s)
        :param Xpos1: 1E-6 (for 1us) : Cursor1 of horizantal cursors
        :param Xpos2: 9E-6 (for 9us) : Cursor2 of horizantal cursors
        '''
        cmd = "CURS:FUNC VBA;:CURS:VBA:POSITION1 {};:CURS:VBA:POSITION2 {}".format(Xpos1,Xpos2)
        self.instance.write(cmd)

    def Get_Cursor_VBarsDelta(self):
        '''
        # This returns the horizantal difference between the two vertical bar cursors
        '''
        cmd="CURS:VBA:DELT?"
        return self.instance.query(cmd)

    def Cursor_Screen(self, Xpos1,Xpos2,Ypos1,Ypos2):
        '''
        # This sets the X and Y positions of the specified SCREEN type cursor
        :param Xpos1: 15E-3 (15ms)
        :param Xpos2: 20E-3 (20ms)
        :param Ypos1: 20E-3 (20mV)
        :param Ypos2: 30E-3 (30mV)
        '''
        cmd = "CURS:FUNC SCREEN;:CURS:SCREEN:XPOSITION1 {};:CURS:SCREEN:XPOSITION2 {}" \
              ":CURS:SCREEN:YPOSITION1 {};:CURS:SCREEN:YPOSITION2 {}".format(Xpos1,Xpos2,Ypos1,Ypos2)
        self.instance.write(cmd)

    def Cursor_Source(self,curpos,source):
        '''
        # This sets the source for the currently selected cursor type
        :param curpos: 1|2 (for cursor1 or cursor2)
        :param source: CH1 - CH4 (sets an input channel waveform as the cursor source)
                       MATH1 - MATH4 (sets a math waveform as the cursor source)
                       REF1 - REF4 (sets a reference waveform as the cursor source)
        '''
        cmd="CURS:SOU{} {}".format(curpos,source)
        return self.instance.write(cmd)

    def Hist_State(self,en):
        '''
        # Sets the histogram calculations Enabled/Disabled
        :param en: ON | OFF or 0 | 1
        '''
        cmd = "HIS:STATE {}".format(en)
        self.instance.write(cmd)

    def Hist_Boxcoordinates(self,left,top,right,bottom):
        '''
        # It sets the left, top, right and bottom boundaries of the histogram box, in source waveform coordinates:
        :param left, top, right, bottom; Eg: 1E-9, 0.25, 2E-9, 0.5
        left, right - in time (sec); Top, bottom - in Volts (V)
        '''
        cmd = "HIS:BOX {},{},{},{}".format(left,top,right,bottom)
        self.instance.write(cmd)

    def Hist_Horizantal_Function(self,Size_div,source):
        '''
        # This sets the histogram type in Horizantal mode (Time distribution) and sets the size and source
        :param Size_div:  Can vary from 0.1 to 8.0 divisions in HORizontal mode
        :param source: CH1 | MATH1 |REF1 (similar for other 3 channels)
        '''
        cmd = "HIS:FUNC HOR; :HIS:SIZ {}; :HIS:SOU {} ".format(Size_div,source)
        self.instance.write(cmd)

    def Hist_Vertical_Function(self,Size_div,source):
        '''
        # This sets the histogram type in Vertical mode (Voltage distribution) and sets the size and source
        :param Size_div:  Can vary from 0.1 to 10.0 divisions in VERTical mode
        :param source: CH1 | MATH1 |REF1 (similar for other 3 channels)
        '''
        cmd = "HIS:FUNC VERT; :HIS:SIZ {}; :HIS:SOU {} ".format(Size_div,source)
        self.instance.write(cmd)

    def Horizantal_ScaleperDiv(self,secperdiv):
        '''
        # This sets the Horizantal scale in seconds per division
        :param secperdiv: Eg: 2E-9 (2ns per division)
        '''
        cmd = "HOR:MODE:SCALE {} ".format(secperdiv)
        self.instance.write(cmd)

    def Horizantal_Samplerate(self,Samplespersec):
        '''
        # This sets the sample rate in samples per second.
        :param Samplespersec: Eg: 1E6 (1million samples per second)
        '''
        cmd = "HOR:MODE:SAMPLER {} ".format(Samplespersec)
        self.instance.write(cmd)

    def Horizantal_Position(self,position):
        '''
        #This sets the position the waveform horizontally on the display
        :param position:  Eg: 10 (sets the trigger position of the waveform such that
        10.format( of the display is to the left of the trigger position.))
        '''
        cmd = "HOR:POS {} ".format(position)
        self.instance.write(cmd)

    def Math_Define(self, x, Mathexp):
        '''
        # This allows us to define new waveforms using mathematical expressions
        :param x: 1 | 2 | 3 | 4 (for defining functions to MATH1 to MATH4)
        :param Mathexp: Eg; CH1+CH2 or CH1-CH2 etc
        '''
        cmd = "MATH{}:DEF '{}' ".format(x, Mathexp)
        self.instance.write(cmd)

    # def Get_MeasResults(self,measnum,statistics):
    #     '''
    #     #It returns the measurement parameters for the displayed measurement specified by x, which can range from 1 through 8
    #     :param measnum: 1 to 8
    #     :param statistics: VALue | MEAN | MINI | MAX | STDdev | COUNt |
    #     '''
    #     # cmd = "MEASU:MEAS{}:{}?;:MEASU:MEAS{}:UNITS?".format(measnum, statistics, measnum)
    #     cmd = "MEASU:MEAS{}:{}?".format(measnum,statistics)
    #     return self.instance.query(cmd)

    def Get_MeasResults(self, measnum, statistics):
        cmd = "MEASU:MEAS{}:{}?".format(measnum, statistics)
        return self.instance.query(cmd)


    def Meas_Select(self,measnum,type,source,display):
        '''
        # It allows to select the measurement type from specified source and display on the front panel
        :param measnum: 1 to 8
        :param type: {AMPlitude|AREa|BURst|CARea|CMEan|CRMs|DELay|DISTDUty|EXTINCTDB|EXTINCTPCT|EXTINCTRATIO|EYEHeight|EYEWIdth|FALL|FREQuency|HIGH|HITs|LOW|
                    MAXimum|MEAN|MEDian|MINImum|NCROss|NDUty|NOVershoot|NWIdth|PBASe|PCROss|PCTCROss|PDUty|PEAKHits|PERIod|PHAse|PK2Pk|PKPKJitter|
                    PKPKNoise|POVershoot|PTOP|PWIdth|QFACtor|RISe|RMS|RMSJitter|RMSNoise|SIGMA1|SIGMA2|SIGMA3|SIXSigmajit|SNRatio|STDdev|UNDEFINED| WAVEFORMS}
        :param source: CH1-4 | MATH1-4 | REF1-4 | HISTogram
        :param display: ON | OFF or 0|1
        '''
        cmd= "MEASU:MEAS{}:TYP {};:MEASU:MEAS{}:SOU1 {}; :MEASU:MEAS{}:STATE {}".format(measnum,type,measnum,source,measnum,display)
        return self.instance.write(cmd)

    def AllMeas_Display(self,measnum):
        '''
        # It returns all measurement parameters for the displayed measurement specified by x, which can range from 1 through 8.
        '''
        #select the channel of power supply
        cmd = "MEASU:MEAS{}?".format(measnum)
        return self.instance.query(cmd)

    def Save_Setup(self,filepath):
        '''
         # This command stores the state of the instrument to a specified memory location.
        :param filepath: Accepts ".SET" format. Mention the target location for storing the setup file
        Default directory: C:\\Users\\Public\\Tektronix\\TekScope\\setups
        '''
        cmd = "SAVE:SETUP '{}'".format(filepath)
        self.instance.write(cmd)
        print("Setup saved in the path: C:\\Users\Tek_Local_Admin\\Tektronix\\TekScope\\Setups")

    def Save_Waveform(self,wfm,filecsvwfmtxt):
        '''
        # It saves a waveform to one of four reference memory locations or a file.
        :param wfm: CH<x> | MATH<x> | REF<x> | ALL | DIGITALALL
        :param filecsvwfmtxt: REF<x> | filepath <drive>/<dir>/<filename>.wfm|.txt|.csv|.dat extension
        '''
        cmd = "SAVE:WAVEFORM {},'{}'".format(wfm,filecsvwfmtxt)
        self.instance.write(cmd)
        print("Waveform saved in the path: C:\\Users\\Tek_Local_Admin\\Tektronix\\TekScope\\Waveforms")

    def Get_Waveform(self, source, start, stop):
        '''
        This command gets the source channel waveform given start and stop index
        :param source: CH<x> | | MATH<x> | REF<x> | ALL | DIGITALALL
        :param start: <x>
        :param stop: <x>
        :return: waveform
        '''
        cmd = "DATA:SOURCE CH{}".format(source)
        self.instance.write(cmd)
        # Set encoding
        cmd = "DATA:ENCDG RPBinary"
        self.instance.write(cmd)
        cmd = "DATA:START {}".format(start)
        self.instance.write(cmd)
        cmd = "DATA:STOP {}".format(stop)
        self.instance.write(cmd)
        cmd = "CURVE?"
        y1 = self.instance.query(cmd)
        # Convert unicode to int
        y2 = map(ord, y1)

        return list(y2)

    def Force_Trigger(self):
        '''
        This command forces a trigger event to occur.
        '''
        self.instance.write("TRIGGER FORCE")

    def TriggerType_Edge(self,TrigEvent,Source,Couplingtype,slopetype,level):
        '''
        # This command sets the EDGE type of A or B trigger and its parameters.
        :param TrigEvent: A|B
        :param Source: AUXiliary|CH<x>|LINE|D<x>
        :param Couplingtype: AC|DC|HFRej|LFRej|NOISErej|ATRIGger
        :param slopetype: RISe|FALL|EITher
        :param level: trigger level in volts Eg: 1.3 or 0.5
        '''
        cmd = "TRIGGER:{}:TYPE EDGE;:TRIGGER:{}:EDGE:SOURCE {};:TRIGGER:{}:EDGE:COUPLING {};:TRIGGER:{}:EDGE:SLOPE" \
              " {};:TRIGGER:{}:LEVEL {}".format(TrigEvent,TrigEvent,Source,TrigEvent,Couplingtype,TrigEvent,slopetype,TrigEvent,level)
        self.instance.write(cmd)

    def Vertical_Scale(self,Channelno,voltsperdiv,position,offset):
        '''
        # This command sets the vertical scale, position and offset for the specified channel
        :param Channelno: 1|2|3|4
        :param voltsperdiv: Vertical channel scale in units per division. Eg: 100E-03 (100mV per div)
        :param position: Vertical position for the specified channel in divisions ranging from 8 to -8.
        :param offset: Vertical offset for the specified channel in volts. Eg: 2E-3 (2mV)
        '''
        cmd = "CH{}:SCALE {};:CH{}:POSITION {};:CH{}:OFFSET {}".format(Channelno,voltsperdiv,Channelno,position,Channelno,offset)
        self.instance.write(cmd)

    def Display_Source(self,source,en):
        '''
        # This command sets the displayed state of the specified channel waveform.
        :param source: CH<x>
        :param en: ON|OFF
        '''
        cmd = "SELECT:{} {}".format(source,en)
        self.instance.write(cmd)

    def Export_Imagefile(self,filename):
        '''
        # This initiates export to the file specified by filename
        :param filename: Eg: "C:\\Users\\Public\\Tektronix\\TekScope\\Screen Captures\\TEK.BMP"
        '''
        cmd = "EXPORT:FILENAME '{}';:EXPORT START".format(filename)
        self.instance.write(cmd)

    def Run_Single(self):
        """
        Takes a single acquisition. Equivalent to pressing "Single" button on scope.
        """
        cmds = ["ACQUIRE:STOPAFTER SEQUENCE",
                "ACQUIRE:STATE 1"]
        for cmd in cmds:
            self.instance.write(cmd)

    def Run(self):
        """
        Begins continuously taking acquisitions. Equivalent to pressing "Run" button on scope.
        """
        cmds = ["ACQUIRE:STOPAFTER RUNSTop",
                "ACQUIRE:STATE 1"]
        for cmd in cmds:
            self.instance.write(cmd)

    def Get_Cursor_VBars(self):
        '''
        # This returns the horizontal values the two vertical bar cursors
        '''
        cmd="CURS:VBA?"
        return self.instance.query(cmd)

    def Get_Cursor_VBarsUnits(self):
        '''
        # This returns the horizontal units of the vertical bar cursors
        '''
        cmd="CURS:VBA:UNI?"
        return self.instance.query(cmd)

    def Measurement_Gating(self,en):
        '''
        # This enables or disables gating in the measurements
        '''
        cmd = "MEASU:GAT {}".format(en)
        return self.instance.write(cmd)

    def Set_TriggerLevel(self, level=None):
        '''
        # This sets the trigger level for A, if no level provided, trigger is set to 50%
        '''
        if not level:
            cmd = "TRIGGER:A SETLevel"
        else:
            cmd = "TRIGGER:A:Level {}".format(level)
        
        return self.instance.write(cmd)


######################## PLL Measurement Functions by Noelle ##############################

    def Cmd(self, cmd):
        """
        Executes all newline-separated command(s) in a string or commands in a list.
        """
        # run all command(s) in newline-separated string
        if isinstance(cmd, str):
            for c in cmd.split("\n"):
                self.instance.write(c.strip())

        # run all commands in list
        elif isinstance(cmd, list):
            for c in cmd:
                self.instance.write(c.strip())

    def PLL_Config(self, duration):                         # duration in ms
        """
        Configures scope to measure the ASIC2A PLL clock waveform for specified duration.
        :param duration: Length of acquisition in ms.
        """
        # connect to scope and apply settings
        self.Meas_Select(1, "AMP", "CH1", "ON")          # set measurement to channel 1
        self.Horizantal_Samplerate(10e9)                 # set sample rate to 10 GS/s
        self.AcquisitionModesel("SAM")                   # set acquisition type to Sample (real time (RT)?)
        cmds = ["CLEAR ALL",                                # clear all acquisitions, measurements, waveforms
                "SELECT:CH1 ON",                            # turn channel 1 on and all others off
                "SELECT:CH2 OFF",
                "SELECT:CH3 OFF",
                "SELECT:CH4 OFF",
                "CH1:LABel:NAMe 'Clock'",                   # label the waveform
                "CH1:COUPling DC",                          # use dc coupling
                "CH1:TERmination 50",                       # termination set to 50 ohms
                "TRIGGER:A:EDGE:SOURCE CH1",                # set trigger source to channel 1
                "TRIGger:A SETLevel",                       # autoset trigger to 50.format( of input signal)
                "HORIZONTAL:MODE MANUAL",                   # manually set sample rate and record length
                "ACQuire:SAMPlingmode RT",
                "HORizontal:MODE:SAMPLERate 10E9",
                "HORIZONTAL:MODE:RECORDLENGTH {}".format(duration * 10e6),  # set record length  (default: 15 million)
                "HORIZONTAL:MAIN:POSITION 0",               # display all of waveform to right of trigger
                "CH1:SCAle 200E-3",                         # set vertical scale to 200 mV/div
                "CH1:POSITION -2.5",                        # display input signal 2.5 div below center graticule
                "MEASUrement:MEAS2:STATE ON",
                "MEASUrement:MEAS2:TYPe FREQUENCY"]
        for cmd in cmds:
            self.instance.write(cmd)

    def LVDS_Config(self):
        """
        Configures scope to measure the ASIC2A LVDS clock waveform for specified duration.
        Expects LVDS+ on Channel 1 and LVDS- on Channel 2. Outputs differential signal on Math 1.
        """
        # connect to scope and apply settings
        self.Horizantal_Samplerate(20e9)                    # set sample rate to 20 GS/s
        self.AcquisitionModesel("SAM")                      # set acquisition type to Sample (real time (RT)?)
        cmds = ["CLEAR ALL",                                # clear all acquisitions, measurements, waveforms
                "SELECT:CH1 ON",                            # turn channel 1 on and all others off
                "SELECT:CH2 ON",
                "SELECT:CH3 OFF",
                "SELECT:CH4 OFF",
                "CH1:COUPling DC",                          # use dc coupling
                "CH2:COUPling DC",                          # use dc coupling
                "CH1:TERmination 50",                       # termination set to 50 ohms
                "CH2:TERmination 50",                       # termination set to 50 ohms
                "CH1:BANdwidth 2E9",
                "CH2:BANdwidth 2E9",
                "CH1:LABel:NAMe 'LVDS+'",                   # label the waveform
                "CH2:LABel:NAMe 'LVDS-'",                   # label the waveform
                "TRIGGER:A:EDGE:SOURCE MATH1",              # set trigger source to math 1
                "TRIGger:A SETLevel",                       # autoset trigger to 50.format( of input signal)
                "HORIZONTAL:MODE MANUAL",                   # manually set sample rate and record length
                "ACQuire:SAMPlingmode RT",
                "HORIZONTAL:MODE:RECORDLENGTH 1E3",         # lower record length to give room to raise sample rate
                "HORIZONTAL:MODE:SAMPLERate 50E9",
                "HORIZONTAL:MODE:RECORDLENGTH 50E6",        # then actually set record length
                "HORIZONTAL:MAIN:POSITION 0",               # display all of waveform to right of trigger
                "CH1:SCAle 30E-3",                          # set vertical scale to 30 mV/div
                "CH2:SCAle 30E-3",                          # set vertical scale to 30 mV/div
                "CH1:POSITION -4.7",                        # display input signal 3 div below center graticule
                "CH2:POSITION -4.4",                        # display input signal 3 div below center graticule
                "MATH1:DEFINE \"CH1-CH2\"",
                "MATH1:LABEL:NAME LVDS"
                "MATH1:VERTICAL:POSITION 520E-3",
                "MATH1:VERTICAL:SCALE 40E-3",
                "ACQUIRE:STATE RUN"]

        for cmd in cmds:
            self.instance.write(cmd)

        self.Meas_Select(1, "PK2Pk", "MATH1", "ON")
        self.Meas_Select(2, "AMP", "MATH1", "ON")
        self.Meas_Select(3, "FREQ", "MATH1", "ON")
        self.Meas_Select(4, "MEAN", "CH1", "ON")
        self.Meas_Select(5, "MEAN", "CH2", "ON")

    def DPOJET_Logging(self, on):
        """
        Turns continuous data logging on or off for selected DPOJET measurements.
        :param on: Logging on (True) or off (False)
        """
        if on:
            cmd = "DPOJET:LOGging:MEASurements: 1"
        else:
            cmd = "DPOJET:LOGging:MEASurements: 0"
        self.instance.write(cmd)

    def DPOJET_Run(self):
        """
        Run DPOJET measurements.
        """
        cmd = "DPOJET:STATE RUN"
        self.instance.write(cmd)

    def DPOJET_Wait_Done(self):
        """
        Sleep until DPOJET measurement complete.
        """
        cmd = "DPOJET:STATE?"
        status = None
        while status != "STOP":
            try:
                status = self.instance.query(cmd)
                status = status.strip()
                time.sleep(0.2)     # sleep for 0.2 s
            except pyvisa.errors.VisaIOError:
                print("### PyVisa Error Occurred. Continuing Test. ###")

    def Config_NPeriod(self, cycles):
        """
        Set up DPOJET for N-Period measurements at specified numbers of cycles.
        :param cycles: List of numbers of periods at which to make measurements.
               e.g. [1, 10, 20] ->  Set up to measure time for clock to make 1, 10, and 20 cycles.
        """
        # clear any current DPOJET measurements
        cmd = """DPOJET:CLEARALLMeas"""
        self.instance.write(cmd.strip())

        # add a DPOJET measurement for each number of periods
        for i, t in enumerate(cycles, 1):
            cmds = """DPOJET:ADDMeas NPERIOD
                      DPOJET:MEAS{0}:SOUrce1 CH1
                      DPOJET:MEAS{0}:SIGNALType CLOCK
                      DPOJET:MEAS{0}:EDGE1 RISE
                      DPOJET:MEAS{0}:N {1}
                      DPOJET:MEAS{0}:LOGging:MEASurements:SELect 1""".format(i, t)
            for cmd in cmds.split("\n"):
                self.instance.write(cmd.strip())

        # set to take one measurement of each type (i.e. # of periods) per scope acquisition
        cmds = """DPOJET:POPULATION:STATE 1
                  DPOJET:POPULATION:LIMITBY POPULATION
                  DPOJET:POPULATION:LIMIT 1
                  DPOJET:POPULATION:CONDition EACHmeas"""
        for cmd in cmds.split("\n"):
            self.instance.write(cmd.strip())

        # turn off scope logging since it creates a new file for each # periods, inefficient for this type of measurement
        # scope_logging(False)

        time.sleep(10)  # takes a while to set up measurement, don't overload scope in the meantime

    def Run_Single_NPeriod(self, cycles):
        """
        Make a single N-Period measurement for each of the specified numbers of cycles.
        :param cycles: List of numbers of periods at which to make measurements.
                    e.g. [1, 10, 20] ->  Measure time for clock to make 1, 10, and 20 cycles.
        :return acq: Data from the single acquisition: List of N-Period measurements.
                    e.g. [1e-6, 10e-6, 20e-6] -> Time for a 1 MHz clock to make 1, 10, and 20 cycles.
                         None -> Scope returned error value.
        """

        # clear any current measurement values
        cmd = """DPOJET:STATE CLEAR"""
        self.instance.write(cmd)

        # take a single acquisition and then compute measurements
        cmd = """DPOJET:STATE SINGLE"""
        self.instance.write(cmd)

        time.sleep(1)               # takes some time to make measurement
        self.DPOJET_Wait_Done()

        # read measurements off scope
        for timeout in range(3):    # try three times before timing out
            acq = []
            for i in range(cycles.size):
                cmd = "DPOJET:MEAS{}:RESULts:CURRentacq:MEAN?".format(i+1)
                a = self.instance.query(cmd).strip()
                acq.append(a)
            if float(acq[0]) != 9.91E+037:      # verify that we didn't get the error value
                break
            else:
                acq = None                      # if we did get the error value, return None
            time.sleep(0.2)

        return acq

    def Run_NPeriod(self, cycles, repeats, path):
        """
        Takes many N-Period measurements and stores the results in a.csv file.
        :param cycles: List of numbers of periods at which to make measurements.
                    e.g. [1, 10, 20] ->  Measure time for clock to make 1, 10, and 20 cycles.
        :param repeats: How many times to repeat the N-Period measurements.
        :param path: Path and filename at which to store measurements.
                    e.g. "C:\\Projects\\EElab\\Working\\NPeriod.csv"
        """
        start = time.time()

        acqs = []       # compile a list of measurements and write to file afterward bc get errors if write one by one
        cnt = 0
        while cnt < repeats:
            acq = self.Run_Single_NPeriod(cycles)
            if acq is not None:
                acqs.append(acq)
                logger.info("\t{}/{}: {},..., {}, {}".format(cnt + 1, repeats, acq[0], acq[-2], acq[-1]))
                cnt += 1

        with open(path, "w") as f:
            f.writelines([",".join(acq) + "\n" for acq in acqs])

        logger.info("N-Period measurement completed in {:.0f} min".format(time.time() - start) / 60)

    def Jitter_Decomp(self, reps, path):
        """
        Takes various jitter measurements and exports the results as a.mhtml report on the scope.
        :param reps: Number of times to repeat the jitter measurements.
        :param path: Filename and directory on scope to save the report.
        :return:
        """

        # set up DPOJET to use spectral and BUJ analysis and to limit population to specified # of reps
        cmds = """DPOJET:CLEARALLMeas
                DPOJET:JITtermodel BUJ
                DPOJET:MINBUJUI 10E3
                DPOJET:POPULATION:STATE 1
                DPOJET:POPULATION:LIMITBY POPULATION
                DPOJET:POPULATION:LIMIT {:d}
                DPOJET:POPULATION:CONDition LASTmeas""".format(reps)
        self.Cmd(cmds)

        # set up TIE, decomposition, and frequency measurements
        meas1 = ["DPOJET:ADDmeas TIE",
                "DPOJET:ADDmeas RJ",
                "DPOJET:ADDmeas DJ",
                "DPOJET:ADDmeas DJDirac",
                "DPOJET:ADDmeas NPJ",
                "DPOJET:ADDmeas PJ",
                "DPOJET:ADDmeas FREQ"]
        for i, m in enumerate(meas1, 1):
            self.Cmd(m)                                 # add each measurement
            cmds = """DPOJET:MEAS{0}:SOUrce1 MATH1
                    DPOJET:MEAS{0}:SIGNALType CLOCK
                    DPOJET:MEAS{0}:EDGE1 RISE
                    DPOJET:MEAS{0}:CLOCKRecovery:METHod CONSTMEAN
                    DPOJET:MEAS{0}:CLOCKRecovery:MEANAUTOCalculate EVERY""".format(i)
            self.Cmd(cmds)                              # configure each measurement

        # set up eye diagram measurements
        meas2 = ["DPOJET:ADDmeas HEIGHT",
                "DPOJET:ADDmeas WIDTH"]

        for i, m in enumerate(meas2, len(meas1) + 1):
            self.Cmd(m)                                 # add each measurement
            cmds = """DPOJET:MEAS{0}:SOUrce1 MATH1
                    DPOJET:MEAS{0}:SOUrce2 MATH 1
                    DPOJET:MEAS{0}:SIGNALType CLOCK
                    DPOJET:MEAS{0}:EDGE1 RISE
                    DPOJET:MEAS{0}:CLOCKRecovery:METHod EXPEDGE""".format(i)
            self.Cmd(cmds)                              # configure each measurement

        cmds = """DPOJET:CLEARALLPlots
                DPOJET:ADDPlot TIMEtrend, MEAS1
                DPOJET:ADDPlot SPECtrum, MEAS1
                DPOJET:ADDPlot EYE, MEAS8
                DPOJET:ADDPlot HISTOgram, MEAS1"""
        self.Cmd(cmds)

        time.sleep(1)

        # run measurements and wait until complete
        self.DPOJET_Run()
        time.sleep(5)
        self.DPOJET_Wait_Done()

        # generate and save report
        cmds = """DPOJET:REPORT:REPORTName \"{}\"
                DPOJET:REPORT:PLOTimages 1
                DPOJET:REPORT:PASSFailresults 0
                DPOJET:REPORT:DISPunits 0
                DPOJET:REPORT:DETailedresults 1
                DPOJET:REPORT:APPlicationconfig 1
                DPOJET:REPORT EXECute""".format(path)
        self.Cmd(cmds)

        time.sleep(5)

        cmds = """DPOJET:CLEARALLPlots
                DPOJET:CLEARALLMeas"""
        self.Cmd(cmds)

        time.sleep(5)

    def Config_Period_Stat(self, cycles):
        """
        Sets up DPOJET for N-Period measurements at specified numbers of cycles.
            NOTE: To run the measurements, next call Measure_Period_Stat().
        :param cycles: Number of clock periods over which to take measurements.
        """
        # clear any current DPOJET measurements
        cmds = """DPOJET:CLEARALLMeas
                  DPOJET:LOGging:MEASurements: 0"""
        self.Cmd(cmds)

        # add a DPOJET N-Period measurement, N=1
        cmds = """DPOJET:ADDMeas NPERIOD
                  DPOJET:MEAS1:SOUrce1 CH 1
                  DPOJET:MEAS1:SIGNALType CLOCK
                  DPOJET:MEAS1:EDGE1 RISE
                  DPOJET:MEAS1:N 1
                  DPOJET:MEAS1:EDGEIncre 1"""
        self.Cmd(cmds)

        # set to take the specified number of measurements in one DPOJET "Run"
        cmds = """DPOJET:POPULATION:STATE 1
                  DPOJET:POPULATION:LIMITBY POPULATION
                  DPOJET:POPULATION:LIMIT {:d}
                  DPOJET:POPULATION:CONDition EACHmeas""".format(cycles)
        self.Cmd(cmds)

        # takes a while to set up measurement, don't overload scope in the meantime
        time.sleep(2)

    def Measure_Period_Stat(self):
        """
        Find clock period statistics (mean and standard deviation) over the number of cycles
        set by Config_Period_Stat().
            NOTE: Must run Config_PeriodStat(cycles) before this function to set up measurements.
        :return mean, stdev: Mean and standard deviation of the clock period in seconds.
        """

        # clear any current measurement values
        cmd = """DPOJET:STATE CLEAR"""
        self.instance.write(cmd)

        # take a single acquisition and then compute measurements
        cmd = """DPOJET:STATE RUN"""
        self.instance.write(cmd)

        time.sleep(1)               # takes some time to make measurement
        self.DPOJET_Wait_Done()

        # read mean and stdev of period off scope
        for timeout in range(3):    # try three times before timing out
            cmd = "DPOJET:MEAS1:RESULts:ALLAcqs:MEAN?"
            mean = float(self.instance.query(cmd).strip())
            cmd = "DPOJET:MEAS1:RESULts:ALLAcqs:STDDev?"
            stdev = float(self.instance.query(cmd).strip())
            if mean != 9.91E+037 and stdev != 9.91E+037:            # verify that we didn't get an error value
                break
            else:
                mean = stdev = None                                 # if we did get the error value, return None
            time.sleep(0.2)

        return mean, stdev

    def DPOJET_NPeriod(self, cycles):
        """
        Set up DPOJET for N-Period measurements at specified numbers of cycles.
        :param cycles: List of numbers of periods at which to make measurements.
               e.g. [1, 10, 20] ->  Set up to measure time for clock to make 1, 10, and 20 cycles.
        """
        # clear any current DPOJET measurements
        cmd = """DPOJET:CLEARALLMeas"""
        self.instance.write(cmd.strip())

        # add a DPOJET measurement for each number of periods
        cmds = """DPOJET:ADDMeas NPERIOD
                DPOJET:MEAS:SOUrce1 CH1
                DPOJET:MEAS:SIGNALType CLOCK
                DPOJET:MEAS:EDGE1 RISE
                DPOJET:MEAS:N {}
                DPOJET:STATE CLEAR
                DPOJET:STATE RUN
                """.format(cycles)

        for cmd in cmds.split("\n"):
            self.instance.write(cmd.strip())
        time.sleep(10)



    def Get_Results_DPOJET_NPeriod(self, stats):
        '''
        :param stats: MEAN|STDDev|MIN etc...
        '''
        cmd = "DPOJET:MEAS:RESULts:ALLAcqs:{}?".format( stats)
        return self.instance.query(cmd)

    

if __name__ == "__main__":
    MSO = TekScope_5204B("TCPIP0::10.10.29.4::inst0::INSTR")
    print(MSO.GetInfo())
