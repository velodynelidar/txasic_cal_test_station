# $Id: AWG.py 2018-28-02 Nishitha $
# This drive is for AWG
# Model Number: Tektronix AWG 70001A Arbitrary Waveform Generator

import pyvisa


class AWG_70001A(object):

    def __init__(self, USB):

        # Initiate Ethernet instance
        self.rm = pyvisa.ResourceManager()
        self.instance = self.rm.open_resource(str(USB))
        #self.timeout = 250000

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def Reset(self):
        '''
        Resets the AWG Instrument
        '''
        self.instance.write("*RST")

    def Clock_SourceSel(self,source):
        '''
        Clock source selection:
        INT (Internal),
        EXT (External),
        EFIX (External Fixed),
        EVAR (External Variable)
        '''
        cmd = "AWGControl:CLOCK:SOURCE %s"%source
        self.instance.write(cmd)

    def AWG_Compile(self,equfilename):
        '''
        Accepts the .equ file
        compiles and imports the waveform to AWG
        '''
        cmd = "AWGControl:COMPILE '%s'"%equfilename
        self.instance.write(cmd)

    def RunMode(self,Modesel):
        '''
        Selects the Run mode for AWG for channel 1;
        Mode Selection is: CONT (Continous) or TRIG (Triggered)
        '''
        cmd = "AWGControl:RMODe %s"%Modesel
        self.instance.write(cmd)

    def Runstate(self):
        '''
        # Returns the run state for AWG:
        0-stopped;
        1-Waiting for Trigger;
        2-Running
        '''
        return self.instance.query("AWGControl:RSTate?")

    def AWG_Run(self):
        '''
        # Initiates the output waveform or sequence;
        Pushing play button on front-panel or display
        '''
        cmd = "AWGControl:RUN:IMMEDIATE"
        self.instance.write(cmd)

    def AWG_Stop(self):
        '''
        # This will stop the output waveform or sequence;
        '''
        cmd = "AWGControl:STOP:IMMEDIATE"
        self.instance.write(cmd)

    def AWG_Open_Setupfile(self,filepath):
        '''
        # Opens the setup file (.awgx) in to the AWG's setup memory
        '''
        cmd = "INST:MODE AWG;:AWGControl:SREStore '%s'"%filepath
        self.instance.write(cmd)

    def AWG_Save_Setupfile(self,filepath):
        '''
        # Save the AWG's setup file (.awgx) with waveforms
        '''
        cmd = "AWGControl:SSAVe '%s'"%filepath
        self.instance.write(cmd)

    def AWG_Wfm_Normalize(self,type):
        '''
        # This command sets if the imported data is normalized during select file format import operations.
        :param type: NONE|FSCale|ZREFerence
                    NONE - will not normalize the imported data. The data may contain points outside of the AWG's amplitude range
                    FSCale - normalizes the imported data to the full amplitude range
                    ZREF - normalizes the imported data while preserving the offset
        '''
        cmd = "MMEMORY:IMPORT:NORM %s"%type
        self.instance.write(cmd)

    def AWG_Wfm_Shift(self,wfm_name,phase):
        '''
        # This command sets if the imported data is normalized during select file format import operations.
        :param wfm_name: Name of the waveform Eg: AWGwaveform1pulse250mV
               phase - NR1 value Eg: 180 shifts the waveform if exists by 180 degrees
        '''
        cmd = "WLISt:WAVeform:SHIFt '%s',%d"%(wfm_name,phase)
        self.instance.write(cmd)

    def AWG_Importfile(self,Filepath,waveform):
        '''
        # This command imports the waveform (txt) file from the specified folder with 'normalizing while preserving the offset'
        and assigns to the channel
        :param Filepath: Eg: C:\\Users\\OEM\\Desktop\\Velodyne_Confidential_Data
        :param waveform: Eg: AWGwaveform_CFDexpt_REF6nS_20180124_155656990
        '''
        cmd1 = "MMEMORY:IMPORT '%s','%s\%s.txt',txt" %(waveform,Filepath,waveform)
        self.instance.write(cmd1)

    def AWG_LoadWaveform(self,wfmname):
        '''
        # Assigns the mentioned waveform from the Waveform list to specified channel
        Eg: AWGwaveform_CFDexpts_99x0_x210_20180202_134722307
        '''
        cmd = "SOURCE1:CASSET:WAVEFORM '%s'"%wfmname
        self.instance.write(cmd)

    def BWfm_TypeAmplitude(self,Type,Amp):
        '''
        # It Sets the Basic Waveform editor plug-in waveform Type and P-P Amplitude value
        :param Type: Sine | Square | Triangle | Ramp | Noise | DC
        :param Amp: (in Volts) Eg: 0.2 / 200E-3
        '''
        cmd = "BWAVEFORM:FUNC %s;:BWAVEFORM:AMPLITUDE %f"%(Type,Amp)
        self.instance.write(cmd)

    def BWfm_AutoCalculate(self,parameter):
        '''
        # It Sets the Basic Waveform editor plug-in Auto-calculate setting
        parameter: length | cycle | duration | frequency | sample rate
        '''
        cmd = "BWAVEFORM:AUTO %s"%parameter
        self.instance.write(cmd)

    def BWfm_Compileassign(self):
        '''
        #It will compile the waveform from Basic waveform editor and immediately assign it to a specified channel
        '''
        cmd = "BWAVeform:COMPile:CASSign 1;:BWAV:COMPILE"
        self.instance.write(cmd)

    def Clock_Samplerate(self,Srate):
        '''
        # Sets the sample rate for the clock
        :param Srate: (in S/s) Eg: 25E9
        range : 1.49kS/s to 50GS.
                When the clock source is set to External: 4times the External clock In freq
        '''
        cmd = "CLOCK:SRATE %f"%Srate
        self.instance.write(cmd)

    def Fgen_Freq_Amp(self,freq,amp):
        '''
        # Sets the Function generator's waveform frequency and Amplitude value
        :param freq: (in Hz) Range in 1Hz to 50MHz; Eg: 1.25E6
        :param amp: (in Volts) Eg: 0.5
        '''
        cmd = "INST:MODE FGEN;:FGEN:CHANNEL1:FREQ %f;:FGEN:CHANNEL1:AMPL %f"%(freq,amp)
        self.instance.write(cmd)

    def Fgen_SignalType(self, signaltype):
        '''
        # Sets the Function generator's waveform type (Shape)
        :param signaltype: SINE | SQUare | TRIangle | NOIse | DC | GAUSsian | EXPRise | EXPDecay | NONE
        '''
        cmd = "INST:MODE FGEN;:FGEN:CHANNEL1:TYPE %s" % signaltype
        self.instance.write(cmd)

    def Memory_Openawgxfile(self,awgxfile):
        '''
        # Restores the setup file from the filepath mentioned and supports only .awgx format
        :param awgxfile: "filepath" Eg: "C:\TestFiles\mySetup.awgx"
        '''
        cmd = "INST:MODE AWG;:MMEMORY:OPEN:SETUP '%s'"%awgxfile
        self.instance.write(cmd)

    def OUTPUT(self,en):
        '''
        # Enables/Disables the output state
        :param en:  0 | 1
        '''
        self.instance.write("OUTPUT %d"%en)

    def DAC_Resolution(self,noofbits):
        '''
        # Sets the DAC Resolution
        :param noofbits: 8 | 9 |10
        8 indicates 8bit DAC Resolution +2 marker bits
        9 indicates 9bit DAC Resolution +1 marker bit
        10 indicates 10bit DAC Resolution +0 marker bits
        '''
        cmd = "SOURCE1:DAC:RESOLUTION %d"%noofbits
        self.instance.write(cmd)

    def AWG_Marker_Amplitude(self,markersel,Amp):
        '''
        # Sets the marker voltage Amplitude of the specified marker of the specified channel
        :param markersel: 1 | 2
        :param Amp: (in Volts) Eg: 0.5 sets to 500mV
        '''
        cmd = "SOURCE1:MARKER%d:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE %f"%(markersel,Amp)
        self.instance.write(cmd)

    def AWG_Analog_Amplitude(self,Amp):
        '''
         # Sets the voltage Amplitude for the waveform associated with the channel
        :param Amp: in Volts Eg: 0.25 sets to 250mV
        '''
        cmd = "SOURCE1:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE %f"%Amp
        self.instance.write(cmd)

    def Trigger_Settings(self,Trigsel,impedance,interval,voltagelevel):
        '''
        # Generates the trigger A or B with the mentioned impedance, interval, voltage levels
        :param Trigsel: A | B (Sets the trigger A or B)
        :param impedance: 50 | 1000 (Sets External trigger impedance)
        :param interval: Range 1us to 10us Eg: 5E-6 (sets the internal trigger level to 5us)
        :param voltagelevel: (in Volts) Eg: 0.2 (sets the external trigger input level to 200mV)
        '''
        cmd = "TRIGGER:SOURCE EXT;:TRIGGER:IMMEDIATE %sTRIGGER;:TRIGGER:IMP %d;:TRIGGER:INTerval %f;:TRIGGER:LEVEL %f,%sTRIGGER"%(Trigsel,impedance,interval,voltagelevel,Trigsel)
        self.instance.write(cmd)

    def Trig_Sel(self,Sel_EXTorINT):
        '''
        # Sets the trigger source
        :param Sel_EXTorINT: EXT | INT
        EXTernal selects external trigger as the trigger source
        INTernal select internal interval timing as the trigger source
        '''
        cmd = "TRIGGER:SOURCE %s"%Sel_EXTorINT
        self.instance.write(cmd)

if __name__ == "__main__":

    AWG = AWG_70001A("TCPIP0::10.20.50.51::inst0::INSTR")
    print(AWG.GetInfo())
    #AWG.AWG_Open_Setupfile("C:\Users\OEM\Desktop\AWG_Waveform\AWGwaveform_CFDexpts.awgx")
    #help (AWG.Clock_SourceSel())

    '''
    rm = visa.ResourceManager()
    #print rm.list_resources()
    #('ASRL1::INSTR', 'ASRL2::INSTR', 'GPIB0::12::INSTR')
    #Tekscope MSO 5204B with IP Address: 10.20.50.52
    #inst=rm.open_resource("TCPIP0::tekscope-739500::inst0::INSTR")
    inst=rm.open_resource("TCPIP0::10.20.50.51::inst0::INSTR")
    #Tekscope MSO 5204B with Ethernet Socket Connection
    #inst=rm.open_resource("TCPIP0::10.20.50.52::4000::SOCKET",read_termination='\n')
    
    print inst.query("*IDN?")
    print inst.query("SOURCE1:VOLTAGE:AMPLITUDE?")
    print inst.query("OUTPUT?")
    '''