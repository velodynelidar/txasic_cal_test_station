# $Id: Temperature.py 2018-20-02 Nishitha $
# This drive is for Temperature Chamber_F4T Controller
# Model Number: TEST EQUITY MODEL 115A Temperature Chamber and F4T Touch Screen Controller

import pyvisa
import time


class Temperature(object):

    def __init__(self, USB):

        # Initiate Ethernet_TCPIP Socket instance
        self.rm = pyvisa.ResourceManager()
        #self.instance = self.rm.open_resource(str(USB),read_termination='\n',write_termination='\r\n')
        self.instance = self.rm.open_resource(str(USB), read_termination='\n')
        #self.timeout = 250000

    def close(self):
        self.instance.close()

    def GetInfo(self):
        return self.instance.query("*IDN?")

    def SetTemp_SP(self,data):
        '''
        # It is used to set the temperature
        :param data: Eg: 25.7; 103.45
        '''
        cmd = ":SOURCE:CLOOP1:SPOINT {}\n".format(data)
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def GetTemp_SP(self):
        '''
        # Get the Temperature Set Point value
        '''
        self.instance.write(":SOURCE:CLOOP1:SPOINT?")
        time.sleep(0.1)
        return self.instance.read('\n')

    def GetProcessTemp_PV(self):
        '''
        # Get the Actual Temperature value (Process value)
        '''
        cmd = ":SOURCE:CLOOP1:PVALUE?"
        return self.instance.query(cmd)

    def SetIdle_SP(self,data):
        '''
        # Set the Idle Set Point Temperature value
        :param data: Eg: 124.5
        '''
        cmd = ":SOURCE:CLOOP1:IDLE {}".format(data)
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def GetIdle_SP(self):
        '''
        # Get the Idle Set Point Temperature value
        '''
        cmd = ":SOURCE:CLOOP1:IDLE?"
        return self.instance.query(cmd)

    def GetTempUnits(self):
        '''
        # Get the Temperature Units C or F
        '''
        cmd = ":UNIT:TEMPERATURE?"
        return self.instance.query(cmd)

    def SetTempUnits(self,CF):
        '''
        # Set the Temperature Units C or F
        :param CF: C | F
        '''
        cmd = ":UNIT:TEMPERATURE {}".format(CF)
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def SetEventOutput(self,Sel):
        '''
        # Set the Event output ON/OFF
        :param Sel: ON | OFF
        '''
        cmd = ":OUTPUT1 {}".format(Sel)
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def GetEventOutput(self):
        '''
        # Get the Event output ON/OFF
        '''
        cmd = ":OUTPUT1?"
        return self.instance.query(cmd)

    def Set_RampOFF(self):
        '''
        # Set the ramping OFF; Controls instantly to set point
        '''
        cmd = ":SOURCE:CLOOP1:RACTION OFF"
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def Set_RampON_Startup(self):
        '''
        # Set the ramping ON with Startup; ramps to set point on controller power on
        '''
        cmd = ":SOURCE:CLOOP1:RACTION STARTUP"
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def Set_RampON_Setpoint(self):
        '''
        # Set the ramping ON with Setpoint; ramps to set point on change of set point
        '''
        cmd = ":SOURCE:CLOOP1:RACTION SETPOINT"
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def Set_RampON_Both(self):
        '''
        # Set the ramping ON with both Startup and Set point; ramps to set point on controller power on OR change of set point
        '''
        cmd = ":SOURCE:CLOOP1:RACTION BOTH"
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def Set_Ramp_Scale_rate(self,scale,rate):
        '''
        # Set the Ramping rate (which controller ramps to set point) and ramp rate is per minute/hour;
        :param scale: MINUTES | HOURS
        :param rate: Eg: 5 | 10
        '''
        cmd = ":SOURCE:CLOOP1:RSCALE {}; :SOURCE:CLOOP1:RRATE {}".format(scale,rate)
        self.instance.write(cmd)
        time.sleep(0.1)
        self.instance.read()

    def Get_Ramp_Rate(self):
        '''
         # Get the Ramping rate (which controller ramps to set point)
        '''
        cmd = ":SOURCE:CLOOP1:RRATE?"
        return self.instance.query(cmd)


if __name__ == "__main__":
    OVEN = Temperature('TCPIP::10.10.28.147::5025::SOCKET')

    # print(OVEN.GetInfo())
