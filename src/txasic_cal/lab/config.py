from txasic_cal.host import host

# Current lab instruments connected
# Updated on cal start based on host
CONNECTED_INSTRUMENTS = host.LAB_HOST_DETAILS[host.HOST_NAME]["lab"].get("CONNECTED_INSTRUMENTS")

# Measurements to be measured
MEASUREMENT_HEADER = ['Optical Power in mW', #'Optical Power Scale Factor', 
                      'Pulse Width(ns)', 'Pulse Width STD(ns)',
                      'Pulse Amplitude(mV)', 'Pulse Amplitude STD(mV)', 'Oven Temperature(C)', 
                    #   'Gonio_X_OP_Pos', 'Gonio_Y_OP_Pos', 'Gonio_X_Amp_Pos', 'Gonio_Y_Amp_Pos', 'Gonio_Cal_Time', 
                      'Pulse', 'MSO Vertical Scale(mV)', 'Power Supply Voltage(V)', 'Power Supply Current(A)',
                      'Power_Meter_Attenuation_Factor']

# Scale factor for optical power measurements
OP_SCALE_FACTOR = 1

# Pulse width measurement position on scope
MEAS_PW_POS = 1
MEAS_PW_SETTING = 'MEAN'

# Pulse amplitude measurement on scope
MEAS_PA_POS = 2
MEAS_PA_SETTING = 'MEAN'

# Pulse channel on scope
PULSE_CH = 1

# Lab Instrument sleep time
SLEEP_TIME = 0.5

# Power Supply CH
POWER_SUPPLY_CH = 'CH1'
