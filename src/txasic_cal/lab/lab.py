#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Top level lab software for controlling lab instruments

import math
import time
import sys

from txasic_cal.tools.logger import logger

from txasic_cal.lab import instruments

from txasic_cal.lab import config
from txasic_cal.pfmea_code import pfmea_table

class lab:
    def __init__(self):
        """Declaration of all the equipment related variables that can be used 
        anywhere to control instruments
        """
        self.mso = None
        self.pm = None
        self.dmm = None
        self.power_supply = None
        self.oven = None
        self.gonio = None

        self.measurement_header = config.MEASUREMENT_HEADER

        self.current_measurements = {i:None for i in self.measurement_header}

        # TODO: Make connected instruments platform dependent
        self.connected_instruments = config.CONNECTED_INSTRUMENTS

        # TODO: Read from table
        # self.optical_power_scale_factor = [4.132, 4.343, 4.1, 4.347, 4.122, 4.374, 4.174, 6.216]
        # self.optical_power_scale_factor = [3.67, 3.67, 3.67, 3.67, 3.67, 3.67, 3.67, 5.21]
        # TODO: Read from power meter

    def __del__(self):
        self.close()

    def close(self):
        if self.mso:
            self.mso.instance.close()
            self.mso = None
        if self.pm:
            self.pm.instance.close()
            self.pm = None
        if self.dmm:
            self.dmm.instance.close()
            self.dmm = None
        if self.oven:
            self.oven.close()
            self.oven = None
        if self.gonio:
            self.gonio.close()
            self.gonio = None
        if self.power_supply:
            self.power_supply.instance.close()
            self.power_supply = None

    def init(self):
        self.connect_instruments()

        if self.pm:
            # Set power meter to 905nm wavelength
            logger.debug("Set power meter wavelength to 905nm.")
            self.pm.set_wavelength(905)
        else:
            logger.error("No power meter connected.")
            pfmea_table.pfmea_filter.error('SETUP_CAL_PMDISCONNECT')
            logger.input("Hit enter to continue.")

        if self.mso:
            # Set horizontal to 5ns
            self.mso.Horizantal_ScaleperDiv(5e-9)

            # Turn on averaging
            self.mso.AcquisitionModesel('AVE')

            # Set measurements
            self.mso.Meas_Select(1, 'PWIdth', 'CH1', 'ON')
            self.mso.Meas_Select(2, 'AMP', 'CH1', 'ON')

            self.mso.Clear()

    def connect_instruments(self):
        """Setup and configure bench (all the equipment connected on this bench)
        """
        logger.debug("Connecting to instruments.")

        if 'Scope' in self.connected_instruments:
            try:
                self.mso = instruments.TekScope_5204B(instruments.SCOPE)
                logger.debug(str(self.mso.GetInfo()))
            except Exception as e:
                logger.warn("Scope not connected {}".format(e))
                self.mso = None
        if 'Power Meter' in self.connected_instruments:
            try:
                self.pm = instruments.Thorlabs_PM200(instruments.POWERMETER)
                logger.debug(str(self.pm.GetInfo()))
            except Exception as e:
                logger.error("Power meter not connected {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_CAL_PMDISCONNECT')
                logger.error("Restart calibration. Exiting.")
                sys.exit()
        if 'Oven' in self.connected_instruments:
            try:
                self.oven = instruments.Temperature(instruments.OVEN)
                logger.debug(str(self.oven.GetInfo()))
            except Exception as e:
                logger.error("Oven not connected {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_PRE_TEMPCHAMBERDISCONNECT')
                logger.error("Restart calibration. Exiting.")
                sys.exit()
        if 'Template' in self.connected_instruments:
            try:
                self.oven = instruments.TempPlate(instruments.TEMPLATE)
                logger.debug(instruments.TEMPLATE)
            except Exception as e:
                logger.warn("Temperature Plate not connected {}".format(e))
                self.oven = None
        if 'Gonio' in self.connected_instruments:
            try:
                self.gonio = instruments.Gonio()
                if self.gonio.init() < 0:
                    self.gonio = None
                    pass
                self.gonio.lab_connect(self)
            except Exception as e:
                logger.warn("Gonio not connected {}".format(e))
                self.gonio = None
        if 'Power Supply' in self.connected_instruments:
            try:
                self.power_supply = instruments.Keithley2230(instruments.POWER_SUPPLY)
                logger.debug(str(self.power_supply.GetInfo()))
            except Exception as e:
                logger.warn("Power supply not connected {}".format(e))
                self.power_supply = None

    def measure(self, *args, **kwargs):
        """Measure data from lab equipments
        Measurement name must match column header

        Returns:
            [dict] -- [Measurement name]
        """
        if kwargs['measurement'] == 'Optical Power in mW':
            # Measured in mW and scaled if required
            self.current_measurements['Optical Power in mW'] = self.optical_power(args[0]['Channel'])

            return self.current_measurements['Optical Power in mW']
        elif kwargs['measurement'] == 'Pulse Width(ns)':
            try:
                # Measured in ns, makes sure the MEAS_PW_POS position correctly matches the scope
                if self.gonio:
                    # Move to amp position
                    self.gonio.move_to_position(
                        self.gonio.chan_amp_pos['x'][args[0]['Channel']],
                        self.gonio.chan_amp_pos['y'][args[0]['Channel']])
                    self.mso_autoset()
                self.current_measurements['Pulse Width(ns)'] = self.pulse_width()
            except Exception as e:
                if self.mso:
                    logger.warn("Pulse Width(ns) failed.")
                    self.current_measurements['Pulse Width(ns)'] = None
            
            return self.current_measurements['Pulse Width(ns)']
        elif kwargs['measurement'] == 'Pulse Width STD(ns)':
            try:
                self.current_measurements[kwargs['measurement']] = round(abs((float(
                    self.mso.Get_MeasResults(config.MEAS_PW_POS, "STD").split(';')[0]) * 10 ** 9)), 3)
            except Exception as e:
                if self.mso:
                    logger.warn("Pulse Width STD(ns) failed.")
                    self.current_measurements[kwargs['measurement']] = None
            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Pulse Amplitude(mV)':
            try:
                # Measured in mV
                self.current_measurements['Pulse Amplitude(mV)'] = self.pulse_amp()
            except Exception as e:
                if self.mso:
                    logger.warn("Pulse Amplitude(mV) failed.")
                    self.current_measurements['Pulse Amplitude(mV)'] = None
            
            return self.current_measurements['Pulse Amplitude(mV)']
        elif kwargs['measurement'] == 'Pulse Amplitude STD(mV)':
            try:
                self.current_measurements[kwargs['measurement']] = abs((float(
                    self.mso.Get_MeasResults(config.MEAS_PA_POS, "STD").split(';')[0]) * 10 ** 3))
            except Exception as e:
                if self.mso:
                    logger.warn("Pulse Amplitude STD(mV) failed.")
                    self.current_measurements[kwargs['measurement']] = None
            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Pulse':
            try:
                # Get pulse waveform from scope
                self.current_measurements[kwargs['measurement']] = self.mso.Get_Waveform(config.PULSE_CH, '0', '1000')[6:1000]
            except Exception as e:
                if self.mso:
                    logger.warn("Pulse failed.")
                    self.current_measurements[kwargs['measurement']] = None
            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'MSO Vertical Scale(mV)':
            try:
                # Get current vertical scale
                self.current_measurements[kwargs['measurement']] = self.mso.Get_Vertical_Scale(config.PULSE_CH)
            except Exception as e:
                if self.mso:
                    logger.warn("MSO Vertical Scale(mV) failed.")
                    self.current_measurements[kwargs['measurement']] = None
            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Oven Temperature(C)':
            # Get oven temperature
            self.current_measurements[kwargs['measurement']] = self.get_oven_temperature()
            
            return self.current_measurements[kwargs['measurement']]
        # elif kwargs['measurement'] == 'Optical Power Scale Factor':
        #     self.current_measurements[kwargs['measurement']] = self.optical_power_scale_factor[args[0]['Channel']]

        #     return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Power Supply Voltage(V)':
            try:
                self.current_measurements[kwargs['measurement']] = float(self.power_supply.GetMeasVolt(config.POWER_SUPPLY_CH))
            except Exception as e:
                if self.power_supply:
                    logger.warn("GetMeasVolt failed {}".format(e))
                    self.current_measurements[kwargs['measurement']] = None

            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Power Supply Current(A)':
            try:
                self.current_measurements[kwargs['measurement']] = float(self.power_supply.GetMeasCurr(config.POWER_SUPPLY_CH))
            except Exception as e:
                if self.power_supply:
                    logger.warn("GetMeasVolt failed {}".format(e))
                    self.current_measurements[kwargs['measurement']] = None

            return self.current_measurements[kwargs['measurement']]
        elif kwargs['measurement'] == 'Power_Meter_Attenuation_Factor':
            try:
                self.current_measurements[kwargs['measurement']] = float(self.pm.get_attenuation_factor())
            except Exception as e:
                logger.warn("Get power meter attenuation factor failed {}".format(e))
                self.current_measurements[kwargs['measurement']] = None

            return self.current_measurements[kwargs['measurement']]
        elif self.gonio:
            return self.gonio.populate_cur_row(*args, **kwargs)


    def set_input(self, *args, **kwargs):
        """Set lab inputs
        """
        if kwargs['result'] == 'MSO Vertical Scale(mV)':
            self.mso.Vertical_Scale(args[0], args[1], -8, 0)
        elif kwargs['result'] == 'Oven Set Temperature(C)':
            self.set_temperature(args[0])

    def optical_power(self, cur_ch=0):
        """Get optical power readout from power meter

        Keyword Arguments:
            cur_ch {int} -- [Channel used for scale factor] (default: {0})

        Returns:
            [float] -- [Measured optical power value]
        """
        optical_power = 0.0
        avg_cnt = 3
        for i in range(avg_cnt):
            # if self.gonio:
            #     config.OP_SCALE_FACTOR = self.optical_power_scale_factor[cur_ch]
            while True:
                try:
                    optical_power += round((abs(self.pm.Meas_Pow()) * 10 ** 3)
                                   * config.OP_SCALE_FACTOR, 3)
                except Exception as e:
                    logger.error("Power meter disconnected {}".format(e))
                    pfmea_table.pfmea_filter.error('SETUP_CAL_PMDISCONNECT')
                    logger.input("Check power meter and hit enter to continue.")
                    continue
                break
        optical_power /= avg_cnt
        optical_power = round(optical_power, 3)
        logger.debug("Optical Power in mW: {}".format(optical_power))
        return optical_power

    def pulse_width(self, cur_ch=0):
        """Get pulse width readout from scope

        Keyword Arguments:
            cur_ch {int} -- [Currently not used] (default: {0})

        Returns:
            [float] -- [Measured pulse width value]
        """
        pulse_width = 0.0
        avg_cnt = 3
        if self.mso == None:
            return pulse_width
        for i in range(avg_cnt):
            pulse_width += round(abs((float(self.mso.Get_MeasResults(
                config.MEAS_PW_POS, config.MEAS_PW_SETTING).split(';')[0]) * 10 ** 9)), 3)

        pulse_width /= avg_cnt
        pulse_width = round(pulse_width, 3)
        logger.debug("Pulse Width(ns): {}".format(pulse_width))
        return pulse_width

    def pulse_amp(self, cur_ch=0):
        """Get pulse amplitude readout from scope

        Keyword Arguments:
            cur_ch {int} -- [Currently not used] (default: {0})

        Returns:
            [float] -- [Measured pulse amplitude value]
        """
        pulse_amp = 0.0
        avg_cnt = 3
        if self.mso == None:
            return pulse_amp
        for i in range(avg_cnt):
            pulse_amp += round(abs((float(self.mso.Get_MeasResults(
                config.MEAS_PA_POS, config.MEAS_PA_SETTING).split(';')[0]) * 10 ** 3)), 3)

        pulse_amp /= avg_cnt
        pulse_amp = round(pulse_amp, 3)
        logger.debug("Pulse Amplitude(mV): {}".format(pulse_amp))
        return pulse_amp

    def get_oven_temperature(self):
        """Get current oven temperature

        Returns:
            [float]: Get current oven temperature in float, return None if oven not connected
        """
        oven_temp = None
        if 'Oven' in self.connected_instruments:
            while True:
                try:
                    oven_temp = float(self.oven.GetProcessTemp_PV())
                except Exception as e:
                    logger.error("Oven disconnected {}".format(e))
                    pfmea_table.pfmea_filter.error('SETUP_CAL_TEMPCHAMBERDISCONNECT')
                    logger.input("Check oven and hit enter to continue.")
                    continue
                logger.debug("Oven Temperature: {}".format(oven_temp))
                break
        
        return oven_temp

    def set_oven_temperature_sp(self, cur_sp):
        """Set current oven temperature setpoint

        Args:
            cur_sp ([float]): Set temperature chamber setpoint between -40 to 125 C
        """
        if -40 <= cur_sp <= 125:
            if 'Oven' in self.connected_instruments:
                while True:
                    try:
                        logger.debug("Setting to temperature {}".format(cur_sp))
                        self.oven.SetTemp_SP(cur_sp)
                    except Exception as e:
                        logger.error("Oven disconnected {}".format(e))
                        pfmea_table.pfmea_filter.error('SETUP_CAL_TEMPCHAMBERDISCONNECT')
                        logger.input("Check oven and hit enter to continue.")
                        continue
                    break
        else:
            logger.error("Enter valid temperature chamber setpoint [-40 - 125]")

    def set_temperature(self, temperature_setpoint=None):
        """Set oven temperature with soak time

        Keyword Arguments:
            temperature_setpoint {float} -- [Oven temperature to be set to] (default: {None})

        Return:
            wait_time {float} -- Calculated soak time given setpoint
            temp_set_counter {int} -- Counter for number of iterations to reach setpoint
        """
        if temperature_setpoint is None:
            logger.error("Please provide temperature setpoint.")
            return None, None
        if temperature_setpoint < -40:
            logger.error("Temperature setpoint below -40C")
            return None, None
        if temperature_setpoint > 85:
            logger.error("Temperature setpoint above 125C")
            return None, None

        # Nishitha's work from EElab
        # Soak time calculation - Assuming 1 min soak time per 10 degree change
        logger.debug("Setting to temperature {}".format(temperature_setpoint))
        current_temp = self.get_oven_temperature()
        temp_diff = round(math.fabs(current_temp - temperature_setpoint), 2)
        wait_time = round(temp_diff / 10, 2)

        i = 0
        temp_set_counter = 0
        if temp_diff < 1.0:
            logger.debug("Temperature already set.")
        else:
            self.set_oven_temperature_sp(temperature_setpoint)
            while i < 3:
                time.sleep(10)
                current_temp = self.get_oven_temperature()
                logger.debug("Current temperature value {}".format(current_temp))
                temp_diff = round(math.fabs(current_temp - temperature_setpoint), 2)
                logger.debug("Current temp diff {}".format(temp_diff))

                # If temperature is within 1 C, iterate for 3 times to ensure and exit loop
                if temp_diff < 1.0:
                    logger.debug("Near to the desired temperature {}".format(temperature_setpoint))
                    i = i + 1
                else:
                    logger.debug("Desired temperature {} not reached with difference {} and counter {}"\
                        .format(temperature_setpoint, temp_diff, temp_set_counter))
                    i = 0

                    '''If temperatue set counter crosses 3000 iterations, the temperature chamber is stuck at a temperature
                    and needs to be debugged, restarting temperature chamber usually fixes this issue, seems to be a bug with
                    the temperature chamber as the setpoint is correctly set'''
                    if temp_set_counter > 3000:
                        logger.error("Temperature chamber has not reached setpoint {} in {} iterations."\
                            .format(temperature_setpoint, temp_set_counter))
                        pfmea_table.pfmea_filter.error('SETUP_CAL_TEMPCHAMBERRESPONSE')
                        logger.input("Debug temperature chamber and hit enter to continue.")
                        temp_set_counter = 0

                    '''The temperature loop can get stuck because the ramp response of the temperature chamber 
                    stops if it thinks it's close enough to the setpoint (32 for a setpoint of 30) 
                    but that might not be good enough for us, to prevent this slightly lower or increase setpoint
                    to restart the ramp at every 500 iterations'''
                    if temp_set_counter != 0 and (temp_set_counter % 500) == 0:
                        if current_temp > temperature_setpoint:
                            new_setpoint = temperature_setpoint - 1.0
                        else:
                            new_setpoint = temperature_setpoint + 1.0
                        logger.warning("Stuck at temperature {} for {} iterations. Setting temperature to {}"\
                            .format(current_temp, temp_set_counter, new_setpoint))
                        self.set_oven_temperature_sp(new_setpoint)
                        time.sleep(10)

                    temp_set_counter += 1

            logger.debug("Temperature is at desired temperature {}".format(current_temp))
            logger.debug(f"Soak wait time for system {wait_time + 5} mins ({wait_time} (1 minute soak time per 10 C change) + {5} (extra minutes))")
            time.sleep((wait_time * 60) + (5 * 60))

        return wait_time, temp_set_counter

    def sphere_peak_power(self, prf):
        """Compute sphere peak power as pulse amplitude / (pulse width * prf)

        Pulse amplitude is used as a subsitutue for optical power (Sphere amplitude closely tracks optical power)

        Arguments:
            prf {float} -- Pulse repetition frequency

        Returns:
            {float} -- Peak power
        """
        try:
            peak_power = round((self.pulse_amp() * 1e-3) / (self.pulse_width() * 1e-9 * prf * 1e3), 3)
        except (ZeroDivisionError, TypeError, AttributeError):
            logger.warn("Zero division/Type/Attribute error for peak power calculation. Setting peak power to 0.")
            peak_power = 0.0

        logger.debug("Peak Power: {}".format(peak_power))
        
        return peak_power

    def populate_cur_row(self, results_row=None, display=True):
        """Populate current row for results csv

        Keyword Arguments:
            results_row {dict} -- [Current row] (default: {None})
            display {bool} -- [Display results] (default: {True})
        """
        if results_row == None:
            logger.error("Please provide results_row")
            return

        logger.debug("********Lab Results Start********")

        # Clear scope readings before collecting scope data
        if self.mso:
            self.mso.Clear()

        results_row.update({i:self.measure(results_row, measurement=i) for i in self.measurement_header})

        if self.gonio:
            # Move to op position once done with pulse measurements
            self.gonio.move_to_position(
                self.gonio.chan_op_pos['x'][results_row['Channel']],
                self.gonio.chan_op_pos['y'][results_row['Channel']])
            self.mso_autoset()

        if display:
            for key, value in enumerate(self.current_measurements):
                logger.info("{}={}".format(value, self.current_measurements[value]))

        logger.debug("********Lab Results End********")

    def mso_autoset(self):
        """Autoset the scope so pulse is not saturating
        """
        cur_vp = self.mso.Get_Vertical_Scale(config.PULSE_CH)
        pulse_amp = self.pulse_amp()

        # Times 5 because autoset puts the pulse at the middle of the scope
        # if (cur_vp * 3) < pulse_amp or pulse_amp < 5.0:
        # if (cur_vp * 3) < pulse_amp or pulse_amp < 1.0:
        logger.debug("Autoset MSO as pulse amp {} and vertical scale {}".format(pulse_amp, cur_vp * 3))
        # time.sleep(1)
        self.mso.AutoSet("EXEC")
        # time.sleep(1)
        self.mso.Horizantal_ScaleperDiv(10e-9)
        # self.mso.Clear()
        time.sleep(config.SLEEP_TIME + 3.0)
        # print(cur_vp, self.mso.Get_Vertical_Scale(config.PULSE_CH))
        # print(pulse_amp, self.pulse_amp())

    def mso_vertical_level(self, channelno, voltsperdiv, position, offset, trigger_level=None):
        """Set MSO vertical level and trigger level

        Arguments:
            channelno {int}: 1|2|3|4
            voltsperdiv {float}: Vertical channel scale in units per division. Eg: 100E-03 (100mV per div)
            position {int}: Vertical position for the specified channel in divisions ranging from 8 to -8.
            offset {float}: Vertical offset for the specified channel in volts. Eg: 2E-3 (2mV)

        Keyword Argument:
            trigger_level {int} : Level position, default : {None}
        """
        if self.mso:
            self.mso.Vertical_Scale(channelno, voltsperdiv, position, offset)
            self.mso.Set_TriggerLevel(trigger_level)

    def toggle_power_supply(self):
        """Toggle power supply
        """
        if self.power_supply:
            cur_ps_state = int(self.power_supply.GetOutput())
            self.power_supply.SetOutput(not cur_ps_state)

            time.sleep(1)
            logger.debug("Power supply ch 1 voltage {}".format(self.power_supply.GetMeasVolt('CH1')))
            logger.debug("Power supply ch 1 current {}".format(self.power_supply.GetMeasCurr('CH1')))
        else:
            logger.debug("Power supply not connected. Can not toggle.")

if __name__ == "__main__":
    l = lab()
    l.init()