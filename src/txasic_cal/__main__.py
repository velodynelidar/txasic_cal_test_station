#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Top level software for starting TxCal

import time
import datetime
import argparse
import pathlib

from txasic_cal.host import host
from txasic_cal.calibration import calibration
from txasic_cal.calibration import post_calibration
from txasic_cal.calibration import pre_calibration
from txasic_cal.sensor import sensor, SENSOR_MODEL, SENSOR_MODEL_FW_VERSION
from txasic_cal.lab import lab
from txasic_cal.tools import file_io

from txasic_cal.tools.logger import logger
from txasic_cal.tools import interpolate_table

from . import __version__

from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from dataclasses import dataclass

@dataclass
class cal_module_args:
    # CLI arguments
    unit: str = None
    # Type of cal - (IS/TS)
    cal_type: str = None           
    cal_version_num: str = None
    cal_file_name: str = None
    cal_hv: str = None              
    interp_table: str = None
    vcsel_cal: bool = False
    post_txcal: bool = False
    stability_test: bool = False
    live_pl_test: bool = False
    channel: int = None
    power_level: int = None
    oven_set_temperature: int = None

    # Possible run-time arguments
    _sensor_family: str = ''
    _sensor_name: str = ''

    _sensor_module: str = _sensor_family + "." + _sensor_name

    @property
    def sensor_family(self) -> str:
        return self._sensor_family

    @property
    def sensor_name(self) -> str:
        return self._sensor_name

    @property
    def sensor_module(self) -> str:
        return self._sensor_module

    @sensor_family.setter
    def sensor_family(self, val) -> None:
        self._sensor_family = val
        self._sensor_module = self._sensor_family + "." + self._sensor_name

    @sensor_name.setter
    def sensor_name(self, val) -> None:
        self._sensor_name = val
        self._sensor_module = self._sensor_family + "." + self._sensor_name

class cal_module(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)
    msg_sig = pyqtSignal(str, str)
    input_sig = pyqtSignal(str)
    post_cal_sig = pyqtSignal(pathlib.Path)

    def __init__(self, cm_args: cal_module_args = cal_module_args(), gui: bool = False) -> None:
        """Cal module to parse arguments, decide sensor and calibration choice

        Args:
            cal_module_args (optional): Cal module args that satisfies CLI choices.
            gui (bool, optional): If called through GUI. Defaults to False.
        """
        self.cm_args = cm_args

        self._sensor = None
        self._lab = None
        self._cal = None

        self._gui = gui

        super().__init__()

        # If using GUI, set logger/input message signal to pass to GUI
        if gui:
            logger.msg_sig = self.msg_sig
            logger.input_sig = self.input_sig
        
    @pyqtSlot()
    def txcal(self) -> None:
        """Run TxCal given input options
        """
        unit_name = "P20"
        version = "IS"

        cm_args = self.cm_args
        sensor_module = self.cm_args.sensor_module

        if(cm_args.unit):
            unit_name = cm_args.unit.upper()

        if(cm_args.version):
            version = cm_args.version.upper()

        if(cm_args.version_num):
            version_num = cm_args.version_num

        if(not cm_args.file):
            file_name = unit_name + "_" + version + "_" + "power_level_table_" + \
                        datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".csv"
        else:
            file_name = cm_args.file

        self.msg_sig.emit('file_input', file_name)

        hv_setpoint = None
        if (cm_args.hv):
            hv_setpoint = cm_args.hv.split(",")

        # Create log file with same name as csv file
        logger.log_file_setup(filename=file_name[:-3] + 'log')

        # Cal file used for interpolation verification
        cal_file_name = r"~/txasic_cal/results/B111_TS_power_level_table_200929173722.csv"
        logger.info("Starting calibration for {} unit {} with version {}, software version {} and host {}".format(sensor_module,
                                                                                unit_name, version, __version__, host.HOST_NAME))
        self._sensor = sensor.sensor(sensor_module=sensor_module, version=version)
        self._lab = lab.lab()
        _file_io = file_io.file_io(file_name)
        _file_io.file_open()
        self._cal = calibration.tx_calibration(sensor=self._sensor, lab=self._lab, file_io=_file_io, \
            unit=unit_name, version=version, version_num=version_num, hv_setpoint=hv_setpoint)

        # Cal init
        self._cal.init()

        # Pre-cal check
        _pre_cal = pre_calibration.pre_calibration(self._cal)
        _pre_cal.pre_cal_setup_checks()
        _pre_cal.pre_cal_unit_checks()

        self._cal.run(cal_file_name)
        
        _file_io.file_close()
        self._lab.close()
        self._sensor.sensor_module.close()

        if self._cal.stop:
            return

        file_upload_name = str(_file_io.file_name)

        # Interpolate table post completion of TxCal
        if cm_args.interp_table:
            if cm_args.interp_table != True:
                cal_file_name = cm_args.interp_table
            else:
                if version == "TS":
                    cal_file_name = _file_io.file_name
                
            file_name = unit_name + "_" + version + "_" + "interp_power_level_table_" + \
                        datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".csv"

            logger.info("Creating interpolated table {}".format(file_name))

            self.msg_sig.emit('file_input', file_name)

            _interp_file_io = file_io.file_io(file_name)
            _interp_file_io.file_open()
            _intrep = interpolate_table.interpolate(self._sensor, self._lab, _interp_file_io, unit_name, version)
            _intrep.init(cal_file_name)

            temp_list = [-40, 0, 40, 85]
            _intrep.linear_interpolation(-40, 125, 34, 5, 30, temp_list)
            _interp_file_io.file_close()

            file_upload_name = str(_interp_file_io.file_name)

        # Verify table
        if version in ('IS', 'TS'):
            if self._gui:
                # Run post txcal verify from main thread as matplotlib needs to be run from main thread
                self.post_cal_sig.emit(_file_io.file_name)
            else:
                _post_cal = post_calibration.post_calibration(self._cal)
                _post_cal.cal_verify(_file_io.file_name, True, False)

        if self.cm_args._sensor_family == 'velarray':
            try:
                # Upload data to SQL location
                from txasic_cal.tools.caltx_load_velarray import load_caltxt_data
                load_caltxt_data(file_upload_name, unit_name, str(self._sensor.sensor_module), 0)
            except Exception as e:
                # No upload
                logger.debug(f"Exception during SQL upload: {e}")

        self.finished.emit()

    @pyqtSlot()
    def post_txcal(self) -> None:
        """Run Post-TxCal given input options
        """
        unit_name = "P20"
        version = "IS"

        cm_args = self.cm_args
        sensor_module = self.cm_args.sensor_module

        if(cm_args.unit):
            unit_name = cm_args.unit.upper()

        if(cm_args.version):
            version = cm_args.version.upper()

        file_name = unit_name + "_" + version + "_" + "post_txcal_" + \
                    datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".csv"

        cal_file_name = pathlib.Path()
        if(not cm_args.file):
            if 'V16' in sensor_module:
                cal_file_name = pathlib.Path('/usr/local/etc/velodyne/cal/velarray/v16sr/b0') / pathlib.Path(unit_name)
            elif 'V8' in sensor_module:
                cal_file_name = pathlib.Path('/usr/local/etc/velodyne/cal/velarray/v8_a0/') / pathlib.Path(unit_name.lower())
            elif 'VB2' in sensor_module:
                cal_file_name = pathlib.Path('/usr/local/etc/velodyne/cal/velabit/vb2/') / pathlib.Path(unit_name.lower())
            
            if cm_args.vcsel_cal:
                cal_file_name /= pathlib.Path('vcsel_power_level_table.csv')
            else:
                cal_file_name /= pathlib.Path('power_level_table.csv')
            
        else:
            cal_file_name = cm_args.file

        self.msg_sig.emit('file_input', file_name)

        hv_setpoint = None
        if (cm_args.hv):
            hv_setpoint = cm_args.hv.split(",")

        # Create log file with same name as csv file
        logger.log_file_setup(filename=file_name[:-3] + 'log')

        logger.info("Starting post calibration for {} unit {} with version {}, software version {} and host {}".format(sensor_module,
                                                                                unit_name, version, __version__, host.HOST_NAME))
        logger.info("Using cal file {}".format(cal_file_name))

        self._sensor = sensor.sensor(sensor_module=sensor_module, version=version)
        self._lab = lab.lab()
        _file_io = file_io.file_io(file_name)
        self._cal = calibration.tx_calibration(sensor=self._sensor, lab=self._lab, file_io=_file_io, unit=unit_name, version=version, hv_setpoint=hv_setpoint)
        _post_cal = post_calibration.post_calibration(self._cal)

        if cm_args.stability_test or cm_args.live_pl_test:
            _file_io.file_open()
            if cm_args.live_pl_test:
                _post_cal.cal_verify_live(cal_file_name, cm_args.channel, cm_args.oven_set_temperature, True, True)
            else:
                _post_cal.pl_test(cal_file_name, int(cm_args.channel), int(cm_args.power_level), cm_args.oven_set_temperature)
            _file_io.file_close()
        else:
            if self._gui:
                # Run post txcal verify from main thread as matplotlib needs to be run from main thread
                self.post_cal_sig.emit(cal_file_name)
            else:
                _post_cal.cal_verify(cal_file_name, True, True)

        self._lab.close()
        self._sensor.sensor_module.close()

        if self._cal.stop:
            return

        self.finished.emit()

    @pyqtSlot()
    def vcsel_cal(self) -> None:
        """Run VCSEL Cal given input options
        """
        unit_name = "P7"
        version = "VC"

        cm_args = self.cm_args
        sensor_module = self.cm_args.sensor_module

        if(cm_args.unit):
            unit_name = cm_args.unit.upper()

        if(cm_args.version):
            version = cm_args.version.upper()

        if(not cm_args.file):
            file_name = unit_name + "_" + version + "_" + "vcsel_power_level_table_" + \
                        datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".csv"
        else:
            file_name = cm_args.file

        self.msg_sig.emit('file_input', file_name)

        # Create log file with same name as csv file
        logger.log_file_setup(filename=file_name[:-3] + 'log')

        cal_file_name = r"~/txasic_cal/results/B12_VC_vcsel_power_level_table_200527121134.csv"
        self._sensor = sensor.sensor(sensor_module=sensor_module, version=version)
        self._lab = lab.lab()
        _file_io = file_io.file_io(file_name)
        _file_io.file_open()
        self._cal = calibration.vcsel_calibration(sensor=self._sensor, lab=self._lab, file_io=_file_io, unit=unit_name, version=version)

        # Cal init
        self._cal.init()

        # Pre-cal check
        _pre_cal = pre_calibration.pre_calibration(self._cal)
        _pre_cal.pre_cal_setup_checks()
        _pre_cal.pre_cal_unit_checks()

        self._cal.run(cal_file_name)
        _file_io.file_close()
        self._lab.close()
        self._sensor.sensor_module.close()

        if self._cal.stop:
            return

        file_upload_name = str(_file_io.file_name)

        # Verify table
        if version in ('VC'):
            if self._gui:
                # Run post txcal verify from main thread as matplotlib needs to be run from main thread
                self.post_cal_sig.emit(_file_io.file_name)
            else:
                _post_cal = post_calibration.post_calibration(self._cal)
                _post_cal.cal_verify(_file_io.file_name, True, False)

        if self.cm_args._sensor_family == 'velarray':
            try:
                # Upload data to SQL location
                from txasic_cal.tools.caltx_load_velarray import load_caltxt_data
                load_caltxt_data(file_upload_name, unit_name, str(self._sensor.sensor_module), 1)
            except Exception as e:
                # No upload
                logger.debug(f"Exception during SQL upload: {e}")
        
        self.finished.emit()

    @pyqtSlot()
    def run(self) -> None:
        """Decides between running txcal or vcsel cal or post txcal given input options
        """
        if self.cm_args.post_txcal:
            self.post_txcal()
        elif self.cm_args.vcsel_cal:
            self.vcsel_cal()
        else:
            self.txcal()


def cli_module(parser_args) -> None:
    """Update cal_module_args with parser args and run cal module

    Args:
        parser_args (parser): Parser from argparse
    """
    cm_args = cal_module_args()
    for key, value in parser_args._get_kwargs():
        setattr(cm_args, key, value)
    
    if SENSOR_MODEL is not None and cm_args.sensor_family == '':
        logger.info(f"Sensor connected discovered as {SENSOR_MODEL} on firmware version {SENSOR_MODEL_FW_VERSION}")
        cm_args.sensor_family, cm_args.sensor_name = SENSOR_MODEL.split(".")

    cal_mod = cal_module(cm_args)
    cal_mod.run()


def main():
    start_time = time.time()
    parser = argparse.ArgumentParser(description='TxCal {}'.format(__version__))
    parser.add_argument('-u', '--unit', help='Unit name')
    parser.add_argument('-s', '--sensor_family', default='', help='Sensor name as <velarray/velabit>')
    parser.add_argument('-m', '--sensor_name', default='', help='Sensor name as <V8/V16/VB2>')
    parser.add_argument('-v', '--version', help='TxCal version: IS (Integrating Sphere),\
        TG (Temperature Gonio), IV (Interpolation Verification), PV (Power level Verification),\
        FB (Fixed boot Binary-search bias tune), FP (Fixed boot PID-search bias tune),\
        TS (Temperature Sphere), ST (Scratchpad Test),\
        \nVcselCal version: VC (Vcsel Cal integrating sphere)')
    parser.add_argument('-vn', '--version_num', default='5.2', help='Init TxCal version number to be picked for calibration algorithm')
    parser.add_argument('-f', '--file', help='File name used for verification or power level sweep.')
    parser.add_argument('-it', '--interp_table', nargs='?', default=False, const=True, 
        help='File name used to create interpolated power level table.')
    parser.add_argument('-vc', '--vcsel_cal', action='store_true', help='Run vcsel calibration')
    parser.add_argument('-hv', help='Provide HV setpoint values as <hv0>,<hv1>')
    parser.add_argument('-pc', '--post_txcal', action='store_true', help='Run post txcal')
    parser.add_argument('-st', '--stability_test', action='store_true', help='Run stability test for post txcal')
    parser.add_argument('-ch', '--channel', help='Select channel')
    parser.add_argument('-pl', '--power_level', help='Select power level')
    parser.add_argument('-lt', '--live_pl_test', action='store_true', help='Run live power level test for post txcal')
    parser.add_argument('-ot', '--oven_set_temperature', default=None, help='Select oven set temperature in C')
    parser_args = parser.parse_args()
    
    cli_module(parser_args)

    end_time = time.time()

    logger.info("\n\nCompleted in {} sec or {} min".format((end_time - start_time), (end_time - start_time)/60.0))


if __name__ == "__main__":
    main()
