#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Calibration algorithm software

import numpy as np
import pandas as pd
import datetime
import time

from PyQt5.QtCore import QThread

from txasic_cal.calibration import config
from txasic_cal.calibration import cal_algo
from txasic_cal.calibration import init_txcal_version
from txasic_cal.tools.logger import logger
from txasic_cal.host import host
from txasic_cal.pfmea_code import pfmea_table

from txasic_cal import __version__

class tx_calibration(init_txcal_version.init_txcal, cal_algo.cal_algo):

    def __init__(self, sensor=None, lab=None, file_io=None, unit='P0', version='IS', version_num=config.VERSION_NUMBER, hv_setpoint=None):
        self.power_level_table = np.array(config.CAL_AVG_POW)
        self.cur_power_level = config.START_POWER_LEVEL
        self.calibration_header = config.CALIBRATION_HEADER

        self.sensor = sensor
        self.sensor_module = sensor.sensor_module
        self.lab = lab

        # Results file
        self.results_header = self.sensor.results_header + self.lab.measurement_header + \
                              self.calibration_header
        self.results_file = file_io
        self.results_row = {}

        self.unit = unit
        config.VERSION_NUMBER = version_num
        self.version = version + "_" + config.VERSION_NUMBER
        self.hv_setpoint = hv_setpoint
        if hv_setpoint is not None:
            self.sensor_module.hv_setpoint = hv_setpoint
        self.file_name = self.results_file.file_name.stem

        self.setpoint_threshold = config.SETPOINT_THRESHOLD

        # Start with LP E
        self.cur_lp_num = self.sensor.config.LP_BITS[1]

        # Temperature cal
        self.cur_temp = 0

        # Power level data table
        self.pl_data_table_df = None

        # Git
        self._git_tag = None
        self._git_tag_message = None
        self._git_commit_number = None
        self._git_commit_id = None
        
        # PL 0 lock
        self.pl0_locked_ch_width = [None for i in range(self.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_intensity = [{'mean':None, 'std':None} for i in range(self.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_distance = [{'mean':None, 'std':None} for i in range(self.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_adc_cap = [None for i in range(self.sensor_module.config.NUM_CHANNELS)]

        # MCM
        self._mcm_sdk_version = None

        # Thread stop flag
        self.stop = False

        # LES (Laser Eye-Safety) fault
        self.les_fault_pos = False

        # Skip channel flag
        self.skip_channel = False

        super(tx_calibration, self).__init__(version=config.VERSION_NUMBER, cal_module=self)

    def __str__(self):
        return 'tx_calibration'

    def check_stop(self):
        """Check if current thread is interrupt requested, if it is exit cal
        """
        if QThread.currentThread().isInterruptionRequested():
            self.stop = True
            return True
        else:
            return False

    def init(self, file_write=True):
        """Calibration Initialization
        """
        logger.debug("********Starting calibration init!********")

        # Initialize lab instruments
        self.lab.init()

        # Initialize sensor
        self._mcm_sdk_version = self.sensor.init()
        logger.debug("MCM-SDK Version {}".format(self._mcm_sdk_version))

        # Update hv setpoint
        if self.hv_setpoint is not None:
            self.sensor_module.hv_setpoint = self.hv_setpoint

        self.unit_init()

        # Read PRF
        self.sensor_module.get_prf()

        # Update power level table with PRF
        self.power_level_table = np.around(np.multiply(self.power_level_table,
                                             (self.sensor_module.cur_prf / self._cal_version._default_cal_prf)), 3)

        if file_write:
            # File write init
            self.results_file.init(fieldnames=self.results_header)

        # Update pulse width target based on version, here if gonio is used, change pulse width target
        if self.lab.gonio:
            config.PULSE_WIDTH_TARGET = 3.5

        # Load average power table
        # self.pl_data_table_df = pd.read_csv(config.AVG_POWER_TABLE)

    def unit_init(self):
        """Init all unit params
        """
        logger.debug("Unit Initialization.")

        logger.debug("Disabling all channels.")
        self.sensor_module.disable_all_channel()

        # Disable NFK
        self.sensor_module.set_nfbias_enable(0)

        # Disable bias tracking
        logger.debug('Disable bias tracking.')
        if str(self.sensor_module) == 'V16':
            logger.debug("Set NFK force init to 1.")
            self.sensor_module.api_field_write('set_field_nfk_cfg_force_init', 1)
            logger.debug("Set vbiastr enable to 0.")
            self.sensor_module.api_field_write('set_field_vbiastr_cfg_en', 0)
            
            # Set HV
            # TROSA HV is swapped, TROSA 1 is 1 and TROSA 2 is 0 in FW
            logger.debug("Set MLA APD bias 0 setpoint to {}".format(self.sensor_module.hv_setpoint[1]))
            if self.sensor_module.api_field_write('set_field_mla_apd_bias_0_setpoint_hv', int(self.sensor_module.hv_setpoint[1])) < 0:
                logger.error("HV write fail to TROSA 0")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')
            
            logger.debug("Set MLA APD bias 1 setpoint to {}".format(self.sensor_module.hv_setpoint[0]))
            if self.sensor_module.api_field_write('set_field_mla_apd_bias_1_setpoint_hv', int(self.sensor_module.hv_setpoint[0])) < 0:
                logger.error("HV write fail to TROSA 1")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')

            if self.sensor_module.api_field_read('get_field_mla_apd_bias_0_setpoint_hv') !=  int(self.sensor_module.hv_setpoint[1]):
                logger.error("HV read TROSA 0 does not match setpoint")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')

            if self.sensor_module.api_field_read('get_field_mla_apd_bias_1_setpoint_hv') !=  int(self.sensor_module.hv_setpoint[0]):
                logger.error("HV read TROSA 1 does not match setpoint")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')

        elif str(self.sensor_module) == 'V8':
            logger.debug("Set NFK force init to 1.")
            self.sensor_module.api_field_write('set_field_nfk_cfg_force_init', 1)
            
            # Set HV
            logger.debug("Set MLA APD bias 0 setpoint to {}".format(self.sensor_module.hv_setpoint[0]))
            if self.sensor_module.api_field_write('set_field_apd_bias_0_setpoint_hv', int(self.sensor_module.hv_setpoint[0])) < 0:
                logger.error("HV write fail to TROSA 0")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')
            
            if self.sensor_module.api_field_read('get_field_apd_bias_0_setpoint_hv') !=  int(self.sensor_module.hv_setpoint[0]):
                logger.error("HV read TROSA 0 does not match setpoint")
                pfmea_table.pfmea_filter.error('UNIT_PRE_REGAPDHV')

        # Init TxCal Mode
        self.sensor_module.init_tx_cal()

        # Scan fault allow
        self.sensor_module.scan_fault_allow()

        # Clear fault
        # If faulted at init, needs to be fixed before continuing
        self.les_fault_check()

        # Set vertical mirror position (only doing this for room temperature txcal)
        if self.version[0:2] == "IS" and self.sensor_module in ('V8', 'V16', 'VB2'):
            self.sensor_module.set_vertical_mirror_position(config.MIN_VERT_DAZZLE_POS)

    def les_fault_check(self):
        """Clear faults in the unit
        """
        # Clear fault
        if self.sensor_module.clear_fault():
            logger.error("Fault not cleared. Pausing.")
            pfmea_table.pfmea_filter.error('TROSACAL_PRE_REGEYESAFETYFAULT')
            logger.input("Check fault on unit. Hit enter to continue.")

            self.les_fault_pos = True
        else:
            self.les_fault_pos = False

    def populate_cur_row(self):
        """Populate current results_row
        """
        self.sensor_module.get_prf()
        self.results_row['PRF'] = self.sensor_module.cur_prf
        self.results_row['Power Level'] = self.cur_power_level
        self.sensor.populate_cur_row(version=self.version, results_row=self.results_row, display=True)
        self.lab.populate_cur_row(results_row=self.results_row, display=True)

        self.results_row['Peak Power(W)'] = self.lab.sphere_peak_power(self.sensor_module.cur_prf)
        self.results_row['Oven Set Temperature(C)'] = self.cur_temp
        self.results_row['Average_Power_Table'] = self.power_level_table[self.cur_power_level if self.cur_power_level > 0 else 0]
        self.results_row['Total_Iter'] = self.total_iter
        self.results_row['Input_List'] = self.input_list
        self.results_row['Meas_List'] = self.meas_list
        self.results_row['Meas_Time'] = self.meas_time
        if self.results_row['Optical Power in mW'] != 0:
            self.results_row['Average_Power_Ratio'] = round(self.results_row['Average_Power_Table'] / self.results_row['Optical Power in mW'], 3)
        else:
            self.results_row['Average_Power_Ratio'] = 0

        self.results_row['Average_Power_Percent_Difference'] = round((self.results_row['Average_Power_Table'] - 
            self.results_row['Optical Power in mW']) * 100 / self.results_row['Average_Power_Table'], 3)

        if self.cur_power_level == 0:
            self.results_row['PL0_Width'] = self.pl0_locked_ch_width[self.sensor_module.cur_ch]
            self.results_row['PL0_Target_Intensity'] = self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['mean']
            self.results_row['PL0_Target_Distance'] = self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['mean']
            self.results_row['PL0_Target_Intensity_STD'] = self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['std']
            self.results_row['PL0_Target_Distance_STD'] = self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['std']

            self.results_row['ADC_Capture'] = self.pl0_locked_ch_adc_cap[self.sensor_module.cur_ch]
        else:
            self.results_row['PL0_Width'] = None
            self.results_row['PL0_Target_Intensity'] = None
            self.results_row['PL0_Target_Distance'] = None
            self.results_row['PL0_Target_Intensity_STD'] = None
            self.results_row['PL0_Target_Distance_STD'] = None
            self.results_row['ADC_Capture'] = None

        self.results_row['LES_Fault'] = 1 if self.les_fault_pos else 0
        self.results_row['Unit'] = self.unit
        self.results_row['Version'] = self.version
        self.results_row['Git_Tag'] = self._git_tag
        self.results_row['Git_Tag_Message'] = self._git_tag_message
        self.results_row['Git_Develop_Commit_Number'] = self._git_commit_number
        self.results_row['Git_Commit_ID'] = self._git_commit_id
        self.results_row['Sensor'] = str(self.sensor_module)
        self.results_row['MCM-SDK_Version'] = self._mcm_sdk_version
        self.results_row['PFMEA_Operation_Code'] = pfmea_table.pfmea_filter._cur_op_code
        self.results_row['SW_Version'] = __version__
        self.results_row['Hostname'] = host.HOST_NAME
        self.results_row['Filename'] = self.file_name
        self.results_row['Datetime'] = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        self.results_row['Skip_Channel'] = 1 if self.skip_channel else 0

        # reset LES fault column
        self.les_fault_pos = False

    def measure(self, device=None, measurement=None):
        if measurement is None:
            logger.error("Please provide measurement.")
            return
        
        if device == 'lab':
            return self.lab.measure(measurement=measurement)
        elif device == 'sensor':
            return self.sensor.measure(measurement=measurement)

    def set_input(self, device=None, cur_result_input=None, result=None):
        if result is None:
            logger.error("Please provide result that needs to be updated.")
            return
        if cur_result_input is None:
            logger.error("Please provide input to result to be updated.")
            return

        if device == 'sensor':
            self.sensor.set_input(cur_result_input, result=result)
        elif device == 'lab':
            self.lab.set_input(cur_result_input, result=result)

    def cal_boot_bias_check(self):

        # check thread stop
        if self.check_stop():
            return

        logger.debug("Starting boot, bias min & max check.")
        cur_boot = self.sensor_module.boot
        cur_bias = self.sensor_module.bias

        # Boot Check
        self.sensor_module.set_bias(self.sensor_module.config.START_BIAS)
        cur_bias = self.sensor_module.bias
        
        self.sensor_module.set_boot(self.sensor_module.config.BOOT_MIN)
        time.sleep(config.SLEEP_TIME)
        min_op = self.lab.optical_power()
        self.sensor_module.set_boot(self.sensor_module.config.BOOT_MAX)
        time.sleep(config.SLEEP_TIME)
        max_op = self.lab.optical_power()

        # If difference between max and min boot is under 1mW, boot check has failed
        if abs(max_op - min_op) < 1.0:
            logger.error("Boot cal min-max check fail as optical power at max boot {} and min boot {} is within 1mw."\
                .format(max_op, min_op))
            pfmea_table.pfmea_filter.error('UNIT_CAL_REGBOOT')
            logger.input("Check boot. Hit enter to continue.")

        # Bias Check
        self.sensor_module.set_boot(self.sensor_module.config.START_BOOT)
        cur_boot = self.sensor_module.boot
        
        self.sensor_module.set_bias(self.sensor_module.config.BIAS_MIN)
        time.sleep(config.SLEEP_TIME)
        min_op = self.lab.optical_power()
        # Using 3.5 V for bias max
        self.sensor_module.set_bias(3.5)
        time.sleep(config.SLEEP_TIME)
        max_op = self.lab.optical_power()

        # If difference between max and min bias is under 1mW, bias check has failed
        if abs(max_op - min_op) < 1.0:
            logger.error("Bias cal min-max check fail as optical power at max bias {} and min bias {} is within 1mw."\
                .format(max_op, min_op))
            pfmea_table.pfmea_filter.error('UNIT_CAL_REGBIAS')
            logger.input("Check bias. Hit enter to continue.")

        self.sensor_module.set_boot(cur_boot)
        self.sensor_module.set_bias(cur_bias)

        logger.debug("Boot, bias min & max check complete.")

    def handle_cal_algo_fail(self, cur_input):
        """Handle search failure by increasing boot/bias

        Arguments:
            cur_input (float) -- [current boot/bias value]
        """
        logger.debug("*******Entering handle fail!*******")

        # check thread stop
        if self.check_stop():
            return

        if(self.input_name == "boot"):
            cur_bias = self.sensor_module.bias
            if abs(self.sensor.config.BOOT_MAX - cur_input) < abs(cur_input - self.sensor.config.BOOT_MIN):
                # Boot is > 6V
                cur_bias = cur_bias + 0.1 if self.cur_power_level > 4 else cur_bias + 0.05
                if cur_bias > config.END_BS_RESULT["bias"]:
                    logger.error("Handle fail bias {} is greater than max {} for ch {} and pl {}".format(
                        cur_bias, config.END_BS_RESULT["bias"], self.sensor_module.cur_ch, self.cur_power_level))
                    cur_bias = config.END_BS_RESULT["bias"]
                logger.debug("Bias is too low. Increasing to {}".format(cur_bias))
                self.sensor_module.set_bias(cur_bias)

                # Reset boot
                self.sensor_module.set_boot(self.sensor_module.config.START_BOOT)
            else:
                # Boot is <= 6V
                cur_bias = cur_bias - 0.1 if self.cur_power_level > 4 else cur_bias - 0.05
                if cur_bias < config.START_BS_RESULT["bias"]:
                    logger.error(
                        "Handle fail bias {} is less than min {} for ch {} and pl {}".format(
                            cur_bias, config.START_BS_RESULT["bias"], self.sensor_module.cur_ch, self.cur_power_level))
                    cur_bias = config.START_BS_RESULT["bias"]
                logger.debug("Bias is too high. Decreasing to {}".format(cur_bias))
                self.sensor_module.set_bias(cur_bias)

                # If power level is 4 or under and boot is too low, switch to bias search
                if self.cur_power_level <= 7 and self.sensor_module.boot <= 4.25:
                    logger.debug("Boot at 4V. Switching to bias search for ch {} and pl {}.".format(
                        self.sensor_module.cur_ch, self.cur_power_level))
                    self.input_name = 'bias'
                    self.input_func = self.sensor_module.set_bias
                    self.step_size = 0.0025
                    self.sensor_module.set_boot(config.START_BS_RESULT["boot"])
                else:
                    # Reset boot
                    self.sensor_module.set_boot(self.sensor_module.config.START_BOOT)

        elif(self.input_name == "bias"):
            cur_boot = self.sensor_module.boot
            if abs(self.sensor.config.BIAS_MAX - cur_input) < abs(cur_input - self.sensor.config.BIAS_MIN):
                # Bias is > 3 V
                cur_boot = cur_boot + 0.1 if self.cur_power_level > 4 else cur_boot + 0.05
                if cur_boot > config.END_BS_RESULT["boot"]:
                    logger.error(
                        "Handle fail boot {} is greater than max {} for ch {} and pl {}".format(
                            cur_boot, config.END_BS_RESULT["boot"], self.sensor_module.cur_ch, self.cur_power_level))
                    cur_boot = config.END_BS_RESULT["boot"]
                logger.debug("Boot is too low. Increasing to {}".format(cur_boot))
                self.sensor_module.set_boot(cur_boot)
            else:
                # Bias is <= 3 V
                cur_boot = cur_boot - 0.1 if self.cur_power_level > 4 else cur_boot - 0.05
                if cur_boot < config.START_BS_RESULT["boot"]:
                    logger.error(
                        "Handle fail boot {} is less than max {} for ch {} and pl {}".format(
                            cur_boot, config.START_BS_RESULT["boot"], self.sensor_module.cur_ch, self.cur_power_level))
                    cur_boot = config.START_BS_RESULT["boot"]
                logger.debug("Boot is too high. Decreasing to {}".format(cur_boot))
                self.sensor_module.set_boot(cur_boot)

            # Reset bias
            self.sensor_module.set_bias(self.sensor_module.config.START_BIAS)

        # Failure 1 - Fault not cleared
        # Clear fault
        # For integrating sphere, if there is a fault, we need to fix it
        self.les_fault_check()
        # Failure 2 - Optical power is too low
        if self.lab.optical_power() < config.MIN_OPTICAL_POWER:
            logger.error("Optical power too low for ch {} and pl {}.".format(self.sensor_module.cur_ch, self.cur_power_level))
            self.cal_boot_bias_check()
            pfmea_table.pfmea_filter.error('SETUP_CAL_OPTICALPOWERLOW')
            logger.input("Check if unit is firing laser or if unit position is correct. Hit enter to continue.")
        # Failure 3 - Crossed maximum number of iterations to converge (only used for room temperature calibration with IS)
        if self.version[0:2] == 'IS' and self.total_iter >= config.BS_FAIL_ITER and self.cur_power_level not in [0, 1]:
            logger.error("Too many iterations for ch {} to converge to current power level {}.".format(
                self.sensor_module.cur_ch, self.cur_power_level))
            self.cal_boot_bias_check()
            pfmea_table.pfmea_filter.error('TROSACAL_CAL_ITER')
            logger.input("Check if unit position is correct. Hit enter to continue.")

            skip_choice = logger.input("Do you wish to skip channel? (y/n)")

            if skip_choice is not None and skip_choice.lower() == 'y':
                self.skip_channel = True

    def pl0_error_check(self):
        """Error check for PL0, checks for min/max, large standard deviation on width, distance, intensity
        """

        def error_check(force=False):
            if not force:
                logger.warn("Possible LEC failure. Report for test.")
                logger.input("Hit enter to continue.")
                return

            logger.input("Replace LEC, hit enter to continue.")

        # Intensity error checking
        # Intensity hard check
        if self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['mean'] < config.PL0_INT_HARD_MIN:
            logger.warn("Ch {} PL0 Intensity {} smaller than {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['mean'], config.PL0_INT_HARD_MIN))
            pfmea_table.pfmea_filter.warn('TROSACAL_PST_PL0INT')
            error_check()

        # Intensity soft check
        if self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['mean'] < config.PL0_INT_SOFT_MIN:
            logger.warn("Ch {} PL0 Intensity {} smaller than {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_intensity[self.sensor_module.cur_ch]['mean'], config.PL0_INT_SOFT_MIN))
            error_check(False)

        # Intensity STD check
        if self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['std'] > config.PL0_INT_STD:
            logger.warn("Ch {} PL0 Distance STD {} larger than allowed STD {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['std'], config.PL0_INT_STD
            ))
            pfmea_table.pfmea_filter.warn('TROSACAL_PST_PL0INT')
            error_check()

        # Distance error checking
        # Distance mean check
        if self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['mean'] < config.PL0_DIST_MIN:
            logger.warn("Ch {} PL0 Distance {} smaller than {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['mean'], config.PL0_DIST_MIN))
            pfmea_table.pfmea_filter.warn('TROSACAL_PST_PL0DIST')
            error_check()
        elif self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['mean'] > config.PL0_DIST_MAX:
            logger.warn("Ch {} PL0 Distance {} larger than {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['mean'], config.PL0_DIST_MAX))
            pfmea_table.pfmea_filter.warn('TROSACAL_PST_PL0DIST')
            error_check()
        
        # Distance STD check
        if self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['std'] > config.PL0_DIST_STD:
            logger.warn("Ch {} PL0 Distance STD {} larger than allowed STD {}.".format(
                self.sensor_module.cur_ch, self.pl0_locked_ch_distance[self.sensor_module.cur_ch]['std'], config.PL0_DIST_STD
            ))
            pfmea_table.pfmea_filter.warn('TROSACAL_PST_PL0DIST')
            error_check()

    def txcal(self, cur_input=3.0):
        """Main calibration algorithm that generates power level table

        Keyword Arguments:
            cur_input [int] : Starting input condition for search
        """
        start_time = time.time()
        setpoint = self.power_level_table[self.cur_power_level]
        setpoint_margin = setpoint * self.setpoint_threshold

        # Clear fault
        # For integrating sphere, if there is a fault, we need to fix it
        self.les_fault_check()

        if not self.skip_channel:
            super().txcal(cur_input=cur_input, setpoint=setpoint, setpoint_margin=setpoint_margin)

        # check thread stop
        if self.check_stop():
            return
        
        self.lab.mso_vertical_level(1, config.SCOPE_PL_VERTICAL[self.cur_power_level], -4, 0)

        if self.skip_channel:
            logger.info(f"Skipping txcal search for channel {self.sensor_module.cur_ch}.")
            logger.info(f"Populating table with default values for power level {self.cur_power_level}.")

            self.sensor_module.ch_init(self.sensor_module.cur_ch)
            self.sensor_module.set_boot(config.NOMINAL_BOOT[self.cur_power_level])
            self.sensor_module.set_bias(config.NOMINAL_BIAS[self.cur_power_level])
            self.sensor_module.set_lp(config.NOMINAL_LP[self.cur_power_level])
            time.sleep(config.SLEEP_TIME)

        end_time = time.time()
        self.meas_time = end_time - start_time
        self.populate_cur_row()
        self.results_file.write(self.results_row)

        self.clear_cal_algo()
        self.results_file.file.flush()

    def cal_IS(self):
        """Calibration method using integrating sphere
        """
        for ch in self.sensor_module.channels:
            # init channel
            self.sensor_module.ch_init(ch)
            for pl in self._cal_version._cal_pl_order:

                # check thread stop
                if self.check_stop():
                    return

                # Disabling PL16 using IS for now
                if pl >= 16:
                    continue
                self.cur_power_level = pl
                logger.info("\n\nStarting calibration for ch {} and pl {}"
                             .format(ch, self.cur_power_level))

                self.txcal()

            # deinit channel
            self.sensor_module.ch_deinit()

            # reset skip channel flag
            self.skip_channel = False
        
        # Only run NFK if HW exists, or to fake it, or if PL0 doesn't exists in init version
        if (config.NFK or config.FAKE_NFK) and (0 not in self._cal_version._cal_pl_order):
            for ch in self.sensor_module.channels:
                # init channel
                self.sensor_module.ch_init(ch)
                for pl in self._cal_version._cal_nfk_order:
                    
                    # check thread stop
                    if self.check_stop():
                        return

                    self.cur_power_level = pl
                    logger.debug("\n\nStarting calibration for ch {} and pl {}"
                                .format(ch, self.cur_power_level))

                    self.txcal()

                # deinit channel
                self.sensor_module.ch_deinit()

                # reset skip channel flag
                self.skip_channel = False

    def gonio_op_amp_cal(self):
        """Cal gonio position for maximizing optical power and amplitude
        """
        # Clear fault
        if self.sensor_module.clear_fault() < 0:
            logger.error("Fault not cleared.")
            pfmea_table.pfmea_filter.error('TROSACAL_PRE_REGEYESAFETYFAULT')
            # For gonio cal, if there is a fault, we need to fix it
            logger.input("Please hit enter to continue.")

        # if self.sensor_module.cur_ch == 0:
        # Move to op position (initial)
        self.lab.gonio.move_to_position(
            self.lab.gonio.chan_op_pos['x'][self.sensor_module.cur_ch],
            self.lab.gonio.chan_op_pos['y'][self.sensor_module.cur_ch])

        self.lab.mso_autoset()

        # Optical power gonio cal
        self.lab.gonio.X_Y_gonio_cal(0, self.sensor_module.cur_ch)
        self.cur_power_level = -2
        self.populate_cur_row()
        self.results_file.write(self.results_row)
        self.results_file.file.flush()

        # Amplitude gonio cal
        self.lab.gonio.X_Y_gonio_cal(1, self.sensor_module.cur_ch)
        self.cur_power_level = -1
        self.populate_cur_row()
        self.results_file.write(self.results_row)
        self.results_file.file.flush()

        # Move to op position
        self.lab.gonio.move_to_position(
            self.lab.gonio.chan_op_pos['x'][self.sensor_module.cur_ch],
            self.lab.gonio.chan_op_pos['y'][self.sensor_module.cur_ch])

    def cal_TG(self):
        """Calbration method using temperature gonio
        """
        # For temperature cal
        # temp_list = [85, 40, 20, 0, -20, -40]
        # temp_list.reverse()
        temp_list = [30]
        for each_temp in temp_list:
            # Set oven temperature
            self.cur_temp = each_temp
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")
            for ch in self.sensor_module.channels:
                # init channel
                self.sensor_module.ch_init(ch)

                self.gonio_op_amp_cal()

                for pl in config.CAL_PL_ORDER:
                    self.cur_power_level = pl
                    logger.debug("\n\nStarting calibration for ch {} and pl {}"
                                 .format(ch, self.cur_power_level))

                    # cur_input only used by PID search
                    cur_input = 3.0
                    if each_temp >= 65:
                        cur_input = 2.5
                    self.txcal(cur_input)

                self.sensor_module.ch_deinit()
    
    def cal_TS(self):
        """Calbration method using temperature and integrating sphere
        """
        # For temperature cal
        temp_list = [30]
        for each_temp in temp_list:
            # Set oven temperature
            self.cur_temp = each_temp
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")
            for ch in self.sensor_module.channels:
                # init channel
                self.sensor_module.ch_init(ch)

                for pl in self._cal_version._cal_pl_order:
                    # check thread stop
                    if self.check_stop():
                        return

                    self.cur_power_level = pl
                    logger.info("\n\nStarting calibration for ch {} and pl {}"
                                 .format(ch, self.cur_power_level))

                    # cur_input only used by PID search
                    cur_input = 3.0
                    if each_temp >= 65:
                        cur_input = 2.5
                    self.txcal(cur_input)

                self.sensor_module.ch_deinit()
            
            if config.NFK or config.FAKE_NFK:
                for ch in self.sensor_module.channels:
                    # init channel
                    self.sensor_module.ch_init(ch)
                    for pl in self._cal_version._cal_nfk_order:
                        # check thread stop
                        if self.check_stop():
                            return

                        self.cur_power_level = pl
                        logger.info("\n\nStarting calibration for ch {} and pl {}"
                                    .format(ch, self.cur_power_level))

                        self.txcal()

                    # deinit channel
                    self.sensor_module.ch_deinit()
        
        # Calibrate using 4-point cal
        self.cal_fixed_boot(self.results_file.file_name)
        self.set_input("lab", 30, "Oven Set Temperature(C)")

    def power_level_interpolation_verify(self, cal_file=None):
        """Calibration method for verifying interpolation

        Keyword Arguments:
            cal_file {str} -- [Cal file with interpolation] (default: {None})
        """
        if cal_file is None:
            logger.error("Please provide cal file used for interpolation.")
            return
        temp_list = [-40, -20, 0, 20, 40, 85]
        # temp_list.reverse()
        df = pd.read_csv(cal_file)
        for each_temp in temp_list:
            # Set oven temperature
            self.cur_temp = each_temp
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")

            for ch in self.sensor_module.channels:
                self.sensor_module.ch_init(ch)

                self.gonio_op_amp_cal()

                for pl in self._cal_version._cal_pl_order:
                    self.cur_power_level = pl
                    index = df.loc[(df['Power Level'] == pl) & (df['Channel'] == ch)
                            & (df['Oven Set Temperature(C)'] == each_temp)].index[0]
                    lp = int(df.iloc[index]['LP'], 16)
                    boot = df.iloc[index]['Boot']
                    bias = df.iloc[index]['Bias']

                    self.sensor_module.set_lp(lp)
                    self.sensor_module.set_boot(boot)
                    self.sensor_module.set_bias(bias)
            
                    self.populate_cur_row()

                    self.results_file.write(self.results_row)
                
                self.sensor_module.ch_deinit()
                self.results_file.file.flush()

    def power_level_sweep_verify(self, cal_file=None):
        """Calibration method for verifying power level load

        Keyword Arguments:
            cal_file {str} -- [Cal file with interpolation] (default: {None})
        """
        if cal_file is None:
            logger.error("Please provide cal file used for interpolation.")
            return
        # temp_list = np.linspace(-40, 85, 26, dtype=int)
        temp_list = [-40, -20, 0, 20, 40, 85]
        # temp_list = [85]
        temp_list = temp_list[::-1]

        temp_bin_list = np.linspace(-40, 125, 34, dtype=int)

        df = pd.read_csv(cal_file)
        for _, each_temp in enumerate(temp_list):
            # Set oven temperature
            self.cur_temp = each_temp
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")

            for ch in self.sensor_module.channels:
                self.sensor_module.cur_ch = ch

                # for pl in self._cal_version._cal_pl_order:
                for pl in range(16):
                    # if pl == 16:
                    #     continue

                    logger.debug("\n\nSetting to power level {}".format(pl))
                    self.sensor_module.enable_channel(power_level=pl)

                    # if pl == self._cal_version._cal_pl_order[0]:
                    #     self.gonio_op_amp_cal()

                    trosa_temp = self.sensor_module.get_temperature()
                    temp_bin = np.where(temp_bin_list <= trosa_temp)[0][-1]
                    temp_bin = temp_bin - 2 if temp_bin >= 2 else 0

                    logger.debug("Current TROSA Temperature {} and bin {}".format(trosa_temp, temp_bin))

                    self.cur_power_level = pl
                    index = df.loc[(df['Power Level'] == pl) & (df['Channel'] == ch)
                                   & (df['Temperature Bin'] == temp_bin)].index[0]
                    self.sensor_module.lp = int(df.iloc[index]['LP'], 16)
                    self.sensor_module.boot = df.iloc[index]['Boot']
                    self.sensor_module.boot_in_dec = int(
                        (self.sensor_module.boot * self.sensor_module.config.MAX_DAC_DECIMAL)
                        / self.sensor_module.config.MAX_DAC_VOLT)
                    self.sensor_module.bias = df.iloc[index]['Bias']
                    self.sensor_module.bias_in_dec = int(
                        (self.sensor_module.bias * self.sensor_module.config.MAX_DAC_DECIMAL)
                        / self.sensor_module.config.MAX_DAC_VOLT)

                    self.populate_cur_row()

                    self.results_file.write(self.results_row)

                self.sensor_module.ch_deinit()
                self.results_file.file.flush()

    def cal_fixed_boot(self, cal_file=None):
        """Calibration method with fixed boot

        Keyword Arguments:
            cal_file {str} -- [Cal file with interpolation] (default: {None})
        """
        if cal_file is None:
            logger.error("Please provide cal file used for interpolation.")
            return
        
        cal_temp = 30
        prev_temp = cal_temp
        temp_list = [-40, 0, 40, 85]
        # temp_list = [40]
        df = pd.read_csv(cal_file)
        for each_temp in temp_list:
            # Set oven temperature
            self.cur_temp = each_temp
            if each_temp == -40:
                # Turn off unit
                self.lab.toggle_power_supply()
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")
            if each_temp == -40:
                # Turn on unit
                self.lab.toggle_power_supply()
                time.sleep(0.2)

                self.unit_init()

            for ch in self.sensor_module.channels:
                self.sensor_module.ch_init(ch)

                if self.lab.gonio:
                    self.gonio_op_amp_cal()

                for pl in self._cal_version._cal_pl_order:
                    # check thread stop
                    if self.check_stop():
                        return

                    start_time = time.time()
                    self.cur_power_level = pl

                    logger.info("\n\nCal fixed boot for current ch {} and power level {}".format(ch, pl))

                    # Clear fault
                    if self.sensor_module.clear_fault() < 0:
                        logger.error("Fault not cleared.")
                        pfmea_table.pfmea_filter.error('TROSACAL_PRE_REGEYESAFETYFAULT')

                    index = df.loc[(df['Power Level'] == pl) & (df['Channel'] == ch)
                            & (df['Oven Set Temperature(C)'] == cal_temp)].index[0]
                    lp = int(df.iloc[index]['LP'], 16)
                    boot = df.iloc[index]['Boot']
                    bias = df.iloc[index]['Bias']

                    # Set lp, boot from table
                    self.sensor_module.set_lp(lp)
                    # self.sensor_module.set_boot(boot)

                    if each_temp == cal_temp:
                        self.sensor_module.set_bias(bias)
                        self.sensor_module.set_boot(boot)
                    else:
                        # Tune for bias
                        setpoint = self.power_level_table[self.cur_power_level]
                        setpoint_margin = setpoint * self.setpoint_threshold

                        # Adjust boot as 0.0022V per degree C
                        boot = boot - (each_temp - cal_temp) * config.BOOT_SLOPE
                        if boot > self.sensor.config.BOOT_MAX:
                            boot = self.sensor.config.BOOT_MAX
                        elif boot < self.sensor.config.BOOT_MIN:
                            boot = self.sensor.config.BOOT_MIN
                        
                        if pl in self._cal_version._max_boot_pl:
                           boot = 8.0

                        self.sensor_module.set_boot(boot)

                        if pl >= 16:
                            cur_input = 2.5
                            self.txcal(cur_input)
                            continue
                        elif pl == 1:
                            self.txcal()
                            continue
                        else:
                            if each_temp > cal_temp:
                                end_input = bias
                            else:
                                end_input = None
                            
                            step_size = config.BS_STEP_SIZE
                            if pl <= 4:
                                step_size = 0.005

                            self.meas_name = 'Optical Power in mW'
                            self.meas_func = self.lab.optical_power
                            self.input_name = 'bias'
                            self.input_func = self.sensor_module.set_bias
                            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, 
                                end_input=end_input, step_size=step_size)

                            if pl == 0:
                                self._cal_version.pl0_locked_ch_bias[self.sensor_module.cur_ch] = self.sensor_module.bias
                            elif pl == 2:
                                self._cal_version.pl2_ch_bias[self.sensor_module.cur_ch] = self.sensor_module.bias

                    # check thread stop
                    if self.check_stop():
                        return
                    end_time = time.time()
                    self.meas_time = end_time - start_time
                    self.populate_cur_row()
                    self.results_file.write(self.results_row)
                    self.clear_cal_algo()

                self.sensor_module.ch_deinit()
                self.results_file.file.flush()

            prev_temp = each_temp

    def cal_fixed_boot_PID(self, cal_file=None):
        """Calibration method with fixed boot using PID (TODO: Update to match BS)

        Keyword Arguments:
            cal_file {str} -- [Cal file with interpolation] (default: {None})
        """
        if cal_file is None:
            logger.error("Please provide cal file used for interpolation.")
            return
        
        cal_temp = 20
        # temp_list = [0, -20, -40, 20, 40, 85]
        # temp_list.reverse()
        temp_list = [85]
        df = pd.read_csv(cal_file)
        df2 = None
        prev_temp = temp_list[0]
        prev_bias = 3.0
        for each_temp in temp_list:
            # Set oven temperature
            self.cur_temp = each_temp
            self.set_input("lab", each_temp, "Oven Set Temperature(C)")

            for ch in self.sensor_module.channels:
                self.sensor_module.ch_init(ch)

                self.gonio_op_amp_cal()

                for pl in config.CAL_PL_ORDER:
                    start_time = time.time()
                    self.cur_power_level = pl

                    logger.debug("\n\nCal fixed boot for current ch {} and power level {}".format(ch, pl))

                    # Clear fault
                    if self.sensor_module.clear_fault() < 0:
                        logger.error("Fault not cleared.")
                        pfmea_table.pfmea_filter.error('TROSACAL_PRE_REGEYESAFETYFAULT')

                    index = df.loc[(df['Power Level'] == pl) & (df['Channel'] == ch)
                            & (df['Oven Set Temperature(C)'] == cal_temp)].index[0]
                    lp = int(df.iloc[index]['LP'], 16)
                    boot = df.iloc[index]['Boot']
                    bias = df.iloc[index]['Bias']

                    try:
                        if df2 is not None:
                            df2 = pd.read_csv(self.results_file.file_name)
                            index2 = df2.loc[(df2['Power Level'] == pl) & (df2['Channel'] == ch)
                                    & (df2['Oven Set Temperature(C)'] == prev_temp)].index[0]
                            prev_bias = df2.iloc[index2]['Bias']
                        else:
                            df2 = pd.read_csv(self.results_file.file_name)
                            prev_bias = bias
                    except Exception as e:
                        logger.debug("Table read failed due to {}, using bias {}".format(e, bias))
                        prev_bias = bias

                    # Set lp, boot from table
                    self.sensor_module.set_lp(lp)
                    self.sensor_module.set_boot(boot)

                    # if each_temp == cal_temp:
                    #     self.sensor_module.set_bias(bias)
                    # else:
                    # Tune for bias
                    setpoint = self.power_level_table[self.cur_power_level]
                    setpoint_margin = setpoint * self.setpoint_threshold

                    if pl == 16 or pl == 0:
                        cur_input = 3.0
                        self.txcal(cur_input)
                        continue
                    else:
                        self.meas_name = 'Optical Power in mW'
                        self.meas_func = self.lab.optical_power
                        self.input_name = 'bias'
                        self.input_func = self.sensor_module.set_bias
                        self.tune_pid(setpoint=setpoint, setpoint_margin=setpoint_margin, cur_input=prev_bias)

                    end_time = time.time()
                    self.meas_time = end_time - start_time
                    self.populate_cur_row()
                    self.results_file.write(self.results_row)
                    self.clear_cal_algo()
                
                self.sensor_module.ch_deinit()
                self.results_file.file.flush()

            prev_temp = each_temp

    def scratchpad_test(self):
        # temp_list = [5, 25, 40, 80]
        temp_list = [15, 25]
        for each_temp in temp_list:
            for num_iter in range(10):
                self.cur_temp = each_temp
                self.set_input("lab", each_temp, "Oven Set Temperature(C)")
                self.total_iter = num_iter
                # Turn off power supply
                self.lab.power_supply.SetOutput(False)
                time.sleep(0.1)
                # Turn on power supply
                self.lab.power_supply.SetOutput(True)
                time.sleep(0.1)

                self.populate_cur_row()
                self.results_file.write(self.results_row)

    def parse_pcap_column(self, ch, df, col):
        """Parse chosen column from pcap file
        
        Arguments:
            ch [int] -- Parse channel intensity
            df [pandas dataframe] -- Pandas dataframe object
            col [str] -- Column to parse from dataframe

        Returns:
            [float] -- Mean value in float
            [float] -- STD value in float
        """
        dat = df.loc[df['Channel'] == ch]
        mean_arr = dat.loc[:, col].mean()
        std_arr = dat.loc[:, col].std()

        return mean_arr.squeeze(), std_arr.squeeze()

    def run(self, *args, **kwargs):
        """Run TxCal
        """
        version = self.version.split("_")[0]

        if version == "IS":
            self.cal_IS()
        elif version == "TG":
            self.cal_TG()
        elif version == "TS":
            self.cal_TS()
        elif version == "IV":
            self.power_level_interpolation_verify(args[0])
        elif version == "PV":
            self.power_level_sweep_verify(args[0])
        elif version == "FB":
            self.cal_fixed_boot(args[0])
        elif version == "FP":
            self.cal_fixed_boot_PID(args[0])
        elif version == "ST":
            self.scratchpad_test()


class vcsel_calibration(cal_algo.cal_algo):
    def __init__(self, sensor=None, lab=None, file_io=None, unit='P0', version='IS'):
        self.cur_power_level = config.START_POWER_LEVEL
        self.calibration_header = config.VCSEL_CALIBRATION_HEADER

        self.sensor = sensor
        self.sensor_module = sensor.sensor_module
        self.lab = lab
        self.end_input = None

        if str(self.sensor_module) == 'V8':
            config.VCSEL_CAL_AVG_POW = config.VCSEL_AVG_POW_AMS
            config.DEFAULT_VCSEL_CAL_PRF = config.DEFAULT_VCSEL_CAL_PRF_AMS
            self.end_input = 6.396
            config.VCSEL_PL15_RECHARGE = 5.863
        else:
            config.VCSEL_CAL_AVG_POW = config.VCSEL_AVG_POW_LUM
            config.DEFAULT_VCSEL_CAL_PRF = config.DEFAULT_VCSEL_CAL_PRF_LUM

        self.power_level_table = np.array(config.VCSEL_CAL_AVG_POW)

        # Results file
        self.results_header = self.sensor.results_header + self.lab.measurement_header + \
                              self.calibration_header
        self.results_file = file_io
        self.results_row = {}

        self.unit = unit
        self.version = version + "_" + config.VERSION_NUMBER
        self.file_name = self.results_file.file_name.stem

        self.setpoint_threshold = config.SETPOINT_THRESHOLD

        # Git
        self._git_tag = None
        self._git_tag_message = None
        self._git_commit_number = None
        self._git_commit_id = None

        # MCM
        self._mcm_sdk_version = None

        # Thread stop flag
        self.stop = False

        # LES (Laser Eye-Safety) fault
        self.les_fault_pos = False

        # Skip channel flag
        self.skip_channel = False

        super(vcsel_calibration, self).__init__()

    def __str__(self):
        return 'vcsel_calibration'

    def check_stop(self):
        """Check if current thread is interrupt requested, if it is exit cal
        """
        if QThread.currentThread().isInterruptionRequested():
            self.stop = True
            return True
        else:
            return False

    def init(self):
        """Init for calibration
        1. Initializes lab equipments
        2. Initializes sensor
        3. Update power level table based off current PRF
        """
        logger.debug("********Starting calibration init!********")

        # Initialize lab instruments
        self.lab.init()

        # Initialize sensor
        self._mcm_sdk_version = self.sensor.init()

        # TODO: Read diagnostic packet for unique ID

        # Read PRF
        self.sensor_module.get_vcsel_prf()

        # Update power level table with PRF
        self.power_level_table = np.multiply(self.power_level_table,
                                             (self.sensor_module.cur_vcsel_prf / config.DEFAULT_VCSEL_CAL_PRF))

        # Init TxCal Mode
        self.sensor_module.init_tx_cal()

        # Scan fault allow
        self.sensor_module.scan_fault_allow()

        # Clear fault
        if self.sensor_module.clear_fault() < 0:
            logger.error("Fault not cleared. Pausing.")
            # If faulted at init, needs to be fixed before continuing
            logger.input("Please hit enter to continue.")

        # File write init
        self.results_file.init(fieldnames=self.results_header)

    def populate_cur_row(self):
        """Populate current results_row
        """
        self.sensor_module.get_vcsel_prf()
        self.results_row['PRF'] = self.sensor_module.cur_vcsel_prf
        self.results_row['Power Level'] = self.cur_power_level
        self.sensor.populate_cur_row(version=self.version, results_row=self.results_row, display=True)
        self.lab.populate_cur_row(results_row=self.results_row, display=True)

        self.results_row['Peak Power(W)'] = self.lab.sphere_peak_power(self.sensor_module.cur_vcsel_prf)
        self.results_row['Average_Power_Table'] = self.power_level_table[self.cur_power_level]
        if self.results_row['Optical Power in mW'] != 0:
            self.results_row['Average_Power_Ratio'] = round(self.results_row['Average_Power_Table'] / self.results_row['Optical Power in mW'], 3)
        else:
            self.results_row['Average_Power_Ratio'] = 0

        self.results_row['Average_Power_Percent_Difference'] = (self.results_row['Average_Power_Table'] -
            self.results_row['Optical Power in mW']) * 100 / self.results_row['Average_Power_Table']
        self.results_row['Total_Iter'] = self.total_iter
        self.results_row['Input_List'] = self.input_list
        self.results_row['Meas_List'] = self.meas_list
        self.results_row['Meas_Time'] = self.meas_time

        self.results_row['LES_Fault'] = 1 if self.les_fault_pos else 0
        self.results_row['Unit'] = self.unit
        self.results_row['Version'] = self.version
        self.results_row['Git_Tag'] = self._git_tag
        self.results_row['Git_Tag_Message'] = self._git_tag_message
        self.results_row['Git_Develop_Commit_Number'] = self._git_commit_number
        self.results_row['Git_Commit_ID'] = self._git_commit_id
        self.results_row['Sensor'] = str(self.sensor_module)
        self.results_row['MCM-SDK_Version'] = self._mcm_sdk_version
        self.results_row['PFMEA_Operation_Code'] = pfmea_table.pfmea_filter._cur_op_code
        self.results_row['SW_Version'] = __version__
        self.results_row['Hostname'] = host.HOST_NAME
        self.results_row['Filename'] = self.file_name
        self.results_row['Datetime'] = datetime.datetime.now().strftime("%y%m%d%H%M%S")

        # reset LES fault column
        self.les_fault_pos = False

    def measure(self, device=None, measurement=None):
        if measurement is None:
            logger.error("Please provide measurement.")
            return
        
        if device == 'lab':
            return self.lab.measure(measurement=measurement)
        elif device == 'sensor':
            return self.sensor.measure(measurement=measurement)

    def set_input(self, device=None, cur_result_input=None, result=None):
        if result is None:
            logger.error("Please provide result that needs to be updated.")
        if cur_result_input is None:
            logger.error("Please provide input to result to be updated.")

        if device == 'sensor':
            self.sensor.set_input(cur_result_input, result=result)
        elif device == 'lab':
            self.lab.set_input(cur_result_input, result=result)

    def cal_recharge_ctr_check(self):
        """Recharge Counter Min-Max Check
        """
        logger.debug("Starting recharge counter min & max check.")
        cur_recharge_ctr = self.sensor_module.vcsel_recharge_ctr

        # Recharge Ctr Check
        self.sensor_module.set_vcsel_recharge(self.sensor_module.config.VCSEL_RECHARGE_MIN)
        min_op = self.lab.optical_power()
        self.sensor_module.set_vcsel_recharge(self.sensor_module.config.VCSEL_RECHARGE_MAX)
        max_op = self.lab.optical_power()

        # If difference between max and min boot is under 1mW, recharge ctr check has failed
        if abs(max_op - min_op) < 1.0:
            logger.error("VCSEL recharge counter cal min-max check fail as optical power at \
                max recharge counter {} and min recharge counter {} is within 1mw."\
                .format(max_op, min_op))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGRECHARGECYCLE')
            logger.input("Check vcsel recharge counter. Hit enter to continue.")

        self.sensor_module.set_vcsel_recharge(cur_recharge_ctr)

        logger.debug("Recharge ctr min & max check complete.")

    def handle_cal_algo_fail(self, cur_input):
        """Handle cal algo failure

        Args:
            cur_input (float): Recharge ctr float value
        """
        # Failure 1 - Optical power is too low
        if self.lab.optical_power() < config.MIN_OPTICAL_POWER:
            logger.error("Optical power too low for ch {} and pl {}.".format(self.sensor_module.cur_ch, self.cur_power_level))
            self.cal_recharge_ctr_check()
            pfmea_table.pfmea_filter.error('SETUP_CAL_OPTICALPOWERLOW')
            logger.input("Check if unit is firing laser or if unit position is correct. Hit enter to continue.")
        # Failure 2 - Crossed maximum number of iterations to converge
        if self.total_iter >= config.VCSEL_BS_FAIL_ITER:
            logger.error("Too many iterations for ch {} to converge to current power level {}.".format(
                self.sensor_module.cur_ch, self.cur_power_level))
            self.cal_recharge_ctr_check()
            pfmea_table.pfmea_filter.error('VCSELCAL_CAL_ITER')
            logger.input("Check if unit position is correct. Hit enter to continue.")
    
    def vcsel_cal(self):
        """Vcsel cal
        """
        start_time = time.time()
        setpoint = self.power_level_table[self.cur_power_level]

        if self.cur_power_level in [0, 1]:
            # Set threshold as 10% for PL 0,1
            setpoint_margin = setpoint * 0.1
        else:
            setpoint_margin = setpoint * self.setpoint_threshold

        # Clear fault
        if self.sensor_module.clear_fault() < 0:
            if self.version == 'VC':
                # For integrating sphere, if there is a fault, we need to fix it
                self.les_fault_pos = True
                logger.error("Fault not cleared.")
                logger.input("Please hit enter to continue.")
            else:
                # For the rest, we will let this power level fail to continue data collection
                logger.warn("Fault not cleared.")
                pass

        # Power level 0
        if self.cur_power_level == 0:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.002)
        # Power level 1
        elif self.cur_power_level == 1:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 2
        elif self.cur_power_level == 2:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self.cur_power_level == 3:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input, step_size=0.005)
        # Power level 4
        elif self.cur_power_level == 4:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input, step_size=0.005)
        # Power level 5
        elif self.cur_power_level == 5:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input, step_size=0.005)
        # Power level 6
        elif self.cur_power_level == 6:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 7
        elif self.cur_power_level == 7:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 8
        elif self.cur_power_level == 8:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 9
        elif self.cur_power_level == 9:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 10
        elif self.cur_power_level == 10:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 11
        elif self.cur_power_level == 11:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 12
        elif self.cur_power_level == 12:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 13
        elif self.cur_power_level == 13:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 14
        elif self.cur_power_level == 14:
            self.meas_name = 'Optical Power in mW'
            self.meas_func = self.lab.optical_power
            self.input_name = 'vcsel_recharge_ctr'
            self.input_func = self.sensor_module.set_vcsel_recharge
            self.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=self.end_input)
        # Power level 15
        elif self.cur_power_level == 15:
            self.sensor_module.set_vcsel_recharge(config.VCSEL_PL15_RECHARGE)

        # check thread stop
        if self.check_stop():
            return
        end_time = time.time()
        self.meas_time = end_time - start_time
        self.populate_cur_row()
        self.results_file.write(self.results_row)

        self.clear_cal_algo()
        self.results_file.file.flush()

    def vcsel_cal_IS(self):
        """VCSEL Calibration method using integrating sphere
        """
        # enable vcsel
        self.sensor_module.enable_vcsel(1)
        for pl in config.VCSEL_CAL_PL_ORDER:
            # check thread stop
            if self.check_stop():
                return
            self.cur_power_level = pl
            logger.debug("\n\nStarting vcsel calibration for pl {}"
                            .format(self.cur_power_level))

            self.vcsel_cal()

        # disable vcsel
        self.sensor_module.enable_vcsel(0)

    def run(self, *args, **kwargs):
        """Run Vcsel Cal
        """
        version = self.version.split("_")[0]

        if version == "VC":
            self.vcsel_cal_IS()

