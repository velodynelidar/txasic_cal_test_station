
Imax = 50.0
Imin = -50.0

class PID(object):
    def __init__(self, P=0.2, I=0.0, D=0.0):
        
        self.Kp = P
        self.Ki = I
        self.Kd = D

        self.Istate = 0.0
        self.Dstate = 0.0

        self.Pterm = 0.0
        self.Iterm = 0.0
        self.Dterm = 0.0

        self.prev_time = 0.0

        self.output = 0.0

    def update(self, error=0.0, input=0.0, cur_time=0.0):
        delta_time = 1e-3
        self.prev_time = cur_time

        self.Pterm = self.Kp * error

        self.Istate += error

        if self.Istate > Imax:
            self.Istate = Imax
        elif self.Istate < Imin:
            self.Istate = Imin

        self.Iterm = self.Ki * self.Istate * delta_time

        self.Dterm = self.Kd * (input - self.Dstate) / delta_time
        self.Dstate = input

        self.output = self.Pterm + self.Iterm + self.Dterm

        return self.output