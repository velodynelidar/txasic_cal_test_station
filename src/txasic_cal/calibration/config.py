from txasic_cal.host import host

## Calibration File Configurations
VERSION_NUMBER = '5.2'

## Calibration Configurations
# Log linear power steps for PRF at 60 KHz (old pl)
# AVG_POW = [0.100, 0.137, 0.187, 0.257, 0.352, 0.482, 0.661, 0.905, 1.24, 1.7, 2.32, 3.19, 4.37, 5.98, 8.2, 11.2]

# Default PRF used to create PL table 60KHz
DEFAULT_CAL_PRF = 60

# New pl values (scaled to 0.3mW for PL1)
# v2.0 average power
# AVG_POW = [0.229, 0.300, 0.393, 0.515, 0.674, 0.884, 1.157, 1.516, 1.986, 2.6, 3.41, 4.47, 5.85, 7.66, 10.0, 13.1, 17.2, 22.56]

# v4.0 average power
# AVG_POW = [0.223, 0.669, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193]

# v5.0 average power
AVG_POW = [0.223, 0.271, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193]

CAL_AVG_POW = AVG_POW

START_POWER_LEVEL = 10

# Tune to 5% of desired value
SETPOINT_THRESHOLD = 0.05

DEFAULT_SEARCH_ALGO = 'BS'

# CAL_PL_ORDER = [10]

# Order in which cal occurs
# v2.0
# CAL_PL_ORDER = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12, 13, 14, 15]

# v4.0
# CAL_PL_ORDER = [10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1, 11, 12, 13, 14, 15]

# v5.0
CAL_PL_ORDER = [8, 7, 6, 5, 4, 3, 2, 9, 10, 11, 12, 13, 14, 15]

VCSEL_CAL_PL_ORDER = [15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
# VCSEL_CAL_PL_ORDER = [0]

DEFAULT_VCSEL_CAL_PRF_AMS = 85.3
DEFAULT_VCSEL_CAL_PRF_LUM = 36.66

DEFAULT_VCSEL_CAL_PRF = None

# PL values in mW
# VCSEL_AVG_POW = [0.335, 0.671, 1.343, 2.6875, 5.375, 10.75, 21.5, 43, 86, 172] # Old PL0-PL9 values (divide by 2 factor)
VCSEL_AVG_POW_AMS = [0.392, 0.589, 0.883, 1.325, 1.988, 2.982, 4.474, 6.711, 10.066, 15.1, 22.65, 33.975, 50.963, 76.444, 100, 172] # PL0-15 (divide by 1.5 Factor)
VCSEL_AVG_POW_LUM = [0.34, 0.51, 0.77, 1.15, 1.73, 2.6, 3.9, 5.85, 8.78, 13.17, 19.75, 29.63, 44.44, 66.67, 100, 150] # PL0-15 (divide by 1.5 Factor)

VCSEL_CAL_AVG_POW = None

VCSEL_PL15_RECHARGE = 3.198

MAX_ITER = 25
BS_FAIL_ITER = 50
TOTAL_FAIL_ITER = 125
VCSEL_BS_FAIL_ITER = 125
VCSEL_TOTAL_FAIL_ITER = 225
# TOTAL_FAIL_ITER = 10
PID_FAIL_ITER = 200

START_BS_RESULT = {'bias':1.5, 'boot':4.0, 'vcsel_recharge_ctr':0}
END_BS_RESULT = {'bias':4.5, 'boot':8.0, 'vcsel_recharge_ctr':3.198}

BS_STEP_SIZE = 0.01

CALIBRATION_HEADER = ['Peak Power(W)', 'Oven Set Temperature(C)', 'Average_Power_Table', 'Total_Iter', 'Input_List',
                      'Meas_List', 'Meas_Time', 'Average_Power_Ratio', 'Average_Power_Percent_Difference', 'Bias Delta',
                      'PL0_Width', 'PL0_Target_Intensity', 'PL0_Target_Intensity_STD', 'PL0_Target_Distance', 'PL0_Target_Distance_STD',
                      'ADC_Capture', 'LES_Fault',
                    #   'Boot_Within_Average', 'Bias_Within_Average', 
                      'Unit', 'Version', 'Git_Tag',  'Git_Tag_Message', 'Git_Develop_Commit_Number', 'Git_Commit_ID', 
                      'Sensor', 'MCM-SDK_Version', 'PFMEA_Operation_Code', 'SW_Version', 'Hostname', 'Datetime', 'Filename', 'Skip_Channel']

VCSEL_CALIBRATION_HEADER = ['Peak Power(W)', 'Average_Power_Table', 'Total_Iter', 'Input_List',
                            'Meas_List', 'Meas_Time', 'Average_Power_Ratio', 'Average_Power_Percent_Difference', 'LES_Fault',
                            'Unit', 'Version', 'Git_Tag',  'Git_Tag_Message', 'Git_Develop_Commit_Number', 'Git_Commit_ID',
                            'Sensor', 'MCM-SDK_Version', 'PFMEA_Operation_Code', 'SW_Version', 'Hostname', 'Datetime', 'Filename', 'Skip_Channel']

# Time per measurement wait
SLEEP_TIME = 0.35

PULSE_WIDTH_TARGET = 7.9
PULSE_WIDTH_THRESHOLD = 0.01

FAILURE_COUNT = 5

# Average power table location (Not being used right now)
AVG_POWER_TABLE = r"txasic_cal/tools/post_processing/data/txcal_power_level_analytics.csv"

# Minimum optical power for failure check 10uW
MIN_OPTICAL_POWER = 0.01

# Maximum average power variation allowed
CAL_FAILURE_AVG_POWER_PERCENT = 15.0

# NFK Enable (Overwritten by host)
NFK = host.LAB_HOST_DETAILS[host.HOST_NAME]["NFK"]["station"]
FAKE_NFK = host.LAB_HOST_DETAILS[host.HOST_NAME]["NFK"]["fake"]

# Slope approximation for PL1
PL1_SLOPE = 5/8

# Scan vertical position for minimum dazzle
MIN_VERT_DAZZLE_POS = 3

# Boot Slope
BOOT_SLOPE = 0.0022

# Target Distance (m)
TARGET_DIST = 1

# PL0 Checks
PL0_WIDTH_MIN = 35
PL0_WIDTH_MAX = 45

PL0_DIST_MIN = TARGET_DIST - 0.2
PL0_DIST_MAX = TARGET_DIST + 0.2
PL0_DIST_STD = 0.5

PL0_INT_HARD_MIN = 2.5
PL0_INT_SOFT_MIN = 5
PL0_INT_STD = 10

# Scope vertical settings required to trigger without saturation
SCOPE_PL_VERTICAL = [10e-3, 10e-3, 10e-3, 10e-3, 10e-3, 20e-3, 30e-3, 30e-3, 30e-3, 50e-3, 80e-3, 80e-3, 100e-3, 100e-3, 200e-3, 200e-3]

# Nominal values for each power level
NOMINAL_BOOT = [4.457, 4.457, 4.457, 4.545, 4.653, 4.786, 4.951, 5.158, 5.428, 5.193, 5.193, 5.057, 5.352, 5.352, 5.961, 8]
NOMINAL_BIAS = [2.083, 2.127, 2.151, 2.151, 2.151, 2.151, 2.151, 2.151, 2.151, 2.149, 2.24, 2.24, 2.239, 2.366, 2.367, 2.391]
NOMINAL_LP = [14, 14, 14, 14, 14, 14, 14, 14, 14, 12, 12, 0, 0, 0, 0, 0]