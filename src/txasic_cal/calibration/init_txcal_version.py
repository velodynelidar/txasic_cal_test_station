#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-8-12
#  Description   : Init calibration version classes

import sys
import time
import numpy as np
import pandas as pd


from txasic_cal.calibration import config
from txasic_cal.tools.logger import logger
from txasic_cal.tools import TXCAL_DATA_DIR

class version_v1_0:
    '''Version 1.0
    Calibration Order - 10-1, 11-15
    LP - 0xE PL 0-10, 0x0 PL 11-15, 0xF PL 0
    Boot cal - PL 8, 9, 11, 12 (Max Boot 8V - PL14, 15)
    Bias cal - PL 1-7, 10, 13-15
    Pulse width cal - PL 15

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "1.0"
        self._default_cal_prf = 60.0       # KHz
        
        # Set as PL1 = 0.3 and PL2-15 as PL1 * 1.31 ** PL
        self._cal_avg_power = [0, 0.300, 0.393, 0.515, 0.674, 0.884, 1.157, 
            1.516, 1.986, 2.6, 3.41, 4.47, 5.85, 7.66, 10.0, 13.1, 17.2, 22.56]
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12, 13, 14, 15]

        self.pl10_bias = 0

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [14, 15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']

        # Power level 0
        if self._cm.cur_power_level == 0:
            # Set lp 0xF
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[0]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
        # Power level 1
        elif self._cm.cur_power_level == 1:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.lab.mso.Vertical_Scale(1,10e-3,-4,0)
            self._cm.lab.mso.Set_TriggerLevel()
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            # Tune PL15 using PID
            # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.search_algo = 'PID'
            sphere_pulse_width_target = 7.9
            setpoint_margin = sphere_pulse_width_target * self._cm.setpoint_threshold
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Pulse Width(ns)'
            self._cm.meas_func = self._cm.lab.pulse_width
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias

            if self._cm.lab.gonio:
                # Move to amp position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_amp_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_amp_pos['y'][self._cm.sensor_module.cur_ch])
                self._cm.lab.mso_autoset()

            self._cm.tune_pid(setpoint=setpoint, setpoint_margin=setpoint_margin,
                        cur_input=cur_input)

            if self._cm.lab.gonio:
                # Move to op position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_op_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_op_pos['y'][self._cm.sensor_module.cur_ch])

            # Reset search algo
            self._cm.search_algo = config.DEFAULT_SEARCH_ALGO


class version_v2_0:
    '''Version 2.0
    Calibration Order - 10-0, 11-15
    LP - 0xE PL 0-10, 0x0 PL 11-15
    Boot cal - PL 1-9, 11, 12 (Max Boot 8V - PL14, 15)
    Bias cal - PL 10, 0, 13-15

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "2.0"
        self._default_cal_prf = 60.0       # KHz
        
        # Set as PL1 = 0.3 and PL2-15 as PL1 * 1.31 ** PL and PL0 = PL1 / 1.31 ** 1
        self._cal_avg_power = [0.229, 0.300, 0.393, 0.515, 0.674, 0.884, 1.157, 
            1.516, 1.986, 2.6, 3.41, 4.47, 5.85, 7.66, 10.0, 13.1, 17.2, 22.56]
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12, 13, 14, 15]

        self.pl10_bias = 0

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [14, 15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']

        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 1
        elif self._cm.cur_power_level == 1:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.lab.mso.Vertical_Scale(1,10e-3,-4,0)
            self._cm.lab.mso.Set_TriggerLevel()
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)


# v3 calibration algorithm is the same as v2
class version_v3_0:
    '''Version 3.0
    Calibration Order - 10-0, 11-15
    LP - 0xE PL 0-10, 0x0 PL 11-15
    Boot cal - PL 1-9, 11, 12 (Max Boot 8V - PL14, 15)
    Bias cal - PL 10, 0, 13-15

    PL10 Start boot - 6V
    PL10 Start bias - 3V

    v3.0 is the same as v2.0
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "3.0"
        self._default_cal_prf = 60.0       # KHz
        
        # Set as PL1 = 0.3 and PL2-15 as PL1 * 1.31 ** PL and PL0 = PL1 / 1.31 ** 1
        self._cal_avg_power = [0.229, 0.300, 0.393, 0.515, 0.674, 0.884, 1.157, 
            1.516, 1.986, 2.6, 3.41, 4.47, 5.85, 7.66, 10.0, 13.1, 17.2, 22.56]
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 11, 12, 13, 14, 15]

        self.pl10_bias = 0

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [14, 15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']

        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 1
        elif self._cm.cur_power_level == 1:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 16
        elif (self._cm.cur_power_level) == 16 and (self._cm.version[0:2] != "IS"):
            # Tune PL16 using PID
            # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.search_algo = 'PID'
            config.PULSE_WIDTH_TARGET = 3.5
            setpoint = config.PULSE_WIDTH_TARGET
            setpoint_margin = config.PULSE_WIDTH_THRESHOLD * setpoint
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Pulse Width(ns)'
            self._cm.meas_func = self._cm.lab.pulse_width
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias

            if self._cm.lab.gonio:
                # Move to amp position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_amp_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_amp_pos['y'][self._cm.sensor_module.cur_ch])
                self._cm.lab.mso_autoset()

            self._cm.tune_pid(setpoint=setpoint, setpoint_margin=setpoint_margin,
                        cur_input=cur_input)

            if self._cm.lab.gonio:
                # Move to op position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_op_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_op_pos['y'][self._cm.sensor_module.cur_ch])

            # Reset search algo
            self._cm.search_algo = config.DEFAULT_SEARCH_ALGO
        # Power level 17
        elif (self._cm.cur_power_level) == 17 and (self._cm.version[0:2] != "IS"):
            # Tune PL17 using PID
            self._cm.search_algo = 'PID'
            config.PULSE_WIDTH_TARGET = 4.0
            setpoint = config.PULSE_WIDTH_TARGET
            setpoint_margin = config.PULSE_WIDTH_THRESHOLD * setpoint
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Pulse Width(ns)'
            self._cm.meas_func = self._cm.lab.pulse_width
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias

            if self._cm.lab.gonio:
                # Move to amp position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_amp_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_amp_pos['y'][self._cm.sensor_module.cur_ch])
                self._cm.lab.mso_autoset()

            self._cm.tune_pid(setpoint=setpoint, setpoint_margin=setpoint_margin,
                        cur_input=cur_input)

            if self._cm.lab.gonio:
                # Move to op position
                self._cm.lab.gonio.move_to_position(
                    self._cm.lab.gonio.chan_op_pos['x'][self._cm.sensor_module.cur_ch],
                    self._cm.lab.gonio.chan_op_pos['y'][self._cm.sensor_module.cur_ch])

            # Reset search algo
            self._cm.search_algo = config.DEFAULT_SEARCH_ALGO


class version_v4_0:
    '''Version 4.0
    Calibration Order - 10-2, 0, 1, 11-15
    LP - 0xE PL 0, 2-10, 0x0 PL 11-15
    Boot cal - PL 2-9, 11, 12 (Max Boot 8V - PL14, 15)
    Bias cal - PL 10, 0, 13-15
    LP search - PL 1 (~3xPL0)

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "4.0"
        self._default_cal_prf = 60.0       # KHz
        
        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = PL1 / 1.34 ** 1 and PL1 ~ 3xPL0
        self._cal_avg_power = np.array([0.223, 0.669, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 
            1.737, 2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1, 11, 12, 13, 14, 15]

        self.pl10_bias = 0
        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [14, 15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']

        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 1
        elif self._cm.cur_power_level == 1:
            # Iterate through LP bits and store LP bit that is 3x PL0
            min_op_setpoint = 100000
            min_lp = self._cm.cur_lp_num
            for lp in self._cm.sensor.config.LP_BITS[2:]:
                self._cm.cur_lp_num = lp
                self._cm.sensor_module.set_lp(lp)
                time.sleep(config.SLEEP_TIME)
                pl1_op = self._cm.lab.optical_power()
                diff_op = abs(pl1_op - setpoint)
                if diff_op < min_op_setpoint:
                    min_op_setpoint = diff_op
                    min_lp = lp
            self._cm.sensor_module.set_lp(min_lp)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)


class version_v4_5:
    '''Version 4.5
    Calibration Order - 8-2, 0, 1, 9-15
    LP - 0xE PL 0-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-7, 9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 8, 10, 0, 13, 15
    PL 1 bias = PL 0 bias + (PL 2 bias - PL 0 bias) * 5/8

    PL10 Start boot - 5.2V
    PL10 Start bias - 2.7V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "4.5"
        self._default_cal_prf = 60.0       # KHz
        
        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = PL1 / 1.34 ** 1 and PL1 ~ 3xPL0
        self._cal_avg_power = np.array([0.223, 0.271, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 
            1.737, 2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 0, 1, 11, 12, 13, 14, 15]

        self.pl10_bias = 0
        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']

        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
        # Power level 1
        elif self._cm.cur_power_level == 1:
            cur_bias = self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] + (self.pl2_ch_bias[self._cm.sensor_module.cur_ch] - \
                self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch]) * config.PL1_SLOPE
            self._cm.set_input("sensor", cur_bias, "set_bias")
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)


class version_nfk:
    '''
    PL0 target calibration

    Check pl0 sphere lock, if false, prompt to move sphere
    Set PL2 boot to PL0 for all channels, enable all channels along with NFK
    Capture PCAP
    Set pl0 sphere lock to true
    Measure PL0 values

    This class can't executed by itself, needs a supported version
    '''
    def __init__(self):
        self.pl0_lock = False
        self.nftarget_csv_file = TXCAL_DATA_DIR / 'results' / 'pl0_target_snapshot.csv'
        self.pl0_locked_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

    def pl0_target_cal(self):
        if self.pl0_lock == False:
            self._cm.sensor_module.set_intensity()
            self._cm.sensor_module.disable_all_channel()
            logger.input("Remove sphere from front of unit and press enter.")
            # Set all channels to PL0
            self._cm.sensor_module.set_power_all(self._cm.cur_power_level)
            
            # Set values from calibrated PL2
            for i in self._cm.sensor_module.channels:
                self._cm.sensor_module.set_boot(self.pl2_ch_boot[i], ch_num=i, pl=self._cm.cur_power_level, set_all=False)
                self._cm.sensor_module.set_bias(2.0, ch_num=i, pl=self._cm.cur_power_level, set_all=False)
                self._cm.sensor_module.set_lp(self._cm.sensor.config.LP_BITS[1], ch_num=i, pl=self._cm.cur_power_level, set_all=False)

            # Enable all channels so PL0 can lock
            self._cm.sensor_module.enable_all_channel()

            # Enable NFK
            self._cm.sensor_module.set_nfbias_enable(1)

            time.sleep(0.1)

            # Measure width, bias, target return intensity and distance
            self._cm.sensor_module.get_nfbias_width()
            self._cm.sensor_module.get_nfbias_snapshot()
            self._cm.sensor_module.gen_pcap(500, self.nftarget_csv_file)

            for i in self._cm.sensor_module.channels:
                try:
                    self._cm.pl0_locked_ch_adc_cap[i] = self._cm.sensor_module.get_adc_cap(ch_num=i)
                except ValueError:
                    self._cm.pl0_locked_ch_adc_cap[i] = None

            # Disable all channels
            self._cm.sensor_module.disable_all_channel()

            # Set all channels to scratchpad power level
            self._cm.sensor_module.set_power_all(self._cm.sensor.config.SCRATCHPAD_PL)

            # Disable NFK
            self._cm.sensor_module.set_nfbias_enable(0)
            logger.input("Put sphere back in front of unit and press enter.")
            self.pl0_lock = True
            self._cm.sensor_module.enable_channel(self._cm.sensor_module.cur_ch, self._cm.sensor.config.SCRATCHPAD_PL)
        
        self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.\
            convert_from_dec("bias", self._cm.sensor_module.nfk_bias[self._cm.sensor_module.cur_ch])
        
        self._cm.pl0_locked_ch_width[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.nfk_width[self._cm.sensor_module.cur_ch]

        df = pd.read_csv(self.nftarget_csv_file)
        
        self._cm.pl0_locked_ch_intensity[self._cm.sensor_module.cur_ch]['mean'], self._cm.pl0_locked_ch_intensity[self._cm.sensor_module.cur_ch]['std'] \
        = self._cm.parse_pcap_column(self._cm.sensor_module.cur_ch, df, 'Intensity')
        
        self._cm.pl0_locked_ch_distance[self._cm.sensor_module.cur_ch]['mean'], self._cm.pl0_locked_ch_distance[self._cm.sensor_module.cur_ch]['std'] \
        = self._cm.parse_pcap_column(self._cm.sensor_module.cur_ch, df, 'Distance')

        self._cm.sensor_module.set_bias(self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch])
        self._cm.sensor_module.set_boot(self.pl2_ch_boot[self._cm.sensor_module.cur_ch])
        self._cm.sensor_module.set_lp(self._cm.sensor.config.LP_BITS[1])
        time.sleep(config.SLEEP_TIME)

        self._cm.pl0_error_check()

class version_v5_0(version_nfk):
    '''Version 5.0
    Calibration Order - 8-2, 0, 1, 9-15
    LP - 0xE PL 0-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-7, 9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 8, 10, 0, 13, 15

    PL 0 - NFK - Remove sphere from front of sphere, set PL 2 boot and enable NFK
    PL 1 bias = PL 0 bias + (PL 2 bias - PL 0 bias) * 5/8

    PL8 Start boot - 5.2V
    PL8 Start bias - 2.7V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "5.0"
        self._default_cal_prf = 60.0       # KHz

        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = NFK, PL1 = PL0 bias + (PL2 bias - PL0 bias) * 5/8
        self._cal_avg_power = np.array([0.223, 0.271, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 
            2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        self._cal_pl_order = [8, 7, 6, 5, 4, 3, 2, 9, 10, 11, 12, 13, 14, 15]
        self._cal_nfk_order = [0, 1]

        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_boot = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 5.2
        self._start_bias = 2.7

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [15]

        super().__init__()

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:

            self.pl0_target_cal()

        # Power level 1
        elif self._cm.cur_power_level == 1:
            cur_bias = round(self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] + (self.pl2_ch_bias[self._cm.sensor_module.cur_ch] - \
                self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch]) * config.PL1_SLOPE, 3)
            self._cm.set_input("sensor", cur_bias, "set_bias")
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
            self.pl2_ch_boot[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.boot
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl8_bias = self._cm.sensor_module.bias
        # Power level 9
        elif self._cm.cur_power_level == 9:
            # Set mid lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[len(self._cm.sensor.config.LP_BITS)//2]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.set_input("sensor", self.pl8_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)


class version_v5_1(version_nfk):
    '''Version 5.1
    Calibration Order - 10-2, 0, 1, 11-15
    LP - 0xE PL 0, 2-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 10, 0, 13, 15

    PL 0 - NFK - Remove sphere from front of sphere, set PL 2 boot and enable NFK
    PL 1 bias = PL 0 bias + (PL 2 bias - PL 0 bias) * 5/8

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "5.1"
        self._default_cal_prf = 60.0       # KHz

        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = NFK, PL1 = PL0 bias + (PL2 bias - PL0 bias) * 5/8
        self._cal_avg_power = np.array([0.223, 0.271, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 
            1.737, 2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        
        self._cal_pl_order = [10, 9, 8, 7, 6, 5, 4, 3, 2, 11, 12, 13, 14, 15]
        self._cal_nfk_order = [0, 1]

        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_boot = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [14, 15]

        super().__init__()

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:

            self.pl0_target_cal()

        # Power level 1
        elif self._cm.cur_power_level == 1:
            cur_bias = round(self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] + (self.pl2_ch_bias[self._cm.sensor_module.cur_ch] - \
                self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch]) * config.PL1_SLOPE, 3)
            self._cm.set_input("sensor", cur_bias, "set_bias")
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
            self.pl2_ch_boot[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.boot
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)


class version_v5_2(version_nfk):
    '''Version 5.2
    Calibration Order - dummy 10, 8-2, 0, 1, 9-15
    LP - 0xE PL 0-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-7, 9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 8, 10, 0, 13, 15

    PL 0 - NFK - Remove sphere from front of sphere, set PL 2 boot and enable NFK
    PL 1 bias = PL 0 bias + (PL 2 bias - PL 0 bias) * 5/8

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "5.2"
        self._default_cal_prf = 60.0       # KHz

        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = NFK, PL1 = PL0 bias + (PL2 bias - PL0 bias) * 5/8
        self._cal_avg_power = np.array([0.223, 0.271, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 
            2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        self._cal_pl_order = [8, 7, 6, 5, 4, 3, 2, 9, 10, 11, 12, 13, 14, 15]
        self._cal_nfk_order = [0, 1]

        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_boot = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [15]

        super().__init__()

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:

            self.pl0_target_cal()

        # Power level 1
        elif self._cm.cur_power_level == 1:
            cur_bias = round(self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] + (self.pl2_ch_bias[self._cm.sensor_module.cur_ch] - \
                self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch]) * config.PL1_SLOPE, 3)
            self._cm.set_input("sensor", cur_bias, "set_bias")
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
            self.pl2_ch_boot[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.boot
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")

            # Dummy PL10
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            pl10_setpoint = self._cm.power_level_table[10]
            pl10_setpoint_margin = pl10_setpoint * config.SETPOINT_THRESHOLD

            logger.debug("Performing dummy PL 10 cal.")
            self._cm.tune_bs(setpoint=pl10_setpoint, setpoint_margin=pl10_setpoint_margin)

            if self._cm.skip_channel:
                return

            # PL8
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            logger.debug("Post dummy PL 8 boot cal")
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl8_bias = self._cm.sensor_module.bias
        # Power level 9
        elif self._cm.cur_power_level == 9:
            # Set mid lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[len(self._cm.sensor.config.LP_BITS)//2]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.set_input("sensor", self.pl8_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)


class version_v5_3:
    '''Version 5.3
    Calibration Order - dummy 10, 8-2, 0, 1, 9-15
    LP - 0xE PL 0-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-7, 9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 8, 10, 0, 1, 13, 15

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "5.3"
        self._default_cal_prf = 60.0       # KHz

        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = NFK, PL1 = PL0 bias + (PL2 bias - PL0 bias) * 5/8
        self._cal_avg_power = np.array([0.167, 0.223, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 
            2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        self._cal_pl_order = [8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 10, 11, 12, 13, 14, 15]
        self._cal_nfk_order = [0, 1]

        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_boot = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [15]

        super().__init__()

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 1
        elif self._cm.cur_power_level == 1:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
            self.pl2_ch_boot[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.boot
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")

            # Dummy PL10
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            pl10_setpoint = self._cm.power_level_table[10]
            pl10_setpoint_margin = pl10_setpoint * config.SETPOINT_THRESHOLD

            logger.debug("Performing dummy PL 10 cal.")
            self._cm.tune_bs(setpoint=pl10_setpoint, setpoint_margin=pl10_setpoint_margin)

            if self._cm.skip_channel:
                return

            # PL8
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            logger.debug("Post dummy PL 8 boot cal")
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl8_bias = self._cm.sensor_module.bias
        # Power level 9
        elif self._cm.cur_power_level == 9:
            # Set mid lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[len(self._cm.sensor.config.LP_BITS)//2]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.set_input("sensor", self.pl8_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)

class version_v5_4:
    '''Version 5.4
    Calibration Order - dummy 10, 8-2, 0, 1, 9-15
    LP - 0xE PL 0-8, 0xC PL 9-10, 0x0 PL 11-15
    Boot cal - PL 2-7, 9, 11-12, 14 (Max Boot 8V - 15)
    Bias cal - PL 8, 10, 0, 1, 13, 15

    PL10 Start boot - 6V
    PL10 Start bias - 3V
    '''
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "5.4"
        self._default_cal_prf = 60.0       # KHz

        # Set as PL2 = 0.3 and PL3-15 as PL2 * 1.34 ** PL and PL0 = NFK, PL1 = PL0 bias + (PL2 bias - PL0 bias) * 5/8
        self._cal_avg_power = np.array([0.167, 0.223, 0.3, 0.402, 0.539, 0.722, 0.967, 1.296, 1.737, 
            2.327, 3.119, 4.179, 5.6, 7.504, 10.055, 13.474, 18.055, 24.193])
        self._cal_pl_order = [8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 10, 11, 12, 13, 14, 15]
        self._cal_nfk_order = [0, 1]

        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl0_locked_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [15]

        super().__init__()

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
        # Power level 1
        elif self._cm.cur_power_level == 1:
            cur_bias = self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch] + (self.pl2_ch_bias[self._cm.sensor_module.cur_ch] - \
                self.pl0_locked_ch_bias[self._cm.sensor_module.cur_ch]) * config.PL1_SLOPE
            self._cm.set_input("sensor", cur_bias, "set_bias")
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")

            # Dummy PL10
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            pl10_setpoint = self._cm.power_level_table[10]
            pl10_setpoint_margin = pl10_setpoint * config.SETPOINT_THRESHOLD

            logger.debug("Performing dummy PL 10 cal.")
            self._cm.tune_bs(setpoint=pl10_setpoint, setpoint_margin=pl10_setpoint_margin)

            if self._cm.skip_channel:
                return

            # PL8
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            logger.debug("Post dummy PL 8 boot cal")
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl8_bias = self._cm.sensor_module.bias
        # Power level 9
        elif self._cm.cur_power_level == 9:
            # Set mid lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[len(self._cm.sensor.config.LP_BITS)//2]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.set_input("sensor", self.pl8_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 10
        elif self._cm.cur_power_level == 10:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
            self.pl10_bias = self._cm.sensor_module.bias
        # Power level 11
        elif self._cm.cur_power_level == 11:
            # # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL10 Bias Voltage
            self._cm.set_input("sensor", self.pl10_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)


class version_v6_0:
    def __init__(self, cm):
        self._cm = cm
        self._version_number = "6.0"
        self._default_cal_prf = 74.64       # KHz

        # Set as PL0 = 0.275 and PL1-15=PL0*(1.351)^pl
        self._cal_avg_power = np.array([0.275, 0.372, 0.502, 0.678, 0.916, 1.238, 1.672, 2.26, 
            3.052, 4.123, 5.57, 7.53, 10.167, 13.736, 18.557, 25.07])
        
        self._cal_pl_order = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 10, 11, 12, 13, 14, 15]

        self.pl2_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]
        self.pl8_bias = 0
        self.pl10_bias = 0
        self.pl2_ch_boot = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self.pl0_locked_ch_bias = [0 for i in range(self._cm.sensor_module.config.NUM_CHANNELS)]

        self._start_boot = 6.0
        self._start_bias = 3.0

        self._cm.power_level_table = self._cal_avg_power
        self._cm.sensor_module.start_boot = self._start_boot
        self._cm.sensor_module.start_bias = self._start_bias

        self._max_boot_pl = [13, 14, 15]

    def txcal(self, *args, **kwargs):

        cur_input = kwargs['cur_input']
        setpoint = kwargs['setpoint']
        setpoint_margin = kwargs['setpoint_margin']
        
        # Power level 0
        if self._cm.cur_power_level == 0:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 1
        elif self._cm.cur_power_level == 1:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
            self.pl2_ch_bias[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.bias
            self.pl2_ch_boot[self._cm.sensor_module.cur_ch] = self._cm.sensor_module.boot
        # Power level 2
        elif self._cm.cur_power_level == 2:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 3
        elif self._cm.cur_power_level == 3:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 4
        elif self._cm.cur_power_level == 4:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, step_size=0.005)
        # Power level 5
        elif self._cm.cur_power_level == 5:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 6
        elif self._cm.cur_power_level == 6:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 7
        elif self._cm.cur_power_level == 7:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 8
        elif self._cm.cur_power_level == 8:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 9
        elif self._cm.cur_power_level == 9:
            # Set min lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
            self.pl9_bias = self._cm.sensor_module.bias
        # Power level 10
        elif self._cm.cur_power_level == 10:
            # Set max lp
            self._cm.cur_lp_num = self._cm.sensor.config.LP_BITS[-1]
            self._cm.set_input("sensor", self._cm.cur_lp_num, "set_lp")
            # Set PL9 Bias Voltage
            self._cm.set_input("sensor", self.pl9_bias, "set_bias")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 11
        elif self._cm.cur_power_level == 11:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'boot'
            self._cm.input_func = self._cm.sensor_module.set_boot
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 12
        elif self._cm.cur_power_level == 12:
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin)
        # Power level 13
        elif self._cm.cur_power_level == 13:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 14
        elif self._cm.cur_power_level == 14:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)
        # Power level 15
        elif self._cm.cur_power_level == 15:
            self._cm.set_input("sensor", self._cm.sensor.config.BOOT_MAX, "set_boot")
            self._cm.meas_name = 'Optical Power in mW'
            self._cm.meas_func = self._cm.lab.optical_power
            self._cm.input_name = 'bias'
            self._cm.input_func = self._cm.sensor_module.set_bias
            self._cm.tune_bs(setpoint=setpoint, setpoint_margin=setpoint_margin, end_input=4.0)


class init_txcal:
    def __init__(self, version, cal_module, *args, **kwargs):
        self._cal_version = getattr(sys.modules[__name__], "version_v{}_{}".format(version.split('.')[0], 
                                version.split('.')[1]))(cal_module)
        logger.info("Txcal init version: {}".format(self._cal_version._version_number))

        super().__init__()
    
    def txcal(self, *args, **kwargs):
        if config.FAKE_NFK and self.cur_power_level == 0 and 0 not in self._cal_version._cal_pl_order:
            logger.debug("Fake NFK")
            # For pl 0, fake NFK is pl2 bias - 0.1
            self.sensor_module.set_lp(self.sensor.config.LP_BITS[1])
            self.sensor_module.set_boot(self._cal_version.pl2_ch_boot[self.sensor_module.cur_ch])
            self._cal_version.pl0_locked_ch_bias[self.sensor_module.cur_ch] = self._cal_version.pl2_ch_bias[self.sensor_module.cur_ch] - 0.1
            self.sensor_module.set_bias(self._cal_version.pl0_locked_ch_bias[self.sensor_module.cur_ch])
            time.sleep(0.2)
            return

        return self._cal_version.txcal(*args, **kwargs)
