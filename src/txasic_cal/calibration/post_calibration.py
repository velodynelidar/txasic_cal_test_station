#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-11-12
#  Description   : Post calibration scripts

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import datetime
import time

from txasic_cal.calibration import config
from txasic_cal.tools.logger import logger
from txasic_cal.host import host
from txasic_cal.tools.plot import csv_plots
from txasic_cal.pfmea_code import pfmea_table
from txasic_cal import __version__

class post_calibration:
    def __init__(self, calibration_module):
        """Post TX and VCSEL calibration related modules

        Arguments:
            calibration_module [object] : Calibration class object
        """
        self._cm = calibration_module
        self._tp = csv_plots.txcal_plot(x_name='Power Level', y_name='Optical Power in mW')

        self.init_cal_version = None

        self.results_header = ['Iter', 'Channel', 'Power Level', 'Boot', 'Bias', 'LP', 'PRF', 'System Temperature(C)', 'Temperature',\
             'Optical Power in mW', 'Average_Power_Table', 'Total_Iter', 'Average_Power_Percent_Difference',\
                  'Average_Power_Ratio', 'Oven Set Temperature(C)','Unit', 'Sensor', 'Hostname', 'Version', 'Filename', 'Datetime', 'SW_Version']
        self.results_row = {i:None for i in self.results_header}

    def tx_ch_all_pl_test(self, df, ch, plot_op, plot_rest, each_temp=None):
        """Check all power levels for given channel

        Arguments:
            df [dataframe] : pandas dataframe
            ch [int] : channel number
            plot_op [bool] : Plot optical power vs power level graph
            plot_rest [boot] : Plot everything else
        
        Keyword Arguments:
            each_temp [float] : Oven set temperature
        """
        percent_diff_pl = 10.0
        df = df.sort_values('Power Level')

        op_list = np.around(np.array(df['Optical Power in mW'].to_list()), 3)
        pl_table_list = np.around(np.array(df['Average_Power_Table'].to_list()), 3)
        av_pl_list = np.around(np.array(df['Average_Power_Percent_Difference'].to_list()), 3)

        boot_list = np.around(np.array(df['Boot'].to_list()), 3)
        bias_list = np.around(np.array(df['Bias'].to_list()), 3)

        check_mono_op_list = [op_list[i] <= op_list[i+1] for i in range(len(op_list)-1)]
            
        total_iter = np.array(df['Total_Iter'].to_list())
      
        for i in self._cm.sensor.config.POWER_LEVEL_NUM:
            if abs(av_pl_list[i]) > config.CAL_FAILURE_AVG_POWER_PERCENT and i not in [0, 1]:
                logger.warn("Ch {} Pl {} average power percent difference {} with optical power {} mW and power table {} mW."
                                .format(ch, i, av_pl_list[i], op_list[i], pl_table_list[i]))
                pfmea_table.pfmea_filter.warn('TROSACAL_PST_TGTPERCENTDIFF')

            if i not in [0, 1, 15] and not check_mono_op_list[i]:
                logger.error("Ch {} Pl {} with value {} is non-monotonic with Pl {} value {}".format(ch, i, op_list[i], i + 1, op_list[i + 1]))
                pfmea_table.pfmea_filter.error('TROSACAL_PST_RESULTLOGLIN')
            
            pl_next_diff = round(abs(((op_list[i + 1] - op_list[i])/op_list[i]) * 100), 3) if i not in [0, 1, 15] else 100
            if pl_next_diff < percent_diff_pl:
                logger.error("Ch {} Pl {} with value {} is under 10% (value {}%) of Pl {} value {}.".format(ch, i, op_list[i], \
                    pl_next_diff, i + 1, op_list[i + 1]))
                pfmea_table.pfmea_filter.error('TROSACAL_PST_PLMARGIN')

            if total_iter[i] >= config.TOTAL_FAIL_ITER:
                logger.error("Ch {} Pl {} with optical power {} mW, took {} iterations to complete.".format(ch, i, op_list[i], total_iter[i]))
                pfmea_table.pfmea_filter.error('TROSACAL_PST_SEARCHFAIL')

        if plot_op:
            self._tp.update_xy(y_name='Optical Power in mW')
            self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, op_list, self._tp.y,\
                 label='Channel {}'.format(ch), marker='o', title='Unit {} init cal version {}'.format(self._cm.unit, self.init_cal_version))
            
        if plot_rest:
            self._tp.update_xy(y_name='Boot')
            self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, boot_list, self._tp.y,\
                 label='Channel {}'.format(ch), marker='o', title='Unit {} init cal version {}'.format(self._cm.unit, self.init_cal_version))

            self._tp.update_xy(y_name='Bias')
            self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, bias_list, self._tp.y,\
                 label='Channel {}'.format(ch), marker='o', title='Unit {} init cal version {}'.format(self._cm.unit, self.init_cal_version))

    def vcsel_pl_test(self, df, plot_op, plot_rest):
        """Check vcsel power levels

        Args:
            df (dataframe): Pandas dataframe of power level table
            plot_op (bool) : Plot optical power vs power level graph
            plot_rest (boot) : Plot everything else
        """
        percent_diff_pl = 10.0
        df = df.sort_values('Power Level')

        op_list = np.around(np.array(df['Optical Power in mW'].to_list()), 3)
        pl_table_list = np.around(np.array(df['Average_Power_Table'].to_list()), 3)
        av_pl_list = np.around(np.array(df['Average_Power_Percent_Difference'].to_list()), 3)

        recharge_time_list = np.around(np.array(df['Recharge Time (in microseconds)'].to_list()), 3)

        check_mono_op_list = [op_list[i] <= op_list[i+1] for i in range(len(op_list)-1)]

        total_iter = np.array(df['Total_Iter'].to_list())

        for i in self._cm.sensor.config.POWER_LEVEL_NUM:
            if abs(av_pl_list[i]) > config.CAL_FAILURE_AVG_POWER_PERCENT and i not in [15]:
                logger.warn("Pl {} average power percent difference {} with optical power {} mW and power table {} mW."
                                .format(i, av_pl_list[i], op_list[i], pl_table_list[i]))
                pfmea_table.pfmea_filter.warn('VCSELCAL_PST_TGTPERCENTDIFF')

            if i not in [15] and not check_mono_op_list[i]:
                logger.error("Pl {} with value {} is non-monotonic with Pl {} value {}".format(i, op_list[i], i + 1, op_list[i + 1]))
                pfmea_table.pfmea_filter.error('VCSELCAL_PST_RESULTLOGLIN')
            
            pl_next_diff = round(abs(((op_list[i + 1] - op_list[i])/op_list[i]) * 100), 3) if i not in [15, 14] else 100
            if pl_next_diff < percent_diff_pl:
                logger.error("Pl {} with value {} is under 10% (value {}%) of Pl {} value {}.".format(i, op_list[i], \
                    pl_next_diff, i + 1, op_list[i + 1]))
                pfmea_table.pfmea_filter.error('VCSELCAL_PST_PLMARGIN')

            if total_iter[i] >= config.VCSEL_TOTAL_FAIL_ITER:
                logger.error("Pl {} with optical power {} mW, took {} iterations to complete.".format(i, op_list[i], total_iter[i]))
                pfmea_table.pfmea_filter.error('VCSELCAL_PST_SEARCHFAIL')

        if plot_op:
            self._tp.update_xy(y_name='Optical Power in mW')
            self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, op_list, self._tp.y,\
                 label='VCSEL', marker='o', title='Unit {} init vcsel cal'.format(self._cm.unit))
            
        if plot_rest:
            self._tp.update_xy(y_name='Recharge Time (in microseconds)')
            self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, recharge_time_list, self._tp.y,\
                 label='VCSEL', marker='o', title='Unit {} init vcsel cal'.format(self._cm.unit))


    def cal_verify(self, file_name, plot_op=False, plot_rest=False):
        """Verify csv file

        Arguments:
            file_name [str] : Power level table to verify
        
        Keyword Arguments:
            plot_op [bool] : Plot optical power vs power level graph
            plot_rest [bool] : Plot everything else
        """
        df = pd.read_csv(file_name)

        version = df.iloc[0]['Version']
        self.init_cal_version = version

        # Integrating sphere tx cal verify
        if version[0:2] == "IS":
            df2 = None
            for ch in self._cm.sensor_module.channels:
                df2 = df[df['Channel'] == ch]
                self.tx_ch_all_pl_test(df2, ch, plot_op, plot_rest)

            if plot_op:
                df2 = df2.sort_values('Power Level')
                pl_table_list = df2['Average_Power_Table'].to_list()
                self._tp.update_xy(y_name='Optical Power in mW')
                self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, pl_table_list, self._tp.y,\
                    label='Power Table', marker='o', title='Unit {} init cal version {}'.format(self._cm.unit, self.init_cal_version))
                plt.show()
        
        # Integrating sphere tx temp cal verify
        if version[0:2] == "TS":
            temp_list = [-40, 0, 30, 40, 85]
            for each_temp in temp_list:
                logger.info("Cal verify for Oven Set Temperature {} (C)".format(each_temp))
                df2 = None
                for ch in self._cm.sensor_module.channels:
                    df2 = df[(df['Channel'] == ch) & (df['Oven Set Temperature(C)'] == each_temp)]
                    self.tx_ch_all_pl_test(df2, ch, plot_op, plot_rest)

            if plot_op:
                df2 = df2.sort_values('Power Level')
                pl_table_list = df2['Average_Power_Table'].to_list()
                self._tp.update_xy(y_name='Optical Power in mW')
                self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, pl_table_list, self._tp.y,\
                    label='Power Table', marker='o', title='Unit {} init cal version {}'.format(self._cm.unit, self.init_cal_version))
            
                plt.show()

        # Integrating sphere vcsel cal verify
        if version[0:2] == "VC":
            df2 = df
            self.vcsel_pl_test(df, plot_op, plot_rest)

            if plot_op:
                df2 = df2.sort_values('Power Level')
                pl_table_list = df2['Average_Power_Table'].to_list()
                self._tp.update_xy(y_name='Optical Power in mW')
                self._tp.plot_fig(self._cm.sensor.config.POWER_LEVEL_NUM, pl_table_list, self._tp.y,\
                    label='Power Table', marker='o', title='Unit {} init vcsel cal'.format(self._cm.unit))
                plt.show()

    def pl_stability_test(self, df, ch, pl, ot):
        """Single power level stability test

        Args:
            df (datafram): Pandas dataframe of power level table csv
            ch (int): Channel
            pl (int): Power level
            ot (int): Oven set temperature
        """
        if self.init_cal_version == 'IS':
            df2 = df[df['Channel'] == ch]
        elif self.init_cal_version == 'TS':
            df2 = df[(df['Channel'] == ch) & (df['Oven Set Temperature(C)'] == ot)]

        df2 = df2.sort_values('Power Level')
        pl_table_list = df2['Average_Power_Table'].to_list()
        
        boot_list = np.around(np.array(df2['Boot'].to_list()), 3)
        bias_list = np.around(np.array(df2['Bias'].to_list()), 3)
        lp_list = np.array(df2['LP'].to_list())

        self._cm.init(False)

        self._cm.results_file.init(fieldnames=self.results_header)

        self._cm.sensor_module.enable_channel(ch, pl)
        self._cm.sensor_module.set_boot(boot_list[pl])
        self._cm.sensor_module.set_bias(bias_list[pl])
        self._cm.sensor_module.set_lp(int(lp_list[pl], 0))

        cur_pl_val = pl_table_list[pl]
        pre_pl_val = pl_table_list[pl - 1] if pl > 0 else None
        post_pl_val = pl_table_list[pl + 1] if pl < 15 else None

        self.results_row['Channel'] = ch
        self.results_row['Power Level'] = pl
        self.results_row['Boot'] = boot_list[pl]
        self.results_row['Bias'] = bias_list[pl]
        self.results_row['LP'] = lp_list[pl]
        self.results_row['PRF'] = self._cm.sensor_module.get_prf()
        self.results_row['Average_Power_Table'] = cur_pl_val
        self.results_row['Unit'] = self._cm.unit
        self.results_row['Sensor'] = str(self._cm.sensor_module)
        self.results_row['Hostname'] = host.HOST_NAME
        self.results_row['Version'] = self.init_cal_version
        self.results_row['Filename'] = self._cm.file_name
        self.results_row['Datetime'] = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        self.results_row['SW_Version'] = __version__

        op = []
        for i in range(10):
            # check thread stop
            if self._cm.check_stop():
                return
            time.sleep(0.5)
            op.append(self._cm.lab.optical_power())

            self.results_row['Iter'] = i
            self.results_row['Optical Power in mW'] = op[i]
            self.results_row['System Temperature(C)'] = self._cm.sensor_module.get_system_temperature()
            self.results_row['Temperature'] = self._cm.sensor_module.get_temperature()
            self.results_row['Average_Power_Ratio'] = round(self.results_row['Average_Power_Table'] / self.results_row['Optical Power in mW'], 3)
            self.results_row['Average_Power_Percent_Difference'] = round((self.results_row['Average_Power_Table'] - \
                self.results_row['Optical Power in mW']) * 100 / self.results_row['Average_Power_Table'], 3)
            self.results_row['Total_Iter'] = 1

            self._cm.results_file.write(self.results_row)
            self._cm.results_file.file.flush()

        self._cm.sensor_module.disable_channel(ch, pl)
        
        for i in op:
            if abs(cur_pl_val - i) > 0.15:
                logger.warn("Average power {} for ch {} pl {} is greater than 15% of {}".format(i, ch, pl, cur_pl_val))

            if pre_pl_val and i <= (pre_pl_val + pre_pl_val * 0.1):
                logger.error("Average power {} for ch {} pl {} is lower than pre pl {}".format(i, ch, pl, pre_pl_val + pre_pl_val * 0.1))
            
            if post_pl_val and i >= (post_pl_val - post_pl_val * 0.1):
                logger.error("Average power {} for ch {} pl {} is lower than post pl {}".format(i, ch, pl, post_pl_val - post_pl_val * 0.1))

        plt.figure()
        plt.plot(op)
        if pre_pl_val:
            plt.axhline(pre_pl_val + pre_pl_val * 0.1, color='r')
        if post_pl_val:
            plt.axhline(post_pl_val - post_pl_val * 0.1, color='r')
        plt.axhline(cur_pl_val, color='g')
        plt.grid()
        plt.xlabel("Num Iter")
        plt.ylabel("Measurement")
        plt.title("Measurement vs Num Iter")
        plt.show()

    def pl_test(self, file_name, ch, pl, ot=None):
        """Run power level test

        Args:
            file_name (str): Name of power level table
            ch (int): Channel
            pl (int): Power level
            ot (int): Oven set temperature
        """
        df = pd.read_csv(file_name)

        version = df.iloc[0]['Version']
        self.init_cal_version = version

        self.pl_stability_test(df, ch, pl, ot)

    def live_tx_ch_all_pl_test(self, df, ch):
        """Check all power levels for given channel live

        Args:
            df (pandas dataframe): Truncated dataframe
            ch (int): Channel number
        """
        df2 = df.sort_values('Power Level')
        pl_table_list = df2['Average_Power_Table'].to_list()
        
        boot_list = np.around(np.array(df2['Boot'].to_list()), 3)
        bias_list = np.around(np.array(df2['Bias'].to_list()), 3)
        lp_list = np.array(df2['LP'].to_list())

        self.results_row['Channel'] = ch
        self.results_row['Unit'] = self._cm.unit
        self.results_row['Sensor'] = str(self._cm.sensor_module)
        self.results_row['Hostname'] = host.HOST_NAME
        self.results_row['Filename'] = self._cm.file_name
        self.results_row['Datetime'] = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        self.results_row['SW_Version'] = __version__

        i = 0
        for pl in self._cm.sensor.config.POWER_LEVEL_NUM:
            logger.debug(f"\n\nStarting live pl {pl} test for ch {ch}\n")
            # check thread stop
            if self._cm.check_stop():
                return
            self._cm.sensor_module.enable_channel(ch, pl)
            self._cm.sensor_module.set_boot(boot_list[pl])
            self._cm.sensor_module.set_bias(bias_list[pl])
            self._cm.sensor_module.set_lp(int(lp_list[pl], 0))
            time.sleep(config.SLEEP_TIME)

            op = self._cm.lab.optical_power()

            self.results_row['Iter'] = i
            self.results_row['Power Level'] = pl
            self.results_row['Boot'] = boot_list[pl]
            self.results_row['Bias'] = bias_list[pl]
            self.results_row['LP'] = lp_list[pl]
            self.results_row['System Temperature(C)'] = self._cm.sensor_module.get_system_temperature()
            self.results_row['Temperature'] = self._cm.sensor_module.get_temperature()
            self.results_row['Average_Power_Table'] = pl_table_list[pl]
            self.results_row['Optical Power in mW'] = op
            self.results_row['Average_Power_Ratio'] = round(self.results_row['Average_Power_Table'] / self.results_row['Optical Power in mW'], 3)
            self.results_row['Average_Power_Percent_Difference'] = round((self.results_row['Average_Power_Table'] - \
                self.results_row['Optical Power in mW']) * 100 / self.results_row['Average_Power_Table'], 3)
            self.results_row['Total_Iter'] = 1

            self._cm.sensor_module.disable_channel(ch, pl)

            self._cm.results_file.write(self.results_row)
            self._cm.results_file.file.flush()

            i += 1

    def cal_verify_live(self, file_name, ch, ot, plot_op, plot_rest):
        """Verify cal table by checking live

        Args:
            file_name (str): Cal file string name
            ch (int): ch number
            ot (int): oven set temperature
            plot_op (bool): Plot optical power vs power level graph
            plot_rest (bool): Plot everything else
        """
        df = pd.read_csv(file_name)

        version = df.iloc[0]['Version']

        logger.info(f"Starting cal_verify_live with version {version}")

        self.init_cal_version = version

        if ch:
            channels = [int(ch)]
        else:
            channels = self._cm.sensor_module.channels
        
        temp_list = [int(ot)] if ot else [30, 85, 40, 0, -40]

        self._cm.init(False)

        self._cm.results_file.init(fieldnames=self.results_header)

        self.results_row['Version'] = version
        self.results_row['PRF'] = self._cm.sensor_module.get_prf()
        self.results_row['SW_Version'] = __version__

        # Integrating sphere tx cal verify
        df2 = None
        if version[0:2] == "IS":
            for ch in channels:
                df2 = df[df['Channel'] == ch]
                self.live_tx_ch_all_pl_test(df2, ch)
        elif version[0:2] == "TS":
            for cur_temp in temp_list:
                if self._cm.lab.oven:
                    self._cm.lab.set_temperature(cur_temp)
                    self.results_row['Oven Set Temperature(C)'] = cur_temp

                for ch in channels:
                    df2 = df[(df['Channel'] == ch) & (df['Oven Set Temperature(C)'] == cur_temp)]
                    self.live_tx_ch_all_pl_test(df2, ch)

            if self._cm.lab.oven:
                self._cm.lab.set_temperature(30)

        self.cal_verify(self._cm.results_file.file_name, plot_op, plot_rest)

