#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Tuning algorithms used for calibration

from txasic_cal.calibration import config
from txasic_cal.calibration.algorithm_files import pid_cont

import time
from txasic_cal.tools.logger import logger

class cal_algo:
    def __init__(self, *args, **kwargs):
        """Class contains the tuning search algorithms

        Keyword Arguments:
            search_algo {str} -- [Default search algorithm to be used] (default: {config.DEFAULT_SEARCH_ALGO})
        """
        # Default search algorithm is binary search
        self.search_algo = config.DEFAULT_SEARCH_ALGO
        self.max_iter = config.MAX_ITER
        self.cur_iter = 0
        self.total_iter = 0
        self.fail_count = 0

        self.input_list = []
        self.meas_list = []

        self.meas_func = None
        self.meas_name = ""
        self.input_func = None
        self.input_name = ""

        self.meas_time = 0

        self.clos_pos = {"input":0, "meas":0, "diff":100000}

        self.step_size = config.BS_STEP_SIZE
        
        super().__init__()

    def clear_cal_algo(self):
        """Clear the cal algo metric params
        """
        self.input_func = None
        self.input_list = []
        self.meas_func = None
        self.meas_list = []
        self.cur_iter = 0
        self.total_iter = 0
        self.clos_pos['input'] = 0
        self.clos_pos['meas'] = 0
        self.clos_pos['diff'] = 100000
        self.step_size = config.BS_STEP_SIZE

    def check_params(self):
        if(self.meas_func == None):
            logger.error("Please provide measurement function.")
            return -1
        if(self.input_func == None):
            logger.error("Please provide input function.")
            return -1
        return 0

    def check_clos(self, input, meas, diff):
        """Update closest value to setpoint if required

        Arguments:
            input {float} -- [Boot/Bias input value]
            meas {float} -- [Measured output value]
            diff {float} -- [Difference to setpoint]
        """
        if diff < self.clos_pos['diff']:
            self.clos_pos['input'] = input
            self.clos_pos['meas'] = meas
            self.clos_pos['diff'] = diff

    def tune_bs(self, setpoint, setpoint_margin, start_input=None, end_input=None, step_size=None):
        """Tune using binary search

        Arguments:
            setpoint {float} -- [Desired setpoint]
            setpoint_margin {float} -- [Desired margin for setpoint]
        """
        logger.debug("*************************************")

        if(self.check_params() < 0):
            logger.error("Tuning BS failed because of incorrect params.")
            return

        logger.debug("Tuning {} using BS for {} with margin {}."\
            .format(self.input_name, setpoint, setpoint_margin))

        # Reset cur iter
        self.cur_iter = 0

        cur_input = getattr(self.sensor_module, self.input_name)
        self.input_list.append(cur_input)
        measured_value = self.meas_func(self.sensor_module.cur_ch)
        self.meas_list.append(measured_value)
        setpoint_diff = abs(setpoint - measured_value)

        self.check_clos(cur_input, measured_value, setpoint_diff)
        
        if setpoint_diff <= setpoint_margin:
            self.cur_iter += 1
            self.total_iter = self.cur_iter
            logger.debug("Satisfied BS {} search on {} iteration with measured {} value {}."\
                .format(self.input_name, self.cur_iter, self.meas_name, measured_value))
            return

        if start_input == None:
            start_result_input = config.START_BS_RESULT[self.input_name]
        else:
            start_result_input = start_input
            config.START_BS_RESULT[self.input_name] = start_input

        if end_input == None:
            end_result_input = config.END_BS_RESULT[self.input_name]
        else:
            end_result_input = end_input
            config.END_BS_RESULT[self.input_name] = end_input

        while setpoint_diff > setpoint_margin:

            # check thread stop
            if self.check_stop():
                return

            if self.cur_iter >= self.max_iter:
                logger.warn("Reached maximum iter {}".format(self.cur_iter))
                self.handle_cal_algo_fail(cur_input)

                # Reset and try-again
                self.total_iter += self.cur_iter
                self.cur_iter = 0

                if self.total_iter > config.BS_FAIL_ITER:
                    if str(self) == 'tx_calibration':
                        logger.warn("Total number of tries exceeded {}.".format(config.BS_FAIL_ITER))

                    logger.warn("Setting tune result to previously seen closest value.")
                    logger.debug("Input: {}, Measured: {}, Setpoint Diff: {}"
                                 .format(self.clos_pos['input'], self.clos_pos['meas'], self.clos_pos['diff']))
                    self.input_func(self.clos_pos['input'])
                    self.input_list.append(self.clos_pos['input'])
                    measured_value = self.meas_func(self.sensor_module.cur_ch)
                    self.meas_list.append(measured_value)
                    self.total_iter += 1
                    return
                
                start_result_input = config.START_BS_RESULT[self.input_name]
                end_result_input = config.END_BS_RESULT[self.input_name]

            cur_input = round((start_result_input + end_result_input) / 2.0, 3)

            if cur_input > config.END_BS_RESULT[self.input_name]:
                logger.warn("Input {} greater than {}".format(
                    self.input_name, config.END_BS_RESULT[self.input_name]))
                cur_input = config.END_BS_RESULT[self.input_name] + 0.1
                logger.debug("Setting {} to {}".format(self.input_name, cur_input))
            elif cur_input < config.START_BS_RESULT[self.input_name]:
                logger.warn("Input {} smaller than {}".format(
                    self.input_name, config.START_BS_RESULT[self.input_name]))
                cur_input = config.START_BS_RESULT[self.input_name] + 0.1
                logger.debug("Setting {} to {}".format(self.input_name, cur_input))

            self.input_func(cur_input)
            time.sleep(config.SLEEP_TIME)
            self.input_list.append(cur_input)
            self.cur_iter += 1
            measured_value = self.meas_func(self.sensor_module.cur_ch)

            self.meas_list.append(measured_value)

            setpoint_diff = abs(setpoint - measured_value)

            self.check_clos(cur_input, measured_value, setpoint_diff)
            
            if setpoint_diff <= setpoint_margin:
                logger.debug("Satisfied BS {} search on {} iteration with measured {} value {}."\
                    .format(self.input_name, self.cur_iter, self.meas_name, measured_value))
                self.total_iter += self.cur_iter
                return

            # default step_size = 0.01
            if step_size is not None:
                self.step_size = step_size
            if measured_value < setpoint:
                start_result_input = cur_input + self.step_size
            elif measured_value > setpoint:
                end_result_input = cur_input - self.step_size

            if start_result_input >= end_result_input:
                # Noisy measurements can lead to this situation, ideally the search should stop
                logger.warn("BS start larger than end.")
                
                # Reset start and end and continue
                end_result_input = cur_input + 0.3 if (cur_input + 0.3) < config.END_BS_RESULT[self.input_name] else config.END_BS_RESULT[self.input_name]
                start_result_input = cur_input - 0.3 if (cur_input - 0.3) > config.START_BS_RESULT[self.input_name] else config.START_BS_RESULT[self.input_name]

        self.total_iter += self.cur_iter

    def tune_pid(self, setpoint, setpoint_margin, cur_input):
        """Tune using PID

        Arguments:
            setpoint {float} -- [Desired setpoint]
            setpoint_margin {float} -- [Desired setpoint margin]
            cur_input {flaot} -- [Start input]
        """
        logger.debug("*************************************")

        if(self.check_params() < 0):
            logger.error("Tuning PID failed because of incorrect params.")
            return

        logger.debug("Tuning {} using PID for {} with margin {}."\
            .format(self.input_name, setpoint, setpoint_margin))

        # Reset cur iter
        self.cur_iter = 0

        pid = pid_cont.PID(P=0.1, I=0.0025, D=0.0)

        self.input_func(cur_input)
        time.sleep(config.SLEEP_TIME)
        self.input_list.append(cur_input)

        measured_value = self.meas_func(self.sensor_module.cur_ch)
        self.meas_list.append(measured_value) 
        
        setpoint_diff = setpoint - measured_value

        self.check_clos(cur_input, measured_value, setpoint_diff)
        
        if abs(setpoint_diff) <= setpoint_margin:
            self.cur_iter += 1
            self.total_iter += self.cur_iter
            logger.debug("Satisfied PID {} search on {} iteration with measured {} value {}."\
                .format(self.input_name, self.cur_iter, self.meas_name, measured_value))
            return

        while abs(setpoint_diff) > setpoint_margin:

            if self.cur_iter >= (self.max_iter + 50):
                logger.warn("Reached maximum iter {}".format(self.cur_iter))
                self.handle_cal_algo_fail(cur_input)

                # Reset and try-again
                self.total_iter += self.cur_iter
                self.cur_iter = 0

                if self.total_iter > config.PID_FAIL_ITER:
                    logger.warn("Total number of tries exceeded {}. Exiting.".format(config.PID_FAIL_ITER))
                    logger.warn("Setting tune result to previously seen closest value.")
                    logger.debug("Input: {}, Measured: {}, Setpoint Diff: {}"
                                 .format(self.clos_pos['input'], self.clos_pos['meas'], self.clos_pos['diff']))
                    self.input_func(self.clos_pos['input'])
                    self.input_list.append(self.clos_pos['input'])
                    measured_value = self.meas_func(self.sensor_module.cur_ch)
                    self.meas_list.append(measured_value)
                    self.total_iter += 1
                    return

            feedback = pid.update(setpoint_diff, cur_input, cur_time=0.0)
            cur_input = round(cur_input + feedback, 3)

            if cur_input > config.END_BS_RESULT[self.input_name]:
                logger.error("Input {} is greater than {}".format(
                    cur_input, config.END_BS_RESULT[self.input_name]))
                cur_input = config.END_BS_RESULT[self.input_name] - 0.1
                logger.debug("Setting {} to {}".format(self.input_name, cur_input))
            elif cur_input < config.START_BS_RESULT[self.input_name]:
                logger.error("Input {} smaller than {}".format(
                    self.input_name, config.START_BS_RESULT[self.input_name]))
                cur_input = config.START_BS_RESULT[self.input_name] + 0.1
                logger.debug("Setting {} to {}".format(self.input_name, cur_input))

            self.input_func(cur_input)
            time.sleep(config.SLEEP_TIME)
            self.input_list.append(cur_input)
            self.cur_iter += 1
            measured_value = self.meas_func(self.sensor_module.cur_ch)
            self.meas_list.append(measured_value)

            setpoint_diff = setpoint - measured_value

            self.check_clos(cur_input, measured_value, setpoint_diff)
            
            if abs(setpoint_diff) <= setpoint_margin:
                logger.debug("Satisfied PID {} search on {} iteration with measured {} value {}."\
                    .format(self.input_name, self.cur_iter, self.meas_name, measured_value))
                self.total_iter += self.cur_iter
                return

        self.total_iter += self.cur_iter
