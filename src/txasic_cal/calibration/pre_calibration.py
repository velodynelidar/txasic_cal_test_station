#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-12-30
#  Description   : Pre calibration scripts

import time

from txasic_cal.tools.logger import logger
from txasic_cal.pfmea_code import pfmea_table

class pre_calibration:
    def __init__(self, calibration_module):
        """Pre TX and VCSEL calibration related modules

        Arguments:
            calibration_module [object] : Calibration class object
        """
        self._cm = calibration_module
        self.sensor = self._cm.sensor
        self.sensor_module = self.sensor.sensor_module
        self.lab = self._cm.lab

        self.init_cal_version = None

    def pre_cal_setup_checks(self):
        """Setup checks
        """
        logger.info("Pre-Cal Setup Checks")
        
        # Power meter checks
        # Wavelength should be 905nm
        if int(self.lab.pm.get_wavelength()) != 905:
            logger.warn("Power meter wavelength is not 905nm.")
            logger.info("Setting power meter wavelength to 905nm.")
            try:
                self.lab.pm.set_wavelength(905)
            except Exception as e:
                logger.error("Setting power meter wavelength failed with exception {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_PRE_PWRMETERWAVELENGTH')
        
        # Power meter zero value should be less than 20uW
        while True:
            time.sleep(0.5)
            op = self.lab.optical_power()
            if abs(op) >= 0.02:
                logger.error("Power meter at zero value is at {} mW.".format(op))
                pfmea_table.pfmea_filter.error('SETUP_PRE_PWRMETERZERO')
                logger.input("Zero power meter correctly before continuing. Hit enter to continue.")
                continue
            break
        
        # Power meter range auto check
        if self.lab.pm.get_power_range_auto() != 1:
            logger.warn("Power meter range is not auto.")
            logger.info("Setting power meter range to auto.")
            try:
                self.lab.pm.set_power_range_auto(1)
            except Exception as e:
                logger.error("Setting power meter power range failed with exception {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_PRE_PWRMETERRANGE')

        # Power meter unit check
        if self.lab.pm.get_power_unit() == 'W\n':
            logger.warn("Power meter unit is not W.")
            logger.info("Setting power meter unit to W.")
            try:
                self.lab.pm.set_power_unit('W')
            except Exception as e:
                logger.error("Setting power meter power unit failed with exception {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_PRE_PWRMETERUNIT')

        # Power meter BW check
        if self.lab.pm.get_bandwidth() == 0:
            logger.warn("Power meter BW is not low.")
            logger.info("Setting power meter BW to low.")
            try:
                self.lab.pm.set_bandwidth("ON")
            except Exception as e:
                logger.error("Setting power meter bandwidth failed with exception {}".format(e))
                pfmea_table.pfmea_filter.error('SETUP_PRE_PWRMETERBW')
                
        logger.info("Pre-Cal Setup Checks Completed")

    def pre_cal_unit_checks(self):
        """Unit checks
        """
        logger.info("Pre-Cal Unit Checks")
        # Voltage Checks
        # 12V check
        if self.sensor_module._v12 is not None and (self.sensor_module._v12 < 10.8 or self.sensor_module._v12 > 13.2):
            logger.error("12V is at {}".format(self.sensor_module._v12))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG12V')
            logger.input("Check 12V. Hit enter to continue.")

        # 5V check
        if self.sensor_module._v5 is not None and (self.sensor_module._v5 < 4.5 or self.sensor_module._v5 > 5.5):
            logger.error("5V is at {}".format(self.sensor_module._v5))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG5V')
            logger.input("Check 5V. Hit enter to continue.")

        # VLAS check
        if self.sensor_module._vlas is not None and (self.sensor_module._vlas < 18 or self.sensor_module._vlas > 22):
            logger.error("VLAS is at {}".format(self.sensor_module._vlas))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REGVLAS')
            logger.input("Check VLAS. Hit enter to continue.")

        # 3.3V check
        if self.sensor_module._v3_3 is not None and (self.sensor_module._v3_3 < 2.97 or self.sensor_module._v3_3 > 3.63):
            logger.error("3.3V is at {}".format(self.sensor_module._v3_3))
            pfmea_table.pfmea_filter.error('UNIT_PRE_REG3-3V')
            logger.input("Check 3.3V. Hit enter to continue.")

        # Eye-safety check
        # Clear fault
        if self.sensor_module.clear_fault() < 0:
            logger.error("Fault not cleared.")
            pfmea_table.pfmea_filter.error('TROSACAL_PRE_REGEYESAFETYFAULT')
            logger.input("Check eye-safety register faults. Please hit enter to continue.")

        # System temperature check
        system_temp = self.sensor_module.get_system_temperature()
        if system_temp is not None and system_temp < 36.0:
            pfmea_table.pfmea_filter.warn('UNIT_PRE_SYSTEMTEMP')
            logger.input(f"Ensure system temperature is above 36.0C. Current system temperature {system_temp}C. Hit enter to continue.")
        
        logger.info("Pre-Cal Setup Checks Completed")

