#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-6-16
#  Description   : Host related config file

import socket

# Get current host name
HOST_NAME = socket.gethostname()

LAB_HOST_DETAILS = \
{
    # Test
    "test":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":[]
        },
        "instruments":
        {
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    },
    # Temperature-TxCal
    "a2teststn-E7470":
    {
        "communication":
        {
            "COMM_TYPE":"ssh",
            "IP_ADDRESS":"10.10.28.80",
            "USERNAME":"velodyne",
            "PASSWORD":"Velodyne@123"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope', 'Oven', 'Gonio', 'Power Supply']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::192.168.5.200::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8075::P5001603::INSTR",
            "OVEN":"TCPIP::10.10.28.79::5025::SOCKET",
            "POWER_SUPPLY":"USB0::0x05E6::0x2230::802901012747810020::INSTR",
            "Y_sn":"27255969",
            "X_sn":"27255783"
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # NPI V8 cal station
    "velodyne-Precision-7510":
    {
        "communication":
        {
            "COMM_TYPE":"cli"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.10.29.4::inst0::INSTR",
            "POWERMETER":"USB0::4883::32885::P5001756::0::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # NPI 0.5.5
    "79Q27H2-E7470":
    {
        "communication":
        {
            "COMM_TYPE":"cli"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.10.29.4::inst0::INSTR",
            "POWERMETER":"USB0::4883::32885::P5001756::0::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # 2nd floor TxCal
    "fpga-laptop-01":
    {
        "communication":
        {
            "COMM_TYPE":"cli"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.30.50.50::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8075::P5001086::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    },
    # 2nd floor vcsel cal station
    "61XCLH2-L10":
    {
        "communication":
        {
            "COMM_TYPE":"ssh",
            "IP_ADDRESS":"10.30.50.65",
            "USERNAME":"velodyne",
            "PASSWORD":"Velodyne@123"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Oven', 'Power Supply']
        },
        "instruments":
        {
            "OVEN":"TCPIP::10.30.50.60::5025::SOCKET",
            "POWER_SUPPLY":"USB0::0x05E6::0x2230::802901012747810001::INSTR",
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # Rnd land Temp-TxCal 1
    "yellowstone":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Oven', 'Power Supply']
        },
        "instruments":
        {
            "POWERMETER":"USB0::0x1313::0x8078::P0027897::INSTR",
            "OVEN":"TCPIP::192.168.0.222::5025::SOCKET",
            "POWER_SUPPLY":"",  
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # Rnd land Temp-TxCal 2
    "olympic":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Oven', 'Power Supply']
        },
        "instruments":
        {
            "POWERMETER": "USB0::0x1313::0x8078::P0028279::INSTR",
            "OVEN": "TCPIP::192.168.0.222::5025::SOCKET",
            "SCOPE": "TCPIP0::10.10.28.212::inst0::INSTR",
            "POWER_SUPPLY":"",
            "AFG":"USB0::0x0699::0x0353::1742164::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # NPI V16 cal station
    "everest":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.10.28.90::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8078::P0016421::INSTR"
        },
        "NFK":
        {
            "station":True,
            "fake":False
        }
    },
    # NPI V16 cal station - 2
    "shasta":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.10.29.15::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8078::P0016145::INSTR"
        },
        "NFK":
        {
            "station":True,
            "fake":False
        }
    },
    # EElab V16 PL0 slope cal
    "70B0HH2-E7480":
    {
        "communication":
        {
            "COMM_TYPE": "api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS": ['Power Meter', 'Template','Power Supply']
        },
        "instruments":
        {
            "POWERMETER": "USB0::0x1313::0x8078::P0016818::INSTR",
            "TEMPLATE": '/dev/ttyUSB0',
            "POWER_SUPPLY":"USB0::0x05E6::0x2230::802901012747410003::0::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":False
        }
    },
    # NPI V8 cal station - 2
    "crater":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::10.10.29.4::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8078::P0027898::INSTR"
            # "POWERMETER":"USB0::0x1313::0x8075::P5002010::INSTR"
        },
        "NFK":
        {
            "station":True,
            "fake":False
        }
    },
    # NPI V16 cal station - 3
    "rainier":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::192.165.1.200::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8078::P0027895::INSTR"
        },
        "NFK":
        {
            "station":True,
            "fake":False
        }
    },
    # NPI V16 cal station - 4
    "fuji":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Scope']
        },
        "instruments":
        {
            "SCOPE":"TCPIP0::169.254.180.186::inst0::INSTR",
            "POWERMETER":"USB0::0x1313::0x8078::P0027899::INSTR"
        },
        "NFK":
        {
            "station":True,
            "fake":False
        }
    },
    # Upstairs TxCal
    "yosemite":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter', 'Power Supply']
        },
        "instruments":
        {
            "POWERMETER":"USB0::0x1313::0x8078::P0028278::INSTR",
            "POWER_SUPPLY":"USB0::0x05E6::0x2230::9201039::0::INSTR"
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    },
    # Upstairs TxCal VLX
    "sierra":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter']
        },
        "instruments":
        {
            "POWERMETER":"USB0::0x1313::0x8078::P0022847::INSTR",
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    },
    # TxCal APA
    "denali":
    {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter']
        },
        "instruments":
        {
            "POWERMETER":"",
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    },
}

# Test platform
if HOST_NAME not in LAB_HOST_DETAILS.keys():
    LAB_HOST_DETAILS[HOST_NAME] = {
        "communication":
        {
            "COMM_TYPE":"api"
        },
        "lab":
        {
            "CONNECTED_INSTRUMENTS":['Power Meter']
        },
        "instruments":
        {
            "POWERMETER":"",
        },
        "NFK":
        {
            "station":False,
            "fake":True
        }
    }
