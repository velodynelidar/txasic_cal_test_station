#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Phong Le <phongle@velodyne.com>, Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2021-3-4
#  Description   : Top Software for starting GUI

import time
import sys

from txasic_cal.GUI import txcal_gui
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtRemoveInputHook, QThread, pyqtSignal
from txasic_cal.__main__ import cal_module
from txasic_cal.calibration import post_calibration
from txasic_cal.host import host
from txasic_cal import __version__
from txasic_cal.tools.logger import logger
from txasic_cal.sensor import SENSOR_MODEL, SENSOR_MODEL_FW_VERSION
from pkg_resources import resource_stream
GUI_VERSION = 0.2
CAL_EXIT_TIMEOUT = 1000
SENSOR_POS_INDEX = {'V8':0, 'V16':1, 'VB2':2, 'VLX16':3, 'APAMLA1':4, 'APAMLA2':5}
class GuiControl(QtWidgets.QMainWindow):
    pause_sig = pyqtSignal(bool)

    def __init__(self):
        super(GuiControl, self).__init__()

        self.UiClass = txcal_gui.Ui_MainWindow()
        self.UiClass.setupUi(self)
        
        self.UiClassOnly = txcal_gui
        self.updateGUI = QtWidgets.QApplication

        # thread lock
        mutex = QtCore.QMutex()
        logger.mutex_ob = mutex

        # Push buttons
        self.Start_Cal = self.UiClass.Start_Cal_Button
        self.Start_Cal.setStyleSheet('background-color: #1e90ff')
        # self.Post_Cal = self.UiClass.Post_Cal_Button
        # self.Post_Cal.setStyleSheet('background-color: #bdb76b')

        # radio buttons
        self.tx_button = self.UiClass.tx_button.setChecked(True)
        self.vcsel_button = self.UiClass.vcsel_button

        # inputs
        self.id_label_input = self.UiClass.id_label_input
        self.sensor_type_input = self.UiClass.sensor_choice
        self.sensor_type_input.setStyleSheet("background-color: white; selection-background-color: blue")
        self.unit_input = self.UiClass.unit_input
        self.unit_input.setStyleSheet("background-color: white")
        self.hv_input = self.UiClass.hv_input
        self.hv_input.setStyleSheet("background-color: white")
        self.txcal_ver_choice_input = self.UiClass.txcal_ver_choice
        self.txcal_ver_choice_input.setStyleSheet("background-color: white; selection-background-color: blue")
        self.update_inputs()

        # station & firmware top left
        self.id_label_input.setText(host.HOST_NAME)
        self.id_label_input.setReadOnly(True)
        self.id_label_input.setStyleSheet('background-color: #d3d3d3')
        self.firmware_label = self.UiClass.firmware_label
        self.firmware_label.setReadOnly(True)
        self.firmware_label.setStyleSheet('background-color: #d3d3d3')

        # file name
        self.file_input = self.UiClass.file_input
        # self.file_input.setText()
        self.file_input.setReadOnly(True)
        self.file_input.setStyleSheet('background-color: #d3d3d3')

        # logger and pfmea
        self.log_info = self.UiClass.log_info_textedit
        self.log_info.setReadOnly(True)
        self.pfmea_error = self.UiClass.cal_error_textedit
        self.pfmea_error.setReadOnly(True)

        # input dialog and enter
        self.input_dialog = self.UiClass.inputCommands
        self.input_dialog.setReadOnly(True)
        self.input_dialog.setStyleSheet('background-color: #d3d3d3')
        self.enterbutton = self.UiClass.Enter_Button
        self.enterbutton.clicked.connect(self.enter_clicked)
        self.enterbutton.setStyleSheet("background-color: white")
        self.input_dialog_set = False

        # status during calibrations
        self.log_info.setStyleSheet("background-color: #000000; color: #7cfc00")
        self.pfmea_error.setStyleSheet("background-color: #000000; color: #ff4500")

        # status labels
        # green
        self.green_label = self.UiClass.green_label
        self.green_label.resize(60, 60)
        self.green_label.setText("")
        self.green_label.setGeometry(100,100,100,100)
        self.green_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

        # yellow
        self.yellow_label = self.UiClass.yellow_label
        self.yellow_label.resize(60, 60)
        self.yellow_label.setText("")
        self.yellow_label.setGeometry(100,100,100,100)
        self.yellow_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

        # red
        self.red_label = self.UiClass.red_label
        self.red_label.resize(60, 60)
        self.red_label.setText("")
        self.red_label.setGeometry(100,100,100,100)
        self.red_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

        # time elapsed
        self.time_elapsed = self.UiClass.time_elapsed_lineEdit
        self.time_elapsed.setReadOnly(True)
        self.time_elapsed.setStyleSheet('background-color: #d3d3d3')
        self.help_tab = self.UiClass.tableWidget
        self.help_tab.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)

        self.start_time = None
        self.end_time = None

        self.connectButtons()

        self.unit_value = None
        self.hv_value = None
        self.txcal_ver_choice_value = None
        self.sensor_name_input_value = None
        self.sensor_family = None

        self.init_cal_version_num = '5.2'

        self.cal_mod = cal_module(gui=True)
        self.cal_mod.msg_sig.connect(self.on_update_elements)
        self.cal_mod.input_sig.connect(self.on_update_input)
        self.pause_sig.connect(logger.on_update_stop)
        self.cal_mod.post_cal_sig.connect(self.on_finished_graph)

        self.new_thread = QThread()
        self.cal_mod.moveToThread(self.new_thread)
        self.new_thread.started.connect(self.cal_mod.run)
        self.cal_mod.finished.connect(self.new_thread.quit)
        self.cal_mod.finished.connect(self.on_update_status)
        self.cal_mod.finished.connect(self.on_update_time)

        self.cal_status = None
        self.cal_error_status = None
        self.cal_warn_status = None

        # Update GUI elements first before display
        self.update_gui_elements()

    def __enter__(self):
        return self
    
    def __exit__(self, *exargs):
        pass

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.new_thread.requestInterruption()
        self.new_thread.quit()

        if not (self.new_thread.wait(CAL_EXIT_TIMEOUT)):
            logger.warning(f"The cal thread took more than {CAL_EXIT_TIMEOUT} ms to terminate. Calling sys.exit now.")
            sys.exit(0)
        return super().closeEvent(a0)

    def on_update_elements(self, loc, msg):
        """Update log message

        Args:
            loc (function name): Info/Error GUI function name
            msg (str): Message to be displayed
        """
        if loc == 'pfmea_error':
            if 'Operation' in msg:
                loc = 'pfmea_error'
                self.cal_error_status = True
                trimmed_pfmea = self.get_pfmea_codes(msg)
                recommended_action = self.get_recommended_act(msg)
                msg = trimmed_pfmea + '\n' + recommended_action + '\n'
            else:
                return

        elif loc == 'pfmea_warn':
            if 'Operation' in msg:
                loc = 'pfmea_error'
                self.cal_warn_status = True
                trimmed_pfmea = self.get_pfmea_codes(msg)
                recommended_action = self.get_recommended_act(msg)
                msg = trimmed_pfmea + '\n' + recommended_action + '\n'
            else:
                return

        elif loc == 'file_input':
            self.file_input.setText(msg)
            return

        self.outputText(getattr(self, loc), msg)

    def on_update_status(self):
        """Update status at the end of calibration based on cal_status state
        """
        if self.cal_error_status and self.cal_warn_status:
            self.cal_status = 'error'
        elif self.cal_error_status and not self.cal_warn_status:
            self.cal_status = 'error'
        elif self.cal_warn_status and not self.cal_error_status:
            self.cal_status = 'warning'

        if self.cal_status == 'error':
            self.get_red_status()
        elif self.cal_status == 'warning':
            self.get_yellow_status()
        else:
            self.get_green_status()

        self.cal_status = None

    def on_update_time(self):
        """Update time elapsed at the end of calibration
        """
        self.end_time = time.time()
        self.time_elapsed.setText(f"Completed in {round((self.end_time - self.start_time), 3)} sec or {round((self.end_time-self.start_time)/60,3)} min")

    def on_update_input(self, msg):
        """Update the action required input tab

        Args:
            msg (str): Message to be displayed
        """
        self.input_dialog.setText(msg)
        self.input_dialog_set = True

    def enter_clicked(self):
        """Change enter status to True when Enter button is pressed
        """
        if self.input_dialog and self.input_dialog_set:
            self.pause_sig.emit(True)
        self.input_dialog_set = False
        self.input_dialog.setText("")

    def update_gui_elements(self):
        """Update GUI visual elements
        """
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", f"TxCal {__version__}      [GUI Version {GUI_VERSION}]"))
        self.UiClass.logo.setPixmap(QtGui.QPixmap(resource_stream('txasic_cal', 'GUI/velodyne_logo.png').name))
        self.setWindowIcon(QtGui.QIcon(resource_stream('txasic_cal', 'GUI/windows_icon.png').name))

        for key, value in SENSOR_POS_INDEX.items():
            self.sensor_type_input.addItem("")
            self.sensor_type_input.setItemText(value, _translate("MainWindow", key))

        if SENSOR_MODEL is not None:
            self.sensor_type_input.setCurrentIndex(SENSOR_POS_INDEX[SENSOR_MODEL.split('.')[1]])
            self.firmware_label.setText(str(SENSOR_MODEL_FW_VERSION))

    def on_finished_graph(self, file_name):
        """Run post txcal verify and display matplotib plot

        Args:
            file_name (pathlib.Path): Path of cal file
        """
        _post_cal = post_calibration.post_calibration(self.cal_mod._cal)
        _post_cal.cal_verify(file_name, True, False)

    def connectButtons(self):
        self.Start_Cal.clicked.connect(self.startCalibration)
        # self.Post_Cal.clicked.connect(self.postCalibration)

    def update_inputs(self):
        """Update inputs based on GUI params
        """
        self.unit_value = self.get_unit_input()
        self.hv_value = self.get_hv_input()
        self.txcal_ver_choice_value = self.get_txcal_ver_choice()
        self.sensor_name_input_value = self.get_sensor_name_input()

        if self.txcal_ver_choice_value == 'ambient':
            self.txcal_ver_choice_value = 'IS'
            self.init_cal_version_num = '5.2'
        else:
            self.txcal_ver_choice_value = 'TS'
            self.init_cal_version_num = '4.5'

        if self.sensor_name_input_value in ('V8', 'V16'):
            self.sensor_family = 'velarray'
        elif self.sensor_name_input_value in ('VB2'):
            self.sensor_family = 'velabit'
        elif self.sensor_name_input_value in ('VLX16'):
            self.sensor_family = 'vlx'
        elif self.sensor_name_input_value in ('APAMLA1', 'APAMLA1'):
            self.sensor_family = 'apa'

    def get_unit_input(self):
        return self.unit_input.text()

    def get_hv_input(self):
        return self.hv_input.text()

    def get_txcal_ver_choice(self):
        return self.txcal_ver_choice_input.currentText()

    def get_sensor_name_input(self):
        return self.sensor_type_input.currentText()

    def outputText(self, widget, text):
        if(widget.toPlainText() == '\n'):
            previous_text = ''
        else:
            previous_text = widget.toPlainText()
        widget.setPlainText(previous_text + text + '\n')
        widget.moveCursor(QtGui.QTextCursor.End)

    def get_pfmea_codes(self, string):
        if "Operation Code" in string:
            start_error = string.index('Operation Code:')
            end_error = string.index('Function')
            pfmea_code = string[start_error+16:end_error-3]
        else:
            pfmea_code = ""
        return pfmea_code

    def get_recommended_act(self, string):
        if "Function:" in string:
            start_func = string.index(' Function:')
            recom_act = string[start_func:]
        else:
            recom_act = ""
        return recom_act

    def get_green_status(self):
        self.green_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color:green")
        self.red_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.yellow_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

    def get_yellow_status(self):
        self.yellow_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: yellow")
        self.green_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.red_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

    def get_red_status(self):
        self.red_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: red;")
        self.green_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.yellow_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")

    def resetStatus(self):
        self.green_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.red_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.yellow_label.setStyleSheet("border-radius: 50px; border:3px solid black; background-color: #000000")
        self.time_elapsed.setText("")
        self.input_dialog.setText("")

    def startCalibration(self):
        """Start a worker thread for calibration by reading GUI inputs
        """
        self.pfmea_error.setText("")
        self.resetStatus()

        self.start_time = time.time()

        self.update_inputs()

        self.cal_mod.cm_args.unit = self.unit_value
        self.cal_mod.cm_args.hv = self.hv_value
        self.cal_mod.cm_args.version = self.txcal_ver_choice_value

        # default self.cal_mod.cm_args from CLI
        self.cal_mod.cm_args.channel = None
        self.cal_mod.cm_args.file = None
        self.cal_mod.cm_args.interp_table = False
        self.cal_mod.cm_args.post_txcal = False
        self.cal_mod.cm_args.power_level = None
        self.cal_mod.cm_args.live_pl_test = None
        self.cal_mod.cm_args.sensor_family = self.sensor_family
        self.cal_mod.cm_args.sensor_name = self.sensor_name_input_value
        self.cal_mod.cm_args.stability_test = False
        self.cal_mod.cm_args.version_num = self.init_cal_version_num
        self.cal_mod.cm_args.vcsel_cal = None

        if self.vcsel_button.isChecked():
            self.cal_mod.cm_args.vcsel_cal = True
            self.txcal_ver_choice_value = 'VC'
            self.cal_mod.cm_args.version = 'VC'

        self.new_thread.start()

    def postCalibration(self):
        """Start a worker thread for post calibration by reading GUI inputs
        """
        self.pfmea_error.setText("")
        self.start_time = time.time()

        self.calibration_choice = False

        self.update_inputs()

        self.cal_mod.cm_args.post_txcal = True

        self.cal_mod.cm_args.unit = self.unit_value
        self.cal_mod.cm_args.hv = self.hv_value
        self.cal_mod.cm_args.version = self.txcal_ver_choice_value

        # default self.cal_mod.cm_args from CLI
        self.cal_mod.cm_args.channel = None
        self.cal_mod.cm_args.file = None
        self.cal_mod.cm_args.interp_table = False
        self.cal_mod.cm_args.power_level = None
        self.cal_mod.cm_args.live_pl_test = None
        self.cal_mod.cm_args.sensor_family = self.sensor_family
        self.cal_mod.cm_args.sensor_name = self.sensor_name_input_value
        self.cal_mod.cm_args.stability_test = False
        self.cal_mod.cm_args.version_num = self.init_cal_version_num
        self.cal_mod.cm_args.vcsel_cal = None

        if self.vcsel_button.isChecked():
            self.cal_mod.cm_args.vcsel_cal = True
            self.txcal_ver_choice_value = 'VC'
            self.cal_mod.cm_args.version = 'VC'

        self.new_thread.start()


def main():
    pyqtRemoveInputHook()
    app = QtWidgets.QApplication(sys.argv)
    with GuiControl() as application:
        application.show()
        sys.exit(app.exec_())

if __name__ == "__main__":
    main()
