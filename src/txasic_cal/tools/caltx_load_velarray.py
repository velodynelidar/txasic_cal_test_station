# print("==============================================================================")
# print("Copyright (c) 2020 Velodyne LiDAR, Inc. (\"Velodyne\"). All rights reserved.\n\
# NOTICE: All information contained herein is, and remains the sole property of\n\
# Velodyne. The intellectual and technical concepts contained herein are\n\
# proprietary to Velodyne and are protected by trade secret and/or copyright\n\
# law. Dissemination of this information or reproduction of this material is\n\
# strictly prohibited unless prior written permission is obtained from\n\
# Velodyne. Access to the source code contained herein is prohibited to anyone\n\
# except current Velodyne employees or consultants. Any access to the source\n\
# code is subject to the obligations of confidentiality contained in any\n\
# applicable employment or consulting agreements with Velodyne.\n\
#   Author        : Igor Alshanetsky <ialshanetsky@velodyne.com> \n\
#   Creation date : 2021-04-07")
import os, getopt, sys
import csv
import time
import pyodbc
from datetime import datetime, timedelta
import pathlib
import getpass
from pathlib import PurePath
def nextCalFile(directory):
    log_files = []
    for dirpath, dirnames, filenames in os.walk(directory):
            for filename in filenames:
                if filename == 'power_level_table.csv':
                       log_file =  os.path.join(dirpath, filename)
                       log_files.append(log_file)
    yield log_files
def parse_data_txcal(filepath, manufacturing_id, product_model):
  sql_insert_cal_instance = """\
                    DECLARE @out int;
                    EXEC [dbo].[InsertUnitCalInstanceIns]
                     @vld_cal_key  = @out OUTPUT,
                     @manufacturing_id = ?,
                     @product_model = ?,
                     @board = ?,
                     @trosa = ?,
                     @hv_setpoint_0 = ?,
                     @hv_setpoint_1 = ?,
                     @firmware_version = ?,
                     @firmware_version_date = ?,
                     @unit_build_githash = ?,
                     @unit_model_number = ?,
                     @unit_hw_revision = ?,
                     @unit_serial_number = ?,
                     @power_meter_attenuation_factor = ?,
                     @txcal_software_version = ?,
                     @git_tag = ?,
                     @git_tag_message = ?,
                     @git_develop_commit_number = ?,
                     @git_commit_id = ?,
                     @mcm_sdk_version = ?,
                     @pfmea_operation_code = ?,
                     @sw_version = ?,
                     @hostname = ?,
                     @cal_datetime = ?,
                     @cal_username = ?,
                     @cal_file = ?
                     SELECT @out AS the_output;
               """

  sql_insert_channel_dt = """\
      INSERT INTO [dbo].[vld_txcal_data]
      ([vld_cal_key]
      ,[cal_mode]
      ,[channel]
      ,[power_level]
      ,[lp]
      ,[boot]
      ,[boot_in_hex]
      ,[bias]
      ,[bias_in_hex]
      ,[prf]
      ,[temperature]
      ,[v_12v]
      ,[v_3_3v]
      ,[v_5v]
      ,[v_21v]
      ,[system_temperature_c]
      ,[hv]
      ,[optical_power_in_mw]
      ,[oven_temperature_c]
      ,[power_supply_voltage_v]
      ,[power_supply_current_a]
      ,[peak_power_w]
      ,[oven_set_temperature_c]
      ,[average_power_table]
      ,[average_power_ratio]
      ,[average_power_percent_difference]
      ,[bias_delta]
      ,[pl0_width]
      ,[pl0_target_intensity]
      ,[pl0_target_intensity_std]
      ,[pl0_target_distance]
      ,[pl0_target_distance_std]
      ,[file_datetime])
      values (?,?,?,?,?,?,?,?,?,?,
      ?,?,?,?,?,?,?,?,?,?,
      ?,?,?,?,?,?,?,?,?,?,
      ?,?,?)
                      """
  cal_username = getpass.getuser()
  row_count = 0
  con_str = 'Driver={'+ ('SQL Server' if os.name == 'nt' else 'ODBC Driver 17 for SQL Server') + '};'        
  #con_str += 'Server=VL-MES-DB01.velodyne.com; Database=vlp_test;UID=vkrush1;PWD=Vtp4@vlp2020'
  con_str += 'Server=VL-MES-DB01.velodyne.com; Database=production;UID=finalimage;PWD=F1nalIm@g3'
  cnxn = pyodbc.connect(con_str) 
  cursor = cnxn.cursor()
  time_of_cal = datetime.fromtimestamp(float(os.path.getmtime(filepath))).replace(microsecond=0)
  try:
   with open(filepath, 'r') as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
             row_count = row_count + 1
             if row[0] != 'Board':
                if row_count == 2:
                     # manufacturing_id = row[52]
                     board = row[0]
                     trosa = row[1]
                     hv_setpoint_0 = row[17]
                     hv_setpoint_1 = row[18]
                     firmware_version = row[19]
                     firmware_version_date = row[20]
                     unit_build_githash = row[21]
                     unit_model_number = row[22]
                     unit_hw_revision = row[23]
                     unit_serial_number = row[24]
                     power_meter_attenuation_factor = row[35]
                     txcal_software_version = row[53]
                     git_tag = row[54]
                     git_tag_message = row[55]
                     git_develop_commit_number = row[56]
                     git_commit_id = row[57]
                     mcm_sdk_version = row[59]
                     pfmea_operation_code = row[60]
                     sw_version = row[61]
                     hostname = row[62]
                     cal_datetime = datetime.strptime(row[63], '%y%m%d%H%M%S')           
                     cursor.execute(sql_insert_cal_instance,
                    (manufacturing_id,
                     product_model,
                     board,
                     trosa,
                     hv_setpoint_0,
                     hv_setpoint_1,
                     firmware_version,
                     firmware_version_date,
                     unit_build_githash,
                     unit_model_number,
                     unit_hw_revision,
                     unit_serial_number,
                     power_meter_attenuation_factor,
                     txcal_software_version,
                     git_tag,
                     git_tag_message,
                     git_develop_commit_number,
                     git_commit_id,
                     mcm_sdk_version,
                     pfmea_operation_code,
                     sw_version,
                     hostname,
                     cal_datetime,
                     cal_username,
                     filepath))
                     row2 = cursor.fetchone()
                     vld_cal_key = row2[0]
                     print(vld_cal_key)
                
                channel = row[2]
                power_level = row[3]
                lp = row[4]
                boot = row[5]
                boot_in_hex = row[6]
                bias = row[7]
                bias_in_hex = row[8]
                prf = row[9]
                temperature = row[10]
                v_12v = row[11]
                v_3_3v = row[12]
                v_5vv = row[13]
                v_21v = row[14]
                system_temperature_c = row[15]
                hv = row[16]
                optical_power_in_mw = row[25]
                oven_temperature_c = row[30]
                power_supply_voltage_v = row[33]
                power_supply_current_a = row[34]
                peak_power_w = row[36]
                oven_set_temperature_c = row[37]
                average_power_table = row[38]
                average_power_ratio = row[43]
                average_power_percent_difference = row[44]
                bias_delta = row[45]
                pl0_width = row[46]
                pl0_target_intensity = row[47]
                pl0_target_intensity_std = row[48]
                pl0_target_distance = row[49]
                pl0_target_distance_std = row[50]
                cursor.execute(sql_insert_channel_dt,vld_cal_key,'TxCal',
                channel,
                power_level,
                lp,
                boot,
                boot_in_hex,
                bias,
                bias_in_hex,
                prf,
                temperature,
                v_12v,
                v_3_3v,
                v_5vv,
                v_21v,
                system_temperature_c,
                hv,
                optical_power_in_mw,
                oven_temperature_c,
                power_supply_voltage_v,
                power_supply_current_a,
                peak_power_w,
                oven_set_temperature_c,
                average_power_table,
                average_power_ratio,
                average_power_percent_difference,
                bias_delta,
                pl0_width,
                pl0_target_intensity,
                pl0_target_intensity_std,
                pl0_target_distance,
                pl0_target_distance_std,
                time_of_cal)              
   cnxn.commit()
   csvfile.close()
   print("Loaded into database")
  except Exception as e:
       cnxn.rollback()
       csvfile.close()
       print(e)
def parse_data_vcsel(filepath, manufacturing_id,product_model):
 try:
  #print(filepath)
  cal_username = getpass.getuser()
  #path_split = PurePath(filepath).parts
  #print(path_split[-2])
  #manufacturing_id = path_split[-2]
  time_of_cal = datetime.fromtimestamp(float(os.path.getmtime(filepath))).replace(microsecond=0)
  con_str = 'Driver={'+ ('SQL Server' if os.name == 'nt' else 'ODBC Driver 17 for SQL Server') + '};'        
  #con_str += 'Server=VL-MES-DB01.velodyne.com; Database=vlp_test;UID=vkrush1;PWD=Vtp4@vlp2020'
  con_str += 'Server=VL-MES-DB01.velodyne.com; Database=production;UID=finalimage;PWD=F1nalIm@g3'
  cnxn = pyodbc.connect(con_str) 
  c1 = cnxn.cursor()
  c1.execute('''
                SELECT max(vld_cal_key)
                FROM dbo.vld_txcal_instance 
                WHERE manufacturing_id = ? and product_model = ?''', (manufacturing_id,product_model))
  results = (c1.fetchone())
  vld_cal_key = results[0]
  print(vld_cal_key)
  if vld_cal_key is None:
     print('Errror CalTx must be performed first!!!')
  else:
    sql_insert_channel_dt = """\
                    EXEC [dbo].[InsertVcsel]
                     @vld_cal_key = ?,
      @cal_mode = ?,
      @channel  = ?,
      @power_level = ?,
      @lp = ?,
      @boot = ?,
      @boot_in_hex = ?,
      @bias = ?,
      @bias_in_hex = ?,
      @prf = ?,
      @temperature = ?,
      @v_12v = ?,
      @v_3_3v = ?,
      @v_5vv = ?,
      @v_21v = ?,
      @system_temperature_c = ?,
      @optical_power_in_mw = ?,
      @oven_temperature_c = ?,
      @power_supply_voltage_v = ?,
      @power_supply_current_a = ?,
      @peak_power_w = ?,
      @average_power_table = ?,
      @average_power_ratio = ?,
      @average_power_percent_difference = ?,
      @time_of_cal = ?,
      @vcsel_filename = ?,
      @row_num = ?
               """
    row_num = 0
    cursor = cnxn.cursor()
    with open(filepath, 'r') as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
             row_num = row_num + 1
             if row[0] != 'Board':
                channel = row[2]
                power_level = row[3]
                lp = row[4]
                boot = row[5]
                boot_in_hex = row[6]
                bias = row[7]
                bias_in_hex = row[8]
                prf = row[6]
                temperature = row[7]
                v_12v = row[8]
                v_3_3v = row[9]
                v_5vv = row[10]
                v_21v = row[11]
                system_temperature_c = row[12]
                #hv = None
                optical_power_in_mw = row[19]
                oven_temperature_c = row[24]
                power_supply_voltage_v = row[27]
                power_supply_current_a = row[28]
                peak_power_w = row[30]
                #oven_set_temperature_c = None
                average_power_table = row[31]
                average_power_ratio = row[36]
                average_power_percent_difference = row[37]
                #bias_delta = row[45]
                #pl0_width = row[46]
                #pl0_target_intensity = row[47]
                #pl0_target_intensity_std = row[48]
                #pl0_target_distance = row[49]
                #pl0_target_distance_std = row[50]
                #     print(filepath)
                cursor.execute(sql_insert_channel_dt,vld_cal_key,'vcsel',
                channel,
                power_level,
                lp,
                boot,
                boot_in_hex,
                bias,
                bias_in_hex,
                prf,
                temperature,
                v_12v,
                v_3_3v,
                v_5vv,
                v_21v,
                system_temperature_c,
                optical_power_in_mw,
                oven_temperature_c,
                power_supply_voltage_v,
                power_supply_current_a,
                peak_power_w,
                average_power_table,
                average_power_ratio,
                average_power_percent_difference,
                time_of_cal,
                filepath ,
                row_num
                               )          
    cnxn.commit()
    csvfile.close()
    print("Loaded into database")
 except Exception as e:
       cnxn.rollback()
       csvfile.close()
       print(e)
def load_caltxt_data( filepath, manufacturing_id, product_model,cal_mode ):
#cal_mode 0 - power_level_table 1 - vcsel_power_level_table
   if cal_mode == 0:
       parse_data_txcal(filepath, manufacturing_id, product_model)
   elif cal_mode == 1:
       parse_data_vcsel(filepath, manufacturing_id, product_model)
      
if __name__ == "__main__":
    #file = r"/usr/local/etc/velodyne/B1539/power_level_table.csv"
    #file = r"C:\Velodine\TxCal\b0\B1539\power_level_table.csv"
    file = r"C:\Velodine\TxCal\b0\B1539\vcsel_power_level_table.csv"
    product_model = 'V16'
    manufacturing_id = 'B1539' 
    load_caltxt_data( file,manufacturing_id,product_model,1)
    



