#
# Copyright (c) 2018 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical
# concepts contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law.
# Dissemination of this information or reproduction of this material is strictly prohibited unless prior written
# permission is obtained from Velodyne. Access to the source code contained herein is prohibited to anyone except
# current Velodyne employees or consultants. Any access to the source code is subject to the obligations of
# confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#                  Trung Tran <trungtran@velodyne.com>
#  Creation date : 2019-04-24
#  Description   : Software for parsing live UDP packets sent by a LiDAR sensor over ethernet.
#                  Currently supports the legacy packet format, and the reduced packet format version of
#                  APF.A CSV file is dumped with parsed data.
#

import socket
import errno
import time
import struct
import csv
import argparse
from collections import deque

import signal
import sys
from scapy.all import *
# Socket Globals
IP_ADDRESS = '192.168.1.200'
PORT = 2368
LEGACY_PACKET_LEN = 1248
SOCKET_TIMEOUT = 1
SOCKET_BLOCK = 0
RPF_PACKET_LEN = 1304
# Packet Globals
NUM_BLOCKS = 12
NUM_CHAN = 32
RPF_NUM_RETURN = 160
HEADER_LEN = 0
# HEADER_LEN = 42
DATA_BLOCK_HEADER_LEN = 4
CHANNEL_BLOCK_LEN = 3
CHANNEL_BLOCK_TOTAL_LEN = 32 * 3
DATA_BLOCK_LEN = DATA_BLOCK_HEADER_LEN + CHANNEL_BLOCK_TOTAL_LEN

# CSV Globals
RESULTS_HEADER = ["Timestamp", "Elevation", "VDIR",  "Azimuth", "Channel", "Distance", "Intensity", "PSEQF"]
CSV_HEADER = 1

NUM_PACKETS_TO_CAPTURE = 1200

class ClientSocket:
    def __init__(self, sock=None):
        if sock is None:
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        else:
            self._sock = sock


    def __del__(self):
        self.close()

    def connect(self, host, port):
        connected = False
        while not connected:
            try:
                self._sock.bind((host, port))
                connected = True
            except Exception as e:
                print("%s" % e)
                exit(0)

        print('connecting to %s port %s' % (IP_ADDRESS, PORT))


    def set_params(self, timeout=0, block=1):
        self._sock.setblocking(block)
        self._sock.settimeout(timeout)

    def recv(self, msg_len, t_out=0.0000000001):
        received = False
        start_time = time.time()
        data = None
        while not received:
                received = True
                data = self._sock.recvfrom(msg_len)
        return data


    def close(self):
        # print("Closing socket connection with %s" % self._sock.getsockname())
        self._sock.close()

'''
Packet Format
Header : 42 bytes
Data Block : 2 Bytes Elevation + 2 Bytes Azimuth + 32*(Channel Block)
Channel Block : 2 Bytes Distance + 1 Byte Intensity
'''

class ch_packet(object):
    def __init__(self):
        self.distance = 0.0
        self.intensity = 0

        self._ch_packet_st = struct.Struct('H B')

    def unpack(self, r_data=""):

        data = self._ch_packet_st.unpack(r_data)

        # For raw distance, use the next line and comment the line after
        # self.distance = data[0]
        self.distance = (data[0] * 4.0 / 1000.0)
        self.intensity = data[1]
        return self


class data_packet(object):
    def __init__(self):
        self.elevation = 0
        self.azimuth = 0
        self.ch_block = []

        for i in range(0, NUM_CHAN):
            self.ch_block.append(ch_packet())

        self._data_packet_st = struct.Struct('H H')

    def unpack(self, r_data=""):

        data = self._data_packet_st.unpack(r_data[:DATA_BLOCK_HEADER_LEN])

        self.elevation = data[0]
        self.azimuth = data[1]

        k = 0
        for i in range(0, NUM_CHAN):
            self.ch_block[i].unpack(\
                r_data[DATA_BLOCK_HEADER_LEN + k: DATA_BLOCK_HEADER_LEN + k + CHANNEL_BLOCK_LEN])

            k += CHANNEL_BLOCK_LEN

class rpf_payload_header:
    """Payload header parser
    """
    def __init__(self):
        format_string ='!BBBBIQBBH'
        self.parser_struct = struct.Struct(format_string)
        self.length = struct.calcsize(format_string)
    def unpack(self, data):
        parsed_data = self.parser_struct.unpack_from(data)
        self._ver = parsed_data[0] >> 4
        self._hlen= parsed_data[0] & 0x0F
        self._nxthdr = parsed_data[1]
        self._ptype = parsed_data[2] >> 4
        self._tlen = parsed_data[2] & 0x0F
        self._mic = parsed_data[3]
        self._psq = parsed_data[4]
        self._tref = parsed_data[5]
        self._glen = parsed_data[6] >> 4
        self._flen = parsed_data[6] & 0x0F
        self._dset = parsed_data[7]
        self._iset = parsed_data[8]
        return self
    def __str__(self):
        return ("PAYLOAD HEADER:\n"
                "  VER={:x}\n"
                "  HLEN={:x}\n"
                "  NXHDR={:x}\n"
                "  PTYPE={:x}\n"
                "  TLEN={:x}\n"
                "  MIC={:x} \n"
                "  PSEQ={:x}\n"
                "  TREF={}\n"
                "  GLEN={}\n"
                "  FLEN={}\n"
                "  DSET={}\n"
                "  ISET={}\n"
                "\n").format(
                       self._ver,
                       self._hlen,
                       self._nxthdr,
                       self._ptype,
                       self._tlen,
                       self._mic,
                       self._psq,
                       self._tref,
                       self._glen,
                       self._flen,
                       self._dset,
                       self._iset
                       )
class rpf_firing_return():
    """Firing return packet
    """
    def __init__(self):
        format_string ='!HHHBB'
        self.parser_struct = struct.Struct(format_string)
        self.length = struct.calcsize(format_string)
    def unpack(self, data):
        """Unpack data onto this struct
        Arguments:
            data {bytes}: raw udp data payload
        """
        assert(len(data)>=self.length)
        parsed_data = self.parser_struct.unpack_from(data)

        self._hdir = parsed_data[0] >> 15
        self._vdir = (parsed_data[0] >> 14) & 0x1
        self._vdfl = parsed_data[0] & 0x3FFF
        self._azm = parsed_data[1]

        # Convert raw distance to metres.
        self._dist = (parsed_data[2] * 4.0 / 1000.0)
        self._rft = parsed_data[3]
        self._lcn = parsed_data[4]
        return self
    def __str__(self):
        return ("Firing return:\n"
                "  HDIR={:x}\n"
                "  VDIR={:x}\n"
                "  VDFL={:x}\n"
                "  AZM={:x}\n"
                "  DIST={:f}\n"
                "  RFT={:x} \n"
                "  LCN={:x}\n"
                "\n").format(
                       self._hdir,
                       self._vdir,
                       self._vdfl,
                       self._azm,
                       self._dist,
                       self._rft,
                       self._lcn
                       )

class rpf_payload_footer():
    """RPF payload footer
    """
    def __init__(self):
        format_string ='!HBB'
        self.parser_struct = struct.Struct(format_string)
        self.length = struct.calcsize(format_string)
    def unpack(self, data):
        """Unpack data onto this struct
        Arguments:
            data {bytes}: raw udp data payload
        """
        parsed_data = self.parser_struct.unpack_from(data)
        self._crc = parsed_data[0]
        self._ac = parsed_data [1]
        self._pseqf = parsed_data[2]
        return self
    def __str__(self):
        return ("Firing Footer:\n"
                "  CRC={:x}\n"
                "  AC={:x}\n"
                "  PSEQF={:x}\n"
                "\n").format(
                       self._crc,
                       self._ac,
                       self._pseqf
                       )

class rpf_packet():
    """Composition of rpf payload
    1   Header
    160 Firing return packet
    1   Footer
    """
    def __init__(self):
        self.firing_returns = []
    def unpack(self,data=""):
        assert len(data) == RPF_PACKET_LEN
        self.pl_hdr=rpf_payload_header().unpack(data)
        for i in range(0,RPF_NUM_RETURN):
            self.firing_returns.append(rpf_firing_return().unpack(data[self.pl_hdr.length + i*8:]))
        self.pl_ftr = rpf_payload_footer().unpack(data[self.pl_hdr.length + RPF_NUM_RETURN*8:])
        return self
    def __str__(self):
        rstr="" + str(self.pl_hdr) + "\n"
        for i in range(0,RPF_NUM_RETURN):
            rstr += str(self.firing_returns[i]) + "\n"
        rstr += str(self.pl_ftr)
        return rstr


class legacy_packet(object):
    def __init__(self):
        self.header = 0
        self.data_block = []
        for i in range(0, NUM_BLOCKS):
            self.data_block.append(data_packet())

        self.timestamp = 0
        self.factory = 0

        self._packet_rest_st = struct.Struct('I H')

    def unpack(self, r_data=""):
        # header is stripped out
        # packet_header_st = struct.Struct('42B')
        # self.header = packet_header_st.unpack(r_data[:HEADER_LEN])

        k = 0
        for i in range(0, NUM_BLOCKS):
            self.data_block[i].unpack(\
                r_data[HEADER_LEN + k: HEADER_LEN + k + DATA_BLOCK_LEN])
            k += DATA_BLOCK_LEN

        data = self._packet_rest_st.unpack(r_data[HEADER_LEN + k: ])

        self.timestamp = data[0]
        self.factory = data[1]

class utilities:
    def __init__(self):
        raise RuntimeError("This is static class")
    @staticmethod
    def write_to_csv_legacy(filename="temp.csv", udp=None, csv_writer=None, results_file=None, ch=None):
        global CSV_HEADER
        # Create results dictionary
        results_dict = {}
        for keys in RESULTS_HEADER:
            results_dict[keys] = None
        if CSV_HEADER:
            csv_writer.writeheader()
            CSV_HEADER = 0
        if ch:
            start_iter = ch
            end_iter = ch + 1
        else:
            start_iter = 0
            end_iter = NUM_CHAN

        for i in range(0, NUM_BLOCKS):
            for j in range(start_iter, end_iter):
                results_dict['Timestamp'] = udp.timestamp
                results_dict['Elevation'] = udp.data_block[i].elevation
                results_dict['Azimuth'] = udp.data_block[i].azimuth
                results_dict['Channel'] = j
                results_dict['Distance'] = udp.data_block[i].ch_block[j].distance
                results_dict['Intensity'] = udp.data_block[i].ch_block[j].intensity
                csv_writer.writerow(results_dict)

    @staticmethod
    def write_to_csv_rpf(filename="temp.csv", packet=None, csv_writer=None, results_file=None, ch=None, discard_mobis_zero_fill=True):
        """ Writes a single packet worth of information to the CSV. 

        Args:
            filename (str, optional): [description]. Defaults to "temp.csv".
            packet ([type], optional): [description]. Defaults to None.
            csv_writer ([type], optional): [description]. Defaults to None.
            results_file ([type], optional): [description]. Defaults to None.
            ch ([type], optional): [description]. Defaults to None.
            discard_mobis_zero_fill (bool, optional): Defaults to True.
                This is specific to V8 Mobis packet behavior, wherein upon VDIR change, the subsequent returns in a packet are 
                zero-filled. ALL the fields of the firing return section of packet is 0 , when this happens.
                This should be discarded for data analysis purposes, so is enabled by default. This could change with a different
                packet implementation behavior, and so should be reviewed and used carefully with caution. 
        """        
        results_dict = {}
        for i in range(0, RPF_NUM_RETURN):
            results_dict['Timestamp'] = packet.pl_hdr._tref
            results_dict['Elevation'] = packet.firing_returns[i]._vdfl
            results_dict['Azimuth'] = packet.firing_returns[i]._azm
            results_dict['VDIR']    = packet.firing_returns[i]._vdir
            results_dict['Channel'] = packet.firing_returns[i]._lcn
            results_dict['Distance'] = packet.firing_returns[i]._dist
            results_dict['Intensity'] = packet.firing_returns[i]._rft
            results_dict['PSEQF']     = packet.pl_ftr._pseqf
            # Do not write zero-fill returns to the CSV. 
            if (discard_mobis_zero_fill):
                if not ((packet.firing_returns[i]._hdir == 0) and 
                        (packet.firing_returns[i]._vdir == 0) and
                        (packet.firing_returns[i]._vdfl == 0) and
                        (packet.firing_returns[i]._azm == 0) and
                        (packet.firing_returns[i]._dist == 0) and
                        (packet.firing_returns[i]._rft == 0) and
                        (packet.firing_returns[i]._lcn == 0) ):
                    csv_writer.writerow(results_dict)
            else:
                csv_writer.writerow(results_dict)
                
    @staticmethod
    def dump_csv(packets, filename, writer_func, parser, is_pcap=False):
        """Dumping packets to a file using writer_func function

        Arguments:
            packets {list} -- list of packets
            filename {path} -- full path to csv file name
            writer_func {function} -- writer function
            parser {parser} -- packet parser
            is_pcap {bool} -- indicate if the packets are pcap type packets
        """
        print("Writing total of {} packets to {}".format(len(packets), filename))
        first_time = True
        expect_seq  = 0
        total_packet = 0
        first_seq = 0
        last_seq = 0
        dropped = 0
        count = 0
        with open(filename, 'w') as results_file:
            csv_writer = csv.DictWriter(results_file, fieldnames=RESULTS_HEADER)
            csv_writer.writeheader()
            if not isinstance(packets,deque):
                packets = deque(packets)
            while packets:
                raw_packet = packets.popleft()
                packet = None
                if is_pcap:
                    if UDP in raw_packet:
                        if (raw_packet[UDP].dport == PORT):
                            packet=parser().unpack(raw_packet[UDP].load)
                    else:
                        packet = None
                else:
                    packet = parser().unpack(raw_packet)
                if  packet != None:
                    if (first_time):
                            first_seq = packet.pl_hdr._psq
                            expect_seq = packet.pl_hdr._psq
                            first_time = False
                    total_packet = packet.pl_hdr._psq - first_seq + 1
                    if (packet.pl_hdr._psq != expect_seq):
                        dropped += packet.pl_hdr._psq - expect_seq
                        print("[WARNING] {} packet(s) dropped. Expect:{};Get {}. dropped so far={}. Total packet={}. dropped percentage= {}%"
                            .format(
                                packet.pl_hdr._psq - expect_seq,
                                expect_seq,
                                packet.pl_hdr._psq,
                                dropped,
                                total_packet, (dropped/total_packet)*100
                            )
                        )
                    expect_seq = packet.pl_hdr._psq  + 1
                    writer_func(filename, packet, csv_writer, results_file, 0)
                    print('Wrote {}'.format(count),end='\r')
                    count += 1
            print("Drop packet: total {} out of {} packets receive. drop_percentage= {}%".format(dropped,total_packet, "N/A" if total_packet == 0 else (dropped/total_packet)*100 ))
        print("Done!")

class packet_reader:
    def __init__(self, fmt, csv_file=None):
        self.packets = deque()
        if fmt == "legacy":
            self.packet_parser = legacy_packet
            self.pkt_len = LEGACY_PACKET_LEN
        elif fmt == "rpf":
            self.packet_parser = rpf_packet
            self.pkt_len = RPF_PACKET_LEN
        else:
            print("Unrecognized format.")
            return
        if (csv_file == None):
            self.filename = "./result_packet_{}.csv".format(fmt)
        else:
            self.filename = csv_file
        self.packet_format = fmt

        if fmt == 'legacy':
            self.write_func = utilities.write_to_csv_legacy
        elif fmt == 'rpf':
            self.write_func = utilities.write_to_csv_rpf

        # Install the signal handler
        # signal.signal(signal.SIGINT, lambda sig_num,current_stack:utilities.dump_csv(self.packets,self.filename,self.write_func,self.packet_parser))

    def read_from_pcap(self, pcap_file):
        print("Start reading pcap file {}.".format(pcap_file))
        raw_packets = rdpcap(pcap_file)
        print("Done")
        utilities.dump_csv(raw_packets, self.filename, self.write_func, self.packet_parser,True)

    def read_from_device(self, ip_addrr= IP_ADDRESS, port=PORT, num_packets=None):
        self.dev_socket = ClientSocket()
        self.dev_socket.connect(ip_addrr, port)
        self.dev_socket.set_params(SOCKET_TIMEOUT, SOCKET_BLOCK)
        if num_packets is None:
            #default case
            max_packet_count = NUM_PACKETS_TO_CAPTURE
        else:
           max_packet_count = num_packets
        packet_counter = 0
        print(max_packet_count)
        while True:
            packet_counter += 1
            r_data = self.dev_socket.recv(self.pkt_len)
            packet = self.packet_parser()
            self.packets.append(r_data[0])
            print('Captured {} packets'.format(packet_counter),end='\r')
            # Dump csv and exit if max packet count exceeded
            if (packet_counter == max_packet_count):
                self.dev_socket.close()
                utilities.dump_csv(self.packets,self.filename,self.write_func,self.packet_parser)
                break

def main(args):
    filename = args.csv_file
    ch = args.ch
    fmt = args.fmt
    pcap_file = args.pcap_file
    dev_ipaddr = args.ip_addr
    dev_port = args.port
    num_packets = args.count
    if (fmt == None):
        fmt = "rpf"
    if ch is not None:
        ch = int(ch)
        if ch < 0 or ch >= NUM_CHAN:
            print("Channel %d not supported. Exiting" % ch)
            return
    pkt_reader =  packet_reader(fmt,filename)
    if fmt is None:
        print("Provide the packet format type!")
        return
    else:
        if (pcap_file != None):
            pkt_reader.read_from_pcap(pcap_file)
        else:
            pkt_reader.read_from_device(dev_ipaddr,dev_port,num_packets)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-o","--csv_file", help="csv file to write data result")
    parser.add_argument("-A", "--ip_addr", nargs="?", default="0.0.0.0", help="hardware IP address")
    parser.add_argument("-P", "--port", nargs="?", default=2368, help="hardware UDP port")
    parser.add_argument("--ch", help="choose channel, do not use if all channel data is needed")
    parser.add_argument("--fmt", help="Select the format of the packet output from sensor: rpf ; legacy")
    parser.add_argument('-c','--count', type=int, help="Number of packet captures. if this value is -1, it is continous capture mode use Ctrl+C to stop")
    parser.add_argument('-p','--pcap_file', help="Read from pcap file")
    args = parser.parse_args()
    main(args)

