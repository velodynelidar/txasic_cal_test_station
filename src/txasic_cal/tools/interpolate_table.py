#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-10
#  Description   : Python file to interpolate table given temperature points

import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import datetime
import time
import argparse

from txasic_cal.tools.logger import logger
from txasic_cal.calibration import config
from txasic_cal.tools import file_io
from txasic_cal.sensor import sensor
from txasic_cal.lab import lab

START_INTERP_TEMP = -40
END_INTERP_TEMP = 125
TOTAL_TEMP_BINS = 34
SPACE_TEMP_BINS = 5

class interpolate:
    def __init__(self, sensor, lab, file_io, unit, version):
        """Interpolate class

        Args:
            sensor (obj): Sensor class object
            lab (obj): Lab class object
            file_io (obj): File io object
            unit (str): Unit name
            version (str): Version name
        """
        self.input_file = None

        self.sensor = sensor
        self.lab = lab
        self.unit = unit
        self.version = version + "_INT_" + config.VERSION_NUMBER
        self.df = None

        # Results file
        self.results_file = file_io
        self.file_name = self.results_file.file_name.stem
        self.results_header = self.sensor.results_header + ['Temperature Bin'] + \
             self.lab.measurement_header + config.CALIBRATION_HEADER
        self.results_row = {}

    def init(self, file_name=None):
        """Init

        Args:
            file_name (str, optional): String name of file to open. Defaults to None.
        """
        if file_name is None:
            logger.error("Please provide input file name.")
            return

        self.input_file = file_name
        self.df = pd.read_csv(self.input_file)

        # File write init
        self.results_file.init(fieldnames=self.results_header)
    
    def linear_interpolation(self, start_temp, end_temp, bin_temp, space_temp, cal_temp, temp_list=None):
        """Linear interpolation within temperature range, which must be in ascending order

        Args:
            start_temp (int): Start temperature range value
            end_temp (int): End temperature range value
            bin_temp (int): Number of temperature bins
            space_temp (int): Temperature bin spacing
            cal_temp (int): Room temperature cal temp (usually 30 C)
            temp_list (list, optional): Temp TxCal temperature list. Defaults to None.
        """
        if temp_list is None:
            logger.error("Please provide temperature list.")
            return
        
        interp_temp_list, interp_bin_space = np.linspace(
            start_temp, end_temp, bin_temp, dtype=int, retstep=True)
        
        if interp_bin_space != space_temp:
            logger.error("Interpolated bin space does not match requested bin space.")
            return
        
        df = self.df.sort_values('Oven Set Temperature(C)')
        for ch in self.sensor.sensor_module.channels:
            for pl in range(16):
                if pl > 15:
                    continue
                index = df[(df['Channel'] == ch) &
                (df['Power Level'] == pl) & 
                (df['Oven Set Temperature(C)'].isin(temp_list))].index

                index_cal_temp = df[(df['Channel'] == ch) &
                (df['Power Level'] == pl) & 
                (df['Oven Set Temperature(C)'] == cal_temp)].index
                
                lp_list = np.array(self.df.iloc[index]['LP'])
                boot_list = np.array(self.df.iloc[index]['Boot'])
                bias_list = np.array(self.df.iloc[index]['Bias'])

                interp_boot_f = interp1d(temp_list, boot_list, fill_value='extrapolate')
                interp_bias_f = interp1d(temp_list, bias_list, fill_value='extrapolate')

                interp_lp_list = [lp_list[0] for i in range(bin_temp)]
                interp_boot_list = interp_boot_f(interp_temp_list)
                interp_bias_list = interp_bias_f(interp_temp_list)

                # Limit boot within range
                for i in range(len(interp_boot_list)):
                    if interp_boot_list[i] < self.sensor.config.BOOT_MIN:
                        interp_boot_list[i] = self.sensor.config.BOOT_MIN
                    if interp_boot_list[i] > self.sensor.config.BOOT_MAX:
                        interp_boot_list[i] = self.sensor.config.BOOT_MAX

                # Limit bias within range [0 - config max]
                # Config min limit on bias is for room temperature cal
                # We might want the extrapolated value to go lower than room temperature cal min limit
                for i in range(len(interp_bias_list)):
                    # Prevent extrapolation from going negative
                    if interp_bias_list[i] < 0:
                        interp_bias_list[i] = 0
                    if interp_bias_list[i] > self.sensor.config.BIAS_MAX:
                        interp_bias_list[i] = self.sensor.config.BIAS_MAX

                for i, each_temp in enumerate(interp_temp_list):
                    if each_temp < -30:
                        continue
                    if each_temp == -30:
                        each_temp = -40

                    try:
                        for j in self.sensor.results_header:
                            if each_temp in temp_list:
                                self.results_row[j] = self.df.iloc[index][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            elif each_temp == cal_temp:
                                self.results_row[j] = self.df.iloc[index_cal_temp][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            else:
                                self.results_row[j] = None

                        for j in self.lab.measurement_header:
                            if each_temp in temp_list:
                                self.results_row[j] = self.df.iloc[index][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            elif each_temp == cal_temp:
                                self.results_row[j] = self.df.iloc[index_cal_temp][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            else:
                                self.results_row[j] = None

                        for j in config.CALIBRATION_HEADER:
                            if each_temp in temp_list:
                                self.results_row[j] = self.df.iloc[index][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            elif each_temp == cal_temp:
                                self.results_row[j] = self.df.iloc[index_cal_temp][j][df['Oven Set Temperature(C)'] == each_temp].values[0]
                            else:
                                self.results_row[j] = None
                    except KeyError:
                        pass

                    self.results_row['Board'] = 'Top'
                    self.results_row['TROSA'] = 'Top'
                    self.results_row['Channel'] = ch
                    self.results_row['Temperature Bin'] = i - 2
                    self.results_row['Power Level'] = pl
                    self.results_row['LP'] = hex(int(interp_lp_list[0], 16))
                    self.results_row['Boot'] = round(interp_boot_list[i], 3)
                    self.results_row['Boot in Hex'] = hex(
                        int((interp_boot_list[i] * self.sensor.sensor_module.config.MAX_DAC_DECIMAL) / 
                        self.sensor.sensor_module.config.MAX_DAC_VOLT))
                    self.results_row['Bias'] = round(interp_bias_list[i], 3)
                    self.results_row['Bias in Hex'] = hex(
                        int((interp_bias_list[i] * self.sensor.sensor_module.config.MAX_DAC_DECIMAL) / 
                        self.sensor.sensor_module.config.MAX_DAC_VOLT))

                    self.results_row['Unit'] = self.unit
                    self.results_row['Version'] = self.version
                    self.results_row['Filename'] = self.file_name
                    self.results_row['Datetime'] = datetime.datetime.now().strftime("%y%m%d%H%M%S")

                    self.results_file.write(self.results_row)
                
        # Flush file
        self.results_file.file.flush()


def main(args, sensor_module):
    unit_name = "P7"
    version = "TS"

    if(not args.input_file):
        logger.error("Input file required.")
        return

    if(args.unit):
        unit_name = args.unit.upper()

    if(args.version):
        version = args.version.upper()

    if(not args.output_file):
        file_name = unit_name + "_" + version + "_" + "interp_power_level_table_" + \
                    datetime.datetime.now().strftime("%y%m%d%H%M%S") + ".csv"
    else:
        file_name = args.output_file

    _sensor = sensor.sensor(sensor_module=sensor_module, version=version)
    _lab = lab.lab()
    _file_io = file_io.file_io(file_name)
    _file_io.file_open()
    _intrep = interpolate(_sensor, _lab, _file_io, unit_name, version)

    _intrep.init(args.input_file)

    temp_list = [-40, 0, 40, 85]
    _intrep.linear_interpolation(-40, 125, 34, 5, 30, temp_list)
    _file_io.file_close()


if __name__ == "__main__":
    start_time = time.time()
    parser = argparse.ArgumentParser(description='Table Interpolation')
    parser.add_argument('-u', '--unit', help='Unit name')
    parser.add_argument('-v', '--version', help='TxCal version: IS (Integrating Sphere),\
        TG (Temperature Gonio), IV (Interpolation Verification)')
    parser.add_argument('-if', '--input_file', help='File name used for creating interpolated table.')
    parser.add_argument('-of', '--output_file', help='Results file name.')
    args = parser.parse_args()
    sensor_module = 'velarray.V8'
    main(args, sensor_module=sensor_module)
    end_time = time.time()

    logger.debug("\n\nCompleted in {} sec or {} min".format((end_time - start_time), (end_time - start_time)/60.0))