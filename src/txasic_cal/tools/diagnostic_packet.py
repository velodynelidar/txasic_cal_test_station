#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2021-04-26
#  Description   : A Diagnostic stream packet receiver. This is an example to parse a single diagnostic packet coming from socket.
#
#

import socket

from txasic_cal.communication.config import CUSTOM_SDK

class ClientSocket_DP:
    def __init__(self, sock=None):
        if sock is None:
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        else:
            self._sock = sock

    def __del__(self):
        self.close()

    def connect(self, host, port):
        connected = False
        while not connected:
            try:
                self._sock.bind((host, port))
                connected = True
            except Exception as e:
                print("%s" % e)
                return connected
        return connected

    def set_params(self, timeout=0, block=1):
        self._sock.setblocking(block)
        self._sock.settimeout(timeout)

    def recv(self, msg_len, t_out=0.0000000001):
        received = False
        data = None
        while not received:
            received = True
            data = self._sock.recvfrom(msg_len)
        return data

    def close(self):
        self._sock.close()


def run_diagnostic_packet():
    """Run diagnostic packet recv and parse a single UDP packet (RPF format)

    Returns:
        str: Model number, HW version, Firmware version
    """

    model_num = '0x0'
    system_hw_version = '0x0'
    system_image_version = None

    if CUSTOM_SDK:
        return model_num, system_hw_version, system_image_version

    try:
        from velaplatform.utils.diagnostic_stream_parser import diagnostic_stream_parser, DSTREAM_PORT, measurement_data
    except ModuleNotFoundError:
        return model_num, system_hw_version, system_image_version
        
    dev_socket = ClientSocket_DP()
    if not dev_socket.connect('192.168.1.200', DSTREAM_PORT):
        return model_num, system_hw_version, system_image_version

    dev_socket.set_params(1, 0)

    r_data = None
    try:
        # RPF packet size 1304
        r_data = dev_socket.recv(1304)
    except socket.timeout:
        return model_num, system_hw_version, system_image_version

    if not r_data:
        return model_num, system_hw_version, system_image_version

    try:
        diagnostic_stream_parser.parse((r_data[0], r_data[1]))
        ret = diagnostic_stream_parser.sensor_data[r_data[1]]
        model_num = measurement_data(2, ret['Measurements'][2])._val
        system_hw_version = measurement_data(12, ret['Measurements'][12])._val
        system_image_version = measurement_data(13, ret['Measurements'][13])._val

    except Exception as e:
        # If data is corrupted, or error with parsing, return defaults
        return model_num, system_hw_version, system_image_version

    return model_num, system_hw_version, system_image_version

if __name__ == "__main__":
    run_diagnostic_packet()

