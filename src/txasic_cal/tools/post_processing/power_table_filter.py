import pathlib
import numpy as np
import pandas as pd
import operator

import plotly.graph_objects as go
import plotly.express as px

CAL_FOLDER = pathlib.Path(r'/cal/velarray/')

class files:
    def __init__(self, sensor_module=None, filter_module=None):
        """File class to filter files

        Args:
            sensor_module ([str], optional): Name of sensor module (e.g., v16sr/b0). Defaults to None.
            filter_module ([dict], optional): Dict of columns to filter files from. Defaults to None.
        """
        self.sensor_module=sensor_module
        self.filter_module=filter_module
        
        self.filtered_files = None

    def file_filter(self):
        """Filter files and store in filtered_files
        """
        print("Getting files for sensor {} filtered by {}".format(self.sensor_module, self.filter_module))
        cal_folder = CAL_FOLDER
        first_filtered_files = []

        def filter_in(f):
            """Helper function to filter files based on columns

            Args:
                f ([str]): File name with correct directory (e.g., velarray/v16sr/b0/B111/power_level_table.csv)

            Returns:
                [boot]: True or False if column and value matches file
            """
            # Only read column and one row
            # Files are filtered based on values from first row for given column
            try:
                df = pd.read_csv(f, nrows=1)
            except Exception as e:
                print("Exception {} for file {}".format(e, f))
                return False
            try:
                for key, value in self.filter_module.items():
                    try:
                        if value in df[key][0]:
                            continue
                        else:
                            return False
                    except Exception as e:
                        return False
            except Exception as e:
                pass

            return True

        if self.sensor_module:
            cal_folder = cal_folder / pathlib.Path(self.sensor_module)

            for f in cal_folder.glob('*/power_level_table.csv'):
                if self.filter_module:
                    if filter_in(f):
                        first_filtered_files.append(f)
                else:
                    first_filtered_files.append(f)
        else:
            # No sensor module provided, get all tables
            for f in cal_folder.glob('v8_a0/*/power_level_table.csv'):
                if self.filter_module:
                    if filter_in(f):
                        first_filtered_files.append(f)
            for f in cal_folder.glob('v16sr/b0/*/power_level_table.csv'):
                if self.filter_module:
                    if filter_in(f):
                        first_filtered_files.append(f)

        self.filtered_files = first_filtered_files
        print("Found {} files.".format(len(self.filtered_files)))
        
    def combine_files(self, files=None):
        if files is None:
            files = self.filtered_files
            
        print("Combining {} files to power_level_table_combined.csv".format(len(files)))
        combined_csv = pd.concat([pd.read_csv(f) for f in files])
        
        # Export to csv
        combined_csv.to_csv("power_level_table_combined.csv", index=False, encoding='utf-8-sig')
        

class csvs:
    def __init__(self, filter_module=None, result_module=None):
        """Class module to filter csvs and display

        Args:
            filter_module ([dict], optional): Dict of column names and valeus to filter csv on 
            (e.g., Channel:0 will only get rows that match this value). Defaults to None.
            result_module ([list], optional): List of column names to display. Defaults to None.
        """
        self.filter_module = filter_module
        self.csv_frame_values = None
        self.csv_files = []
        
        self.result_module=result_module
        self.result_filtered_files = None

    def csv_filter(self, file=None):
        """Filter csv file based on filter_module

        Args:
            file ([str], optional): File name to open. Defaults to None.

        Returns:
            [dict]: Dict that contains the filtered dataframe
        """
        if not file:
            print("No file provided.")
            return

        csv_frame_values = None

        df = pd.read_csv(file)
        try:
            for key, value in self.filter_module.items():
                df = df[value['expr'](df[key], value['val'])]
        except AttributeError:
            pass

        # Dataframe contains all power table information post filtering
        csv_frame_values = {'dataframe':df}
        try:
            csv_frame_values.update(self.filter_module)
        except TypeError:
            pass
        csv_frame_values['file'] = file
        
        return csv_frame_values
        
    def get_row_from_files(self, file=None):
        """Get required rows from files and add to csv_files

        Args:
            file ([list], optional): String list of file names. Defaults to None.
        """
        if not file:
            print("No file provided.")
            return
        
        print("Getting columns {} filtered by {}".format(self.result_module, self.filter_module))
        
        '''
        You can think of self.csv_files as the master table that holds the filtered dataframes.
        Everytime you add a new filter, this list gets appended. So this way the previous request is still
        in memory.

        This allows for filtering to be separate from displaying
        '''
        csv_files = []
        for f in file:
            csv_files.append(self.csv_filter(f))
            
        self.csv_files.append(csv_files)
        
    def combine_csv(self):
        combined_csv = None
        for j in self.csv_files:
            for i in j:
                if combined_csv is None:
                    combined_csv = i['dataframe']
                    continue
                combined_csv = pd.concat([combined_csv, i['dataframe']])
        
        # Export to csv
        combined_csv.to_csv("power_level_table_combined.csv", index=False, encoding='utf-8-sig')

    def result_filter(self, csv_files=None, result_module=None):
        """Get columns that need to be displayed and concatenate them from the dataframe
        So if for e.g., you filter on Ch 0, Power Level 0, all the dataframe will only contain those rows
        and you can concatenate all the result columns let's say boot values into one numpy array.

        If you given no filters, then the dataframe will contain all channel and power level information and
        correspondingly the numpy array will also concatenate values for all channel and power levels for the chosen
        column (boot in this case).

        The results are stored in a dict called result_filtered_files where each key holds the value to the concatenated
        numpy array. So e.g., 'boot':[4.0, 4.0, 4.3, 4.2, ...]

        Args:
            csv_files ([dict], optional): Dict of csv files with dataframes. Defaults to None.
            result_module ([list], optional): String list of results to display. Defaults to None.
        """
        if csv_files is None:
            csv_files = self.csv_files
        if result_module is None or result_module == []:
            result_module=self.result_module
        
        result = {}
        for r in result_module:
            result[r] = []
            for i in self.csv_files:
                a = np.array([])
                for f in i:
                    try:
                        a = np.concatenate([a, f['dataframe'][r].values])
                    except KeyError:
                        continue

                result[r].append(a)

        self.result_filtered_files = result
        
    def filter_table(self, result_filtered_files=None, stat_type='mean'):
        """Filter the table based on statistical request

        Args:
            result_filtered_files ([dict], optional): Dict of numpy arrays. Defaults to None.
            stat_type (str, optional): Statistical request (mean, std, var, min, max). Defaults to 'mean'.
        """
        if result_filtered_files is None:
            result_filtered_files = self.result_filtered_files
        if self.result_filtered_files is None:
            return

        print("Applying statistical type {}".format(stat_type))

        if stat_type == 'mean':
            stat_type=np.nanmean
        elif stat_type == 'std':
            stat_type=np.nanstd
        elif stat_type == 'var':
            stat_type=np.nanvar
        elif stat_type == 'min':
            stat_type=np.nanmin
        elif stat_type == 'max':
            stat_type=np.nanmax

        post_stat_result_files = {}
        for key, value in result_filtered_files.items():
            try:
                post_stat_result_files[key] = []
                for i in value:
                    try:
                        post_stat_result_files[key].append(np.around(stat_type(i), 3))
                    except Exception as e:
                        post_stat_result_files[key].append(i[-1])
                        continue
            except Exception as e:
                print("Exception {} during stat calculation. Skipping column {}".format(e, key))
                continue

        csvs.plot_table(dict(values=list(post_stat_result_files.keys())), dict(values=list(post_stat_result_files.values())))
        
    def filter_histogram(self,  result_filtered_files=None, bin_num=None):
        """Create histogram for a given number of bins

        Args:
            result_filtered_files ([dict], optional): Dict of numpy arrays. Defaults to None.
            bin_num ([int], optional): Number of bins. Defaults to None.
        """
        if result_filtered_files is None:
            result_filtered_files = self.result_filtered_files

        for key, value in result_filtered_files.items():
            try:
                for i in value:
                    counts, bins = np.histogram(i, bin_num, range=(np.nanmin(i), np.nanmax(i)))
                    bins = 0.5 * (bins[:-1] + bins[1:])
                    print("Histogram for {} using {} number of bins".format(key, bin_num))
                    fig = px.bar(x=bins, y=counts, labels={'x':key, 'y':'Count'})
                    fig.show()
            except Exception as e:
                print("Exception {} during histogram. Skipping column {}".format(e, key))
                continue

    @staticmethod
    def plot_scatter(y_axis=None, y_name='Value', x_axis=None, x_name='Iter'):
        """Scatter plot

        Args:
            y_axis ([list], optional): Y-axis. Defaults to None.
            y_name (str, optional): Y-axis name. Defaults to 'Value'.
            x_axis ([list], optional): X-axis. Defaults to None.
            x_name (str, optional): X-axis name. Defaults to 'Iter'.
        """
        if y_axis is None:
            print("Provide y axis.")
            return
        if x_axis is None:
            x_axis = [i for i in range(len(y_axis))]

        fig = go.Figure()
        fig.add_trace(go.Scatter(
                x=x_axis,
                y=y_axis,
                mode='lines+markers'
        ))

        fig.update_layout(
            title=y_name + ' v ' + x_name,
            xaxis_title=x_name,
            yaxis_title=y_name
        )
        fig.show()

    @staticmethod
    def plot_table(header=None, values=None):
        """Table plot

        Args:
            header ([dict], optional): Result names to display. Defaults to None.
            values ([dict], optional): Values. Defaults to None.
        """
        if header is None:
            print("Provide header for tables.")
            return

        fig = go.Figure(
            data=[go.Table(
                header=header,
                cells=values
            )]
        )

        fig.show()
    

def give_me_tables(f, c, stat_type):
    f.file_filter()
    c.get_row_from_files(f.filtered_files)
    c.result_filter(file=f.filtered_files)
    c.filter_table(stat_type=stat_type)


def main():
    print("starting")
    f = files()
    c = csvs()
    f.sensor_module='v8_a0'
    f.filter_module = {'Version':'5.2'}
    
    #give_me_tables(f, c, 'mean')
    
    f.file_filter()
    #f.combine_files()
    
    c.filter_module = {'Power Level':{'expr':operator.le,'val':1}}
    c.get_row_from_files(f.filtered_files)
    c.combine_csv()

if __name__ == "__main__":
    main()