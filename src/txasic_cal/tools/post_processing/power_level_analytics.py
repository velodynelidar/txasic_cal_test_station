import argparse
import time
import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import plotly.graph_objects as go

from txasic_cal.calibration import config
from txasic_cal.tools import file_io

DATA_TABLE = [
            'Version', 'Power Level', 'Num_Units',
            'Boot_Mean', 'Boot_STD', 'Boot_Min', 'Boot_Max',
            'Bias_Mean', 'Bias_STD', 'Bias_Min', 'Bias_Max',
            'LP_Mean', 'LP_STD', 'LP_Min', 'LP_Max',
            'Optical Power in mW_Mean', 'Optical Power in mW_STD', 'Optical Power in mW_Min', 'Optical Power in mW_Max',
            'Pulse Width(ns)_Mean', 'Pulse Width(ns)_STD', 'Pulse Width(ns)_Min', 'Pulse Width(ns)_Max',
            'Pulse Amplitude(mV)_Mean', 'Pulse Amplitude(mV)_STD', 'Pulse Amplitude(mV)_Min', 'Pulse Amplitude(mV)_Max'
            ]

POWER_LEVEL_TABLE = 'power_level_table.csv'
NUM_PL = 16

class CalData:
    def __init__(self):
        self.version_holder = {}
        self.num_version = {}
        self.data_table = DATA_TABLE
    
    def create_version_table(self, version_number):
        self.version_holder[version_number] = {j:{i:0 for i in DATA_TABLE} for j in range(NUM_PL)}
        self.num_version[version_number] = 0
    
    def append_data_table(self, version_number, df, pl):
        for col in self.data_table:
            
            if col == 'Version':
                self.version_holder[version_number][pl][col] = version_number
            elif col == 'Power Level':
                self.version_holder[version_number][pl][col] = pl
            else:
                power_table_col, col_statistics = col.split("_")
                try:
                    cur_list = df[power_table_col].to_numpy()
                except KeyError:
                    print("Key {} not found.".format(power_table_col))
                    continue
                if col_statistics == 'Mean':
                    self.version_holder[version_number][pl][col] += round(cur_list.mean(), 3)
                elif col_statistics == 'STD':
                    self.version_holder[version_number][pl][col] += round(cur_list.std(), 3)
                elif col_statistics == 'Max':
                    self.version_holder[version_number][pl][col] += round(cur_list.max(), 3)
                elif col_statistics == 'Min':
                    self.version_holder[version_number][pl][col] += round(cur_list.min(), 3)
    
    def folder_iterate_mean_pl(self, folder):
        _file_io = file_io.file_io()
        # override file_io file-name
        _file_io.file_name = config.AVG_POWER_TABLE
        _file_io.file_open()
        _file_io.init(fieldnames=self.data_table)
        dirnames1 = os.listdir(folder)
        for unit_name in dirnames1:
            try:
                cur_dir = os.listdir(folder + "/" + unit_name)
            except Exception as e:
                print("Exception {}".format(e))
                continue
            if POWER_LEVEL_TABLE in cur_dir:
                df = pd.read_csv(folder + "/" + unit_name + "/" + POWER_LEVEL_TABLE, converters={"LP": lambda x: int(x, 16)})

                try:
                    cur_version = df["Version"][0]
                    print("Current unit {} version {}".format(unit_name, cur_version))
                except (KeyError, IndexError) as e:
                    if e == 'KeyError':
                        print("No version found in {}".format(unit_name))
                        cur_version = 'IS_0.0.0'
                    else:
                        continue
                
                if self.version_holder.get(cur_version) is None:
                    self.create_version_table(cur_version)
                
                self.num_version[cur_version] += 1
                for pl in range(NUM_PL):
                    print("Analytics for power level {}".format(pl))
                    try:
                        df2 = df[df['Power Level'] == pl]
                    except KeyError:
                        print("Power Level {} not found for {}".format(pl, unit_name))
                        return
                    
                    self.append_data_table(cur_version, df2, pl)

        for version in self.version_holder:
            for pl in range(NUM_PL):
                self.version_holder[version][pl]['Num_Units'] = self.num_version[version]                
                for i in self.data_table[3:]:
                    self.version_holder[version][pl][i] /= self.num_version[version]
                _file_io.write(self.version_holder[version][pl])
        
        _file_io.file_close()
        
    
    def folder_iterate_cluster_pl(self, folder, pl, col):
        print("Clustering for power level {}".format(pl))
        dirnames1 = os.listdir(folder)
        cur_pl_list = []
        
        if col == "Bias":
            x_list = [round(i/1e3, 3) for i in range(1000, 5000, 100)]

        for unit_name in dirnames1:
            try:
                cur_dir = os.listdir(folder + "/" + unit_name)
            except Exception as e:
                print("Exception {}".format(e))
                continue
            if POWER_LEVEL_TABLE in cur_dir:
                try:
                    df = pd.read_csv(folder + "/" + unit_name + "/" + POWER_LEVEL_TABLE, converters={"LP": lambda x: int(x, 16)})
                except Exception as e:
                    print("Exception: {}".format(e))
                    continue
            
#                 print("Currnt unit {}.".format(unit_name))
                
                for ch in range(8):
                    try:
                        df2 = df[(df['Power Level'] == pl) & (df['Channel'] == ch)]
                    except KeyError:
                        print("Power Level {} not found for {}".format(pl, unit_name))
                        continue

                    try:
                        if len(df2[col].values) == 1:
                            cur_pl_list.append(np.around(df2[col].values, 3)[0])
                    except KeyError:
                        print("Col {} not found for {}".format(col, unit_name))
                        continue
            
#         print(cur_pl_list)
        sorted(cur_pl_list)
        y_list = [None for i in x_list]
#         for key, value in enumerate(x_list):
#             if value in cur_pl_list:
#                 if y_list[key] == None:
#                     y_list[key] = 0
#                 y_list[key] += 1
        for i in cur_pl_list:
            for j in range(0, len(x_list) - 1):
                if x_list[j] < i < x_list[j + 1]:
                    if y_list[j] == None:
                        y_list[j] = 0
                    y_list[j] += 1
        
#         plt.scatter(x_list, y_list)
#         plt.show()
        
        fig = go.Figure()
        fig.add_trace(go.Scatter(
                x=x_list,
                y=y_list,
                mode='markers',
            ))
        fig.update_layout(
            title="Number of chips vs {} at Power Level {}".format(col, pl),
            xaxis_title=col,
            yaxis_title="Number",
        )
        fig.show()

def main(args):
    folder = r"/cal/velarray/v8_a0/"
    if args.folder:
        folder = args.folder
    
    cal_data = CalData()
#     cal_data.folder_iterate_mean_pl(folder)
    cal_data.folder_iterate_cluster_pl(folder, 10, "Bias")

if __name__ == "__main__":
    start_time = time.time()
    parser = argparse.ArgumentParser(description='Power Level Analytics')
    parser.add_argument('--folder', help='Folder name')
    args = parser.parse_args()
    main(args)
    end_time = time.time()

    print("\n\nCompleted in {} sec or {} min".format((end_time - start_time), (end_time - start_time)/60.0))
