#!/bin/python

# ===========================================================================
# Project: Pulse Processing Algorithms
# Module: pulse_processing.py
# Description: This file contains the algorithms for processing pulse returns
# History: Who,Date,Comment
#          Vijay.M, 10/10/2019, Initial
#
# Copyright (c) 2010-2020 Velodyne Lidar, Inc.
# ==========================================================================

import numpy as np
from matplotlib import pyplot as plt
import argparse
import pandas as pd
import ast
import scipy

from txasic_cal.tools.post_processing import peakdetect as pk

NUM_PULSE_POINTS = 1000
WAVEFORM_THRES_OFFSET = 10

DAZZLE_PULSE_POINTS = 100
DAZZLE_SAT_MAX = 253
TRUNCATE_DATA_BUF = 300
NUM_EXTRA_PULSE_POINTS = TRUNCATE_DATA_BUF + NUM_PULSE_POINTS
FACTOR_EXTRA_POINTS = NUM_EXTRA_PULSE_POINTS / TRUNCATE_DATA_BUF

NOISE_FLOOR_START = 1050
# NOISE_FLOOR_START = 250
NOISE_FLOOR_END = NOISE_FLOOR_START + 100

# ADC = 750MHz
# in ns
ADC_TIME_RESOLUTION = 1.33
ADC_TIME_RESOLUTION_INTERP = ADC_TIME_RESOLUTION / FACTOR_EXTRA_POINTS

NUM_PULSE_DATA = 6

class pulse:
    """
    Stores pulse buffer, data in an array
    """
    def __init__(self, num_pulses=1):
        self._points = np.empty(shape=(NUM_PULSE_POINTS,1))
        self._pulse = np.empty(shape=(NUM_PULSE_DATA*num_pulses, 2))

        self._pulse_amp = np.empty(shape=(num_pulses, 1))
        self._pulse_fwidth = np.empty(shape=(num_pulses, 1))
        self._pulse_hwidth = np.empty(shape=(num_pulses, 1))
        self._pulse_rise = np.empty(shape=(num_pulses, 1))
        self._pulse_satwidth = np.empty(shape=(num_pulses, 1))

        self._num_pulses = num_pulses

        self._max = 0
        self._min = 0

        self._buf = None


    def peak_detection(self):
        """
        Computes rms, max and min from data buffer
        """
        rms = np.sqrt(np.mean(np.power(self._points,2)))
        _max, _min =  pk.peakdetect(self._points, lookahead=5, delta=rms)

        return rms, _max, _min


    def cal_pulse_data(self):
        """
        Iterate through pulse data buffer to get interesting points
        Pulse base width, half width, sat width, amplitude and rise time are then computed
        """
        self.waveform_mean = np.mean(self._points[NOISE_FLOOR_START:NOISE_FLOOR_END]) + WAVEFORM_THRES_OFFSET
        _, self._max, self._min = self.peak_detection()

        if self._max == []:
            self._max = np.zeros(shape=(self._num_pulses, 2))

        cur_pulse = 0
        cur_pulse_peak = self._max[cur_pulse]
        self.waveform_mid = (cur_pulse_peak[1] + self.waveform_mean)/2 - WAVEFORM_THRES_OFFSET

        # Get related pulse points
        state = 0
        pulse_point = 0
        for index, val in enumerate(self._points):
            # Pulse base start
            if state is 0:
                if val > self.waveform_mean:
                    state = 1
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
            # Pulse mid start
            elif state is 1:
                if val >= self.waveform_mid:
                    state = 2
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
                if val < self.waveform_mean:
                    state = 0
                    pulse_point -= 1
            # Pulse peak start
            elif state is 2:
                if val == self._max[cur_pulse][1] or int(val) >= DAZZLE_SAT_MAX:
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
                    state = 3
            # Pulse peak end
            elif state is 3:
                if val < self._max[cur_pulse][1] and int(val) < DAZZLE_SAT_MAX:
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
                    state = 4
            # Pulse mid end
            elif state is 4:
                if val <= self.waveform_mid and index > self._max[cur_pulse][0]:
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
                    state = 5
            # Pulse base end
            elif state is 5:
                if val < self.waveform_mean:
                    state = 0
                    self._pulse[pulse_point][0] = index
                    self._pulse[pulse_point][1] = val
                    pulse_point += 1
                    cur_pulse += 1

            if pulse_point == NUM_PULSE_DATA*self._num_pulses:
                break

        # Convert pulse points to data
        cur_pulse = 0
        pulse_fact = NUM_PULSE_DATA
        while cur_pulse < self._num_pulses:
            self._pulse_fwidth[cur_pulse] = (self._pulse[cur_pulse*pulse_fact + 5][0] - self._pulse[cur_pulse*pulse_fact][0]) * ADC_TIME_RESOLUTION_INTERP
            self._pulse_hwidth[cur_pulse] = (self._pulse[cur_pulse*pulse_fact + 4][0] - self._pulse[cur_pulse*pulse_fact + 1][0]) * ADC_TIME_RESOLUTION_INTERP
            self._pulse_amp[cur_pulse] = self._pulse[cur_pulse*pulse_fact + 2][1]
            self._pulse_satwidth[cur_pulse] = (self._pulse[cur_pulse*pulse_fact + 3][0] - self._pulse[cur_pulse*pulse_fact + 2][0]) * ADC_TIME_RESOLUTION_INTERP
            self._pulse_rise[cur_pulse] = (self._pulse[cur_pulse*pulse_fact + 2][0] - self._pulse[cur_pulse*pulse_fact][0]) * ADC_TIME_RESOLUTION_INTERP
            cur_pulse += 1


def plot(ob):
    """
    Plot pulse object with related points such as pulse mid and base
    """
    x_axis = np.linspace(0, len(ob._points), len(ob._points), dtype=int)
    buf_axis = np.linspace(0, len(ob._buf), len(ob._buf), dtype=int)
    
    m = 1
    xm = [p[0] * ADC_TIME_RESOLUTION_INTERP for p in ob._max]
    ym = [p[1] * m for p in ob._max]

    plt.scatter(x_axis * ADC_TIME_RESOLUTION_INTERP, np.multiply(ob._points, m))
    plt.plot(xm, ym, 'r+')
    plt.axhline(ob.waveform_mean, color='r')
    plt.axhline(ob.waveform_mid, color='g')

    plt.show()


def only_plot(x_axis, ob):
    """
    Only plot pulse
    """
    plt.scatter(x_axis, ob._points)
    plt.show()


def get_pulse_info(data_buf=[], p=None):
    """
    Converts pulse data buffer with interpolation and get pulse data information
    Args:
        data_buf = pulse data buffer as a list
        p = plot and print
    Return:
        pulse_ob = pulse object
    """
    # Create x-axis for pulse
    buf = data_buf[:TRUNCATE_DATA_BUF]
    buf_len = len(buf)
    x_axis = np.linspace(0, buf_len, buf_len, dtype=int)
    
    int_x_axis = np.linspace(0, buf_len, NUM_EXTRA_PULSE_POINTS)

    pulse_ob = pulse(1)

    pulse_ob._points = np.around(scipy.interpolate.interp1d(x_axis, buf, kind='quadratic')(int_x_axis), 2)
    pulse_ob._buf = buf

    pulse_ob.cal_pulse_data()

    if p:
        print("Amplitude=", pulse_ob._pulse_amp)
        print("Base Width=", pulse_ob._pulse_fwidth)
        print("Half Width=", pulse_ob._pulse_hwidth)
        print("Sat Width=", pulse_ob._pulse_satwidth)
        print("Rise Time=", pulse_ob._pulse_rise)

        plot(pulse_ob)

    return pulse_ob


if __name__ == '__main__':
    # Test
    temp_max = [39, 40, 38, 38, 38, 39, 38, 41, 44, 41, 36, 33, 32, 36, 43, 44, 42, 38, 37, 35, 120, 232, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 244, 211, 184, 159, 136, 118, 104, 95, 84, 77, 72, 69, 65, 62, 60, 57, 56, 55, 53, 53, 50, 52, 49, 50, 51, 48, 49, 50, 47, 49, 49, 47, 48, 49, 47, 49, 47, 47, 48, 48, 46, 49, 45, 48, 46, 48, 47, 46, 47, 48, 45, 47, 47, 46, 47, 47, 46, 47, 47, 46, 47, 47, 46, 47, 47, 46, 46, 46, 47, 47, 46, 47, 47, 47, 47, 47, 47, 47, 47, 46, 46, 46, 46, 46, 46, 46, 47, 47, 47, 47, 47, 47, 46, 47, 46, 47, 46, 47, 46, 46, 46, 47, 46, 46, 47, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46, 46, 47, 46, 45, 46, 45, 46, 46, 46, 45, 46, 46, 45, 46, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45]
    temp_mid = [39, 40, 39, 40, 39, 40, 38, 42, 45, 41, 37, 34, 32, 37, 43, 45, 43, 39, 37, 35, 36, 48, 187, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 206, 161, 129, 105, 89, 79, 69, 62, 54, 52, 50, 48, 47, 46, 44, 43, 42, 43, 42, 42, 42, 41, 41, 41, 41, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 41, 41, 41, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 40, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 39, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 41, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40]
    temp_min = [40, 39, 39, 40, 39, 39, 38, 41, 45, 41, 37, 34, 33, 37, 44, 45, 42, 39, 37, 35, 36, 44, 156, 252, 255, 255, 225, 132, 76, 59, 55, 52, 50, 46, 43, 41, 41, 41, 42, 42, 42, 40, 40, 40, 39, 40, 40, 40, 40, 40, 40, 40, 39, 40, 40, 39, 40, 40, 39, 39, 40, 40, 40, 40, 39, 39, 39, 39, 40, 40, 40, 40, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 40, 39, 39, 39, 39, 38, 39, 40, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 40, 39, 39, 39, 39, 38, 40, 38, 39, 39, 40, 40, 40, 39, 40, 39, 38, 40, 39, 39, 40, 40, 39, 40, 39, 38, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 40, 39, 39, 39, 39, 39, 39, 39, 40, 39, 39, 39, 40, 39, 39, 40, 38, 40, 38, 40, 37, 40, 39, 39, 40, 40, 38, 40, 40, 38, 40, 40, 39, 40, 39, 39]
    temp_min2 = [39, 40, 39, 39, 40, 40, 38, 41, 46, 42, 38, 34, 33, 37, 43, 44, 43, 38, 37, 35, 36, 46, 175, 255, 255, 255, 247, 153, 88, 63, 57, 55, 53, 49, 45, 41, 39, 42, 41, 43, 42, 40, 39, 39, 39, 39, 40, 40, 39, 40, 39, 38, 39, 39, 40, 40, 39, 40, 39, 38, 41, 38, 39, 40, 39, 39, 39, 40, 39, 40, 41, 39, 39, 39, 39, 39, 40, 39, 39, 40, 40, 39, 40, 39, 40, 40, 40, 39, 39, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 39, 40, 40, 40, 39, 39, 39, 39, 40, 40, 39, 39, 39, 40, 39, 39, 39, 39, 39, 40, 40, 39, 39, 39, 39, 40, 39, 39, 39, 39, 39, 39, 39, 39, 40, 39, 39, 38, 39, 39, 39, 39, 40, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 39, 38, 38, 39, 39, 39, 39, 40]
    get_pulse_info(temp_mid, 1)
