#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : Logging module

import logging
import inspect
import colorlog
import pathlib
from . import TXCAL_DATA_DIR
import time

class logging_tools:
    def __init__(self, level):
        """Logging class that over-rides default logger to provide more functionality,
        Signals used to hand log data to GUI

        Args:
            level (logging.level): Set logger leve
        """

        log_format = (
            '%(message)s - %(levelname)s'
        )

        self.level = level
        bold_seq = '\033[1m'
        colorlog_format = (
            f'{bold_seq} '
            '%(log_color)s '
            f'{log_format}'
        )
        colorlog.basicConfig(format=colorlog_format)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level)
        self._log_dir = TXCAL_DATA_DIR / pathlib.Path("log")

        # GUI related
        self.msg_sig = None
        self.input_sig = None
        self.initial_stop = True
        self.stop = False
        self.mutex_ob = None

    def log_file_setup(self, filename='log_file.log'):
        """Set up a log file handler

        Args:
            filename (str, optional): File name of log file. Defaults to 'log_file.log'.
        """
        self._log_dir.mkdir(exist_ok=True, parents=True)
        log_filename = self._log_dir / filename
        handler = logging.FileHandler(log_filename)
        self.logger.addHandler(handler)
        self.info("Logging to file {}".format(log_filename))

    def info(self, *args):
        """Use similar to logging object info
        """
        frame = inspect.currentframe().f_back
        func = frame.f_code
        info_str = f"{func.co_name} - {frame.f_lineno} - {args[0]}"
        self.logger.info(info_str)
        
        if self.msg_sig:
            self.msg_sig.emit('log_info', info_str)

    def debug(self, *args):
        """Use similar to logging object debug
        """
        frame = inspect.currentframe().f_back
        func = frame.f_code

        if 'velaplatform' in func.co_filename:
            return

        debug_str = f"{func.co_name} - {frame.f_lineno} - {args[0]}"
        self.logger.debug(debug_str)

    def warn(self, *args):
        """Use similar to logging object warn
        """
        frame = inspect.currentframe().f_back
        func = frame.f_code
        warn_str = f"{func.co_name} - {frame.f_lineno} - {args[0]}"
        self.logger.warning(warn_str)

        if self.msg_sig:
            self.msg_sig.emit('log_info', warn_str)
            self.msg_sig.emit('pfmea_warn', warn_str)

    def warning(self, *args):
        """Use similar to logging object warning
        """
        frame = inspect.currentframe().f_back
        func = frame.f_code
        warning_str = f"{func.co_name} - {frame.f_lineno} - {args[0]}"
        self.logger.warning(warning_str)

        if self.msg_sig:
            self.msg_sig.emit('log_info', warning_str)
            self.msg_sig.emit('pfmea_warn', warning_str)

    def error(self, *args):
        """Use similar to logging object error
        """
        frame = inspect.currentframe().f_back
        func = frame.f_code
        error_str = f"{func.co_name} - {frame.f_lineno} - {args[0]}"
        self.logger.error(error_str)

        if self.msg_sig:
            self.msg_sig.emit('log_info', error_str)
            self.msg_sig.emit('pfmea_error', error_str)

    def input(self, *args):
        """Used to handle input conditions to GUI or uses built-in input
        """
        if self.input_sig:
            self.input_sig.emit(args[0])
            
            while self.initial_stop:
                time.sleep(0.1)
                self.mutex_ob.lock()
                cur_stop = self.stop
                self.mutex_ob.unlock()
                if cur_stop:
                    self.stop = False
                    break
        else:
            return input(args[0])

    def on_update_stop(self, stop):
        """Update pause condition once Enter button is pressed
        """
        self.mutex_ob.lock()
        self.stop = stop
        self.mutex_ob.unlock()

# Create top level logger
logger = logging_tools(level=logging.DEBUG)

