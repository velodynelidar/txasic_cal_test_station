#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-1
#  Description   : File create/read/write software

import sys
import csv
import pathlib
from txasic_cal.tools.logger import logger
from txasic_cal.pfmea_code import pfmea_table

from . import TXCAL_DATA_DIR

class file_io:
    """File class to create and write to a results file
    """
    def __init__(self, file_name='temp.csv'):
        self.dir = TXCAL_DATA_DIR
        self.results_dir = self.dir / pathlib.Path("results/")
        self.file_name = self.results_dir / pathlib.Path(file_name)
        self.file = None
        self.csv_writer = None
    
    def __del__(self):
        self.file_close()

    def file_open(self):
        if(self.file):
            logger.error("File already opened.")
            return

        self.results_dir.mkdir(exist_ok=True, parents=True)
        self.file = open(self.file_name, 'w', newline='')

        if (not self.file):
            logger.error("File open failed.")
            pfmea_table.pfmea_filter.error('SETUP_CAL_STORAGE')
            logger.error("Exiting...")
            sys.exit(-1)

    def file_close(self):
        if(self.file):
            self.file.close()
        self.file = None

    def init(self, fieldnames: list):
        """Initialize results file with headers

        Args:
            fieldnames (list): List of str of csv headers
        """
        self.csv_writer = csv.DictWriter(self.file, fieldnames=fieldnames)
        try:
            self.csv_writer.writeheader()
        except Exception as e:
            logger.error(f"CSV file write failed with exception {e}")
            pfmea_table.pfmea_filter.error('SETUP_CAL_STORAGE')
            logger.error("Exiting...")
            sys.exit(-1)

    def write(self, results_row):
        """Write a single row

        Args:
            results_row (Dict): Single row with header and values
        """
        try:
            self.csv_writer.writerow(results_row)
        except Exception as e:
            logger.error(f"CSV file write failed with exception {e}")
            pfmea_table.pfmea_filter.error('SETUP_CAL_STORAGE')
            logger.error("Exiting...")
            sys.exit(-1)

