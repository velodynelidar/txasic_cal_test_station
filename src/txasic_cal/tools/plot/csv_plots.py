#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-13
#  Description   : Plot TxCal

import argparse
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import ast

# Unit Headers
CHANNEL = 'Channel'
POWER_LEVEL = 'Power Level'
POWER_TABLE = 'Average_Power_Table'
BOOT = 'Boot'
BIAS = 'Bias'
LP = 'LP'
TEMPERATURE = 'Temperature'

# Measurement Headers
OPTICAL_POWER = 'Optical Power in mW'
PULSE_WIDHT = 'Pulse Width(ns)'
PULSE_WIDTH_STD = 'Pulse Width STD(ns)'
PULSE_AMPLITUDE = 'Pulse Amplitude(mV)'
PULSE_AMPLITUDE_STD = 'Pulse Amplitude STD(mV)'
OVEN_SET_TEMPERATURE = 'Oven Set Temperature(C)'

# Algorithm Headers
TOTAL_ITER = 'Total_Iter'
INPUT_LIST = 'Input_List'
MEAS_LIST = 'Meas_List'
MEAS_TIME = 'Meas_Time'

class txcal_plot:
    def __init__(self, x_name, y_name):
        """Plot txcal results

        Arguments:
            x_name {str} : String name for x-axis column
            y_name {str} : String name for y-axis column
        """
        self._ch = CHANNEL
        self._pl = POWER_LEVEL
        self._pt = POWER_TABLE
        self._op = OPTICAL_POWER
        self._ot = OVEN_SET_TEMPERATURE

        self.x = x_name
        self.y = y_name

        if y_name == self._op:
            self.plot = 'semilogy'
        else:
            self.plot = 'plot'

        self.figures = {}
        self.figure_num = 1
    
    def update_xy(self, x_name=None, y_name=None):
        """Update x, y axis name

        Keyword Arguments:
            x_name {str} : String name for x-axis column
            y_name {str} : String name for y-axis column
        """
        if x_name:
            self.x = x_name
        if y_name:
            self.y = y_name

        if self.y != OPTICAL_POWER:
            self.plot = "plot"
        else:
            self.plot = 'semilogy'

    def plot_fig(self, x_values, y_values, fig_name, label=None, marker=None, title=None):
        """Plot figure from figures dict

        Arguments:
            x_values [list] : X axis list
            y_values [list] : Y axis list
            fig_name [str] : Name of figure

        Keyword Arguments:
            label [str] : Plot figure label
            marker [str] : Plot marker
            title [str] : Plot title
        """
        if fig_name not in self.figures.keys():
            self.figures[fig_name] = plt.figure().add_subplot(1, 1, 1)
            self.figure_num += 1
        
        cur_fig = self.figures[fig_name]

        getattr(cur_fig, self.plot)(x_values, y_values, marker=marker, label=label)
        cur_fig.set_xlabel(self.x)
        cur_fig.set_ylabel(self.y)
        cur_fig.legend(loc="upper left")
        cur_fig.grid(True)
        cur_fig.set_title(title)
        
    def plot_ch(self, df, ch):
        """Plot optical power vs power level for given channel
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch is None:
            print("Provide channel.")
            return

        df2 = df[(df[self._ch] == ch)]

        # Sort based on power levels
        df3 = df2.sort_values(self._pl)

        getattr(plt, self.plot)(df3[self.x], df3[self.y], 'o-')
        if self.y == self._op:
            getattr(plt, self.plot)(df3[self.x], df3[self._pt], 'o-')
        plt.xlabel(self.x)
        plt.ylabel(self.y)
        plt.grid()
        plt.legend(loc="upper left")

    def plot_ch_temp(self, df, ch, temp=0):
        """Plot optical power vs power level for given channel at a given temperature (Oven Set Temperature(C))
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
            temp {int} : Temperature
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return
        if temp == None:
            temp=0

        df2 = df[(df[self._ot] == temp)]
        self.plot_ch(df2, ch)

    def plot_ch_pl(self, df, ch, pl=None):
        """Plot optical power vs oven set temperature for given channel overlaid with power levels
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
            temp {int} : Temperature
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return

        df2 = df[(df[self._ch] == ch) & (df[self._pl] == pl if pl else 1)]
        df3 = df2.sort_values(self._ot)

        self.update_xy(x_name='Oven Set Temperature(C)')

        getattr(plt, self.plot)(df3[self.x], df3[self.y], 'o-')
        if self.y == self._op:
            getattr(plt, self.plot)(df3[self.x], df3[self._pt], 'o-')
        plt.xlabel(self.x)
        plt.ylabel(self.y)
        plt.grid()
        plt.legend(loc="upper left")

        self.update_xy(x_name='Power Level')
        
    def plot_each_ch_v8(self, df):
        """Plot optical power vs power level for each channel separately for V8
        
        Arguments:
            df {object} : Pandas dataframe object
        """
        if df is None:
            print("Provide pandas dataframe.")
            return

        df2 = df
        num_plot_r, num_plot_c = 2, 4
        f, axarr = plt.subplots(num_plot_r, num_plot_c)
        num_ch = 8
        cur_ch = 0
        for i in range(num_plot_r):
            for j in range(num_plot_c):
                df3 = df2[df2[self._ch] == cur_ch]
                df4 = df3.sort_values(self._pl)
                getattr(axarr[i, j], self.plot)(df4[self.x], df4[self.y], 'o-')
                if self.y == self._op:
                    getattr(axarr[i, j], self.plot)(df4[self.x], df4[self._pt], 'o-')
                axarr[i, j].set_title('Channel {}'.format(cur_ch))
                axarr[i, j].grid()
                cur_ch += 1

    def plot_each_ch_temp_v8(self, df, temp=0):
        """Plot optical power vs power level for each channel separately for V8
        
        Arguments:
            df {object} : Pandas dataframe object
            temp {int} : Temperature value
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if temp == None:
            temp=0

        df2 = df[(df[self._ot] == temp)]
        self.plot_each_ch_v8(df2)

    def plot_all_ch(self, df):
        """Plot optical power vs power level for all channels
        
        Arguments:
            df {object} : Pandas dataframe object
        """
        df2 = df
        df3 = df2.sort_values(self._pl)
        getattr(plt, self.plot)(df3[self.x], df3[self.y], 'o-')
        if self.y == self._op:
            getattr(plt, self.plot)(df3[self.x], df3[self._pt], 'o-')
        plt.xlabel(self.x)
        plt.ylabel(self.y)
        plt.grid()
        plt.legend(loc="upper left")

    def plot_all_ch_temp_v8(self, df, temp=0):
        """Plot optical power vs power level for each channel separately for V8
        
        Arguments:
            df {object} : Pandas dataframe object
            temp {int} : Temperature value
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if temp == None:
            temp=0

        df2 = df[(df[self._ot] == temp)]
        self.plot_all_ch(df2)

    def plot_algo_metrics(self, df, ch, pl, temp=None):
        """Plot algorithm metrics

        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
            pl {int} : Power level
        
        Keyword Arguments:
            temp {int} : Oven set temperature
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return
        if pl == None:
            print("Provide power level.")
            return
        if temp == None:
            cur_temp = 0
        else:
            cur_temp = temp
        
        df2 = df[(df[self._ch] == ch) & (df[self._pl] == pl) & (df[self._ot] == cur_temp)]
        
        total_iter = int(df2[TOTAL_ITER])
        meas_time = round(float(df2[MEAS_TIME]), 3)

        current_op = round(float(df2[self._pt]), 3)

        print("Channel: {}".format(ch))
        print("Power Level: {}".format(pl))
        if temp is not None:
            print("Oven Set Temperature(C): {}".format(temp))
        print("Total Iterations: {}".format(total_iter))
        print("Total Time: {} sec".format(meas_time))

        try:
            input_list = list(df2[INPUT_LIST].apply(ast.literal_eval))[0]
            meas_list = list(df2[MEAS_LIST].apply(ast.literal_eval))[0]
        except ValueError:
            print("Algo metrics empty.")
            return
        
        plt.figure()
        plt.plot(input_list, 'o-')
        plt.grid()
        plt.xlabel("Num Iter")
        plt.ylabel("Input")
        plt.title("Input vs Num Iter")

        plt.figure()
        plt.plot(meas_list, 'o-')
        plt.axhline(current_op, color='r')
        plt.grid()
        plt.xlabel("Num Iter")
        plt.ylabel("Measurement")
        plt.title("Measurement vs Num Iter")


def main(args):
    file_name = args.file_name

    txcal = txcal_plot(x_name='Power Level', y_name='Optical Power in mW')

    df = pd.read_csv(file_name)
    # txcal.plot_algo_metrics(df, 6, 15, 20)
    txcal.plot_ch_pl(df, 6)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='TxCal Plots')
    parser.add_argument('-f', '--file_name', help='TxCal results csv')
    args = parser.parse_args()
    main(args)

