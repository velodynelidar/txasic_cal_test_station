#!/bin/python3

#
# Copyright (c) 2021 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-13
#  Description   : Plot TxCal

import argparse
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import ast

import cufflinks as cf
import plotly.graph_objects as go
from plotly.subplots import make_subplots

NUM_CH = 8
NUM_PL = 16

CAL_TEMP = 30

# Unit Headers
CHANNEL = 'Channel'
POWER_LEVEL = 'Power Level'
POWER_TABLE = 'Average_Power_Table'
BOOT = 'Boot'
BIAS = 'Bias'
LP = 'LP'
TEMPERATURE = 'Temperature'

# Measurement Headers
OPTICAL_POWER = 'Optical Power in mW'
PULSE_WIDHT = 'Pulse Width(ns)'
PULSE_WIDTH_STD = 'Pulse Width STD(ns)'
PULSE_AMPLITUDE = 'Pulse Amplitude(mV)'
PULSE_AMPLITUDE_STD = 'Pulse Amplitude STD(mV)'
OVEN_SET_TEMPERATURE = 'Oven Set Temperature(C)'
PEAK_POWER = 'Peak Power(W)'

PL0_Width = 'PL0_Width'
PL0_Target_Distance = 'PL0_Target_Intensity'
PL0_Target_Distance = 'PL0_Target_Distance'

# Algorithm Headers
TOTAL_ITER = 'Total_Iter'
INPUT_LIST = 'Input_List'
MEAS_LIST = 'Meas_List'
MEAS_TIME = 'Meas_Time'

class txcal_plot:
    def __init__(self, x_name, y_name, num_ch, version):
        """Plot txcal results

        Arguments:
            x_name {str} : String name for x-axis column
            y_name {str} : String name for y-axis column
        """
        self._ch = CHANNEL
        self._pl = POWER_LEVEL
        self._pt = POWER_TABLE
        self._op = OPTICAL_POWER
        self._ot = OVEN_SET_TEMPERATURE

        self.x = x_name
        self.y = y_name
        
        self.unit_name = None
        self.prf = None
        
        self.num_ch = num_ch
        self.version = version

        if y_name == self._op:
            self.plot = 'log'
        else:
            self.plot = 'plot'
    
    def update_xy(self, x_name=None, y_name=None):
        """Update x, y axis name

        Keyword Arguments:
            x_name {str} : String name for x-axis column
            y_name {str} : String name for y-axis column
        """
        if x_name:
            self.x = x_name
        if y_name:
            self.y = y_name

        if self.y == OPTICAL_POWER:
            self.plot = "log"
        else:
            self.plot = "plot"

    def plot_ch(self, df, ch):
        """Plot optical power vs power level for given channel
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch is None:
            print("Provide channel.")
            return

        df2 = df[(df[self._ch] == ch)]

        # Sort based on power levels
        df3 = df2.sort_values(self._pl)

        fig = df3.iplot(asFigure=True,
                        x=self.x,
                        y=self.y,
                        title=self.y + " v " + self.x,
                        xTitle=self.x,
                        yTitle=self.y,
                        mode='lines+markers',
                        yaxis_type='log' if self.plot == 'log' else None,
                        legend=True)
        fig.show()

    def plot_ch_temp(self, df, ch, temp=0):
        """Plot optical power vs power level for given channel at a given temperature (Oven Set Temperature(C))
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
            temp {int} : Temperature
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return
        if temp == None:
            temp=CAL_TEMP
        
        try:
            if 'INT' in self.version:
                df2 = df[(df[self._ot] == temp)]
            else:
                df2 = df
        except KeyError:
            print("{} not in csv".format(self._ot))
            df2 = df
        self.plot_ch(df2, ch)

    def plot_ch_pl(self, df, ch, pl=None):
        """Plot optical power vs oven set temperature for given channel overlaid with power levels
        
        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
        
        Keyword Arguments:
            pl {int} : Optional argument, if PL is provided only specified PL is plotted
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return

        df2 = df[(df[self._ch] == ch) & (df[self._pl] == pl if pl is not None else 1)]
        df3 = df2.sort_values(self._ot)

        self.update_xy(x_name='Oven Set Temperature(C)')

        fig = go.Figure()
        for i in range(NUM_PL):
            df4 = df3[df3[self._pl] == i]
            fig.add_trace(go.Scatter(
                x=df4[self.x],
                y=df4[self.y],
                mode='lines+markers',
                name=self._pl + " " + str(i)
            ))

        fig.update_layout(
            title=self.y + " v " + self.x + " for ch " + str(ch) + ", unit {} at PRF {}".format(self.unit_name, self.prf),
            xaxis_title=self.x,
            yaxis_title=self.y,
            yaxis_type='log' if self.plot == 'log' else None
        )
        fig.show()

        self.update_xy(x_name='Power Level')
        
    def plot_ch_pl_all(self, df, pl=None):
        """Plot optical power vs oven set temperature for all channels overlaid with power levels
        
        Arguments:
            df {object} : Pandas dataframe object
        
        Keyword Arguments:
            pl {int} : Optional argument, if PL is provided only specified PL is plotted
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        
        for ch in range(self.num_ch):
            self.plot_ch_pl(df, ch, pl)
        
    def plot_each_ch(self, df):
        """Plot optical power vs power level for each channel separately
        
        Arguments:
            df {object} : Pandas dataframe object
        """
        if df is None:
            print("Provide pandas dataframe.")
            return

        df2 = df
        num_plot_r, num_plot_c = self.num_ch // 4, self.num_ch // 2
        cur_ch = 0

        fig = make_subplots(
            rows=num_plot_r,
            cols=num_plot_c,
            subplot_titles=[self._ch + " " + str(i) for i in range(NUM_CH)])

        for i in range(num_plot_r):
            for j in range(num_plot_c):
                df3 = df2[df2[self._ch] == cur_ch]
                df4 = df3.sort_values(self._pl)
                fig.append_trace(go.Scatter(
                    x=df4[self.x],
                    y=df4[self.y],
                    mode='lines+markers',
                    name=self._ch + " " + str(i * num_plot_c + j)
                ), row=i + 1, col=j + 1)
                cur_ch += 1
        
                fig.update_xaxes(
                    title_text=self.x,
                    row=i + 1,
                    col=j + 1
                )
                fig.update_yaxes(
                    title_text=self.y,
                    type='log' if self.plot == 'log' else None,
                    row=i + 1,
                    col=j + 1
                )
        
        fig.update_layout(
            height=1000,
            width=1000,
            title=self.y + " v " + self.x + ' per channel'
        )
        fig.show()

    def plot_each_ch_temp(self, df, temp=0):
        """Plot optical power vs power level for each channel separately
        
        Arguments:
            df {object} : Pandas dataframe object
            temp {int} : Temperature value
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if temp == None:
            temp=CAL_TEMP

        try:
            if 'INT' in self.version:
                df2 = df[(df[self._ot] == temp)]
            else:
                df2 = df
        except KeyError:
            print("{} not in csv".format(self._ot))
            df2 = df
        self.plot_each_ch(df2)

    def plot_all_ch(self, df):
        """Plot optical power vs power level for all channels
        
        Arguments:
            df {object} : Pandas dataframe object
        """
        df2 = df
        df3 = df2.sort_values(self._pl)
        
        fig = go.Figure()
        for i in range(self.num_ch):
            df4 = df3[df3[self._ch] == i]
            fig.add_trace(go.Scatter(
                x=df4[self.x],
                y=df4[self.y],
                mode='lines+markers',
                name=self._ch + " " + str(i)
            ))

        if self.y == self._op:
            try:
                fig.add_trace(go.Scatter(
                    x=df4[self.x],
                    y=df4[self._pt],
                    mode='lines+markers',
                    name='Power Table'
                ))
            except KeyError:
                print("{} not in csv".format(self._ot))
                pass
        
        fig.update_layout(
            title=self.y + " v " + self.x + " for unit {} at PRF {}".format(self.unit_name, self.prf),
            xaxis_title=self.x,
            yaxis_title=self.y,
            yaxis_type='log' if self.plot == 'log' else None
        )
        fig.show()

    def plot_all_ch_temp(self, df, temp=0):
        """Plot optical power vs power level for each channel separately
        
        Arguments:
            df {object} : Pandas dataframe object
            temp {int} : Temperature value
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if temp == None:
            temp=CAL_TEMP

        try:
            if 'INT' in self.version:
                df2 = df[(df[self._ot] == temp)]
            else:
                df2 = df
        except KeyError:
            print("{} not in csv".format(self._ot))
            df2 = df
        self.plot_all_ch(df2)

    def plot_algo_metrics(self, df, ch, pl, temp=None):
        """Plot algorithm metrics

        Arguments:
            df {object} : Pandas dataframe object
            ch {int} : Channel number
            pl {int} : Power level
        
        Keyword Arguments:
            temp {int} : Oven set temperature
        """
        if df is None:
            print("Provide pandas dataframe.")
            return
        if ch == None:
            print("Provide channel.")
            return
        if pl == None:
            print("Provide power level.")
            return
        if temp == None:
            cur_temp=30
        else:
            cur_temp = temp
        
        if 'INT' in self.version:
            df2 = df[(df[self._ch] == ch) & (df[self._pl] == pl) & (df[self._ot] == cur_temp)]
        else:
            df2 = df[(df[self._ch] == ch) & (df[self._pl] == pl)]
        
        total_iter = int(df2[TOTAL_ITER])
        meas_time = round(float(df2[MEAS_TIME]), 3)

        if pl == 16:
            current_meas_end = 3.5
        else:
            current_meas_end = round(float(df2[self._pt]), 3)

        print("Channel: {}".format(ch))
        print("Power Level: {}".format(pl))
        if temp is not None:
            print("Oven Set Temperature(C): {}".format(temp))
        print("Total Iterations: {}".format(total_iter))
        print("Total Time: {} sec".format(meas_time))

        try:
            input_list = list(df2[INPUT_LIST].apply(ast.literal_eval))[0]
            meas_list = list(df2[MEAS_LIST].apply(ast.literal_eval))[0]
        except ValueError:
            print("Algo metrics empty.")
            return
        
        x_list = [i for i in range(len(input_list))]

        fig = go.Figure()
        fig.add_trace(go.Scatter(
            x=x_list,
            y=input_list,
            mode='lines+markers',
        ))

        fig.update_layout(
            title="Input v Num Iterations",
            yaxis_title="Input",
            xaxis_title="Num Iter",
        )

        fig.show()

        fig = go.Figure()
        fig.add_trace(go.Scatter(
            x=x_list,
            y=meas_list,
            mode='lines+markers',
        ))

        fig.add_shape(
            # Line Vertical
            type='line',
            x0=0,
            y0=current_meas_end,
            x1=len(input_list) - 1,
            y1=current_meas_end,
            line=dict(
                color='Red',
                width=2
            )
        )

        fig.add_shape(
            type='circle',
            x0=len(input_list) - 0.1 - 1,
            y0=current_meas_end - (current_meas_end * 0.05),
            x1=len(input_list) + 0.1 - 1,
            y1=current_meas_end + (current_meas_end * 0.05),
            line=dict(
                color='Red',
                width=3
            )
        )

        fig.update_layout(
            title="Measurement v Num Iterations",
            yaxis_title="Measurement",
            xaxis_title="Num Iter",
        )

        fig.show()

