#
# Copyright (c) 2020 Velodyne LiDAR, Inc. ("Velodyne"). All rights reserved.
#
# NOTICE: All information contained herein is, and remains the sole property of Velodyne. The intellectual and technical concepts
# contained herein are proprietary to Velodyne and are protected by trade secret and/or copyright law. Dissemination of this information
# or reproduction of this material is strictly prohibited unless prior written permission is obtained from Velodyne. Access to the source
# code contained herein is prohibited to anyone except current Velodyne employees or consultants. Any access to the source code is subject
# to the obligations of confidentiality contained in any applicable employment or consulting agreements with Velodyne.
#

#
#  Author        : Vijay Muthukumaran <vmuthukumaran@velodyne.com>
#  Creation date : 2020-5-22
#  Description   : Script to automatically create and upload html files for cal folder

import argparse
import os
import subprocess
import shutil
from pathlib import Path
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert.preprocessors import CellExecutionError

import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

POWER_LEVEL_TABLE = "power_level_table.csv"
TXCAL_NOTEBOOK_LOC = Path.expanduser(Path("~/txasic_cal/src/txasic_cal/tools/plot/"))
TXCAL_NOTEBOOK = TXCAL_NOTEBOOK_LOC / "txcal_plot_notebook.ipynb"
TXCAL_NOTEBOOK_EXEC = TXCAL_NOTEBOOK_LOC / "txcal_plot_notebook.nbconvert.ipynb"
TXCAL_HTML = TXCAL_NOTEBOOK_LOC / "txcal_plot_notebook.nbconvert.html"

class Watcher:

    def __init__(self, direc):
        """Watcher class to observe changes in a given directory
        
        Arguments:
            direc [str] : Directory location to observe
        """
        self.observer = Observer()
        self._direc = direc

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self._direc, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except Exception as e:
            self.observer.stop()
            print("Exception {}".format(e))

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        """React to events on the observed directory
        
        Arguments:
            event [FileSystemEventHandler] : FileSystemEventHandler object
        """
        print(event.event_type, event.src_path)
        if event.is_directory:
            # Take action here when a folder is first created.
            pass

        if event.event_type == 'created':
            # Take action here when a file is first created.
            print("Received created event - {}.".format(event.src_path))
            cur_path = Path(event.src_path)
            _folder = cur_path.relative_to(*cur_path.parts[:2])
            
            if _folder.name == POWER_LEVEL_TABLE:
                create_txcal_html(_folder.parent)
                
        elif event.event_type == 'moved':
            # Take action here when a file is moved.
            print("Received moved event - {} to {}.".format(event.src_path, event.dest_path))
            cur_path = Path(event.dest_path)
            _folder = cur_path.relative_to(*cur_path.parts[:2])
            
            if _folder.name == POWER_LEVEL_TABLE:
                create_txcal_html(_folder.parent)

        elif event.event_type == 'modified':
            # Take action here when a file is modified.
            print("Received modified - {}".format(event.src_path))
            cur_path = Path(event.src_path)
            _folder = cur_path.relative_to(*cur_path.parts[:2])

            if _folder.name == POWER_LEVEL_TABLE:
                create_txcal_html(_folder.parent)


def create_txcal_html(file_name):
    """Create a txcal notebook, convert it to html and store at nbviewers loc

    Arguments:
        file_name {str} -- String name of unit with sensor directory (e.g., v8a0/p44)
    """
    FILE_NAME = str(file_name)
    os.environ["FILE_NAME"] = FILE_NAME
    print("Creating html for unit {}".format(os.environ["FILE_NAME"]))
    
    try:
        subprocess.run(["jupyter nbconvert --to notebook --execute {}".format(TXCAL_NOTEBOOK)], 
                       shell=True, check=True)
    except Exception as e:
        print("Exception caught during notebook execution {}".format(e))
        return

    try:
        subprocess.run(["jupyter nbconvert --no-input --to html {}".format(TXCAL_NOTEBOOK_EXEC)], 
                         shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    except Exception as e:
        print("Exception caught during html converion {}".format(e))
        return

    html_loc_dir = Path("/nbviewers/html/txcal/{}".format(Path(FILE_NAME).parent))
    print(html_loc_dir)
    html_loc = html_loc_dir / Path(FILE_NAME).stem
    
    try:
        html_loc_dir.mkdir(exist_ok=True, parents=True)
    except FileExistsError:
        return

    try:
        shutil.copyfile(TXCAL_HTML, html_loc)
    except IsADirectoryError:
        shutil.copyfile(TXCAL_HTML, html_loc / Path(FILE_NAME).stem)
    
    print("Copied to {}".format(html_loc))
        
#     with open(os.path.expanduser(TXCAL_NOTEBOOK)) as f:
#         nb = nbformat.read(f, as_version=4)
    
#     ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
    
#     try:
#         out = ep.preprocess(nb)
#     except CellExecutionError:
#         out = None
#         msg = 'Error executing the notebook "%s".\n\n' % TXCAL_NOTEBOOK
#         msg += 'See notebook "%s" for the traceback.' % TXCAL_NOTEBOOK_EXEC
#         print(msg)
#         raise
#     finally:
#         with open(os.path.expanduser(TXCAL_NOTEBOOK_EXEC), mode='w', encoding='utf-8') as f:
#             nbformat.write(nb, f)


def create_folder_txcal(folder):
    """Create txcal html for given folder
    
    Arguments:
        folder [str] : Folder which contains unit names
    """
    dirnames = Path.iterdir(folder)
    for i in dirnames:
        if Path.is_dir(i):
            create_folder_txcal(i)
        else:
            if 'power_level_table' == i.stem:
                create_txcal_html(i.parent.relative_to(*i.parent.parts[:2]))
        

def compare_folders(folder1, folder2):
    """Compare two folders
    
    Arguments:
        folder1 [str] : Folder 1 which contains new units
        folder2 [str] : Folder 2 which contains html for units
    """
    dirnames1 = os.listdir(folder1)
    dirnames2 = os.listdir(folder2)
    
    for unit_name in set(dirnames1) - set(dirnames2):
        if POWER_LEVEL_TABLE in os.listdir(folder1 + "/" + unit_name):
            print(unit_name)


def main(args):
    if args.folder:
        folder = args.folder
        create_folder_txcal(Path(folder))
    if args.check_update:
        w = Watcher(args.check_update)
        w.run()
    if args.comp_folder:
        compare_folders(args.comp_folder[0], args.comp_folder[1])
    if args.unit:
        create_txcal_html(args.unit)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Track changes in box folder.")
    parser.add_argument("--folder", help="Folder name with cal files")
    parser.add_argument("--check_update", help="Run watchdog check for update in cal folder")
    parser.add_argument("--comp_folder", nargs=2, help="Compare cal files in two folders")
    parser.add_argument("--unit", help="Unit name with {}".format(POWER_LEVEL_TABLE))
    args = parser.parse_args()
    main(args)

