import pandas as pd
from txasic_cal.tools.logger import logger
from pkg_resources import resource_stream

pfmea_table = pd.read_csv(resource_stream('txasic_cal', 'pfmea_code/pfmea_operation_codes.csv'))

@pd.api.extensions.register_dataframe_accessor("pfmea_filter")
class PfmeaAccessor:
    def __init__(self, df):
        self._df = df
        self._cur_op_code = None

    def error(self, op_code):
        self._cur_op_code = op_code
        df = self._df[self._df['Operation Code'] == op_code]

        logger.error("\n  Operation Code: {}\n  Function: {}\n  Recommended Action: {}"\
                    .format(df['Operation Code'].values[0], df['Function'].values[0], df['Recommended Actions'].values[0]))

    def warn(self, op_code):
        self._cur_op_code = op_code
        df = self._df[self._df['Operation Code'] == op_code]

        logger.warn("\n  Operation Code: {}\n  Function: {}\n  Recommended Action: {}"\
                    .format(df['Operation Code'].values[0], df['Function'].values[0], df['Recommended Actions'].values[0]))

